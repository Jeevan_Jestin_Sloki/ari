/***********************************************************************************************************************
* File Name    : TeltailsConf.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 28/11/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "TelltaleConf.h"

/***********************************************************************************************************************
Global variables
***********************************************************************************************************************/

/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/

/*
   Left turn indicator telltale Configuration 
*/
 const SignalsValue_St_t			LeftInd_SignalsValue_aSt[LEFT_IND_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		LeftIndSigConf_ast},
};

 const SignalConfig_St_t			LeftIndSigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	LeftInd_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	LeftInd_On_SegConf_aSt},
};

 const SegConfig_St_t			LeftInd_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG27,	0x0EU,		0x00U},
};

 const SegConfig_St_t			LeftInd_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG27,	0x0EU,		0x01U},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Right turn indicator telltale Configuration 
*/
 const SignalsValue_St_t			RightInd_SignalsValue_aSt[RIGHT_IND_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		RightIndSigConf_ast},
};

 const SignalConfig_St_t			RightIndSigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	RightInd_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	RightInd_On_SegConf_aSt},
};

 const SegConfig_St_t			RightInd_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG15,	0x07U,		0x00U},
};

 const SegConfig_St_t			RightInd_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG15,	0x07U,		0x08U,},
};


/*
   High-Beam indicator telltale Configuration 
*/
 const SignalsValue_St_t			HighBeamInd_SignalsValue_aSt[HIGH_BEAM_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		HighBeamIndSigConf_ast},
};

 const SignalConfig_St_t			HighBeamIndSigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	HighBeamInd_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	HighBeamInd_On_SegConf_aSt},
};

 const SegConfig_St_t			HighBeamInd_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG0,		0x07U,		0x00U},
};

 const SegConfig_St_t			HighBeamInd_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG0,		0x07U,		0x08U},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   BLE indicator telltale Configuration 
*/
 const SignalsValue_St_t			BLE_Ind_SignalsValue_aSt[BLE_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		BLE_IndSigConf_ast,},
};

 const SignalConfig_St_t			BLE_IndSigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	BLE_Ind_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	BLE_Ind_On_SegConf_aSt
	},
};

 const SegConfig_St_t			BLE_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG0,		0x0BU,		0x00U},
};

 const SegConfig_St_t			BLE_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG0,		0x0BU,		0x04U},
};



/*
   Warning indicator telltale Configuration 
*/
 const SignalsValue_St_t			Warning_SignalsValue_aSt[WARN_IND_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		Warning_IndSigConf_ast,},
};

 const SignalConfig_St_t			Warning_IndSigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	Warning_Ind_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	Warning_Ind_On_SegConf_aSt},
};

 const SegConfig_St_t			Warning_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG30,	0x0EU,		0x00U},
};

 const SegConfig_St_t			Warning_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG30,	0x0EU,		0x01U},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Service Reminder telltale Configuration 
*/
 const SignalsValue_St_t			ServRem_SignalsValue_aSt[SERV_REM_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		ServRem_SigConf_ast},
};

 const SignalConfig_St_t			ServRem_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	ServRem_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	ServRem_On_SegConf_aSt},
};

 const SegConfig_St_t			ServRem_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG15,	0x0BU,		0x00U},
};

 const SegConfig_St_t			ServRem_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG15,	0x0BU,		0x04U},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Neutral Text indicator telltale Configuration 
*/
 const SignalsValue_St_t			NeutralText_SignalsValue_aSt[NEUTRAL_TEXT_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		NeutralText_SigConf_ast},
};

 const SignalConfig_St_t			NeutralText_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	NeutralText_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	NeutralText_On_SegConf_aSt},
};

 const SegConfig_St_t			NeutralText_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG33,	0x0BU,		0x00U},
};

 const SegConfig_St_t			NeutralText_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG33,	0x0BU,		0x04U},
};
/*
  Neutral Ring indicator telltale Configuration 
*/
const SignalsValue_St_t			 NeutralMode_SignalsValue_aSt[NEUTRAL_MODE_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		 NeutralMode_SigConf_ast},
};

 const SignalConfig_St_t			 NeutralMode_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	 NeutralMode_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	 NeutralMode_On_SegConf_aSt},
};

 const SegConfig_St_t			 NeutralMode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG33,	0x07U,		0x00U},
};

 const SegConfig_St_t			 NeutralMode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG33,	0x07U,		0x08U},
};

/*---------------------------------------------------------------------------------------------------------------------*/
/*
   Economy Text indicator telltale Configuration 
*/
const SignalsValue_St_t			EcoText_SignalsValue_aSt[ECO_TEXT_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		EcoText_SigConf_ast},
};

 const SignalConfig_St_t			EcoText_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	EcoText_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	EcoText_On_SegConf_aSt},
};

 const SegConfig_St_t			EcoText_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG32,	0x0DU,		0x00U,},
};

 const SegConfig_St_t			EcoText_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG32,	0x0DU,		0x02U,},
};

/*
   Economy Mode indicator telltale Configuration 
*/
 const SignalsValue_St_t			EcoMode_SignalsValue_aSt[ECO_MODE_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		EcoMode_SigConf_ast},
};

 const SignalConfig_St_t			EcoMode_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	EcoMode_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	EcoMode_On_SegConf_aSt},
};

 const SegConfig_St_t			EcoMode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG45,	0x0EU,		0x00U,},
};

 const SegConfig_St_t			EcoMode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG45,	0x0EU,		0x01U,},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Sports Text indicator telltale Configuration 
*/
 const SignalsValue_St_t			 SportsText_SignalsValue_aSt[SPORTS_TEXT_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		 SportsText_SigConf_ast},
};

 const SignalConfig_St_t			 SportsText_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	 SportsText_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	 SportsText_On_SegConf_aSt},
};

 const SegConfig_St_t			 SportsText_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG6,		0x0EU,		0x00U},
};

 const SegConfig_St_t			 SportsText_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG6,		0x0EU,		0x01U},
};

/*
   Sports Mode indicator telltale Configuration 
*/
const SignalsValue_St_t			SportsMode_SignalsValue_aSt[SPORTS_MODE_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		SportsMode_SigConf_ast},
};

 const SignalConfig_St_t			SportsMode_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	SportsMode_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	SportsMode_On_SegConf_aSt},
};

 const SegConfig_St_t			SportsMode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG0,		0x0EU,		0x00U},
};

 const SegConfig_St_t			SportsMode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG0,		0x0EU,		0x01U},
};


/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Reverse Text indicator telltale Configuration 
*/
 const SignalsValue_St_t			ReverseText_SignalsValue_aSt[REVERSE_TEXT_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		ReverseText_SigConf_ast},
};

 const SignalConfig_St_t			ReverseText_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	ReverseText_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	ReverseText_On_SegConf_aSt},
};

 const SegConfig_St_t			ReverseText_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG32,		0x0BU,		0x00U,},
};

 const SegConfig_St_t			ReverseText_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG32,		0x0BU,		0x04U,},
};


/*
    Reverse Mode indicator telltale Configuration
*/
 const SignalsValue_St_t			ReverseMode_SignalsValue_aSt[REVERSE_MODE_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		ReverseMode_SigConf_ast},
};

 const SignalConfig_St_t			ReverseMode_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	ReverseMode_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	ReverseMode_On_SegConf_aSt},
};

 const SegConfig_St_t			ReverseMode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG32,		0x07U,		0x00U,},
};

 const SegConfig_St_t			ReverseMode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG32,		0x07U,		0x08U,},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Side-Stand indicator telltale Configuration 
*/
 const SignalsValue_St_t			SideStand_SignalsValue_aSt[SIDE_STAND_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		SideStand_IndSigConf_ast},
};

 const SignalConfig_St_t			SideStand_IndSigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	SideStand_Ind_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	SideStand_Ind_On_SegConf_aSt},
};

 const SegConfig_St_t			SideStand_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG47,	0x0EU,		0x00U},
};

 const SegConfig_St_t			SideStand_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG47,	0x0EU,		0x01U},
};


/*
   Motor-Fault indicator telltale Configuration 
*/
 const SignalsValue_St_t			MotorFlt_SignalsValue_aSt[MOTOR_FLT_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		MotorFlt_IndSigConf_ast},
};

 const SignalConfig_St_t			MotorFlt_IndSigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	MotorFlt_Ind_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	MotorFlt_Ind_On_SegConf_aSt},
};

 const SegConfig_St_t			MotorFlt_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG15,	0x0DU,		0x00U},
};

 const SegConfig_St_t			MotorFlt_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG15,	0x0DU,		0x02U},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Kill-Switch telltale Configuration 
*/
 const SignalsValue_St_t			KillSwitch_SignalsValue_aSt[KILL_SWITCH_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		KillSwitch_SigConf_ast,},
};

 const SignalConfig_St_t			KillSwitch_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	KillSwitch_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	KillSwitch_On_SegConf_aSt},
};

 const SegConfig_St_t			KillSwitch_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG15,	0x0EU,		0x00U},
};

 const SegConfig_St_t			KillSwitch_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG15,	0x0EU,		0x01U},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Battery_fault telltale Configuration 
*/
 const SignalsValue_St_t			Battery_fault_SignalsValue_aSt[BATTERY_FLT_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		Battery_fault_SigConf_ast},
};

 const SignalConfig_St_t			Battery_fault_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	Battery_fault_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	Battery_fault_On_SegConf_aSt},
};

 const SegConfig_St_t			Battery_fault_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG29,	0x0EU,		0x00U},
};

 const SegConfig_St_t			Battery_fault_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG29,	0x0EU,		0x01U},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Parking Break telltale Configuration 
*/
 const SignalsValue_St_t			ParkingBreak_SignalsValue_aSt[PARKING_BREAK_LEN] = 
{
	{ZERO_E,	TWO_E,		ParkingBreak_SigConf_ast},
};

 const SignalConfig_St_t			ParkingBreak_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	ParkingBreak_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	ParkingBreak_On_SegConf_aSt},
};

 const SegConfig_St_t			ParkingBreak_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG0,		0x0DU,		0x00U},
};

 const SegConfig_St_t			ParkingBreak_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG0,		0x0DU,		0x02U},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Common Constant Segments Configuration 
*/
 const SignalsValue_St_t			Common_SignalsValue_aSt[COMMON_SEG_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		Common_SigConf_ast},
};

 const SignalConfig_St_t			Common_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	Common_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	Common_On_SegConf_aSt},
};

 const SegConfig_St_t			Common_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG14,	0x0EU,		0x00U,},
};

 const SegConfig_St_t			Common_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG14,	0x0EU,		0x01U},
};

/*---------------------------------------------------------------------------------------------------------------------*/
/*
    Parking Text indicator telltale Configuration  
*/
 const SignalsValue_St_t			ParkingText_SignalsValue_aSt[PARKING_TEXT_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		ParkingText_SigConf_ast},
};

 const SignalConfig_St_t			ParkingText_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	ParkingText_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	ParkingText_On_SegConf_aSt},
};

 const SegConfig_St_t			ParkingText_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG31,		0x0BU,		0x00U},
};

 const SegConfig_St_t			ParkingText_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG31,		0x0BU,		0x04U},
};

/*
   Parking Mode indicator telltale Configuration 
*/

const SignalsValue_St_t			ParkingMode_SignalsValue_aSt[PARKING_MODE_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		ParkingMode_SigConf_ast},
};

 const SignalConfig_St_t			ParkingMode_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	ParkingMode_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	ParkingMode_On_SegConf_aSt},
};

 const SegConfig_St_t			ParkingMode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG31,		0x07U,		0x00U},
};

 const SegConfig_St_t			ParkingMode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG31,		0x07U,		0x08U},
};

/*
  	Driving Text indicator telltale Configuration  
*/

const SignalsValue_St_t			DrivingText_SignalsValue_aSt[DRIVING_TEXT_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		DrivingText_SigConf_ast},
};

 const SignalConfig_St_t			DrivingText_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	DrivingText_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	DrivingText_On_SegConf_aSt},
};

 const SegConfig_St_t			DrivingText_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG31,		0x0DU,		0x00U},
};

 const SegConfig_St_t			DrivingText_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG31,		0x0DU,		0x02U},
};

/*
 	Driving Mode indicator telltale Configuration 
*/

const SignalsValue_St_t			DrivingMode_SignalsValue_aSt[DRIVING_MODE_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		DrivingMode_SigConf_ast},
};

 const SignalConfig_St_t			DrivingMode_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	 DrivingMode_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	 DrivingMode_On_SegConf_aSt},
};

 const SegConfig_St_t			 DrivingMode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG33,		0x0DU,		0x00U},
};

 const SegConfig_St_t			DrivingMode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG33,		0x0DU,		0x02U},
};












/********************************************************EOF***********************************************************/