
#include "diag_typedefs.h"
bool PFlash_Erase(uint32_t address,uint32_t size);
bool PFlash_Write(uint32_t address,uint32_t size,uint8_t* data_buff);
bool PFlash_Init(void);
bool PFlash_Pattern_Write(uint32_t address, uint32_t data);