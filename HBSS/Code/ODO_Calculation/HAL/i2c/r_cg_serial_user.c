/***********************************************************************************************************************
* File Name    : r_cg_serial_user.c
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for Serial module.
* Creation Date: 28/04/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "serial_user.h"
#include "r_cg_serial.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
//#pragma interrupt r_iic11_interrupt(vect=INTIIC11)


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
extern volatile uint8_t    g_iic11_master_status_flag; /* iic11 start flag for send address check by master mode */
extern volatile uint8_t  * gp_iic11_tx_address;        /* iic11 send data pointer by master mode */
extern volatile uint16_t   g_iic11_tx_count;           /* iic11 send data size by master mode */
extern volatile uint8_t  * gp_iic11_rx_address;        /* iic11 receive data pointer by master mode */
extern volatile uint16_t   g_iic11_rx_count;           /* iic11 receive data size by master mode */
extern volatile uint16_t   g_iic11_rx_length;          /* iic11 receive data length by master mode */
/* Start user code for global. Do not edit comment generated here */
bool	IIC_Tx_Flag_b 	= false;
bool	IIC_Rx_Flag_b 	= false;
bool	IIC_Err_Flag_b 	= false;

uint8_t	IIC_Tx_Buff_au8[2] = {0x00U, 0x00U};
uint8_t	IIC_Rx_Buff_au8[2] = {0x00U, 0x00U};
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: r_iic11_interrupt
* Description  : This function is INTIIC11 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void  r_iic11_interrupt(void)
{
    if (((SSR11 & 0x0002U) == 0x0002U) && (g_iic11_tx_count != 0U))
    {
        r_iic11_callback_master_error(MD_NACK);
    }
    else if(((SSR11 & 0x0001U) == 0x0001U) && (g_iic11_tx_count != 0U))
    {
        r_iic11_callback_master_error(MD_OVERRUN);
    }
    else
    {
        /* Control for master send */
        if ((g_iic11_master_status_flag & 0x01U) == 1U)
        {
            if (g_iic11_tx_count > 0U)
            {
                SDR11L = *gp_iic11_tx_address;
                gp_iic11_tx_address++;
                g_iic11_tx_count--;
            }
            else
            {
                R_IIC11_StopCondition();
                r_iic11_callback_master_sendend();
            }
        }
        /* Control for master receive */
        else 
        {
            if ((g_iic11_master_status_flag & 0x04U) == 0U)
            {
                ST1 |= 0x0002U; 
                SCR11 &= ~0xC000U; 
                SCR11 |= 0x4000U;                
                SS1 |= 0x0002U;
                g_iic11_master_status_flag |= 0x04U;
                
                if(g_iic11_rx_length == 1U)
                {
                    SOE1 &= ~0x0002U;    /* disable IIC11 out */
                }
                
                SDR11L = 0xFFU;
            }
            else
            {
                if (g_iic11_rx_count < g_iic11_rx_length)
                {
                    *gp_iic11_rx_address = SDR11L;
                    gp_iic11_rx_address++;
                    g_iic11_rx_count++;
                    
                    if (g_iic11_rx_count == (g_iic11_rx_length - 1U))
                    {
                        SOE1 &= ~0x0002U;    /* disable IIC11 out */
                        SDR11L = 0xFFU;
                    }
                    
                    if (g_iic11_rx_count == g_iic11_rx_length)
                    {
                        R_IIC11_StopCondition();
                        r_iic11_callback_master_receiveend();
                    }
                    else
                    {
                        SDR11L = 0xFFU;
                    }
                }
            }
        }
    }
}

/***********************************************************************************************************************
* Function Name: r_iic11_callback_master_error
* Description  : This function callback function open for users operation when IIC11 master error.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
static void r_iic11_callback_master_error(MD_STATUS flag)
{
    /* Start user code. Do not edit comment generated here */
    
    IIC_Err_Flag_b = true;
    NOP();
    
    IIC_Tx_Flag_b  = true; /*Set to true to kill the wait process*/
   
    IIC_Rx_Flag_b  = true; /*Set to true to kill the wait process*/
    
    return;
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_iic11_callback_master_receiveend
* Description  : This function callback function open for users operation when IIC11 master receive finish.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
static void r_iic11_callback_master_receiveend(void)
{
    /* Start user code. Do not edit comment generated here */
    IIC_Rx_Flag_b = true;
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_iic11_callback_master_sendend
* Description  : This function callback function open for users operation when IIC11 master transmit finish.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
static void r_iic11_callback_master_sendend(void)
{
    /* Start user code. Do not edit comment generated here */
    IIC_Tx_Flag_b = true;
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
