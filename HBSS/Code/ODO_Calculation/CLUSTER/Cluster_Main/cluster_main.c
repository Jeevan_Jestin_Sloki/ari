/***********************************************************************************************************************
* File Name    : cluster_main.c
* Version      : 01
* Description  : This file implements the cluster display main module.
* Created By   : Jeevan Jestin N
* Creation Date: 02/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "cluster_main.h"
#include "SpeedDisp.h"
#include "DataBank.h"
#include "TelltaleDisp.h"
#include "ODO_Disp.h"
#include "SOC_Disp.h"
#include "PowerConsum_Disp.h"
#include "Time.h"
#include "TimeDisp.h"
#include "RangeKmDisp.h"
#include "Navigation_Disp.h"
#include "App_typedefs.h"
#include "UserInterfaceButton.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/



/***********************************************************************************************************************
* Function Name: Display_Cluster_Data
* Description  : This function displays all the cluster signals on the LCD.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
/*todo:jeevan*/ /* using for navigation status,
			need to obtain from CAN */
			
void Display_Cluster_Data(void)
{
	Display_Speed(ClusterSignals_St.VehicleSpeed_u16, SPEED_E);
	Disp_ODODigit(ClusterSignals_St.ODOmeter_u32 ,ODO_E); 
	Display_SOC((uint16_t)ClusterSignals_St.BatterySOC_u8 , ClusterSignals_St.ChargingIndicator_u8 , BATT_SOC_BAR_E , BATT_SOC_DIGIT_E ,  ClusterSignals_St.RangeKm_u16 );
	Display_PowerConsum( ClusterSignals_St.PowerConsumption_u16,  POWER_CONSUMP_E);
	//Display_Time((uint8_t)ClusterSignals_St.MinutesTime_u8 ,(uint8_t)ClusterSignals_St.HoursTime_u8, MINUTES_TIME_E,HOURS_TIME_E);
	//Blink_Time_Colon(TIME_COLON_E);
	Display_RangeKm( ClusterSignals_St.RangeKm_u16 , RANGE_KM_E);
	Display_Navigation(ClusterSignals_St.NavigationDistance_u16, ClusterSignals_St.NavigationDirection_u8,
			   NAVIG_DIST_E, ClusterSignals_St.Navigation_Status_u8 );
	//Display_Navig_dist(ClusterSignals_St.NavigationDistance_u16, NAVIG_DIST_E);
	Indicate_Telltales();

//	if(UIB_TIME_HH_E == UserButtonTimeSetState_En)
//	{
//		BlinkHourTime(UIB_Hour_time_u8,ClusterSignals_St.MinutesTime_u8);
//	}
//	else if( (UIB_TIME_HM_E == UserButtonTimeSetState_En) )
//	{
//		BlinkHigherMinuteTime(UIB_HighMin_time_u8,ClusterSignals_St.HoursTime_u8,UIB_LowerMin_time_u8);
//	}
//	else if  (UIB_TIME_LM_E == UserButtonTimeSetState_En)
//	{
//		BlinkLowerMinuteTime(UIB_LowerMin_time_u8,ClusterSignals_St.HoursTime_u8,UIB_HighMin_time_u8);
//	}
//	else
//	{
//		Display_Time((uint8_t)ClusterSignals_St.MinutesTime_u8 ,(uint8_t)ClusterSignals_St.HoursTime_u8, MINUTES_TIME_HB_E,MINUTES_TIME_LB_E,HOURS_TIME_E);
//	}
	return;
}


/********************************************************EOF***********************************************************/