/***********************************************************************************************************************
* File Name    : TimeDisp.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 12/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "TimeDisp.h"
#include "SegDispWrite.h"
#include "GenConfig.h"
#include "delay_flags.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool 	     Initial_Minutes_Disp_b = false;
bool 	     Initial_Hours_Disp_b = false;
bool 		 Initial_Tempt_Disp_b = false;
/***********************************************************************************************************************
* Function Name: Display_Time
* Description  : This function validate and displays the Real Time in 24 HRS Format.
* Arguments    : uint16_t Minutes_Time_u16, uint16_t Minutes_Time_u16, ClusterSignals_En_t TIME_ENUM_E
                                            ClusterSignals_En_t HOURS_TIME_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void Display_Time(uint16_t Minutes_Time_u16, uint16_t Hours_Time_u16, 
		ClusterSignals_En_t MINUTES_TIMEHB_ENUM_E,ClusterSignals_En_t MINUTES_TIMELB_ENUM_E, ClusterSignals_En_t HOURS_TIME_ENUM_E)
{
	uint16_t	     ValidMinutes_Time_u16 = 0;
	uint16_t	     ValidHours_Time_u16 = 0;
	static uint16_t      PrevMinutes_Time_u16 = 0;
	static uint16_t      PrevHours_Time_u16 = 0;
	uint8_t 	     MinuteTimeLB_u8 = 0;
	uint8_t 	     MinuteTimeHB_u8 = 0;

	MinutesDispState_En_t  MinutesDispState_En = MINUTES_DISP_HOLD_E;
	HoursDispState_En_t    HoursDispState_En = HOURS_DISP_HOLD_E;
	
	
	/*
		Minutes Time Display
	*/
	
	if(PrevMinutes_Time_u16 == Minutes_Time_u16)
	{
		MinutesDispState_En = MINUTES_DISP_HOLD_E;
		if(false == Initial_Minutes_Disp_b)
		{
			MinutesDispState_En = MINUTES_DISPLAY_E;
			Initial_Minutes_Disp_b = true;
		}
	}
	else
	{
		MinutesDispState_En = MINUTES_DISPLAY_E;
		PrevMinutes_Time_u16 = Minutes_Time_u16;
	}
	switch(MinutesDispState_En)
	{
		case MINUTES_DISPLAY_E:
		{
			ValidMinutes_Time_u16 = ValidateMinutesTime(Minutes_Time_u16);
			MinuteTimeLB_u8 = ValidMinutes_Time_u16%10;
			MinuteTimeHB_u8 = ValidMinutes_Time_u16/10;
			Write_SEG(MINUTES_TIMELB_ENUM_E, (uint32_t)MinuteTimeLB_u8,DECIMAL);
			Write_SEG(MINUTES_TIMEHB_ENUM_E, (uint32_t)MinuteTimeHB_u8,DECIMAL);  
			break;
		}
		case MINUTES_DISP_HOLD_E:
		{
			break;
		}
		default:
		{
			;	
		}
	}
	
	/*
		Hours Time Display
	*/
	
	if(PrevHours_Time_u16 == Hours_Time_u16)
	{
		HoursDispState_En = HOURS_DISP_HOLD_E;
		if(false == Initial_Hours_Disp_b)
		{
			HoursDispState_En = HOURS_DISPLAY_E;
			Initial_Hours_Disp_b = true;
		}
	}
	else
	{
		HoursDispState_En = HOURS_DISPLAY_E;
		PrevHours_Time_u16 = Hours_Time_u16;
	}
	switch(HoursDispState_En)
	{
		case HOURS_DISPLAY_E:
		{
			ValidHours_Time_u16 = ValidateHoursTime(Hours_Time_u16);
			Write_SEG(HOURS_TIME_ENUM_E, (uint32_t)ValidHours_Time_u16,DECIMAL); 
			break;
		}
		case HOURS_DISP_HOLD_E:
		{
			break;
		}
		default:
		{
			;	
		}
	}
	
	return;
}


/***********************************************************************************************************************
* Function Name: ValidateMinutesTime
* Description  : This function validates the Minutes Time value received.
* Arguments    : uint16_t  MinutesCheck_u16
* Return Value : uint16_t  MinutesCheck_u16
***********************************************************************************************************************/
uint16_t ValidateMinutesTime(uint16_t MinutesCheck_u16)
{
	if(MinutesCheck_u16 > MAX_MINUTES_TIME)
	{
		MinutesCheck_u16 = MAX_MINUTES_TIME;
	}
	else
	{
		;
	}
	return MinutesCheck_u16;
}

/***********************************************************************************************************************
* Function Name: ValidateHoursTime
* Description  : This function validates the Hours Time value received.
* Arguments    : uint16_t  HoursCheck_u16
* Return Value : uint16_t  HoursCheck_u16
***********************************************************************************************************************/
uint16_t ValidateHoursTime(uint16_t HoursCheck_u16)
{
	if(HoursCheck_u16 > MAX_HOURS_TIME)
	{
		HoursCheck_u16 = MAX_HOURS_TIME;
	}
	else
	{
		;
	}
	return HoursCheck_u16;
}

/***********************************************************************************************************************
* Function Name: Blink_Time_Colon
* Description  : This function Blinks the Time colon.
* Arguments    : ClusterSignals_En_t TIME_COLON_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void Blink_Time_Colon(ClusterSignals_En_t TIME_COLON_ENUM_E)
{
	static bool ColonState_b = OFF;
	if(TimeColon_500ms_b)
	{
		if(ColonState_b)
		{
			Write_SEG(TIME_COLON_ENUM_E, (uint32_t)OFF,DECIMAL);
			ColonState_b = OFF;
		}
		else
		{
			Write_SEG(TIME_COLON_ENUM_E, (uint32_t)ON,DECIMAL);
			ColonState_b = ON;
		}
		TimeColon_500ms_b = false;
	}
	return;
}

/***********************************************************************************************************************
* Function Name: Display_Temperature
* Description  : The function display the temperature.
* Arguments    : ClusterSignals_En_t TIME_COLON_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void Display_Temperature(ClusterSignals_En_t TEMPERATURE_HB_VALUE_E, ClusterSignals_En_t TEMPERATURE_LB_VALUE_E,
														ClusterSignals_En_t TEMPERATURE_TXT_E, uint8_t Disp_Temperature_u8)
{
	static uint8_t PrevTempt_Value_u8 = 0;
	uint8_t Temp_HB_u8 = (Disp_Temperature_u8>>4)&0x0f;
	uint8_t Temp_LB_u8 = (Disp_Temperature_u8)&0x0f;

	if((Disp_Temperature_u8 == PrevTempt_Value_u8) && (false == Initial_Tempt_Disp_b) )
	{
		if(false == Initial_Tempt_Disp_b)
		{
			Initial_Tempt_Disp_b = true;
		}

		Write_SEG(TEMPERATURE_HB_VALUE_E, (uint32_t)Temp_HB_u8,HEX_DIGIT);
		Write_SEG(TEMPERATURE_LB_VALUE_E, (uint32_t)Temp_LB_u8,HEX_DIGIT);

		Write_SEG(TEMPERATURE_TXT_E, (uint32_t)DISPLAY_T,HEX_DIGIT);
	}
	else if(Disp_Temperature_u8 != PrevTempt_Value_u8)
	{
		Write_SEG(TEMPERATURE_HB_VALUE_E, (uint32_t)Temp_HB_u8,HEX_DIGIT);
		Write_SEG(TEMPERATURE_LB_VALUE_E, (uint32_t)Temp_LB_u8,HEX_DIGIT);

		Write_SEG(TEMPERATURE_TXT_E, (uint32_t)DISPLAY_T,HEX_DIGIT);
	}

	return;
}

/***********************************************************************************************************************
* Function Name: Display_Temperature
* Description  : The function display the temperature.
* Arguments    : ClusterSignals_En_t TIME_COLON_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void Clear_Time_InitFlags(void)
{
	Initial_Minutes_Disp_b = false;
	Initial_Hours_Disp_b = false;
	Initial_Tempt_Disp_b = false;
	return;
}
/********************************************************EOF***********************************************************/