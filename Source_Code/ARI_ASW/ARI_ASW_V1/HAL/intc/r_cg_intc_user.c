/***********************************************************************************************************************
* File Name    : r_cg_intc_user.c
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for INTC module.
* Creation Date: 07/09/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_intc.h"
#include "intc_user.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
//#pragma interrupt r_intc5_interrupt(vect=INTP5)


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool ExternalInterrupt5_b = false;


/***********************************************************************************************************************
* Function Name: r_intc5_interrupt [Active High]
* Description  : This function is INTP5 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_intc5_interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    ExternalInterrupt5_b = true; //Set this flag to true when External Interrupt is Occured.
    return;
    /* End user code. Do not edit comment generated here */
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
