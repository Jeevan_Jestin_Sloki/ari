/***********************************************************************************************************************
* File Name    : can_driver.c
* Version      : 02
* Description  : This file implements the device driver for the CAN Driver
* Created By   : Dileepa B S
* Creation Date: 16/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 
#include "can_driver.h"
#include "r_cg_port.h"
#include "cil_can_conf.h"
#include "can_callback.h"
#include "can_tranceiver.h"
#include "pfdl_user.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
//#pragma interrupt 	Msgbuf_Receive(vect=INTC0REC)
//#pragma interrupt 	Error_Processing(vect=INTC0ERR)
//#pragma interrupt 	ISR_CAN_Wakeup(vect=INTC0WUP)

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
uint8_t   		Rx_Msg_Buffer_au8[8];
CAN_Baud_Sel_En_t	CAN_Baud_Sel_En = CAN_BAUD_500KBPS_E ;
CAN_Frame_Format_En_t	CAN_Frame_Format_En = CAN_FRAME_STD_E;

/***********************************************************************************************************************
* Function Name: CAN_Init
* Description  : This function initializes the CAN Device driver.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void CAN_Init(void)
{	
	PCKSEL = 0x10U;
	
	/****************CAN Pins setting***************/
	//SCAN0 = 1U;	 /* CAN pins on P00(TX) & P01(RX) */		
	//PM0 = 0x02U;
	//P0 = 0x01U;    /* P0 = 0x03U; */
	
	
	SCAN0 = 0U;   /* CAN pins on P71(TX) & P70(RX) */
	PM7 = 0x01U;
	P7 = 0x02U;   /* P7 = 0x03U; */
	/***********************************************/
	
	C0GMCS = 0x03U;
	
	C0GMCTRL = 0x0100U; 
	
	/*****************Baud Rate setting*************/
	//C0BRP = 0x03U;           /* 250 kbps Baudrate */
	//C0BTR = 0x0104U;
//	C0BRP = 0x01U;             /* 500 kbps Baudrate */
//	C0BTR = 0x0104U;
	
	//StoreBaudrateToFlash((uint8_t) CAN_Baud_Sel_En);
	//CAN_Baud_Sel_En = (CAN_Baud_Sel_En_t)RestoreBaudrateFromFlash();
	Set_CAN_BaudRate(CAN_Baud_Sel_En);
//	CAN_Baud_Sel_En = CAN_BAUD_250KBPS_E;
	//StoreBaudrateToFlash((uint8_t) CAN_Baud_Sel_En);
	/***********************************************/
	
	C0IE = 0x3F00U;
	
	C0RECIF = 0U;
	
	C0RECMK = 0U;
	
	C0WUPIF = 0U;
	
	C0WUPMK = 0U;
	
	C0ERRIF = 0U;
	
	C0ERRMK = 0U;
	
	C0CTRL = 0x817EU;
	
	/*
	Initialize all the CAN Message Buffers.
	*/
	CAN_MsgBuf_Init();
	
	Tx_MsgBuf_Init(1,TX_ID_7F1, MSG_LENGTH);      /* Tx_MsgBuf_Init(Buffer_Number_u8, CAN_TX_ID, Message_Length); */
	Tx_MsgBuf_Init(2,TX_ID_16, MSG_LENGTH);
	Tx_MsgBuf_Init(3,0x501, MSG_LENGTH);
	Tx_MsgBuf_Init(4,0x502, MSG_LENGTH);
	
	Rx_MsgBuf_Init(5,RX_IDBB,CAN_Frame_Format_En);
	Rx_MsgBuf_Init(6,RX_IDA3,CAN_Frame_Format_En);
	Rx_MsgBuf_Init(7,RX_ID7EA,CAN_Frame_Format_En);
	Rx_MsgBuf_Init(8,RX_ID7F0,CAN_Frame_Format_En);		    /* Rx_MsgBuf_Init(Buffer_Number_u8,CAN_RX_ID) */

	
	CAN_Frame_Format_En = CAN_FRAME_EXT_E;
	Rx_MsgBuf_Init(9,RX_ID18FF3001,CAN_Frame_Format_En);		    /* Rx_MsgBuf_Init(Buffer_Number_u8,CAN_RX_ID) */
	Rx_MsgBuf_Init(10,RX_ID18FF3000,CAN_Frame_Format_En);
	
	return;
}

/***********************************************************************************************************************
* Function Name: CAN_MsgBuf_Init
* Description  : This function initializes the CAN0 Message buffers
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void CAN_MsgBuf_Init(void)
{
	uint32_t	Msg_Buff_Address_u32;
	uint8_t	        Buffer_Number_u8;

	/* Init all message buffer */
	for (Buffer_Number_u8 = 0 ; Buffer_Number_u8 < 16 ; Buffer_Number_u8++)
	{
		/*	Set CAN message buffer[n] register address */
		Msg_Buff_Address_u32 = (CAN_MSG_BUFFER_ADDRESS + (0x10 * Buffer_Number_u8));

		/* Clear TRQ, DN bit */
		*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E)) = 0x0006;
		
		/* Clear RDY bit */
		*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E)) = 0x0001;

		/* Clear MA0 bit */
		*((__far uint8_t *)(Msg_Buff_Address_u32 + 0x09)) &= 0xF8;
   	}
}


/***********************************************************************************************************************
* Function Name: Tx_MsgBuf_Init
* Description  : This function configures the CAN0 Message buffers for data Transmission.
* Arguments    : uint8_t Buffer_Number_u8,uint32_t Tx_Msg_ID_u32,uint8_t Tx_Msg_DLC_u8
* Return Value : None
***********************************************************************************************************************/
void Tx_MsgBuf_Init(uint8_t Buffer_Number_u8,unsigned int Tx_Msg_ID_u32,uint8_t Tx_Msg_DLC_u8)
{
	uint32_t	Msg_Buff_Address_u32;
	
	/*Set CAN message buffer[n] register address */
	Msg_Buff_Address_u32 = (CAN_MSG_BUFFER_ADDRESS + (0x10 * Buffer_Number_u8));

	/* Set C0MCONFm register */
	*((__far uint8_t *)(Msg_Buff_Address_u32 + 0x09)) = 0x01;      /* Transmit message buffer, MA0=1,msg buffer used */

	/* Set C0MIDLm,C0MIDHm register */
	*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0A)) = 0x0000;   /* standard frame,C0MIDLm=0x0000; */
	*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0C)) = ((Tx_Msg_ID_u32 << 2) & 0x1FFF); /* C0MIDHm */

	/* Set C0MDLCm register */
	*((__far uint8_t *)(Msg_Buff_Address_u32 + 0x08)) = 0x08;     /* set C0MDLCm,data length is 8 bytes */

	/* Clear C0MDATAxm register */
	{
		uint8_t Data_Count_u8;
		for(Data_Count_u8 = 0 ; Data_Count_u8 < Tx_Msg_DLC_u8 ; Data_Count_u8++)
		{
			*((__far uint8_t *)(Msg_Buff_Address_u32 + (0x01 * Data_Count_u8))) = 0x00;    /* clear each byte data=0x00 */
		}
	}

	/* Set C0MCTRLm register */
	#if 1
	*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E)) = 0x001E;       /* clear MOW,IE,DN,TRQ bit
									   MOV=0,The message buffer is not overwritten by a newly received data frame.
									   IE=0,Normal message transmission completion interrupt disabled
									   DN=0,A data frame or remote frame is not stored in the message buffer.
									   TRQ=0,No message frame transmitting request that is pending or being transmitted */
	#endif
		
	#if 0							
	*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E)) = 0x0816;    /* clear MOW,DN,TRQ bit */
									/* set IE=1,Normal message transmission completion interrupt enabled */
	#endif
	/* Set RDY bit */
	*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E)) = 0x0100;    /* set RDY=1,The CAN module can write to the message buffer */
}


/***********************************************************************************************************************
* Function Name: Rx_MsgBuf_Init
* Description  : This function configures the CAN0 Message buffers for data reception.
* Arguments    : uint8_t Buffer_Number_u8,uint32_t Rx_Mask_ID_u32
* Return Value : None
***********************************************************************************************************************/
void Rx_MsgBuf_Init(uint8_t Buffer_Number_u8,unsigned long int Rx_Mask_ID_u32, CAN_Frame_Format_En_t CAN_Frame_Format_En)
{
	uint32_t	Msg_Buff_Address_u32;
	
	/*Set CAN message buffer[n] register address */
	Msg_Buff_Address_u32 = (CAN_MSG_BUFFER_ADDRESS + (0x10 * Buffer_Number_u8));

	/* Set C0MCONFm register */
	*((__far uint8_t *)(Msg_Buff_Address_u32 + 0x09)) = 0x11;    /* Receive message buffer(mask 1), MA0=1,msg buffer used */
	
	
	if(CAN_Frame_Format_En == CAN_FRAME_STD_E)
	{
		/* Set C0MIDLm,C0MIDHm register */ /*Standard Frame*/
		*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0A)) = 0x8000;
		*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0C)) = ((Rx_Mask_ID_u32 << 2) & 0x1FFF); 
	}
	else if(CAN_Frame_Format_En == CAN_FRAME_EXT_E)
	{
		/* Set C0MIDLm,C0MIDHm register */ /*Extended Frame*/
		*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0A)) = (uint16_t)(Rx_Mask_ID_u32 & 0x0000FFFFU);   
		*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0C)) = (((uint16_t)((Rx_Mask_ID_u32 >> 16) & 0x1FFF)) | 0x8000U);
	}
	else
	{
		;
	}
	
	/* Set C0MCTRLm register */
	*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E)) = 0x0916;    /* clear MOW,DN,TRQ bit
									   MOV=0,The message buffer is not overwritten by a newly received data frame.
									   set IE=1,Valid message reception completion interrupt enabled.
									   DN=0,A data frame or remote frame is not stored in the message buffer.
									   TRQ=0,No message frame transmitting request that is pending or being transmitted */
	/* Set RDY bit */
	*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E)) = 0x0100;    /* set RDY=1,The CAN module can write to the message buffer */
}

/***********************************************************************************************************************
* Function Name: Tx_MsgBuf_Processing
* Description  : This function process the data to be transmit.
* Arguments    : uint8_t Buffer_Number_u8,uint8_t Tx_Msg_DLC_u8,uint8_t* Tx_Msg_Buffer_au8
* Return Value : None
***********************************************************************************************************************/
void Tx_MsgBuf_Processing(uint8_t Buffer_Number_u8,uint8_t Tx_Msg_DLC_u8,uint8_t* Tx_Msg_Buffer_au8)
{
	uint32_t	Msg_Buff_Address_u32;
	uint16_t  C0MCTRLm;
	/*Set CAN message buffer[n] register address */
	Msg_Buff_Address_u32 = (CAN_MSG_BUFFER_ADDRESS + (0x10 * Buffer_Number_u8));

	/* Check TRQ bit */
	C0MCTRLm = *((__far uint8_t *)(Msg_Buff_Address_u32 + 0x0E));
	if((C0MCTRLm & 0x0002) != 0)
	{
		return;
	}

	/* Clear RDY bit */
	*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E)) = 0x0001;    /* clear RDY=1,The message buffer can be written by software. */
	C0MCTRLm = *((__far uint8_t *)(Msg_Buff_Address_u32 + 0x0E));
		
	/* Set C0MDATAxm register */
	if((C0MCTRLm & 0x0001) == 0)
	{
		uint8_t Data_Count_u8;
			
		for(Data_Count_u8 = 0 ; Data_Count_u8 < Tx_Msg_DLC_u8 ; Data_Count_u8++)
		{
			*((__far uint8_t *)(Msg_Buff_Address_u32 + (0x01 * Data_Count_u8))) = Tx_Msg_Buffer_au8[Data_Count_u8];    /*clear each byte data=0x00 */
		}
		/* Set RDY bit */
		*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E)) = 0x0100;
		/* Set TRQ bit */
		*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E)) = 0x0200;
	}
	//C0MCTRLm = *((uint8_t *)(MsgBuf_address + 0x0e));
	while((C0MCTRLm & 0x0002) == 0x0002)
	{
		NOP();
		NOP();
	}
	return;
}

/***********************************************************************************************************************
* Function Name: Msgbuf_Receive
* Description  : This function is INTC0REC interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Msgbuf_Receive(void)
{
	uint32_t  	Rx_Msg_ID_u32 		= 0;
	uint32_t	Msg_Buff_Address_u32 	= 0;
	uint16_t 	CANID_LB 		= 0;
	uint16_t 	CANID_HB 		= 0;
	uint16_t 	IDE_BitValue_u16	= 0;
	uint8_t		Rx_Msg_DLC_u8 		= 0;
	uint8_t		Rx_Data_Count_u8 	= 0;
	
	
	/* Get receive message buffer number*/
	uint8_t Rx_Buffer_Number_u8;
	
	C0INTS = 0x0002;
	
	Rx_Buffer_Number_u8 = C0LIPT;
	
	/*Set CAN message buffer[n] register address */
	Msg_Buff_Address_u32 = (CAN_MSG_BUFFER_ADDRESS + (0x10 * Rx_Buffer_Number_u8));
	
	/* Check DN bit */
	while(((*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E))) & 0x2004) != 0) //check DN and MUC bit
	{
		/* Clear DN bit */
		*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0E)) = 0x0004;
		
		/* Get receive message data length */
		Rx_Msg_DLC_u8 = *((__far uint8_t *)(Msg_Buff_Address_u32 + 0x08));

		/* Get receive message ID*/
		CANID_HB =  *((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0C));
		
		IDE_BitValue_u16 = ((CANID_HB >> 15) & 0x0001U);
		
		if(IDE_BitValue_u16 == 1 ) /*Extended Frame*/
		{
			CANID_LB = *((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0A));  
			CANID_HB = ((*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0C))) & 0x01FFF); 
			Rx_Msg_ID_u32 = (((uint32_t)CANID_HB << 16) | (uint32_t)CANID_LB);
		}
		else if(IDE_BitValue_u16 == 0) /*Standard Frame*/
		{
			Rx_Msg_ID_u32 = ((*((__far uint16_t *)(Msg_Buff_Address_u32 + 0x0C)))>>2) & (0x07FF); 
		}
		else
		{
			;
		}
		
		#if 1
		/* Get receive data */
		for(Rx_Data_Count_u8 = 0 ;((Rx_Data_Count_u8 < Rx_Msg_DLC_u8) && (Rx_Data_Count_u8 < 8)) ; Rx_Data_Count_u8++)
		{
			Rx_Msg_Buffer_au8[Rx_Data_Count_u8] = *((__far uint8_t *)(Msg_Buff_Address_u32 + (0x01 * Rx_Data_Count_u8)));
		}
		#endif	
	}
	/*
		Back_Up the CAN Reception data to the CIL 
	*/
	DRV_HAL2CIL_CallBack(0, Rx_Msg_DLC_u8, 0, Rx_Msg_ID_u32, Rx_Msg_Buffer_au8);
}

/***********************************************************************************************************************
* Function Name: Error_Processing
* Description  : This function is INTC0ERR interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Error_Processing(void)
{
	C0INTS = 0x001C;
}

/***********************************************************************************************************************
* Function Name: ISR_CAN_Wakeup
* Description  : This function is INTC0WUP interrupt service routine.
                       This ISR will be served when an Falling edge trigger will be detected at CAN Reception pin
		       CTRxD0.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ISR_CAN_Wakeup(void)
{
	
	C0INTS = 0x0020;                  /*clear CINTS5(Wakeup interrupt)*/
	Release_STBY_Mode();		  /*Release CAN Tranceiver device from Stand-By-Mode [Power-Save Mode]*/
	CAN_SleepMode_Release();          /*Releases the CAN Module from the Sleep mode*/
}


/***********************************************************************************************************************
* Function Name: CAN_SleepMode_Setting
* Description  : This function enables the CAN sleep mode setting.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void CAN_SleepMode_Setting(void)
{
	C0CTRL = 0x0810;                /* set PSMODE0=1,PSMODE1=0, setting CAN sleep mode */
	while((C0CTRL&0x0008) == 0);    /* check PSMODE0=1 */
}


/***********************************************************************************************************************
* Function Name: CAN_SleepMode_Release
* Description  : This function releases the CAN from the sleep mode operation.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void CAN_SleepMode_Release(void)
{
	C0CTRL = 0x0008;                /* clear PSMODE0=0,release sleep mode by software */
	
	#if 0
	/* Sleep mode can also be released by a falling edge at the CAN reception pin---wakeup signal */
	C1INTS = 0x0020;               /* clear CINTS5(Wakeup interrupt) */
	#endif
}


/***********************************************************************************************************************
* Function Name: CAN_StopMode_Setting
* Description  : This function enables the CAN stop mode.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void CAN_StopMode_Setting(void)
{
	CAN_SleepMode_Setting();
	C0CTRL = 0x1800;              /* set PSMODE0=1,PSMODE1=1, setting CAN stop mode */
	while((C0CTRL&0x0010) == 0);  /* check PSMODE1=1 */
}


/***********************************************************************************************************************
* Function Name: CAN_StopMode_Release
* Description  : This function release the CAN from the stop mode
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void CAN_StopMode_Release(void)
{
	C0CTRL = 0x0810;             /* clear PSMODE1=0, release stop mode to sleep mode */
}

/***********************************************************************************************************************
* Function Name: Set_CAN_BaudRate
* Description  : This function sets the CAN driver baudrate.
* Arguments    : CAN_Baud_Sel_En_t CANBaudSelEn
* Return Value : None
***********************************************************************************************************************/
void Set_CAN_BaudRate(CAN_Baud_Sel_En_t CANBaudSelEn)
{
	if(CANBaudSelEn == CAN_BAUD_250KBPS_E)
	{
		C0BRP = 0x03U;           
		C0BTR = 0x0104U;
		NOP();
		NOP();
	}
	else if(CANBaudSelEn == CAN_BAUD_500KBPS_E)
	{
		C0BRP = 0x01U;            
		C0BTR = 0x0104U;
		NOP();
		NOP();
	}
	else
	{
		NOP();
	}
//	switch(CANBaudSelEn)
//	{
//		case CAN_BAUD_125KBPS_E:
//		{
//			break;
//		}
//		case CAN_BAUD_250KBPS_E:
//		{
			
//			break;
//		}
//		case CAN_BAUD_500KBPS_E:
//		{
//			C0BRP = 0x01U;            
//			C0BTR = 0x0104U;
//			break;
//		}
//		case CAN_BAUD_1MBPS_E:
//		{
//			break;
//		}
//		default:
//		{
//			//default - set baudrate to 500kbps
//			C0BRP = 0x01U;            
//			C0BTR = 0x0104U;
//		}
//	}

	return;
}

/***********************************************************************************************************************
* Function Name: StoreBaudrateToFlash
* Description  : The function stores the CAN baudrate to flash
* Arguments    : uint8_t CAN_baud_u8
* Return Value : None
***********************************************************************************************************************/
void StoreBaudrateToFlash(uint8_t CAN_baud_u8)
{
// 	FDL_Erase(0,1);
	
// 	dubWriteBuffer[0] = CAN_baud_u8 & 0xFF;;
	
// 	FDL_Write(0, 0, (uint16_t)CAN_BAUD_LEN);
}
/********************************************************EOF***********************************************************/
/***********************************************************************************************************************
* Function Name: RestoreBaudrateFromFlash
* Description  : The function Restore the CAN baudrate from flash
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint8_t RestoreBaudrateFromFlash(void)
{
	uint8_t CAN_baud_u8;
	FDL_Read(0, 0, CAN_BAUD_LEN);
	CAN_baud_u8 = dubReadBuffer[0];
	
	if(0xFF == CAN_baud_u8)
	{
		CAN_baud_u8 = (uint8_t)CAN_BAUD_250KBPS_E;	
	}
	
	return CAN_baud_u8;
	
}