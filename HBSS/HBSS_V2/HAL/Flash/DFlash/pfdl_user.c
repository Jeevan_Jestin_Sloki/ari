/***********************************************************************************************************************
* File Name    : pfdl_user.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "pfdl_user.h"
/* Standard library (runtime library is used) */
#include <string.h>

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Common variable declaration */
pfdl_status_t   dtyFdlResult;       /* Return value                      */
pfdl_request_t  dtyRequester;       /* PFDL control variable (requester) */
pfdl_u08        dubWriteBuffer[ FDL_WRITE_BUFFER_SIZE ] = {0};
                                        /* Write data input buffer (initial value is set to 0) */
pfdl_u08        dubReadBuffer[ FDL_READ_BUFFER_SIZE ] = {0};
					/* Read data input buffer (initial value is set to 0) */

/***********************************************************************************************************************
* Function Name: FDL_Erase
* Description  : This function process the erasing of the DataFlash memory Block.
* Arguments    : pfdl_u16 Block_Number
* Return Value : None
***********************************************************************************************************************/
void FDL_Erase(pfdl_u16 Block_Number, pfdl_u16 Total_Blocks)
{
	uint16_t Blocks_Count_u16 = 0;
	
	for(Blocks_Count_u16 = 0; Blocks_Count_u16 < Total_Blocks; Blocks_Count_u16++)
	{
		/* If pre-erase is to be performed, perform blank check of the entire target block and then erase the block. */
		if(dtyFdlResult == PFDL_OK)
		{
			if(Block_Number <= TOTAL_FDL_BLOCKS)
			{
				/* Performs erase process for the target block. Sets the erase command */
				dtyRequester.command_enu = PFDL_CMD_ERASE_BLOCK;
				            
				/* Sets the block number of the target block */
				dtyRequester.index_u16   = Block_Number;
				            
				/* Command execution process */
				dtyFdlResult = PFDL_Execute( &dtyRequester );
		            			
				/* Waiting for command to end */
		            	while( dtyFdlResult == PFDL_BUSY )
		            	{
		                	/* Perform any desired process (background operation) */
		                	__nop();
		                	__nop();
		               
		                	/* End confirmation process */
		                	dtyFdlResult = PFDL_Handler();
		            	}
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}
	}
	return;
}


/***********************************************************************************************************************
* Function Name: FDL_Write
* Description  : This function process the Write operation of the data into the Data Flash Memory.
* Arguments    : pfdl_u16 Block_Number, pfdl_u16 Position_at_Block, pfdl_u16 Num_Of_Bytes
* Return Value : None
***********************************************************************************************************************/
void FDL_Write(pfdl_u16 Block_Number, pfdl_u16 Position_at_Block, pfdl_u16 Num_Of_Bytes)
{
	dtyFdlResult = r_pfdl_samFdlStart();	/* FDL initialization processing */
	
	if( dtyFdlResult == PFDL_OK )
	{
		dtyRequester.index_u16 		= ((Block_Number*1024) + (Position_at_Block));
		dtyRequester.bytecount_u16 	= Num_Of_Bytes;
		
		/* Sets the write command (write data and buffer have already been set) */
		dtyRequester.command_enu 	= PFDL_CMD_WRITE_BYTES;
			
		/* Command execution process */
		dtyFdlResult = PFDL_Execute( &dtyRequester );
			
		/* Waiting for command to end */
		while( dtyFdlResult == PFDL_BUSY )
		{
		        /* Perform any desired process (background operation) */
		        __nop();
		        __nop();
		                   
		        /* End confirmation process and trigger process for each 1-byte writing */
		        dtyFdlResult = PFDL_Handler();
		}
				
		/* If writing has been completed normally, executes the internal verification process */
		if( dtyFdlResult == PFDL_OK )
		{
		        /* Sets the internal verification command */
		        dtyRequester.command_enu = PFDL_CMD_IVERIFY_BYTES;
		                    
		        /* Command execution process */
		        dtyFdlResult = PFDL_Execute( &dtyRequester );
		                    
		        /* Waiting for command to end */
		        while( dtyFdlResult == PFDL_BUSY )
		        {
		                /* Perform any desired process (background operation) */
		                __nop();
		                __nop();
		                        
		                /* End confirmation process */
		                dtyFdlResult = PFDL_Handler();
			}	
		}
				
	}
	
	else
	{
		;
	}
	
	r_pfdl_samFdlEnd();			/* FDL end processing */
	
	return;
}


/***********************************************************************************************************************
* Function Name: FDL_Read
* Description  : This function process the Read operation of the data from the Data Flash Memory.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void FDL_Read(pfdl_u16 Block_Number, pfdl_u16 Position_at_Block, pfdl_u16 Num_Of_Bytes)
{
	dtyFdlResult = r_pfdl_samFdlStart();	/* FDL initialization processing */
	
	if(dtyFdlResult == PFDL_OK)
	{
		/* Sets the read command */
		dtyRequester.command_enu = PFDL_CMD_READ_BYTES;
		
		/* Sets the read start address */
		dtyRequester.index_u16 		= ((Block_Number*1024) + (Position_at_Block));
		
		/* Sets the address for the read data input buffer */
                dtyRequester.data_pu08   = dubReadBuffer;
		
		dtyRequester.bytecount_u16 	= Num_Of_Bytes;
		
		/* Command execution process */
                dtyFdlResult = PFDL_Execute( &dtyRequester );
		
	}
	
	r_pfdl_samFdlEnd();			/* FDL end processing */
	
	return;
}


/***********************************************************************************************************************
* Function Name: PFDL start function
* Description  : Start of the pfdl.
* Arguments    : None
* Return Value : pfdl_status_t
***********************************************************************************************************************/

pfdl_status_t r_pfdl_samFdlStart( void )
{
    pfdl_status_t       dtyFdlResult;
    pfdl_descriptor_t   dtyDescriptor;
    
    /* Inputs the initial values */
    dtyDescriptor.fx_MHz_u08            = FDL_FRQUENCY;  /* Sets the frequency    */
    dtyDescriptor.wide_voltage_mode_u08 = FDL_VOLTAGE;  /* Sets the voltage mode */
    
    /* Executes the PFDL initialization function */
    dtyFdlResult = PFDL_Open( &dtyDescriptor );
    
    /* Requester initialization processing */
    dtyRequester.index_u16     = 0;
    dtyRequester.data_pu08     = dubWriteBuffer;
    dtyRequester.bytecount_u16 = 0;
    dtyRequester.command_enu   = PFDL_CMD_READ_BYTES; /* Initializes with PFDL_CMD_READ_BYTES(= 0) */
    
    return dtyFdlResult;
}


/***********************************************************************************************************************
* Function Name: PFDL end function
* Description  : End of the pfdl.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/

void r_pfdl_samFdlEnd( void )
{
    /* Data flash library end processing */
    PFDL_Close();
}

/********************************************************EOF***********************************************************/