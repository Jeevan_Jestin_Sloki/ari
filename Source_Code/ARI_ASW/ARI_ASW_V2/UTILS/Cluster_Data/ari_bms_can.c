﻿

#include "ari_bms_can.h"

bool BMSframeReceived_b = false;
BMS_MSG_1_St_t BMS_MSG_1_St;
BMS_MSG_2_St_t BMS_MSG_2_St;



 uint32_t Deserialize_BMS_MSG_2(BMS_MSG_2_St_t* message, const uint8_t* data)
{
  message->BatteryPackState_u8 = ((data[0] & (SIGNLE_READ_Mask2))) + BMS_MSG_3_CANID_BATTERYPACKSTATE_U8_OFFSET;
   return BMS_MSG_3_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_BMS_MSG_2(BMS_MSG_2_St_t* message, uint8_t* data)
{
  message->BatteryPackState_u8 = (message->BatteryPackState_u8  - BMS_MSG_3_CANID_BATTERYPACKSTATE_U8_OFFSET);
  data[1] = (message->BatteryPackState_u8 & (SIGNLE_READ_Mask2)) ;
   return BMS_MSG_3_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_BMS_MSG_1(BMS_MSG_1_St_t* message, const uint8_t* data)
{
  message->BatteryVoltage_u32 = ((((uint32_t)data[0] & (SIGNLE_READ_Mask8)) << BMS_MSG_1_ST_T_BATTERYVOLTAGE_U16_MASK0) | ((data[1] & (SIGNLE_READ_Mask8)) << BMS_MSG_1_ST_T_BATTERYVOLTAGE_U16_MASK1) | (data[2] & (SIGNLE_READ_Mask8))) + BMS_MSG_1_ST_T_CANID_BATTERYVOLTAGE_U16_OFFSET;
  message->BatteryCurrent_s16 = (((data[3] & (SIGNLE_READ_Mask8)) << BMS_MSG_1_ST_T_BATTERYCURRENT_U16_MASK0) | (data[4] & (SIGNLE_READ_Mask8))) + BMS_MSG_1_ST_T_CANID_BATTERYCURRENT_U16_OFFSET;
  message->BatterySOC_u16 = ((data[5] & (SIGNLE_READ_Mask8))) + BMS_MSG_1_ST_T_CANID_BATTERYSOC_U16_OFFSET;
  message->BatteryTotalCapacity_u16 = (((data[6] & (SIGNLE_READ_Mask8)) << BMS_MSG_1_ST_T_BATTERYTOTALCAPACITY_MASK0) | (data[7] & (SIGNLE_READ_Mask8))) + BMS_MSG_1_ST_T_CANID_BATTERYTOTALCAPACITY_OFFSET;
  
  if(false == BMSframeReceived_b)
  {
	 BMSframeReceived_b = true; 
  }
  
  return BMS_MSG_1_ST_T_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_BMS_MSG_1(BMS_MSG_1_St_t* message, uint8_t* data)
{
  message->BatteryVoltage_u32 = (message->BatteryVoltage_u32  - BMS_MSG_1_ST_T_CANID_BATTERYVOLTAGE_U16_OFFSET);
  message->BatteryCurrent_s16 = (message->BatteryCurrent_s16  - BMS_MSG_1_ST_T_CANID_BATTERYCURRENT_U16_OFFSET);
  message->BatterySOC_u16 = (message->BatterySOC_u16  - BMS_MSG_1_ST_T_CANID_BATTERYSOC_U16_OFFSET);
  message->BatteryTotalCapacity_u16 = (message->BatteryTotalCapacity_u16  - BMS_MSG_1_ST_T_CANID_BATTERYTOTALCAPACITY_OFFSET);
  data[0] = ((message->BatteryVoltage_u32 >> BMS_MSG_1_ST_T_BATTERYVOLTAGE_U16_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[1] = ((message->BatteryVoltage_u32 >> BMS_MSG_1_ST_T_BATTERYVOLTAGE_U16_MASK1) & (SIGNLE_READ_Mask8)) ;
  data[2] = (message->BatteryVoltage_u32 & (SIGNLE_READ_Mask8)) ;
  data[3] = ((message->BatteryCurrent_s16 >> BMS_MSG_1_ST_T_BATTERYCURRENT_U16_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->BatteryCurrent_s16 & (SIGNLE_READ_Mask8)) ;
  data[5] = (message->BatterySOC_u16 & (SIGNLE_READ_Mask8)) ;
  data[6] = ((message->BatteryTotalCapacity_u16 >> BMS_MSG_1_ST_T_BATTERYTOTALCAPACITY_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[7] = (message->BatteryTotalCapacity_u16 & (SIGNLE_READ_Mask8)) ;
   return BMS_MSG_1_ST_T_ID; 
}

void ClearBmsMsg1(void)
{
  BMS_MSG_1_St.BatteryCurrent_s16 = CLEAR;
  BMS_MSG_1_St.BatterySOC_u16 = CLEAR;
  BMS_MSG_1_St.BatteryTotalCapacity_u16 = CLEAR;
  BMS_MSG_1_St.BatteryVoltage_u32 = CLEAR;

}

void ClearBmsMsg2(void)
{
  BMS_MSG_2_St.BatteryPackState_u8 = CLEAR;
}
