/******************************************************************************
 *    FILENAME    : iso14229_serv10.c
 *    DESCRIPTION : This file contains variables and functions of iso14229 
 *                  service 10.
 ******************************************************************************
 * Revision history
 *
 * Ver Author          Date               Description
 * 1   Sandeep K Y     14/01/2020          Initial version
 ******************************************************************************
*/

/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
#include "main.h"
#include "iso14229_serv10.h"

/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */

uint8_t current_active_session_u8 = UDS_SESS_DEFAULT;

/* ************************************************************************** */



/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */

/* ************************************************************************** */

/**
 * @Function: void UDS_Default_Diag_Init()
 * 
 * @Summary: This Function is used to initialize 
 *          default diagnostic session, all flags
 *          are reset and all veriables are re-
 *           initialized
 * 
 * @Params: None
 * 
 * @Return: None
 */
void UDS_Deafault_Diag_Init(void){
    current_active_session_u8 = DEFAULT_SESSION_SUB_ID_E;

}

/**
 * @Function: void UDS_Programming_session_Init()
 * 
 * @Summary: This Function is used to initialize 
 *          Programming session, all flags
 *          are reset and all veriables are re-
 *           initialized
 * 
 * @Params: None
 * 
 * @Return: None
 */
void UDS_Programming_session_Init(void){
    current_active_session_u8 = PROGRAMMING_SESSION_SUB_ID_E;

}

/**
 * @Function: void UDS_extended_Diag_Init()
 * 
 * @Summary: This Function is used to initialize 
 *          extended diagnostic session, all flags
 *          are reset and all veriables are re-
 *           initialized
 * 
 * @Params: None
 * 
 * @Return: None
 */
void UDS_extended_Diag_Init(void){
    current_active_session_u8 = EXTENDED_DIAG_SESSION_SUB_ID_E;
    
}

/**
 * @Function: void UDS_safety_system_Diag_Init()
 * 
 * @Summary: This Function is used to initialize 
 *          safety system diagnostic session, all flags
 *          are reset and all veriables are re-
 *           initialized
 * 
 * @Params: None
 * 
 * @Return: None
 */
//void UDS_safety_system_Diag_Init(){
//    current_active_session_u8 = ENGINEERING_DIAG_SESSION_SUB_ID_E;
//
//
//}


/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */
/**
*  FUNCTION NAME : iso14229_serv10
*  FILENAME      : iso14229_serv10.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_10 requests
*  @return       : Type of response.                  
*/
uint8_t DiagSessionType_u8;
UDS_Serv_resptype_En_t iso14229_serv10(UDS_Serv_St_t*  UDS_Serv_pSt){
	UDS_Serv_resptype_En_t serv_10_resp_En;
    DiagSessionType_u8 = UDS_Serv_pSt->RxBuff_pu8[1];
    if(SERV_10_MAX_LEN != UDS_Serv_pSt->RxLen_u16){
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_SESSIONCONTROL;
    	UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
    	UDS_Serv_pSt->TxLen_u16 = 3;
    	serv_10_resp_En = UDS_SERV_RESP_NEG_E;
    }else{
        switch(DiagSessionType_u8){
            case DEFAULT_SESSION_SUB_ID_E:{
                UDS_Deafault_Diag_Init();
                UDS_Serv_pSt->TxBuff_pu8[2]= DEFAULT_SESSION_SUB_ID_E;
                UDS_Serv_pSt->TxLen_u16  = 1;
                serv_10_resp_En = UDS_SERV_RESP_POS_E;

                break;                     
            }
            case PROGRAMMING_SESSION_SUB_ID_E:{
                UDS_Programming_session_Init();
                UDS_Serv_pSt->TxBuff_pu8[2]= PROGRAMMING_SESSION_SUB_ID_E;
                UDS_Serv_pSt->TxLen_u16  = 1;
                serv_10_resp_En = UDS_SERV_RESP_POS_E;
                break;                
            }
            case EXTENDED_DIAG_SESSION_SUB_ID_E:{
                UDS_extended_Diag_Init();
                UDS_Serv_pSt->TxBuff_pu8[2]= EXTENDED_DIAG_SESSION_SUB_ID_E;
                UDS_Serv_pSt->TxLen_u16  = 1;

                serv_10_resp_En = UDS_SERV_RESP_POS_E;
                break;                
            }
//            case SAFETY_SYSTEM_DIAG_SESSION_SUB_ID_E:{
//                UDS_safety_system_Diag_Init();
//                UDS_Serv_pSt->TxBuff_pu8[2]= SAFETY_SYSTEM_DIAG_SESSION_SUB_ID_E;
//                UDS_Serv_pSt->TxLen_u16  = 1;
//                serv_10_resp_En = UDS_SERV_RESP_POS_E;
//
//                break;
//            }
            default:{
            	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
            	UDS_Serv_pSt->TxBuff_pu8[1] = SID_SESSIONCONTROL;
            	UDS_Serv_pSt->TxBuff_pu8[2] = SUB_FUNC_NOT_SUPPORTED;
            	UDS_Serv_pSt->TxLen_u16 = 3;
                serv_10_resp_En = UDS_SERV_RESP_NEG_E;
                break;
            }
        }
    }
     return serv_10_resp_En;
}

// *****************************************************************************

/* *****************************************************************************
 End of File
 */
