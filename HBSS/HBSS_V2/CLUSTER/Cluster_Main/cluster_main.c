/***********************************************************************************************************************
* File Name    : cluster_main.c
* Version      : 01
* Description  : This file implements the cluster display main module.
* Created By   : Jeevan Jestin N
* Creation Date: 02/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "cluster_main.h"
#include "SpeedDisp.h"
#include "DataBank.h"
#include "TelltaleDisp.h"
#include "ODO_Disp.h"
#include "SOC_Disp.h"
#include "PowerConsum_Disp.h"
#include "Time.h"
#include "TimeDisp.h"
#include "RangeKmDisp.h"
#include "App_typedefs.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
Time_Disp_State_En_t Time_Disp_State_En = DISPLAY_TIME_E;


/***********************************************************************************************************************
* Function Name: Display_Cluster_Data
* Description  : This function displays all the cluster signals on the LCD.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
/*todo:jeevan*/ /* using for navigation status,
			need to obtain from CAN */
			
void Display_Cluster_Data(void)
{
	Display_Speed(ClusterSignals_St.VehicleSpeed_u16, SPEED_E);
	ODOTrip_Disp(ClusterSignals_St.ODOmeter_u32 ,ODO_E); 
	Display_SOC((uint16_t)ClusterSignals_St.BatterySOC_u8 , ClusterSignals_St.ChargingStatus_u8 , BATT_SOC_BAR_E , BATT_SOC_DIGIT_E );
	Display_PowerConsum( ClusterSignals_St.PowerConsumption_u16,  POWER_CONSUMP_E);
	//Blink_Time_Colon(TIME_COLON_E);
	Display_RangeKm( ClusterSignals_St.RangeKm_u16 , RANGE_KM_E);
	Indicate_Telltales();

	return;
}


/********************************************************EOF***********************************************************/