/***********************************************************************************************************************
 * File Name    : DataAquire.c
 * Version      : 01
 * Description  :
 * Created By   : Jeevan Jestin N
 * Creation Date: 01/9/2021
 ***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "DataAquire.h"
#include "DataBank.h"
#include "ODO_Disp.h"
#include "Communicator.h"
#include "UserInterfaceButton.h"
#include "bms_can.h"
#include "SignalCalc_main.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
#define TELLTALE_OFF						0x00U
/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

BuzzerEventData_St_t BuzzerEventData_St[TOTAL_BUZZER_EVENT_E] =
{
	{IGNITION_EVENT_E,		_5000_MS,  _250_MS, false, false},
	{TEMP_WARNING_EVENT_E, 		_3000_MS,  _500_MS, false, false},
	{LOW_BATT_PRIORITY_EVENT_E,	_5000_MS,  _500_MS, false, false},
	{REVERSE_GEAR_EVENT_E, 		_300_MS,   _150_MS, false, false},
	{INDICATOR_EVENT_E, 		_50_MS ,   _500_MS, false, false},
	{HAZARD_WARNING_EVENT_E, 	_500_MS,  _250_MS, false, false}
};

UserInputSigConf_St_t	UserInputSigConf_St[TOTAL_USER_INPUT_SIG_E] = 
{
	{INPUT_1_E,	 	false, IGNITION_ON,		   RESET_VALUE  , 	TELLTALE_OFF, true },
	{INPUT_2_E,	 	false, TELLTALE_SOLID_ON,  RESET_VALUE  , 	TELLTALE_OFF, true },
	{INPUT_3_E,	 	false, TELLTALE_SOLID_ON,  RESET_VALUE  , 	TELLTALE_OFF, false},
	{INPUT_4_E,	 	false, TELLTALE_SOLID_ON,  RESET_VALUE  , 	TELLTALE_OFF, false},
	{INPUT_5_E,	 	false, TELLTALE_BLINK,  RESET_VALUE  , 	TELLTALE_OFF, false},
	{INPUT_6_E,	 	false, TELLTALE_SOLID_ON,  RESET_VALUE  , 	TELLTALE_OFF, false},
	{INPUT_7_E,	 	false, TELLTALE_SOLID_ON,  RESET_VALUE  , 	TELLTALE_OFF, false},
	{INPUT_8_E,	 	false, TELLTALE_SOLID_ON,  RESET_VALUE  , 	TELLTALE_OFF, false},
	{INPUT_9_E,	 	false, TELLTALE_SOLID_ON,  RESET_VALUE  ,	IGNITION_OFF, false},
#if (FALSE == BUZZER_OUTPUT)	
	{INPUT_10_E,		false, TELLTALE_SOLID_ON,  RESET_VALUE  ,   	TELLTALE_OFF, false},
#endif
	{INPUT_11_E,		false, TELLTALE_BLINK	,  RESET_VALUE	,	TELLTALE_OFF, false},
	{INPUT_12_E,		false, TELLTALE_SOLID_ON   ,  RESET_VALUE	, 	TELLTALE_OFF, false},
};

UserButtonConf_St_t		UserButtonConf_St[TOTAL_USER_INPUT_SIG_E]=
{
	{INPUT_1_E,	 	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
	{INPUT_2_E,	 	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
	{INPUT_3_E,	 	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
	{INPUT_4_E,	 	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
	{INPUT_5_E,	 	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
	{INPUT_6_E,	  	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
	{INPUT_7_E,	 	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
	{INPUT_8_E,	 	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
	{INPUT_9_E,	 	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
#if (FALSE == BUZZER_OUTPUT)
	{INPUT_10_E,	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
#endif
	{INPUT_11_E,	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
	{INPUT_12_E,	 USER_BUTTON_RELEASED_E, DEFAULT_VALUE, DEFAULT_VALUE, THRESHOLD_VALUE_3000MS, DEBOUNCE_50MS, false},
};
IgnitionInput_En_t	IgnitionInput_En = IGNITION_NONE_E; //todo:configurable
uint32_t InActiveTimerOfTurnInd_u32 = ZERO_E;
static uint16_t BuzzerAnimationCount_u16 = 0;
/***********************************************************************************************************************
 * Function Name: ReadClusterInput
 * Description  : This function reads the user inputs.
 * Arguments    : None
 * Return Value : None
 ***********************************************************************************************************************/
void ReadClusterInput(void)
{
	uint8_t UserInputSigCount_u8 = 0;

	UserInputSigConf_St[INPUT_1_E].UserInputVal_u8 	= P2_bit.no6; 		/*ANI5*/			/* SET button */
	UserInputSigConf_St[INPUT_2_E].UserInputVal_u8 	= P2_bit.no1; 		/*ANI1*/			/* Mode button*/
	UserInputSigConf_St[INPUT_3_E].UserInputVal_u8 	= P12_bit.no3; 		/*P123*/			/* Right indicator*/
	UserInputSigConf_St[INPUT_4_E].UserInputVal_u8 	= P2_bit.no0; 		/*ANI0*/					
	UserInputSigConf_St[INPUT_5_E].UserInputVal_u8 	= P13_bit.no7; 		/*INTP5/P137*/  	/* Ignition */
	UserInputSigConf_St[INPUT_6_E].UserInputVal_u8 	= P2_bit.no2; 		/*ANI2*/			/* Left indicator */
	UserInputSigConf_St[INPUT_7_E].UserInputVal_u8 	= P2_bit.no3; 		/*ANI3*/			/* Kill switch*/			
	UserInputSigConf_St[INPUT_8_E].UserInputVal_u8 	= P2_bit.no4; 		/*ANI4*/			/* Warning mapped to Parking brake*/	
	UserInputSigConf_St[INPUT_9_E].UserInputVal_u8 	= P2_bit.no5; 		/*ANI6*/			/* High beam*/
#if (FALSE == BUZZER_OUTPUT)
	UserInputSigConf_St[INPUT_10_E].UserInputVal_u8 = P2_bit.no7; 		/*ANI7*/
#endif
	UserInputSigConf_St[INPUT_11_E].UserInputVal_u8 = P12_bit.no4;		/*P124*/			/* Reverse Ind*/
	UserInputSigConf_St[INPUT_12_E].UserInputVal_u8 = P9_bit.no4; 		/*PWM-IN/P94*/  	/* Eco Mode */

	for(UserInputSigCount_u8 = 0; UserInputSigCount_u8 < TOTAL_USER_INPUT_SIG_E; UserInputSigCount_u8++ )
	{
		if(( true == UserInputSigConf_St[UserInputSigCount_u8].UserInputVal_u8 ) && (true == UserInputSigConf_St[UserInputSigCount_u8].UserButtonEnable_b ))
		{
			if( (USER_BUTTON_RELEASED_E == UserButtonConf_St[UserInputSigCount_u8].UserButtonState_En))
			{
				UserButtonConf_St[UserInputSigCount_u8].UserButtonState_En = USER_BUTTON_PRESSED_E;
				UserButtonConf_St[UserInputSigCount_u8].UserInputCaptureTime_u32 = GET_TIME_MS();
			}
			else if(USER_BUTTON_PRESSED_E == UserButtonConf_St[UserInputSigCount_u8].UserButtonState_En)
			{
				UserButtonConf_St[UserInputSigCount_u8].UserInpActiveStateTime_u32 = GET_TIME_MS() - UserButtonConf_St[UserInputSigCount_u8].UserInputCaptureTime_u32;
			}
			
			// if( (THRESHOLD_VALUE_3000MS <= UserButtonConf_St[UserInputSigCount_u8].UserInpActiveStateTime_u32))
			// {
			// 	UserButtonConf_St[UserInputSigCount_u8].UserInputLongPressed_b = true;
			// }
		}
		else if (( false == UserInputSigConf_St[UserInputSigCount_u8].UserInputVal_u8 ) && (true == UserInputSigConf_St[UserInputSigCount_u8].UserButtonEnable_b ))
		{
			if(( USER_BUTTON_PRESSED_E == UserButtonConf_St[UserInputSigCount_u8].UserButtonState_En) && 
			   ( false == UserButtonConf_St[UserInputSigCount_u8].UserInputLongPressed_b) /*&&
			   ( DEBOUNCE_50MS <=  UserButtonConf_St[UserInputSigCount_u8].UserInpActiveStateTime_u32 )*/)
			{
				UserInputSigConf_St[UserInputSigCount_u8].SignalState_u8 = UserInputSigConf_St[UserInputSigCount_u8].SigTurnOnVal_u8;
				UserButtonConf_St[UserInputSigCount_u8].UserButtonState_En = USER_BUTTON_RELEASED_E;
			}
			else if( ( USER_BUTTON_PRESSED_E == UserButtonConf_St[UserInputSigCount_u8].UserButtonState_En ) ||
				     ( true ==  UserButtonConf_St[UserInputSigCount_u8].UserInputLongPressed_b))
			{
				UserButtonConf_St[UserInputSigCount_u8].UserInputLongPressed_b = false;
				UserButtonConf_St[UserInputSigCount_u8].UserInputCaptureTime_u32 = RESET_VALUE;
				UserButtonConf_St[UserInputSigCount_u8].UserInpActiveStateTime_u32 = RESET_VALUE;
				UserButtonConf_St[UserInputSigCount_u8].UserButtonState_En = USER_BUTTON_RELEASED_E;
			}
			else 
			{
				UserInputSigConf_St[UserInputSigCount_u8].SignalState_u8 = UserInputSigConf_St[UserInputSigCount_u8].SigTurnOffVal_u8;
				UserButtonConf_St[UserInputSigCount_u8].UserInpActiveStateTime_u32 = RESET_VALUE;
				UserButtonConf_St[UserInputSigCount_u8].UserInputCaptureTime_u32 = RESET_VALUE;
			}
		}
		else if(true == UserInputSigConf_St[UserInputSigCount_u8].UserInputVal_u8)
		{
			UserInputSigConf_St[UserInputSigCount_u8].SignalState_u8 = UserInputSigConf_St[UserInputSigCount_u8].SigTurnOnVal_u8;
		}
		else
		{
			UserInputSigConf_St[UserInputSigCount_u8].SignalState_u8 = UserInputSigConf_St[UserInputSigCount_u8].SigTurnOffVal_u8;
		}
	}

	if(IgnitionInput_En == IGNITION_GPIO_E)
	{
		ClusterInput_St.IgnitionStatus_u8 = UserInputSigConf_St[INPUT_5_E].SignalState_u8;
	}
	else if(IgnitionInput_En == IGNITION_CAN_E)
	{
		ClusterInput_St.IgnitionStatus_u8 = 1;
	}
	else if(IgnitionInput_En == IGNITION_NONE_E)
	{
		ClusterInput_St.IgnitionStatus_u8 = 0x01U; //Set Ignition to ON
	}
	else
	{
		;
	}
	
	UpdateUserButton();
	return;
}

/***********************************************************************************************************************
 * Function Name: UpdateSigToDataBank
 * Description  : This function updates the user input signals and signals received over CAN to the Data Bank.
 * Arguments    : None
 * Return Value : None
 ***********************************************************************************************************************/
void UpdateSigToDataBank(void)
{
	ClusterInput_St.ChargingStatus_u8 				= GET_CHARGER_PLUG_IN_STATE();

	ClusterSignals_St.ODOmeter_u32 					= GET_ODO_METER();

	ClusterSignals_St.PowerConsumption_u16 				= GET_VEHICLE_SPEED()/POWER_CONSUMP_FACTOR;

	ClusterSignals_St.VehicleSpeed_u16 				= GET_VEHICLE_SPEED();

	ClusterSignals_St.NavigationDistance_u16 			= 0;

	ClusterSignals_St.RangeKm_u16 					= GET_RANGEKM();

	ClusterSignals_St.LeftIndicator_u8 				= UserInputSigConf_St[INPUT_6_E].SignalState_u8;

	ClusterSignals_St.EngineFault_u8 				= GET_BATT_FAULT_STATE();

	ClusterSignals_St.Warning_u8 					= UserInputSigConf_St[INPUT_8_E].SignalState_u8; /* Parking brake */

	ClusterSignals_St.KillSwitch_u8 				= UserInputSigConf_St[INPUT_7_E].SignalState_u8;;

	ClusterSignals_St.MotorFault_u8 				= GET_MOTOR_FAULT_STATE();

	ClusterSignals_St.ServiceReminder_u8 				= 0;

	ClusterSignals_St.RightIndicator_u8 				= UserInputSigConf_St[INPUT_3_E].SignalState_u8;

	ClusterSignals_St.NavigationDirection_u8 			= 0;

	ClusterSignals_St.BatterySOC_u8 				= BatteryGeneralInfo_0xBB_St.SOC;

	ClusterSignals_St.PowerIcon_u8 					= 0;

	ClusterSignals_St.TOPText_u8 					= GET_TOP_TEXT_STATE();

	if( TELLTALE_BLINK ==  UserInputSigConf_St[INPUT_11_E].SignalState_u8 )
	{
		ClusterSignals_St.ReverseMode_u8 = TELLTALE_BLINK;
		ClusterSignals_St.EconomyMode_u8 = TELLTALE_OFF;
		ClusterSignals_St.NeutralMode_u8 = TELLTALE_OFF;
		ClusterSignals_St.SportsMode_u8	 = TELLTALE_OFF;
	}
	else
	{
		ClusterSignals_St.NeutralMode_u8  = TELLTALE_OFF;
		ClusterSignals_St.EconomyMode_u8 = TELLTALE_SOLID_ON;
		ClusterSignals_St.ReverseMode_u8 = TELLTALE_OFF;
		ClusterSignals_St.SportsMode_u8	 = TELLTALE_OFF;

	}

	ClusterSignals_St.SideStand_u8 				= 0;//UserInputSigConf_St[INPUT_8_E].SignalState_u8;

	ClusterSignals_St.NetworkConnection_u8 			= 0;

	ClusterSignals_St.BLEIcon_u8 				= 0;

	ClusterSignals_St.HighBeam_u8 				=  UserInputSigConf_St[INPUT_9_E].SignalState_u8;

	ClusterSignals_St.HoursTime_u8 				= HoursTime_u8;

	ClusterSignals_St.MinutesTime_u8 			= MinutesTime_u8;

	ClusterSignals_St.TimeColon_u8 				= 0;//GET_TIME_COLON_STATE();

	ClusterSignals_St.Navigation_Status_u8 			= 0;

	ClusterSignals_St.Commonsig_u8 				= 3;
	
	ClusterSignals_St.AText_u8 = GET_TRIP_A_TEXT_STATE();
	ClusterSignals_St.BText_u8 = GET_TRIP_B_TEXT_STATE();
	ClusterSignals_St.TRIPText_u8 = GET_TRIP_TEXT_STATE();
	ClusterSignals_St.ODOText_u8 = GET_ODO_TEXT_STATE();

//	UpdateBuzzerEvent();
//	BuzzerScheduler();
	
	return;
}

/***********************************************************************************************************************
 * Function Name: update_ODOtrip
 * Description  : The function updates trip meter status
 * Arguments    : None
 * Return Value : None
 ***********************************************************************************************************************/
void update_ODOtrip(uint8_t Tripmeter_state)
{
	switch (Tripmeter_state)
	{
	case 0:
	{
		OdoTrip_Sel_En = ODO_DISP_E;
		ClusterSignals_St.AText_u8 = 0;
		ClusterSignals_St.BText_u8 = 0;
		ClusterSignals_St.TRIPText_u8 = 0;
		ClusterSignals_St.ODOText_u8 = 3;
		break;
	}
	case 1:
	{
		OdoTrip_Sel_En = TRIP_DISP_E;
		TripAB_Sel_En = DISP_TRIP_A_E;
		ClusterSignals_St.ODOText_u8 = 0;
		ClusterSignals_St.TRIPText_u8 = 3;
		ClusterSignals_St.AText_u8 = 3;
		ClusterSignals_St.BText_u8 = 0;
		break;
	}
	case 2:
	{
		OdoTrip_Sel_En = TRIP_DISP_E;
		TripAB_Sel_En = DISP_TRIP_B_E;
		ClusterSignals_St.ODOText_u8 = 0;
		ClusterSignals_St.TRIPText_u8 = 3;
		ClusterSignals_St.AText_u8 = 0;
		ClusterSignals_St.BText_u8 = 3;
		break;
	}

	default:
	{
		break;
	}

	}
}
/***********************************************************************************************************************
 * Function Name: ResetTheUserInput
 * Description  : This function resets the user input signals state.
 * Arguments    : None
 * Return Value : None
 ***********************************************************************************************************************/
void ResetTheUserInput(void)
{
	BuzzerAnimationCount_u16 = CLEAR;
	P2_bit.no7 =  PORT_OUTPUT_LOW;
	return;
}

/***********************************************************************************************************************
* Function Name: BuzzerScheduler
* Description  : This function implements main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void BuzzerScheduler(void)
{
	BuzzerEvent_En_t BuzzerEventCount_En;
	static uint32_t BuzzerFrequency_u32 = 0;
	static uint16_t DutyCycleBuzzer_u16 = 0;
	static uint8_t BuzzerTriggered_u8 = 0;
	static bool BeepSoundInit_b = false;
	static int16_t DutyCycleCount_s16 = 0;

	for(BuzzerEventCount_En = IGNITION_EVENT_E; BuzzerEventCount_En < TOTAL_BUZZER_EVENT_E; BuzzerEventCount_En++)
	{
		if(true == BuzzerEventData_St[BuzzerEventCount_En].BuzzerEventTriggered_b)
		{
			BuzzerFrequency_u32 = BuzzerEventData_St[BuzzerEventCount_En].BuzzerEventFrequency_ms_u32;
			DutyCycleBuzzer_u16 = BuzzerEventData_St[BuzzerEventCount_En].BuzzerDutyCycle_u16;
			BuzzerEventData_St[BuzzerEventCount_En].BuzzerEventStart_b = true;
			BuzzerTriggered_u8 = true;
			break;
		}
		else
		{
			BuzzerTriggered_u8 = false;
		}
	}

	if(true == BuzzerTriggered_u8)
	{
		if(0 == (TaskSchedulerCounter_u32 % BuzzerFrequency_u32 ) && ( false == BeepSoundInit_b))
		{
			P2_bit.no7 = PORT_OUTPUT_HIGH;
			BeepSoundInit_b = true;
			DutyCycleCount_s16 = DutyCycleBuzzer_u16;
		}
		if(true == BeepSoundInit_b)
		{
			if(0 >= DutyCycleCount_s16 )
			{
				P2_bit.no7 =  PORT_OUTPUT_LOW;
				BeepSoundInit_b = false;
			}
			else
			{
				DutyCycleCount_s16-=5;
			}
		}
	}
	else
	{
		if(PORT_OUTPUT_HIGH == P2_bit.no7)
		{
			P2_bit.no7 =  PORT_OUTPUT_LOW;
			DutyCycleCount_s16 = RESET_VALUE;
			BeepSoundInit_b = false;
			BuzzerFrequency_u32 = RESET_VALUE;
			DutyCycleCount_s16 = RESET_VALUE;
		}
	}


}

/***********************************************************************************************************************
* Function Name: BuzzerScheduler
* Description  : This function implements main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void UpdateBuzzerEvent(void)
{
	if( ( TELLTALE_SOLID_ON == ClusterSignals_St.LeftIndicator_u8 )||
	    ( TELLTALE_SOLID_ON == ClusterSignals_St.RightIndicator_u8 ))
	{
		BuzzerEventData_St[INDICATOR_EVENT_E].BuzzerEventTriggered_b = true;
		InActiveTimerOfTurnInd_u32 = ZERO_E; /**/
	}
	else
	{
		InActiveTimerOfTurnInd_u32++;
		BuzzerEventData_St[INDICATOR_EVENT_E].BuzzerEventTriggered_b = false;
	}

	// if( ( TELLTALE_BLINK == ClusterSignals_St.Warning_u8 ) && (InActiveTimerOfTurnInd_u32 > 100))
	// {
	// 		/* 	The turn indicator has highest priority than Hazard warning. Turn indicator input is a
	// 	   	   	Pulsuating input. During off time " InActiveTimerOfTurnInd_u32 " is updated to check turn indicator is inactive for 1 sec. */
	// 	BuzzerEventData_St[HAZARD_WARNING_EVENT_E].BuzzerEventTriggered_b = true;
	// }
	// else
	// {
	// 	BuzzerEventData_St[HAZARD_WARNING_EVENT_E].BuzzerEventTriggered_b = false;
	// }

	if(TELLTALE_BLINK == ClusterSignals_St.ReverseMode_u8)
	{
		BuzzerEventData_St[REVERSE_GEAR_EVENT_E].BuzzerEventTriggered_b = true;
	}
	else
	{
		BuzzerEventData_St[REVERSE_GEAR_EVENT_E].BuzzerEventTriggered_b = false;
	}

	if( ( TELLTALE_BLINK == ClusterSignals_St.MotorFault_u8 ) || 
		( TELLTALE_BLINK == ClusterSignals_St.EngineFault_u8 ) )
	{
		BuzzerEventData_St[TEMP_WARNING_EVENT_E].BuzzerEventTriggered_b = true;
	}
	else
	{
		BuzzerEventData_St[TEMP_WARNING_EVENT_E].BuzzerEventTriggered_b = false;
	}

	if((ClusterSignals_St.RangeKm_u16 < LOW_SOC_RANGE ) && (ClusterSignals_St.ChargingIndicator_u8 != BATTERY_CHARGING) )
	{
		BuzzerEventData_St[LOW_BATT_PRIORITY_EVENT_E].BuzzerEventTriggered_b = true;
	}
	else
	{
		BuzzerEventData_St[LOW_BATT_PRIORITY_EVENT_E].BuzzerEventTriggered_b = false;
	}
}

void BuzzerAnimation(void)
{
	if(0 == BuzzerAnimationCount_u16)
	{
		P2_bit.no7 = PORT_OUTPUT_HIGH;
	}
	else
	{
		;
	}

	if(500 <= BuzzerAnimationCount_u16)
	{
		P2_bit.no7 = PORT_OUTPUT_LOW;
	}
	else
	{
		;
	}

	BuzzerAnimationCount_u16+=5;
}
/********************************************************EOF***********************************************************/