/***********************************************************************************************************************
* File Name    : Speed_Calc.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 06/10/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Speed_Calc.h"
#include"ODO_Calc.h"
#include "Communicator.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
#define SPEED_LPF_POS_GAIN				1000 /*todo:configurable*/
#define SPEED_LPF_NEG_GAIN				SPEED_LPF_POS_GAIN

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
SpeedTimeOut_St_t	SpeedTimeOut_St = 
{
	0, /*Present Speed Pulse Count*/
	0, /*Previous Speed Pulse Count*/
};

SpeedCal_En_t		SpeedCal_En = SPEED_CAL_NONE_E;

uint64_t PresentODOmm_u64  			= 0;
uint64_t PreviousODOmm_u64 			= 0; 
volatile uint32_t TimeTick_1ms_u32 	= 0;
uint32_t Cal_VehicleSpeed_u32 		= 0;
uint32_t PrevTime_ms_u32 			= 0;
uint32_t DifferenceODOmm_u32  		= 0;
uint32_t DifferenceTimems_u32 		= 0;
float 	 SpeedKmph 					= 0;
uint16_t SpeedToDisplay_u16 		= 0;
uint16_t SpeedFilter_Out_u16 		= 0;
uint16_t TimeOutCounter_u16 		= 0;
uint16_t Counter5ms_u16 			= 0;
uint16_t SpeedDecimal_u16 			= 0;
bool 	 CalculateSpeed_b 			= false;

uint32_t Calculated_Speed_u32       = 0;
uint16_t SpeedValidationCounter_u16 = 0;

/***********************************************************************************************************************
* Function Name: Calculate_VehicleSpeed
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Calculate_VehicleSpeed(void)
{
	bool ResetVehicleSpeed_b = false;
	
	Counter5ms_u16++; /*Increment the counter by 1 for each 5ms*/
	
	if(CalculateSpeed_b == false)
	{
		PrevTime_ms_u32 = (GET_TIME_MS() - SPEED_CAL_SCHEDULE_TIME_MS);
		PresentODOmm_u64 = ((Get_ODOmeter()*1000) + Get_ODOmeter_in_mm());
		PreviousODOmm_u64 = PresentODOmm_u64;
		CalculateSpeed_b = true;

	}
	
	else
	{
		PresentODOmm_u64 = ((Get_ODOmeter()*1000) + Get_ODOmeter_in_mm());
						/*Get the Present ODOmeter in mm*/
		DifferenceODOmm_u32 = (uint32_t)(PresentODOmm_u64 - PreviousODOmm_u64);
						/*Change in the distance*/
		if(DifferenceODOmm_u32 >= 500)
		{
			DifferenceTimems_u32 = GET_TIME_MS() - PrevTime_ms_u32;
						/*Change in time*/
			SpeedKmph = ((float)DifferenceODOmm_u32 / DifferenceTimems_u32) * 3.6;
						/*3.6 is multipled to convert m/s to Km/Hr*/
						
						if(SpeedKmph > 199)
						{
							NOP();
							NOP();
						}
			if((SpeedKmph != 0.0f) && (SpeedKmph < 1.0f))
			{
				SpeedKmph = 1;
			}
			
			Cal_VehicleSpeed_u32 = (uint32_t)SpeedKmph;
			
			SpeedDecimal_u16 = (uint16_t)((SpeedKmph - (float)Cal_VehicleSpeed_u32)*1000);
			
			if(SpeedDecimal_u16 >= 500)
			{
				Cal_VehicleSpeed_u32 += 1;
			}
			
			PreviousODOmm_u64 = PresentODOmm_u64;
			PrevTime_ms_u32 = GET_TIME_MS();
			
			Validate_Cal_Speed(Cal_VehicleSpeed_u32);
						/*Validate the speed, when Calculated Speed Difference is 1 Kmph*/
		}
	}
	
	if(Counter5ms_u16 == 20)
	{
		/*Check for Speed Timeout for each 100ms*/
		ResetVehicleSpeed_b = CheckSpeedTimeOut();
		
		if(ResetVehicleSpeed_b == true)
		{
			Cal_VehicleSpeed_u32 = 0;
					/*Reset the Vehicle Speed to zero*/
			Calculated_Speed_u32 = Cal_VehicleSpeed_u32;
		}

		FilterVehicleSpeed(Cal_VehicleSpeed_u32);
		
		Counter5ms_u16 = 0;
	}
	//FilterVehicleSpeed(Cal_VehicleSpeed_u32);
	Calculated_Speed_u32 = SpeedToDisplay_u16;
	SET_VEHICLE_SPEED(Calculated_Speed_u32);
	
	return;
}


/***********************************************************************************************************************
* Function Name: Validate_Cal_Speed
* Description  : This function validates the calculated vehicle speed, when the estimated speed difference is 1 Kmph.
* Arguments    : uint32_t Present_CalSpeed_u32
* Return Value : None
***********************************************************************************************************************/
void Validate_Cal_Speed(uint32_t Present_CalSpeed_u32)
{
    uint16_t CalSpeedDifference_u16 = 0;
    
    if(Present_CalSpeed_u32 != Calculated_Speed_u32)
    {
        if(Present_CalSpeed_u32 >=  SPEED_VALIDATION_START_RANGE)
        {
            if(Present_CalSpeed_u32 > Calculated_Speed_u32)
            {
                CalSpeedDifference_u16 = Present_CalSpeed_u32 - Calculated_Speed_u32;
            }
            else
            {
                CalSpeedDifference_u16 = Calculated_Speed_u32 - Present_CalSpeed_u32;
            }
            
            
            if(CalSpeedDifference_u16 == SPEED_DIIF_RANGE_TO_VALIDATE)
            {
                SpeedValidationCounter_u16++;
            }
            else
            {
                Calculated_Speed_u32 = Present_CalSpeed_u32;
                SpeedValidationCounter_u16 = 0;
            }
            
            if(SpeedValidationCounter_u16 == SPEED_DEBOUNCE_COUNT)
            {
                Calculated_Speed_u32 = Present_CalSpeed_u32;
                SpeedValidationCounter_u16 = 0;
            }
            else
            {
                ;
            }
            
        }
        else
        {
            Calculated_Speed_u32 = Present_CalSpeed_u32;
            SpeedValidationCounter_u16 = 0;
        }
    }
    else
    {
        ;
    }
    return;
}


/***********************************************************************************************************************
* Function Name: CheckSpeedTimeOut
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
bool CheckSpeedTimeOut(void)
{
	uint16_t SpeedPulseDone_u16 	= 0;
	bool WheelRotation_b 		= false;
	bool ResetSpeedToZero_b		= false;
	
	SpeedTimeOut_St.PresentSpeedPulseCount_u32 = GET_SPEED_PULSE_COUNT(); /*Get the Total Speed Pulse Count*/
	
	SpeedPulseDone_u16 = (SpeedTimeOut_St.PresentSpeedPulseCount_u32 - 
					SpeedTimeOut_St.PreviousSpeedPulseCount_u32);
					/*Find the SpeedPulse Count Done in the time interval*/
	
	SpeedTimeOut_St.PreviousSpeedPulseCount_u32 = SpeedTimeOut_St.PresentSpeedPulseCount_u32;
					
	if(SpeedPulseDone_u16 == 0)
	{
		/*Wheel is not Rotating --> Vehicle is Stationary*/
		WheelRotation_b = false;
	}
	else
	{
		/*Wheel is  Rotating --> Vehicle is Moving*/
		WheelRotation_b = true;
	}
	
	if(WheelRotation_b == false)
	{
		/*If the Vehicle is Not Moving set the flag to reset Speed to Zero*/
		TimeOutCounter_u16++;
		
		if(TimeOutCounter_u16 >= 5) /*Wait for 500ms to reset the vehicle speed*/
		{
			TimeOutCounter_u16 = 0;
			ResetSpeedToZero_b = true;
		}
		
	}
	else
	{
		/*If the vehicle is moving reset the SpeedTimeout Counter*/
		TimeOutCounter_u16 = 0;
	}
	
	return ResetSpeedToZero_b;
}

/***********************************************************************************************************************
* Function Name: Get_Cal_VehicleSpeed
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint32_t Get_Cal_VehicleSpeed(void)
{
	//return Cal_VehicleSpeed_u32;
	return Calculated_Speed_u32;
}


/***********************************************************************************************************************
* Function Name: FilterVehicleSpeed
* Description  : 
* Arguments    : CalVehicleSpeed_u16
* Return Value : None
***********************************************************************************************************************/
void FilterVehicleSpeed(uint16_t CalVehicleSpeed_u16)
{
	SpeedToDisplay_u16 = Speed_LPF(CalVehicleSpeed_u16, &SpeedFilter_Out_u16, SPEED_LPF_POS_GAIN,
				SPEED_LPF_NEG_GAIN);
	return;
}


/***********************************************************************************************************************
* Function Name: Speed_LPF
* Description  : This function implements the Low-Pass Filter to smooth the speed socillations.
* Arguments    : uint16_t Input_u16, uint16_t *Output_pu16, int16_t PosConstant_s16, int16_t NegConstant_s16
* Return Value : uint16_t *Output_pu16
***********************************************************************************************************************/
uint16_t Speed_LPF(uint16_t Input_u16, uint16_t *Output_pu16, int16_t PosConstant_s16, int16_t NegConstant_s16)
{
	uint16_t DiffVar_u16 	= 0;
	uint16_t OffsetVal_u16  = 0;
	
	// If the current measeured value is not equal to the input
	if(Input_u16 != *Output_pu16)
	{
		// Compute the filter output by using the formula for the first order low pass filter
        	// y[i] = y[i-1] + alpha(x[i] - y[i-1])
		if(Input_u16 > *Output_pu16)
		{
			DiffVar_u16 = (uint16_t)(Input_u16 - *Output_pu16);
			
			if(PosConstant_s16 > 0)
			{
				OffsetVal_u16 = (uint16_t)(((uint32_t)DiffVar_u16 * (uint32_t)PosConstant_s16) >> 15);
									// [x]=[x]*[15]/[15]
			}
			else
			{
				;
			}
		}
		else
		{
			DiffVar_u16 = (uint16_t)(*Output_pu16 - Input_u16);
			
			if(NegConstant_s16 > 0)
			{
				OffsetVal_u16 = (uint16_t)(((uint32_t)DiffVar_u16 * (uint32_t)NegConstant_s16) >> 15);
									// [x]=[x]*[15]/[15]
			}
			else
			{
				;
			}
		}
		
		// If the step value is 0 then set it to 1
		if(OffsetVal_u16 == 0)
		{
			OffsetVal_u16 = 1;
		}
		else
		{
			;
		}
		
		if(Input_u16 > *Output_pu16)
		{
			// Input curve is moving up, hence step up the result.
			*Output_pu16 += OffsetVal_u16;
		}
		else
		{
			// Input curve is moving down, hence step down the result.
			*Output_pu16 -= OffsetVal_u16;
		}
	}
	else
	{
		;
	}
	
	return (*Output_pu16);
}



/***********************************************************************************************************************
* Function Name: Get_VehicleSpeed
* Description  : 
* Arguments    : None
* Return Value : SpeedToDisplay_u16
***********************************************************************************************************************/
uint16_t Get_VehicleSpeed(void)
{
	return SpeedToDisplay_u16;
}


/***********************************************************************************************************************
* Function Name: ResetTheSpeedData
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheSpeedData(void)
{
	PresentODOmm_u64  				= RESET;
	PreviousODOmm_u64 				= RESET;
	DifferenceODOmm_u32  			= RESET;
	
	SpeedKmph 						= RESET;
	Cal_VehicleSpeed_u32 			= RESET;
	SpeedDecimal_u16 				= RESET;
	
	
	SpeedTimeOut_St.PresentSpeedPulseCount_u32  = RESET;
	SpeedTimeOut_St.PreviousSpeedPulseCount_u32 = RESET;
	
	TimeOutCounter_u16 				= RESET;
	Counter5ms_u16 					= RESET;
	
	DifferenceTimems_u32 			= RESET;
	PrevTime_ms_u32 				= RESET;
	CalculateSpeed_b 				= CLEAR;
	
	/*Reset the Speed Filter Data*/
	SpeedFilter_Out_u16 			= RESET;
	SpeedToDisplay_u16 				= RESET;
	
	Calculated_Speed_u32           	= RESET;
	SpeedValidationCounter_u16   	= RESET;
	
	return;
}

/********************************************************EOF***********************************************************/