/***********************************************************************************************************************
* File Name    : Speed_Calc.h
* Version      : 01
* Description  : This file contains the macro definition related to main
* Created By   : Dileepa B S
* Creation Date: 06/10/2021
***********************************************************************************************************************/

#ifndef SPEED_CALC_H
#define SPEED_CALC_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	
#include "App_typedefs.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
extern volatile uint32_t TimeTick_1ms_u32;

#define UPDATE_TIME_MS()				(TimeTick_1ms_u32++)
#define GET_PRESENT_TIME()				(TimeTick_1ms_u32)

#define SPEED_CAL_SCHEDULE_TIME_MS				5		



#define SPEED_VALIDATION_START_RANGE                    10 /*Kmph*/
#define SPEED_DIIF_RANGE_TO_VALIDATE                    1  /*Kmph*/
#define SPEED_DEBOUNCE_COUNT                            5


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	SPEED_CAL_NONE_E,
	SPEED_CAN_E,
	SPEED_CAL_PULSE_E,
	SPEED_CAL_RPM_E,
}SpeedCal_En_t;

typedef struct
{
	uint32_t PresentSpeedPulseCount_u32;
	uint32_t PreviousSpeedPulseCount_u32;
}SpeedTimeOut_St_t;



/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern SpeedTimeOut_St_t	SpeedTimeOut_St;
extern SpeedCal_En_t		SpeedCal_En;
extern uint32_t VehicleSpeed_u32;
extern uint64_t PresentODOmm_u64;
extern uint64_t PreviousODOmm_u64;

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Calculate_VehicleSpeed(void);
void Validate_Cal_Speed(uint32_t);
bool CheckSpeedTimeOut(void);
extern uint32_t Get_Cal_VehicleSpeed(void);
extern void FilterVehicleSpeed(uint16_t);
extern uint16_t Get_VehicleSpeed(void);
uint16_t Speed_LPF(uint16_t, uint16_t *, int16_t, int16_t);
extern void ResetTheSpeedData(void);

#endif /* SPEED_CALC_H */


