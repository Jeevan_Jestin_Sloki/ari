/***********************************************************************************************************************
* File Name    : DataBank.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/9/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "DataBank.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

ClusterSignals_St_t	ClusterSignals_St = 
{
	0,
	0,
	0,
	0,
	0,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00,
	0x00
};


/***********************************************************************************************************************
* Function Name: ResetDataBankSignals
* Description  : This function resets the signals of the data-bank.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
#if(RESET_DATA_BANK == TRUE)
	void ResetDataBankSignals(void)
	{
		ClusterSignals_St.ODOmeterSig_u32 			= 0;
		ClusterSignals_St.PowerConsumBarsSig_u16 	= 0;
		ClusterSignals_St.RangeKmSig_u16 			= 0;
		ClusterSignals_St.HoursTimeSig_u16 			= 0;
		ClusterSignals_St.MinutesTimeSig_u16 		= 0;
		ClusterSignals_St.VehicleSpeedSig_u8 		= 0;
		ClusterSignals_St.SOCSig_u8 				= 0;
		ClusterSignals_St.MileageSig_u8 			= 0;
		ClusterSignals_St.SideStandSig_u8 			= 0;
		ClusterSignals_St.SafeModeSig_u8 			= 0;
		ClusterSignals_St.BatteryTextSig_u8 		= 0;
		ClusterSignals_St.MotorTextSig_u8 			= 0;
		ClusterSignals_St.KillSwitchSig_u8 			= 0;
		ClusterSignals_St.LeftIndicatorSig_u8 		= 0;
		ClusterSignals_St.RightIndicatorSig_u8 		= 0;
		ClusterSignals_St.LowBeamSig_u8 			= 0;
		ClusterSignals_St.HighBeamSig_u8 			= 0;
		ClusterSignals_St.ServiceReminderSig_u8 	= 0;
		ClusterSignals_St.WarningSig_u8 			= 0;
		ClusterSignals_St.RegenSig_u8 				= 0;
		ClusterSignals_St.BLE_Icon_Sig_u8 			= 0;
		ClusterSignals_St.EcoModeSig_u8 			= 0;
		ClusterSignals_St.SportsModeSig_u8 			= 0;
		ClusterSignals_St.ReverseModeSig_u8 		= 0;
		ClusterSignals_St.NeutralModeSig_u8 		= 0;
		ClusterSignals_St.ChargingStatus_u8 		= 0;
		ClusterSignals_St.IgnitionSig_u8 			= 0;
		return;
	}
#endif


/********************************************************EOF***********************************************************/