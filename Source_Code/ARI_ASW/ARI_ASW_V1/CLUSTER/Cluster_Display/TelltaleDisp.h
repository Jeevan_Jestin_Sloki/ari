/***********************************************************************************************************************
* File Name    : TelltaleDisp.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 30/01/2021
***********************************************************************************************************************/

#ifndef TELLTALE_DISP_H
#define TELLTALE_DISP_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "GenConfig.h"
#include "App_typedefs.h" 
#include "Cluster_Conf.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define	LEFT_IND_INDEX				0
#define RIGHT_IND_INDEX				1
#define LOW_BEAM_INDEX				2
#define HIGH_BEAM_INDEX				3
#define BLE_INDEX				4
#define REGEN_BRAKE_INDEX			5
#define WARNING_INDEX				6
#define SERV_REM_INDEX				7
#define NEUTRAL_MODE_INDEX			8
#define ECO_MODE_INDEX				9
#define SPORTS_MODE_INDEX			10
#define REVERSE_MODE_INDEX			11
#define SIDE_STAND_INDEX			12
#define SAFE_MODE_INDEX				13
#define KILL_SWITCH_INDEX			14
#define BATT_FLT_INDEX				15
#define MOTOR_FLT_INDEX				16
#define TOTAL_TELLTALE			(MOTOR_FLT_INDEX+1)

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
	typedef enum
	{
		LEFT_INDICATOR_E 	= LEFT_IND_E,
		RIGHT_INDICATOR_E 	= RIGHT_IND_E,
		LOW_BEAM_IND_E		= LOW_BEAM_E,
		HIGH_BEAM_IND_E 	= HIGH_BEAM_E,
		BLUETOOTH_E 		= BLE_E,
		REGEN_BRAKE_E 		= REGEN_BRAKING_E,
		WARNING_INDICATOR_E 	= WARNING_IND_E,
		SERVICE_IND_E 		= SERV_REM_E,
		NEUTRAL_MODE_IND_E 	= NEUTRAL_MODE_E,
		ECO_MODE_IND_E 		= ECO_MODE_E,
		SPORTS_MODE_IND_E 	= SPORTS_MODE_E,
		REVERSE_MODE_IND_E 	= REVERSE_MODE_E,
		SIDE_STAND_IND_E 	= SIDE_STAND_E,
		SAFE_MODE_IND_E 	= SAFE_MODE_E,
		KILL_SWITCH_IND_E 	= KILL_SWITCH_E,
		BATT_FLT_IND_E 		= BATT_FAULT_E,
		MOTOR_FLT_IND_E 	= MOTOR_FAULT_E
	}Telltales_En_t;

	typedef struct
	{
		Telltales_En_t	 Telltales_En;          	/*Signal Enum*/
		uint8_t 	 Telltale_Value_u8;		/*Signal Present Value*/
		uint8_t 	 Telltale_PrevValue_u8;		/*Signal Previous Value*/
		bool		 TelltaleBlink_b;               /*Signal blinking state*/
	}Telltales_Conf_St_t;

/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Indicate_Telltales(void); 
void Turn_ON_Telltales(void);          
void Turn_OFF_Telltales(void);
void Blink_Telltales(void);
extern void ClrTelltalesStates(void);

#endif /* TELLTALE_DISP_H */


