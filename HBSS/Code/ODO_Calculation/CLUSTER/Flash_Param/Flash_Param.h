/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : DataAcquire.h
|    Project        : IDAN Motor VCU
|    Description    : This file contains the export variables and functions
|                     to initialize and Operate the bms functanality.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 20/04/2021     Name           Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/
#ifndef FLASH_PARAM
#define FLASH_PARAM
/*******************************************************************************
 *  HEARDER FILE INCLUDES
 ******************************************************************************/
#include "App_typedefs.h"

/*******************************************************************************
 *  MACRO DEFNITION
 ******************************************************************************/

/*******************************************************************************
 *  STRUCTURES, ENUMS and TYPEDEFS
 ******************************************************************************/
#pragma pack
typedef struct 
{
    uint32_t ODO_Meter_u32;
    uint32_t TripA_Meter_u32;
    uint32_t TripB_Meter_u32;
    uint16_t WHKh_u16;
    uint16_t RangeKm_u16;
}Flash_Param_St_t;
#pragma unpack

typedef enum
{
    FDL_BLOCK_IDLE_E,
    ERASE_FDL_E,
    COPYING_FDL_E,
    ACTIVE_FDL_E,
    FULL_FDL_E,
}FdlBlockState_En_t;

typedef enum
{
    FDL_SECTOR_0_E,
    FDL_SECTOR_1_E,
    TOTAL_FDL_SECTOR_E,
}FdlSector_En_t;

#pragma pack
typedef struct 
{
    uint32_t ErasePattern_u32;
    uint32_t CopyingPattern_u32;
    uint32_t ActivePattern_u32;
    uint32_t FullPattern_u32;
}FdlStatePattern_St_t;
#pragma unpack
/*******************************************************************************
 *  EXTERN GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *  EXTERN FUNCTION
 ******************************************************************************/
void Read_Flash_Param(void);
void Write_Flash_Param(void);
void DownLoadHmiDataFromFlash(uint8_t *DataBuff_pu8);
bool UploadHmiDataToFlash(uint16_t DataSize_u16, uint8_t* DataBuff_pu8);
void StoreHmiData(void);
#endif
/*---------------------- End of File -----------------------------------------*/