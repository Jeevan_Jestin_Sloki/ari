/***********************************************************************************************************************
* File Name    : task_scheduler.c
* Version      : 01
* Description  : This file implements the Task_Scheduler
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "task_scheduler.h"
#include "timer_user.h"
#include "r_cg_macrodriver.h"
#include "r_cg_wdt.h"
#include "r_cg_userdefine.h"
#include "r_cg_port.h"
#include "r_cg_timer.h"
#include "com_tasksched.h"
#include "PFlash.h"
#include "main.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool   TS_ExitSched_b = false;

/***********************************************************************************************************************
* Function Name: Taskscheduler_start
* Description  : This function schedules the tasks for every 5-ms
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Taskscheduler_start(void)
{
	static uint32_t Counter_100us_u32 = 0;
	
 	while(!TS_ExitSched_b)
	{
		while(_100us_TS_Flag_b)
		{
			_100us_TS_Flag_b = false;
			R_WDT_Restart();   /*Refresh the WDT*/
			Diag_TS_Proc_5ms();
			if(reset_b == true)
			{
				Counter_100us_u32++;
				if(Counter_100us_u32 == 100)
				{
					TS_StopScheduler(); 
				}
			}
		}
	}
	return;
}
/*
*  @brief         : The function stops the task scheduler operation
*                   by Setting the Exit flag to true.
*  @param         : None
*  @return        : None
*/
void TS_StopScheduler (void)
{
    TS_ExitSched_b = true;
}

void TS_PowerOff(void)
{
	int8_t __far* a;            // Create a far-Pointer
  	IAWCTL|=0x80;               // switch IAWEN on (defalut off)
  	a=(int8_t __far*) 0x00000;  // Point to 0x000000 (Flash-ROM area)
  	*a=0; 
}


/********************************************************EOF***********************************************************/