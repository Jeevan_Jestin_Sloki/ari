/*
 * main.h
 *
 *  Created on: 03-Feb-2020
 *      Author: dell
 */

#ifndef MAIN_H_
#define MAIN_H_
//#include "Flash1.h"
#include "diag_typedefs.h"


#define APP_PATTERN					0x55AA55AA
#define BOOT_PATTERN				0xAA55AA55




/* Address of  the Flash from where the application starts executing */
/* Rule: Set BOOTLOADER_FLASH_BASE_ADDRESS to _RESET_ADDR value of application linker script*/
#define APP_START_ADDRESS			0x00009000 /*This is application  memory address*/
#define APP_SIZE					0x0000B000
#define APP_END_ADDRESS     		(APP_START_ADDRESS + (APP_SIZE - 1))
#define BOOT_START_ADDRESS			0x00000000 /*This application boot memory*/
#define CRC_RESET_VALUE             0xFFFFFFFF
#define DRIVE_VALIDATION_START_ADDRESS    (APP_START_ADDRESS - 0x1000)
//#define DRIVE_VALIDATION_START_ADDRESS    (APP_START_ADDRESS - 0x1000)
//#define DRIVE_VALIDATION_END_ADDRESS      (DRIVE_CRC_END_ADDRESS)
#define DRIVE_VALIDATION_END_ADDRESS      (APP_START_ADDRESS - 0x01)

//#define DRIVE_VALID_NUMBER          0xAA55AA55
#define DRIVE_INVALID_NUMBER        0xFEFEFEFE
//uint32_t Pattern_Address_u32 =  0x0000D000;
//extern flash_ssd_config_t flashSSDConfig;
//extern status_t ret;
extern volatile uint32_t TS_time_ms_u32_1;
extern bool boot_flag_b;
extern bool reset_b;
extern bool Flag;
void JumpToUserApplication( unsigned int App_Start_Address);
#endif /* MAIN_H_ */
