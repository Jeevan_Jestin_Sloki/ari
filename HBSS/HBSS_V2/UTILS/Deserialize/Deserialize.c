/***********************************************************************************************************************
* File Name    : Deserialize.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 3/1/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Deserialize.h"
#include "Time.h"
#include "DataAquire.h"
#include "hmi_config_can.h"
#include "mcu_can.h"
#include "bms_can.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
uint16_t Previous_HourTime 	= 0;
uint16_t Previous_MinuteTime 	= 0;
uint8_t  Previous_Meridian	= 0;
uint8_t  Previous_TripState	= 0;

/***********************************************************************************************************************
* Function Name: CANSched_HMI_RxMsgCallback
* Description  : This function works as a call back function to the upper layer segemented reception and Calls the
                 deserialize functions to update the HMI Signals.
* Arguments    : uint16_t CIL_SigName_En,CAN_MessageFrame1_St_t* Can_Applidata_St
* Return Value : None
***********************************************************************************************************************/
void CANSched_HMI_RxMsgCallback(uint16_t CIL_SigName_En,CAN_MessageFrame_St_t* Can_Applidata_St)
{
	switch(CIL_SigName_En)
	{
		case CIL_HMI_MCU_1_RX_E:
		{
			Deserialize_MCU_MSG_1_St_t(&MCU_MSG_1_St_t,Can_Applidata_St->DataBytes_au8);
			break;
		}
		case CIL_HMI_MCU_2_RX_E:
		{
			Deserialize_MCU_MSG_2_St_t(&MCU_MSG_2_St_t,Can_Applidata_St->DataBytes_au8);
			break;
		}
		case CIL_HMI_BMS_1_RX_E:
		{
			Deserialize_BatteryGeneralInfo(&BatteryGeneralInfo_0xBB_St,&Can_Applidata_St->DataBytes_au8[0]);
			if(false == BattGeneralInfoRec_b)
			{
				BattGeneralInfoRec_b = true;
			}
			break;
		}
		case CIL_HMI_BMS_2_RX_E:
		{
			Deserialize_CapacityInfo(&CapacityInfo_0xA3_St,&Can_Applidata_St->DataBytes_au8[0]);
			if(false == CapacityInfoRec_b)
			{
				CapacityInfoRec_b = true;
			}
			
			break;
		}
		case CIL_HMI_CONF_RX_E:
		{
			SetHMIConfigParameter(Can_Applidata_St->DataBytes_au8);
			break;
		}
		default:
		{

		}
	}
  	return;
}


/********************************************************EOF***********************************************************/