/***********************************************************************************************************************
* File Name    : RangeKmDisp.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 16/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "RangeKmDisp.h"
#include "SegDispWrite.h"
#include "GenConfig.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool 	     InitialRangeKm_Disp_b = false;

/***********************************************************************************************************************
* Function Name: Display_RangeKm
* Description  : This function validate and displays the Range in Km 000 - 999
* Arguments    : uint16_t RangeKm_u16, ClusterSignals_En_t RANGE_KM_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void Display_RangeKm(uint16_t RangeKm_u16, ClusterSignals_En_t RANGE_KM_ENUM_E)
{
	uint16_t		ValidRangeKm_u16 = 0;
	static uint16_t 	PrevRangeKm_u16 = 0;
	
	RangeKmDispState_En_t  RangeKmDispState_En = RANGE_KM_DISP_HOLD_E;
	
	if(PrevRangeKm_u16 == RangeKm_u16)
	{
		RangeKmDispState_En = RANGE_KM_DISP_HOLD_E;
		if(false == InitialRangeKm_Disp_b)
		{
			RangeKmDispState_En = RANGE_KM_DISPLAY_E;
			InitialRangeKm_Disp_b = true;
		}
	}
	else
	{
		RangeKmDispState_En = RANGE_KM_DISPLAY_E;
		PrevRangeKm_u16 = RangeKm_u16;
	}
	switch(RangeKmDispState_En)
	{
		case RANGE_KM_DISPLAY_E:
		{
			ValidRangeKm_u16 = ValidateRangeKm(RangeKm_u16);
			Write_SEG(RANGE_KM_ENUM_E, (uint32_t)ValidRangeKm_u16,DECIMAL); 
			break;
		}
		case RANGE_KM_DISP_HOLD_E:
		{
			break;
		}
		default:
		{
			;	
		}
	}
	return;
}


/***********************************************************************************************************************
* Function Name: ValidateRangeKm
* Description  : This function validates the Rangr Km value received.
* Arguments    : uint16_t  RangeKmCheck_u16
* Return Value : uint16_t  RangeKmCheck_u16
***********************************************************************************************************************/
uint16_t ValidateRangeKm(uint16_t RangeKmCheck_u16)
{
	if(RangeKmCheck_u16 > MAX_RANGE_KM_RANGE)
	{
		RangeKmCheck_u16 = MAX_RANGE_KM_RANGE;
	}
	else
	{
		;
	}
	return RangeKmCheck_u16;
}


/********************************************************EOF***********************************************************/