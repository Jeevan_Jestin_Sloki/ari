/***********************************************************************************************************************
* File Name    : r_cg_timer_user.c
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for TAU module.
* Creation Date: 06/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/ 
#include "r_cg_timer.h"
#include "timer_user.h"
#include "task_scheduler.h"
#include "App_typedefs.h"
#include "com_tasksched.h"
#include "ClusterAnimation.h"
#include "can_driver.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/*
	NOTE : Interrupt Vector Table Moved to the RAM 
*/
//#pragma interrupt r_tau0_channel2_interrupt(vect=INTTM02)
//#pragma interrupt r_tau2_channel0_interrupt(vect=INTTM20)
//#pragma interrupt r_tau2_channel4_interrupt(vect=INTTM24)

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
volatile uint32_t	 Time_Tick_u32 		= 0;
volatile uint16_t   	_5ms_Counter_u16 	= 0;
bool 			_5ms_TS_Flag_b 		= false;
bool			_1Second_Flag_b 	= false;

volatile uint32_t PulseCountMultiplier_u32 = 0;
volatile uint16_t PwmPulseInt_u16 = 0;
/***********************************************************************************************************************
* Function Name: r_tau0_channel2_interrupt 
* Description  : This function is INTTM02 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_tau0_channel2_interrupt(void)
{
	Time_Tick_u32++; 				/* Time Tick will be incremented by 1 for every 5ms */

	_5ms_TS_Flag_b = true;
	
	INC_TIME_MS();
	
	if(0 == (Time_Tick_u32 % _1SEC_DIVISOR_200)) 
	{
		_1Second_Flag_b = true;    /*This 1-second flag is used for updation of time*/
	}
	else
	{
		;
	}
	
	/*TODO  CLUSTER ANIMATION FOR 2.5 SECOND*/
	if(Cluster_State_En == DISPLAY_ANIMATION_E)
	{
		_5ms_Counter_u16++;
		if(0 == (_5ms_Counter_u16 % _2500MSEC_ANIMATION_DIVISOR))
		{
			_5ms_Counter_u16 = RESET; /*Reset this counter after 1 second*/
			
			AnimationDone_b = true;
			
			/*change the cluster display state to DISPLAY_DATA*/
			Cluster_State_En = DISPLAY_DATA_E;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
	
	return;
}

#if(HMI_CAN_ONLY == TRUE)

/***********************************************************************************************************************
* Function Name: r_tau0_channel0_interrupt
* Description  : This function is INTTM00 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_tau0_channel0_interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_tau0_channel1_interrupt
* Description  : This function is INTTM01 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_tau0_channel1_interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    /* End user code. Do not edit comment generated here */
}

#endif


/***********************************************************************************************************************
* Function Name: r_tau2_channel0_interrupt
* Description  : This function is INTTM20 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_tau2_channel0_interrupt(void)
{
    return;
}


/***********************************************************************************************************************
* Function Name: r_tau2_channel4_interrupt
* Description  : This function is INTTM24 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_tau2_channel4_interrupt(void)
{
    return;
}

#if(HMI_CAN_ONLY == TRUE)
/***********************************************************************************************************************
* Function Name: Set_PWM1_Duty
* Description  : This function controls the PWM duty cycle of the BackLight-1 PWM.
* Arguments    : uint16_t DutyCycle_u16
* Return Value : None
***********************************************************************************************************************/
void Set_PWM1_Duty(uint16_t DutyCycle_u16)
{
	uint16_t EquRegValue_u16 = 0;
	
	if(DutyCycle_u16 > MAX_DUTY_CYCLE)
	{
		DutyCycle_u16 = MAX_DUTY_CYCLE;
	}
	
	EquRegValue_u16 = (DutyCycle_u16 * REG_VALUE_PER_1PER_DUTY);
	
	TDR01 = EquRegValue_u16;
	
	return;
}
#endif

/***********************************************************************************************************************
* Function Name: r_tau0_channel1_interrupt
* Description  : This function is INTTM01 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_tau0_channel1_interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    PulseCountMultiplier_u32++;
    PwmPulseInt_u16++;
    /* End user code. Do not edit comment generated here */
}
/***********************************************************************************************************************
* Function Name: Set_PWM2_Duty
* Description  : This function controls the PWM duty cycle of the BackLight-2 PWM.
* Arguments    : uint16_t DutyCycle_u16
* Return Value : None
***********************************************************************************************************************/
void Set_PWM2_Duty(uint16_t DutyCycle_u16)
{
	uint16_t EquRegValue_u16 = 0;
	
	if(DutyCycle_u16 > MAX_DUTY_CYCLE)
	{
		DutyCycle_u16 = MAX_DUTY_CYCLE;
	}
	
	EquRegValue_u16 = (DutyCycle_u16 * REG_VALUE_PER_1PER_DUTY);
	
	TDR24 = EquRegValue_u16;
	
	return;
}

/***********************************************************************************************************************
* Function Name: ReadPulseCount
* Description  : The Function provides number of pulse detected
* Arguments    : uint16_t DutyCycle_u16
* Return Value : None
***********************************************************************************************************************/
uint32_t ReadPulseCount(void)
{
	uint32_t PulseCount_u32 = 0;
	uint16_t ExtPulseCount_u16 = 0;
	
	ExtPulseCount_u16 = TCR01;
 	PulseCount_u32 = MAX_PULSE_COUNT - ExtPulseCount_u16 + ( ( MAX_PULSE_COUNT * PulseCountMultiplier_u32 ) + PwmPulseInt_u16 );

	return PulseCount_u32;
}
/***********************************************************************************************************************
* Function Name: SetPulseCount
* Description  : The Function initialize the pulse count.
* Arguments    : uint16_t DutyCycle_u16
* Return Value : None
***********************************************************************************************************************/
void SetPulseCount(void)
{
	TDR01 = MAX_PULSE_COUNT; /* Set initial count value*/
}

/***********************************************************************************************************************
* Function Name: ResetPulseCountData
* Description  : The Function reset the pulse count data
* Arguments    : uint16_t DutyCycle_u16
* Return Value : None
***********************************************************************************************************************/
void ResetPulseCountData(void)
{
	PulseCountMultiplier_u32 = 0;
	PwmPulseInt_u16 = 0;
}
/********************************************************EOF***********************************************************/
