/******************************************************************************
 *    FILENAME    : cil_can_conf.c
 *    DESCRIPTION : File contains the common declarations related to CIL layers.
 ******************************************************************************
 * Revision history
 *  
 * Ver   Author       Date               Description
 * 1     Sushil      27/10/2018		     Initial version
 ******************************************************************************
*/ 

/* Section: Included Files                                       */
//#include "can_if.h"
#include "cil_can_conf.h"
#include "diag_appl_test.h"
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

/* ************************************************************************** */

//#define NULL 0

/*
 * @summary:- can structure message buffer  
 */
CanSchedMsg_St_t CanSB_St [CIL_DCAN_TOTAL_RX_E] = {0};



const CIL_CAN_Conf_St_t CIL_CAN_Conf_aSt[CIL_DCAN_END_E] =
{
#if(TRUE == DIAG_UDS_SUPPORTED && TRUE == DIAG_CANTP_SUPPORTED)
    // { {0x6F0,  0x6F0,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_IVN_RX_E      , CanSched_RxCallBackInt    , &CanSB_St[0].msg  }  ,//@
//    { {0x7E0,  0x7FF, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_TESTER_RX_E   , CanSched_RxCallBackInt    , &CanSB_St[1].msg  }  ,//@

	{ {0x7F0,  0x7F0,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_TESTER_RX_E   , CanSched_RxCallBackInt    , &CanSB_St[CIL_CANTP_REQ_TESTER_RX_E].msg  }  ,//@
    // { {0x7DF,  0x7DF,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_FUNC_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[2].msg  }  ,//@
#endif
// #if(TRUE == DIAG_OBD2_SUPPORTED && TRUE == DIAG_CANTP_SUPPORTED)

// 	{ {0x18DB33F1, 0x18DB33F1,   0x10,    EXT_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_OBD_TESTER_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[3].msg  }  ,//@
// #endif
// //    { {0x7EF, 0x7EF,   0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_EEPROM_FAULT_FUNC_RX_E     , CanSched_RxCallBackInt    , &CanTest_St[0].msg  }  ,//@
// //    { {0x7DF, 0x7DF,   0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_FUNC_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[2].msg  }  ,//@
// #if(TRUE == DIAG_J1939_SUPPORTED)
//     { { 0x18EC0010   , 0x18EC0010, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_TPCM_RX_E       , CanSched_RxCallBackInt    , &CanSB_St[4].msg         }  ,/*@ */
//     { { 0x18ECFF10   , 0x18ECFF10,		0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_TPCM_BAM_RX_E   , CanSched_RxCallBackInt    , &CanSB_St[5].msg         }  ,/*@ */
//     { { 0x18EB0010   ,	0x18EB0010,  0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_TPDT_RX_E       , CanSched_RxCallBackInt    , &CanSB_St[6].msg         }  ,/*@ */
//     { { 0x18EBFF10   ,	0x18EBFF10, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_TPDT_BAM_RX_E   , CanSched_RxCallBackInt    , &CanSB_St[7].msg         }  ,/*@ */
//     { { 0x18C30010   ,	0x18C30010,  0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_DM22_RX_E       , CanSched_RxCallBackInt    , &CanSB_St[8].msg         }  ,/*@ */
//     { { 0x18EEFF00   ,	0x18EEFF00, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_81_NMAC_RX_E    , CanSched_RxCallBackInt    , &CanSB_St[9].msg         }  ,/*@ */
//     { { 0x18EA0000   ,	0x18EA0000, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_REQ_DA_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[10].msg         }  ,/*@ */
//     { { 0x18EAFF00   ,	0x18EAFF00, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_RX }  , CIL_J1939_REQ_DA_BAM_RX_E     , CanSched_RxCallBackInt    , &CanSB_St[11].msg         }  ,/*@ */
// #endif

#if(TRUE == DIAG_UDS_SUPPORTED && TRUE == DIAG_CANTP_SUPPORTED)
//    { {0x150,  0U,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_CANTP_RESP_TESTER_TX_E  , NULL                  	, NULL  }  ,//@
	{ {0x7F1,  0U,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_CANTP_RESP_TESTER_TX_E  , NULL                  	, NULL  }  ,//@
    // { {0x6F1,  0U,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_CANTP_RESP_IVN_TX_E     , NULL                  	, NULL  }  ,//@
#endif
// #if(TRUE == DIAG_OBD2_SUPPORTED && TRUE == DIAG_CANTP_SUPPORTED)
// 	{ {0x18DAF110, 0U,   0x10,    EXT_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_CANTP_RESP_OBD_TESTER_TX_E     , NULL                  	, NULL  }  ,//@
// #endif
// //    { {0x6A3,  0U,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_TEST3_TX_E     	   , NULL                  	, NULL  }  ,//@
// #if(TRUE == DIAG_J1939_SUPPORTED)
//     { { 0x18EC0100   ,	0U, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_TPCM_TX_E       , NULL        , NULL        }  ,/*@ */
//     { { 0x18EB0100   , 0U, 0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_j1939_tpdt_TX_E       , NULL        , NULL        }  ,/*@ */
//     { { 0x18E8FF00   ,	0U,	0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_ACK_TX_E        , NULL        , NULL        }  ,/*@ */
//     { { 0x18EAFF00   ,	0U,	0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_REQ_TX_E        , NULL        , NULL        }  ,/*@ */
//     { { 0x18AA0000   ,	0U,	0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_71_TEST1_TX_E        , NULL        , NULL        }  ,/*@ */
//     { { 0x18AB0000   ,	0U,	0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_71_TEST2_TX_E        , NULL        , NULL        }  ,/*@ */
//     { { 0x18AC0000   ,	0U,	0x10,    EXT_CAN_E ,    DATA_FRAME_E   ,FALSE,    FALSE, CAN_TX }  , CIL_J1939_71_TEST3_TX_E        , NULL        , NULL        }  ,/*@ */
// #endif
    { {0x6A3, 	0U,    0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_TEST3_TX_E     	   , NULL                  	, NULL  }  ,//@

};



/* *****************************************************************************
 End of File
 */

