/***********************************************************************************************************************
* File Name    : GenConfig.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 29/12/2020
***********************************************************************************************************************/

#ifndef GEN_CONFIG_H
#define GEN_CONFIG_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h" 

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
/*
	HMI Cluster Signals Length Configuration.
*/
#define		MIN_SOC_RANGE				0
#define 	MAX_SOC_RANGE				100
#define		EACH_BAR_SOC_RANGE			20

#define		MIN_POWER_CONSUMP_RANGE			0
#define 	MAX_POWER_CONSUMP_RANGE			5000
#define		EACH_BAR_POWER_CONSUMP_RANGE		1000
#define 	MAX_PWR_CONSUM_BARS			5

#define         MAX_MINUTES_TIME			59
#define		MAX_HOURS_TIME				12

#define 	MIN_RANGE_KM_RANGE			0
#define		MAX_RANGE_KM_RANGE			999

#define 	SOLID_TELLTALE				3U
#define 	BLINK_TELLTALE				1U
#define 	TELLTALE_OFF				0U

#define         COMMON_SEG_SIG_LEN			1
#define		SPEED_SIG_LEN				3
#define		RANGE_SIG_LEN				3
#define 	LEFT_IND_SIG_LEN			1
#define 	RIGHT_IND_SIG_LEN			1
#define 	HIGH_BEAM_SIG_LEN			1
#define		BLE_SIG_LEN				1
#define 	WARN_IND_SIG_LEN			1
#define		SERV_REM_SIG_LEN			1
#define		NEUTRAL_TEXT_SIG_LEN			1
#define		ECO_TEXT_SIG_LEN			1
#define     ECO_MODE_SIG_LEN            1
#define		SPORTS_TEXT_SIG_LEN			1
#define     SPORTS_MODE_SIG_LEN         1
#define		REVERSE_TEXT_SIG_LEN		1
#define		SIDE_STAND_SIG_LEN			1
#define		KILL_SWITCH_SIG_LEN			1
#define 	BATT_SOC_BAR_SIG_LEN			5
#define 	BATT_SOC_DIGIT_SIG_LEN			3
#define		POWER_CONSUMP_SIG_LEN			5
#define		ODO_SIG_LEN				6
#define		MINUTES_TIME_SIG_LEN			2
#define 	MINUTE_HBLB_SIG_LEN			1
#define		HOURS_TIME_SIG_LEN			2
#define 	TEXT_AM_SIG_LEN				1
#define 	TEXT_PM_SIG_LEN				1
#define         TIME_COLON_SIG_LEN			1
#define		MOTOR_FLT_SIG_LEN			1
#define 	STRAIGHT_SIG_LEN			1
#define        	LEFT_TURN_SIG_LEN			1
#define		RIGHT_TURN_SIG_LEN			1
#define 	LEFT_U_TURN_SIG_LEN			1
#define 	RIGHT_U_TURN_SIG_LEN			1
#define 	NAVIG_DIST_SIG_LEN			3
#define		BATTERY_FLT_SIG_LEN			1
#define		PARKING_BREAK_LEN		         	1
#define 	ODO_TEXT_SIG_LEN			1
#define 	TRIP_TEXT_SIG_LEN			1
#define		TEXT_A_SIG_LEN				1
#define 	TEXT_B_SIG_LEN				1
#define 	POWER_IND_SIG_LEN			1
#define 	TOP_TEXT_SIG_LEN			1
#define 	CH_STATUS_SIG_LEN			1
#define     PERCEN_TEXT_SIG_LEN         1
#define     PARKING_TEXT_SIG_LEN        1
#define     PARKING_MODE_SIG_LEN        1
#define     REVERSE_MODE_SIG_LEN        1
#define     NEUTRAL_MODE_SIG_LEN        1
#define     DRIVING_TEXT_SIG_LEN        1
#define     DRIVING_MODE_SIG_LEN        1
#define     GEAR_TEXT_SIG_LEN           1
#define     GEAR_DIGIT_SIG_LEN          1



#define 	MAX_LENGTH_SIGNAL		     ODO_SIG_LEN 



#define         _23_UNUSED_SEGMENT		        23
#define         _24_UNUSED_SEGMENT		        24
#define         _44_UNUSED_SEGMENT		        44

#define         CLEAR_SEG_REGISTER			0x00U
#define         SET_SEG_REGISTER			0x0FU


///*Telltale signals value configuration*/
#define TURN_ON						0x03U
#define TURN_OFF					0x00U
#define BLINK						0x01U
#define HOLD 						0x02U

#define TOTAL_SEG_REGISTERS				48


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/


#endif /* GEN_CONFIG_H */


