#include "r_cg_macrodriver.h"
#include "Cluster_Conf.h"

#define TOTALSIGTEST 	45U

extern bool StartSpeedTest_b;
extern bool StartODOmeterTest_b;

typedef void (*Func_pointer)(ClusterSignals_En_t);


void TestMain(void);
void batt_bar_test(ClusterSignals_En_t SIGNAL_E);
void batt_Per_Test(ClusterSignals_En_t SIGNAL_E);

void Telltale_Test(ClusterSignals_En_t SIGNAL_E);

void Navig_Dir_Test(ClusterSignals_En_t SIGNAL_E);
void Navig_dist_Test(ClusterSignals_En_t SIGNAL_E);

void SpeedSignalTest(ClusterSignals_En_t SIGNAL_E);

void ODO_Sig_Test(ClusterSignals_En_t SIGNAL_E);

void Batt_Bar_Test(ClusterSignals_En_t SIGNAL_E);

void Range_Test(ClusterSignals_En_t SIGNAL_E);

void Time_Minute(ClusterSignals_En_t SIGNAL_E);
void Time_Hour(ClusterSignals_En_t SIGNAL_E);

void Gear_Test(ClusterSignals_En_t SIGNAL_E);






