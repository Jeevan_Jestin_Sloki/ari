/***********************************************************************************************************************
* File Name    : BackLightCtrl.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 08/01/2021
***********************************************************************************************************************/

#ifndef BACKLIGHT_CTRL_H
#define BACKLIGHT_CTRL_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/


/*
	BackLight-1 and BackLight-2 Duty Cycle Range Configuration.
*/
#define PWM_DUTY_ON_IGNITION_ON_1				10 /* todo jeevan*/ //Set Static Mode Duty Cycle to 1% [To set before Animation]
#define PWM_DUTY_ON_IGNITION_ON_2				50 //Set Static Mode Duty Cycle to 50% [After Animation]
#define PWM_DUTY_ON_IGNITION_OFF				0 


#define SPEED_LPF_POS_GAIN					3000 //todo:configurable
#define SPEED_LPF_NEG_GAIN					SPEED_LPF_POS_GAIN


//todo:configurable, Set Any One Mode to TRUE 
#define BACKLIGHT_CTRL_STATIC_MODE			TRUE	/* TRUE OR FALSE*/
#define BACKLIGHT_CTRL_ALS_MODE				FALSE
#define BACKLIGHT_CTRL_CAN_MODE				FALSE


/*****************************************`******************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef struct
{
	uint16_t	DutyCycle_u16;			/* Present Duty Cycle */
	uint16_t	PrevDutyCycle_u16;		/* Previous Duty Cycle Value (Presently in Registers)*/
}DutyCycle_Conf_St_t;


typedef enum
{
	BACKLIGHT_CTRL_STATIC_E,
	BACKLIGHT_CTRL_ALS_E,
	BACKLIGHT_CTRL_CAN_E, 
}BackLightCtrl_En_t;

typedef enum
{
	STATIC_MODE_CTRL_1_E,
	STATIC_MODE_CTRL_2_E,
}StaticControlMode_En_t;



/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern BackLightCtrl_En_t		BackLightCtrl_En;
extern StaticControlMode_En_t	StaticControlMode_En;


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Turn_ON_BackLight(void);
extern void Turn_OFF_BackLight(void);
void SetPWMDutyCycle(uint16_t);
uint16_t GetDutyCycleValue(uint32_t);
extern void AdjustLightIntensity(BackLightCtrl_En_t);
extern void ClrBrightnessCtrlData(void);
uint16_t DutyCycle_LPF(uint16_t, uint16_t *, int16_t, int16_t);
extern void SendALSDataOnCAN(void); 


#endif /* BACKLIGHT_CTRL_H */


