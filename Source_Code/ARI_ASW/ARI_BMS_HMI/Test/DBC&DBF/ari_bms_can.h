﻿#include <stdint.h>


#define BMS_INFO_MSG_2_BMS_CHARGESTATUS_MASK0  6U




#define BMS_INFO_MSG_4_SYSTEM_YEAR_MASK0  8U


#define BMS_INFO_MSG_5_PACK_TOTALCAP_MASK0  8U
#define BMS_INFO_MSG_5_PACK_LEFTCAP_MASK0  8U
#define BMS_INFO_MSG_5_CHARGE_TIME_MASK0  8U
#define BMS_INFO_MSG_5_SOH_MASK0  8U


#define BMS_INFO_MSG_1_VOL_BAT_MASK0  8U
#define BMS_INFO_MSG_1_CUR_BAT_MASK0  8U
#define BMS_INFO_MSG_1_SOC_MASK0  8U
#define BMS_INFO_MSG_1_DISPLAY_SOC_MASK0  8U


#define SIGNLE_READ_Mask0     0U    
#define SIGNLE_READ_Mask1     0x01U 
#define SIGNLE_READ_Mask2     0x03U 
#define SIGNLE_READ_Mask3     0x07U 
#define SIGNLE_READ_Mask4     0x0FU 
#define SIGNLE_READ_Mask5     0x1FU 
#define SIGNLE_READ_Mask6     0x3FU 
#define SIGNLE_READ_Mask7     0x7FU 
#define SIGNLE_READ_Mask8     0xFFU 


#define SIGNLE_WRITE_Mask0    0x80U 
#define SIGNLE_WRITE_Mask1    0xC0U 
#define SIGNLE_WRITE_Mask2    0xE0U 
#define SIGNLE_WRITE_Mask3    0xF0U 
#define SIGNLE_WRITE_Mask4    0xF8U 
#define SIGNLE_WRITE_Mask5    0xFCU 
#define SIGNLE_WRITE_Mask6    0xFEU 
#define SIGNLE_WRITE_Mask7    0xFFU 
/* def @BMS_INFO_MSG_2 CAN Message                                   (484) */
#define BMS_INFO_MSG_2_ID                                            (484U)
#define BMS_INFO_MSG_2_IDE                                           (0U)
#define BMS_INFO_MSG_2_DLC                                           (8U)


#define BMS_INFO_MSG_2_BMS_CHARGESTATUSFACTOR                                   (1)
#define BMS_INFO_MSG_2_CANID_BMS_CHARGESTATUS_STARTBIT                           (7)
#define BMS_INFO_MSG_2_CANID_BMS_CHARGESTATUS_OFFSET                             (0)
#define BMS_INFO_MSG_2_CANID_BMS_CHARGESTATUS_MIN                                (0)
#define BMS_INFO_MSG_2_CANID_BMS_CHARGESTATUS_MAX                                (3)


typedef struct
{
  uint8_t BMS_ChargeStatus;
}
BMS_INFO_MSG_2_t;


/* def @BMS_INFO_MSG_3 CAN Message                                   (501) */
#define BMS_INFO_MSG_3_ID                                            (501U)
#define BMS_INFO_MSG_3_IDE                                           (0U)
#define BMS_INFO_MSG_3_DLC                                           (8U)


#define BMS_INFO_MSG_3_ALARM_CODEFACTOR                                   (1)
#define BMS_INFO_MSG_3_CANID_ALARM_CODE_STARTBIT                           (7)
#define BMS_INFO_MSG_3_CANID_ALARM_CODE_OFFSET                             (0)
#define BMS_INFO_MSG_3_CANID_ALARM_CODE_MIN                                (0)
#define BMS_INFO_MSG_3_CANID_ALARM_CODE_MAX                                (255)
#define BMS_INFO_MSG_3_ALARM_LEVELFACTOR                                   (1)
#define BMS_INFO_MSG_3_CANID_ALARM_LEVEL_STARTBIT                           (15)
#define BMS_INFO_MSG_3_CANID_ALARM_LEVEL_OFFSET                             (0)
#define BMS_INFO_MSG_3_CANID_ALARM_LEVEL_MIN                                (0)
#define BMS_INFO_MSG_3_CANID_ALARM_LEVEL_MAX                                (255)


typedef struct
{
  uint8_t Alarm_Code;
  uint8_t Alarm_Level;
}
BMS_INFO_MSG_3_t;


/* def @BMS_INFO_MSG_4 CAN Message                                   (514) */
#define BMS_INFO_MSG_4_ID                                            (514U)
#define BMS_INFO_MSG_4_IDE                                           (0U)
#define BMS_INFO_MSG_4_DLC                                           (8U)


#define BMS_INFO_MSG_4_SYSTEM_YEARFACTOR                                   (1)
#define BMS_INFO_MSG_4_CANID_SYSTEM_YEAR_STARTBIT                           (7)
#define BMS_INFO_MSG_4_CANID_SYSTEM_YEAR_OFFSET                             (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_YEAR_MIN                                (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_YEAR_MAX                                (65535)
#define BMS_INFO_MSG_4_SYSTEM_MONTHFACTOR                                   (1)
#define BMS_INFO_MSG_4_CANID_SYSTEM_MONTH_STARTBIT                           (23)
#define BMS_INFO_MSG_4_CANID_SYSTEM_MONTH_OFFSET                             (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_MONTH_MIN                                (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_MONTH_MAX                                (255)
#define BMS_INFO_MSG_4_SYSTEM_DAYFACTOR                                   (1)
#define BMS_INFO_MSG_4_CANID_SYSTEM_DAY_STARTBIT                           (31)
#define BMS_INFO_MSG_4_CANID_SYSTEM_DAY_OFFSET                             (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_DAY_MIN                                (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_DAY_MAX                                (255)
#define BMS_INFO_MSG_4_SYSTEM_HOURFACTOR                                   (1)
#define BMS_INFO_MSG_4_CANID_SYSTEM_HOUR_STARTBIT                           (39)
#define BMS_INFO_MSG_4_CANID_SYSTEM_HOUR_OFFSET                             (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_HOUR_MIN                                (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_HOUR_MAX                                (255)
#define BMS_INFO_MSG_4_SYSTEM_MINUTEFACTOR                                   (1)
#define BMS_INFO_MSG_4_CANID_SYSTEM_MINUTE_STARTBIT                           (47)
#define BMS_INFO_MSG_4_CANID_SYSTEM_MINUTE_OFFSET                             (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_MINUTE_MIN                                (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_MINUTE_MAX                                (255)
#define BMS_INFO_MSG_4_SYSTEM_SECONDSFACTOR                                   (1)
#define BMS_INFO_MSG_4_CANID_SYSTEM_SECONDS_STARTBIT                           (55)
#define BMS_INFO_MSG_4_CANID_SYSTEM_SECONDS_OFFSET                             (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_SECONDS_MIN                                (0)
#define BMS_INFO_MSG_4_CANID_SYSTEM_SECONDS_MAX                                (0)


typedef struct
{
  uint16_t System_Year;
  uint8_t System_Month;
  uint8_t System_Day;
  uint8_t System_Hour;
  uint8_t System_Minute;
  uint8_t System_Seconds;
}
BMS_INFO_MSG_4_t;


/* def @BMS_INFO_MSG_5 CAN Message                                   (517) */
#define BMS_INFO_MSG_5_ID                                            (517U)
#define BMS_INFO_MSG_5_IDE                                           (0U)
#define BMS_INFO_MSG_5_DLC                                           (8U)


#define BMS_INFO_MSG_5_PACK_TOTALCAPFACTOR                                   (0.1)
#define BMS_INFO_MSG_5_CANID_PACK_TOTALCAP_STARTBIT                           (7)
#define BMS_INFO_MSG_5_CANID_PACK_TOTALCAP_OFFSET                             (0)
#define BMS_INFO_MSG_5_CANID_PACK_TOTALCAP_MIN                                (0)
#define BMS_INFO_MSG_5_CANID_PACK_TOTALCAP_MAX                                (6553.5)
#define BMS_INFO_MSG_5_PACK_LEFTCAPFACTOR                                   (0.1)
#define BMS_INFO_MSG_5_CANID_PACK_LEFTCAP_STARTBIT                           (23)
#define BMS_INFO_MSG_5_CANID_PACK_LEFTCAP_OFFSET                             (0)
#define BMS_INFO_MSG_5_CANID_PACK_LEFTCAP_MIN                                (0)
#define BMS_INFO_MSG_5_CANID_PACK_LEFTCAP_MAX                                (6553.5)
#define BMS_INFO_MSG_5_CHARGE_TIMEFACTOR                                   (1)
#define BMS_INFO_MSG_5_CANID_CHARGE_TIME_STARTBIT                           (39)
#define BMS_INFO_MSG_5_CANID_CHARGE_TIME_OFFSET                             (0)
#define BMS_INFO_MSG_5_CANID_CHARGE_TIME_MIN                                (0)
#define BMS_INFO_MSG_5_CANID_CHARGE_TIME_MAX                                (65535)
#define BMS_INFO_MSG_5_SOHFACTOR                                   (0.1)
#define BMS_INFO_MSG_5_CANID_SOH_STARTBIT                           (55)
#define BMS_INFO_MSG_5_CANID_SOH_OFFSET                             (0)
#define BMS_INFO_MSG_5_CANID_SOH_MIN                                (0)
#define BMS_INFO_MSG_5_CANID_SOH_MAX                                (6553.5)


typedef struct
{
  uint16_t Pack_TotalCap;
  uint16_t Pack_LeftCap;
  uint16_t Charge_Time;
  uint16_t SOH;
}
BMS_INFO_MSG_5_t;


/* def @BMS_INFO_MSG_1 CAN Message                                   (481) */
#define BMS_INFO_MSG_1_ID                                            (481U)
#define BMS_INFO_MSG_1_IDE                                           (0U)
#define BMS_INFO_MSG_1_DLC                                           (8U)


#define BMS_INFO_MSG_1_VOL_BATFACTOR                                   (0.1)
#define BMS_INFO_MSG_1_CANID_VOL_BAT_STARTBIT                           (7)
#define BMS_INFO_MSG_1_CANID_VOL_BAT_OFFSET                             (0)
#define BMS_INFO_MSG_1_CANID_VOL_BAT_MIN                                (0)
#define BMS_INFO_MSG_1_CANID_VOL_BAT_MAX                                (6553.5)
#define BMS_INFO_MSG_1_CUR_BATFACTOR                                   (0.1)
#define BMS_INFO_MSG_1_CANID_CUR_BAT_STARTBIT                           (23)
#define BMS_INFO_MSG_1_CANID_CUR_BAT_OFFSET                             (0)
#define BMS_INFO_MSG_1_CANID_CUR_BAT_MIN                                (0)
#define BMS_INFO_MSG_1_CANID_CUR_BAT_MAX                                (6553.5)
#define BMS_INFO_MSG_1_SOCFACTOR                                   (0.1)
#define BMS_INFO_MSG_1_CANID_SOC_STARTBIT                           (39)
#define BMS_INFO_MSG_1_CANID_SOC_OFFSET                             (0)
#define BMS_INFO_MSG_1_CANID_SOC_MIN                                (0)
#define BMS_INFO_MSG_1_CANID_SOC_MAX                                (6553.5)
#define BMS_INFO_MSG_1_DISPLAY_SOCFACTOR                                   (0.1)
#define BMS_INFO_MSG_1_CANID_DISPLAY_SOC_STARTBIT                           (55)
#define BMS_INFO_MSG_1_CANID_DISPLAY_SOC_OFFSET                             (0)
#define BMS_INFO_MSG_1_CANID_DISPLAY_SOC_MIN                                (0)
#define BMS_INFO_MSG_1_CANID_DISPLAY_SOC_MAX                                (6553.5)


typedef struct
{
  uint16_t Vol_Bat;
  uint16_t Cur_Bat;
  uint16_t SOC;
  uint16_t Display_SOC;
}
BMS_INFO_MSG_1_t;


 extern uint32_t Deserialize_BMS_INFO_MSG_2(BMS_INFO_MSG_2_t* message, const uint8_t* data);
 extern uint32_t Serialize_BMS_INFO_MSG_2(BMS_INFO_MSG_2_t* message, uint8_t* data);
 extern uint32_t Deserialize_BMS_INFO_MSG_3(BMS_INFO_MSG_3_t* message, const uint8_t* data);
 extern uint32_t Serialize_BMS_INFO_MSG_3(BMS_INFO_MSG_3_t* message, uint8_t* data);
 extern uint32_t Deserialize_BMS_INFO_MSG_4(BMS_INFO_MSG_4_t* message, const uint8_t* data);
 extern uint32_t Serialize_BMS_INFO_MSG_4(BMS_INFO_MSG_4_t* message, uint8_t* data);
 extern uint32_t Deserialize_BMS_INFO_MSG_5(BMS_INFO_MSG_5_t* message, const uint8_t* data);
 extern uint32_t Serialize_BMS_INFO_MSG_5(BMS_INFO_MSG_5_t* message, uint8_t* data);
 extern uint32_t Deserialize_BMS_INFO_MSG_1(BMS_INFO_MSG_1_t* message, const uint8_t* data);
 extern uint32_t Serialize_BMS_INFO_MSG_1(BMS_INFO_MSG_1_t* message, uint8_t* data);
