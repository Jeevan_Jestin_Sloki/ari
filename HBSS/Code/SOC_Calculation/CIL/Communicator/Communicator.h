/***********************************************************************************************************************
* File Name    : Communicator.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 05/01/2022
***********************************************************************************************************************/

#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	
#include "App_typedefs.h"


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern uint32_t TaskSchedulerCounter_u32;
extern uint32_t ODO_in_meter_u32;
extern uint16_t VehicleSpeedToDisplay_u16;
extern uint16_t PresentSessionWHkm_u16;
extern uint16_t PresentSessionRangeKm_u16;
extern uint8_t ODOtextState_u8;
extern uint8_t TripATextState_u8;
extern uint8_t TripBTextState_u8;
extern uint8_t TripTextState_u8;
extern uint8_t TimeColonState_u8; 
extern uint8_t MotorFaultState_u8;
extern uint8_t BatteryfaultState_u8;
extern uint8_t TopTextState_u8;
extern uint8_t ChargerPlugInState_u8;
extern bool    TimeSetting_b;
extern uint8_t Batterystate_u8;
extern uint8_t Battery_SOC_u8;
extern float Accurate_SOC_f;
/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define SOLID_ON                0x03
#define BLINK_TT                   0x01


#define GET_ODO_TEXT_STATE()            (ODOtextState_u8)
#define GET_TRIP_A_TEXT_STATE()         (TripATextState_u8)
#define GET_TRIP_B_TEXT_STATE()         (TripBTextState_u8)
#define GET_TRIP_TEXT_STATE()           (TripTextState_u8)
#define GET_TIME_COLON_STATE()          (TimeColonState_u8)
#define GET_MOTOR_FAULT_STATE()         (MotorFaultState_u8)
#define GET_BATT_FAULT_STATE()          (BatteryfaultState_u8)
#define GET_TOP_TEXT_STATE()            (TopTextState_u8)
#define GET_CHARGER_PLUG_IN_STATE()     (ChargerPlugInState_u8)
#define GET_WHKM()                      (PresentSessionWHkm_u16)
#define GET_RANGEKM()                   (PresentSessionRangeKm_u16)
#define GET_SOC()                       (Battery_SOC_u8)
#define GET_ACC_SOC()                   (Accurate_SOC_f)

#define SET_ODO_TEXT_STATE(x)           (ODOtextState_u8 = x)
#define SET_TRIP_A_TEXT_STATE(x)        (TripATextState_u8 = x)
#define SET_TRIP_B_TEXT_STATE(x)        (TripBTextState_u8 = x)
#define SET_TRIP_TEXT_STATE(x)          (TripTextState_u8 = x)
#define SET_TIME_COLON_STATE(x)         (TimeColonState_u8 = x)
#define SET_MOTOR_FAULT_STATE(x)        (MotorFaultState_u8 = x)
#define SET_BATT_FAULT_STATE(x)         (BatteryfaultState_u8 = x)
#define SET_TOP_TEXT_STATE(x)           (TopTextState_u8 = x)
#define SET_CHARGER_PLUG_IN_STATE(x)    (ChargerPlugInState_u8 = x) 
#define SET_WHKM(x)                     (PresentSessionWHkm_u16 = x) 
#define SET_RANGEKM(x)                  (PresentSessionRangeKm_u16 = x) 
#define SET_SOC(x)                      (Battery_SOC_u8 = x)
#define SET_ACC_SOC(x)                  (Accurate_SOC_f = x)

#define SET_VEHICLE_SPEED(x)            (VehicleSpeedToDisplay_u16 = x)
#define SET_ODO_METER(x)                (ODO_in_meter_u32 = x)

#define GET_VEHICLE_SPEED()             (VehicleSpeedToDisplay_u16)
#define GET_ODO_METER()                 (ODO_in_meter_u32)


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/


#endif /* COMMUNICATOR_H */


