/***********************************************************************************************************************
* File Name    : Cluster_Conf.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 26/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"
#include "TelltaleConf.h"
#include "Speed_Conf.h"
#include "ODO_Conf.h"
#include "RangeKm_Conf.h"
#include "Mileage_Conf.h"
#include "Time_Conf.h"
#include "SOC_Conf.h"
#include "PowerConsum_Conf.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Structure declaration
***********************************************************************************************************************/
const ClusterSigConf_St_t		ClusterSigConf_aSt[TOTAL_SIGNALS_E] = 
{
	/* Cluster Signal   Signal Length            Signal Value Configuration  */
	{ COMMON_SIG_E,     COMMON_SEG_SIG_LEN,      Common_SignalsValue_aSt      },
	{ SPEED_E,          SPEED_SIG_LEN,           Speed_SignalsValue_aSt       },
	{ MILEAGE_E,        MILEAGE_SIG_LEN,         Mileage_SignalsValue_aSt     },
	{ RANGE_KM_E,       RANGE_SIG_LEN,           Range_SignalsValue_aSt       },
	{ LEFT_IND_E,       LEFT_IND_SIG_LEN,        LeftInd_SignalsValue_aSt     },
	{ RIGHT_IND_E,      RIGHT_IND_SIG_LEN,       RightInd_SignalsValue_aSt    },
	{ LOW_BEAM_E,       LOW_BEAM_SIG_LEN,        LowBeamInd_SignalsValue_aSt  },
	{ HIGH_BEAM_E,      HIGH_BEAM_SIG_LEN,       HighBeamInd_SignalsValue_aSt },
	{ BLE_E,            BLE_SIG_LEN,             BLE_Ind_SignalsValue_aSt     },
	{ REGEN_BRAKING_E,  REGEN_BRAKING_SIG_LEN,   RegenBrake_SignalsValue_aSt  },
	{ WARNING_IND_E,    WARN_IND_SIG_LEN,        Warning_SignalsValue_aSt     },
	{ SERV_REM_E,       SERV_REM_SIG_LEN,        ServRem_SignalsValue_aSt     },
	{ NEUTRAL_MODE_E,   NEUTRAL_MODE_SIG_LEN,    NeutralMode_SignalsValue_aSt },
	{ ECO_MODE_E,       ECO_MODE_SIG_LEN,        EcoMode_SignalsValue_aSt     },
	{ SPORTS_MODE_E,    SPORTS_MODE_SIG_LEN,     SportsMode_SignalsValue_aSt  },
	{ REVERSE_MODE_E,   REVERSE_MODE_SIG_LEN,    ReverseMode_SignalsValue_aSt },
	{ SIDE_STAND_E,     SIDE_STAND_SIG_LEN,      SideStand_SignalsValue_aSt   },
	{ SAFE_MODE_E,      SAFE_MODE_SIG_LEN,       SafeMode_SignalsValue_aSt    },
	{ KILL_SWITCH_E,    KILL_SWITCH_SIG_LEN,     KillSwitch_SignalsValue_aSt  },
	{ BATT_SOC_E,       BATT_SOC_SIG_LEN,        SOC_SignalsValue_aSt         },
	{ POWER_CONSUMP_E,  POWER_CONSUMP_SIG_LEN,   PwrConsum_SignalsValue_aSt   },
	{ ODO_E,            ODO_SIG_LEN,             ODO_SignalsValue_aSt         },
	{ MINUTES_TIME_E,   MINUTES_TIME_SIG_LEN,    MinutesTime_SignalsValue_aSt },
	{ HOURS_TIME_E,     HOURS_TIME_SIG_LEN,      HoursTime_SignalsValue_aSt   },
	{ BATT_FAULT_E,     BATT_FLT_SIG_LEN,        BattFlt_SignalsValue_aSt     },
	{ MOTOR_FAULT_E,    MOTOR_FLT_SIG_LEN,       MotorFlt_SignalsValue_aSt    },
	{ TIME_COLON_E,     COLON_SIG_LEN,           Colon_SignalsValue_aSt       },
	{ LOGO_E,	    LOGO_SIG_LEN,	     Logo_SignalsValue_aSt	  },
	{ POWER_IND_E,	    POWER_IND_SIG_LEN,	     Power_Ind_SignalsValue_aSt	  },
	{ WHKM_TEXT_E,	    WHKM_TEXT_SIG_LEN,	     WHKM_Text_SignalsValue_aSt	  },
	{ BATTTERY_IND_E,   BATTERY_IND_SIG_LEN,     Batt_Ind_SignalsValue_aSt	  },
	{ SPORTS_TEXT_E,    SPORTS_MODE_SIG_LEN,     Sports_Mode_SignalsValue_aSt },
	{ ECO_TEXT_E,	    ECO_MODE_SIG_LEN,	     Eco_Mode_SignalsValue_aSt    },
	{ NEUTRAL_TEXT_E,   NEUTRAL_MODE_SIG_LEN,    Neutral_Mode_SignalsValue_aSt},
	{ REVERSE_TEXT_E,   REVERSE_MODE_SIG_LEN,    Reverse_Mode_SignalsValue_aSt},
};


/********************************************************EOF***********************************************************/