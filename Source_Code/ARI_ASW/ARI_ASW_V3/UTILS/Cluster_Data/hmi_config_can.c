﻿

#include "hmi_config_can.h"
#include "pfdl_user.h"
#include "DataAquire.h"
#include "Time.h"

uint16_t PerBarPwrConsum_u16        = 0;
uint16_t WheelDia_in_mm_u16         = 0;
uint8_t PulseCountper1Rotation_u8   = 0;
uint8_t  BrightnessDuty_u8          = 0;
uint8_t  GearRatio_u8               = 0;
uint8_t RealTimeMin_u8              = 0;
uint8_t RealTimeHour_u8             = 0;


void SetHMIConfigParameter(uint8_t *DataBytes)
{
  uint8_t FunctionID_u8 = 0x00U;
  FunctionID_u8 = DataBytes[0];
  switch(FunctionID_u8)
  {
    case SET_WATT_CONSUM_PER_BAR:
    {
//      PerBarPwrConsum_u16 = (uint16_t)(((uint16_t)DataBytes[2] << 8U) | (uint16_t)DataBytes[3]);
//      if(PerBarPwrConsum_u16 == 0){PerBarPwrConsum_u16 = 1;}
//      SET_EACH_BAR_PWR_CONSUM(PerBarPwrConsum_u16);
//      StoreUserConfHMIData(PWR_CONSUM_PER_BAR_E);
      break;
    }
    case SET_BRIGHTNESS_DUTY:
    {
      BrightnessDuty_u8 = DataBytes[2];
      if(BrightnessDuty_u8 == 0){BrightnessDuty_u8 = 1;}
      else if(BrightnessDuty_u8 > 100){BrightnessDuty_u8 = 100;}
      StoreUserConfHMIData(BRIGHTNESS_DUTY_E);
      break;
    }
    case SET_SPEED_CAL_DATA:
    {
      uint8_t SubFuncId_u8 = 0x00U;
      SubFuncId_u8 = DataBytes[1];
      switch(SubFuncId_u8)
      {
        case SUB_FUNC_WHEEL_DIA:
        {
          WheelDia_in_mm_u16 = (uint16_t)(((uint16_t)DataBytes[2] << 8U) | (uint16_t)DataBytes[3]);
          StoreUserConfHMIData(WHEEL_DIAMETER_E);
          break;
        }
        case SUB_FUNC_GEAR_RATIO:
        {
          GearRatio_u8 = DataBytes[2];
          StoreUserConfHMIData(GEAR_RATIO_E);
          break;
        }
        case SUB_FUNC_PULSE_COUNT:
        {
          PulseCountper1Rotation_u8 = DataBytes[2];
          StoreUserConfHMIData(PULSE_COUNT_E);
          break;
        }
        default:{;}
      }
      break;
    }
    case REAL_TIME_SET:
    {
      RealTimeHour_u8 = DataBytes[1];
      RealTimeMin_u8 = DataBytes[2];
      Set_Real_Time(RealTimeHour_u8,RealTimeMin_u8,0);
      break;
    }
    default:{;}
  }

  return;
}


void StoreUserConfHMIData(FDL_BLK3_Sig_En_t FDL_BLK3_Sig_En)
{
  uint8_t  FDL3_Bytes_au8[FDL3_TOTAL_BYTES] = {0x00U};
  uint16_t ByteCount_u16 = 0;

  FDL_Read(FDL_BLOCK_3, 0U, FDL3_TOTAL_BYTES);
  for(ByteCount_u16 = 0; ByteCount_u16 < FDL3_TOTAL_BYTES; ByteCount_u16++)
  {
    FDL3_Bytes_au8[ByteCount_u16] = dubReadBuffer[ByteCount_u16];
  }
  switch(FDL_BLK3_Sig_En)
  {
    case PWR_CONSUM_PER_BAR_E:
    {
      FDL3_Bytes_au8[0] = (uint8_t)((PerBarPwrConsum_u16 >> 8) & 0x00FFU);
      FDL3_Bytes_au8[1] = (uint8_t)(PerBarPwrConsum_u16 & 0x00FFU);
      break;
    }
    case WHEEL_DIAMETER_E:
    {
      FDL3_Bytes_au8[2] = (uint8_t)((WheelDia_in_mm_u16 >> 8) & 0x00FFU);
      FDL3_Bytes_au8[3] = (uint8_t)(WheelDia_in_mm_u16 & 0x00FFU);
      break;
    }
    case PULSE_COUNT_E:
    {
      FDL3_Bytes_au8[4] = (uint8_t)(PulseCountper1Rotation_u8 & 0x00FFU);
      break;
    }
    case BRIGHTNESS_DUTY_E:
    {
      FDL3_Bytes_au8[5] = (uint8_t)(BrightnessDuty_u8 & 0x00FFU);
      break;
    }
    case GEAR_RATIO_E:
    {
      FDL3_Bytes_au8[6] = (uint8_t)(GearRatio_u8 & 0x00FFU);
      break;
    }
    default:{;}
  }

  FDL_Erase(FDL_BLOCK_3, 1U);

  for(ByteCount_u16 = 0; ByteCount_u16 < FDL3_TOTAL_BYTES; ByteCount_u16++)
  {
    dubWriteBuffer[ByteCount_u16] = FDL3_Bytes_au8[ByteCount_u16];
  }

  FDL_Write(FDL_BLOCK_3, 0U, FDL3_TOTAL_BYTES);
  
  return;
}


void ReStoreUserConfHMIData(void)
{
  uint8_t  FDL3_Bytes_au8[FDL3_TOTAL_BYTES] = {0x00U};
  uint16_t ByteCount_u16 = 0;

  FDL_Read(FDL_BLOCK_3, 0U, FDL3_TOTAL_BYTES);
  for(ByteCount_u16 = 0; ByteCount_u16 < FDL3_TOTAL_BYTES; ByteCount_u16++)
  {
    FDL3_Bytes_au8[ByteCount_u16] = dubReadBuffer[ByteCount_u16];
  }

  PerBarPwrConsum_u16 = (uint16_t)((FDL3_Bytes_au8[0] << 8U) | FDL3_Bytes_au8[1]);
  if(PerBarPwrConsum_u16 == 0){PerBarPwrConsum_u16 = 1;}
  WheelDia_in_mm_u16 = (uint16_t)((FDL3_Bytes_au8[2] << 8U) | FDL3_Bytes_au8[3]);
  PulseCountper1Rotation_u8 = FDL3_Bytes_au8[4];
  BrightnessDuty_u8 = FDL3_Bytes_au8[5];
  if((BrightnessDuty_u8 == 0)){BrightnessDuty_u8 = 1;}
  else if(BrightnessDuty_u8 > 100){BrightnessDuty_u8 = 100;}
  else{;}
  GearRatio_u8 = FDL3_Bytes_au8[6];

  //if(InstWatts_Chnl_En == INST_WATTS_CHAN_CUSTM_CAN_E){SET_EACH_BAR_PWR_CONSUM(PerBarPwrConsum_u16);}
  
  return;
}


void Clear_HMIConf_CANSignals(void)
{
  SET_WATTS_PER_BAR(0);
  SET_WHEEL_DIA(0);
  SET_PULSE_PER_ROTATION(0);
  SET_BRIGHTNESS_PER(0);
  SET_GEAR_RATIO(0);
	return;
}
