/***********************************************************************************************************************
* File Name    : DataAquire.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/9/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "DataAquire.h"
#include "DataBank.h"
#include "Time.h"
#include "Communicator.h"
#include "hmi_config_can.h"
#include "board_conf.h"
#include "timer_user.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

//uint32_t UserButtonIdleTime_u32 = 0;
UserInputSigConf_St_t	UserInputSigConf_St[TOTAL_USER_INPUT_SIG_E] = 
{
	{0,0,INPUT_1_E,	 	USER_BUTTON_RELEASED_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	{0,0,INPUT_2_E,	 	USER_BUTTON_RELEASED_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	{0,0,INPUT_3_E,	 	USER_BUTTON_RELEASED_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	{0,0,INPUT_4_E,	 	USER_BUTTON_RELEASED_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	{0,0,INPUT_5_E,	 	USER_BUTTON_RELEASED_E, 0x00U, TELLTALE_SOLID_ON, TELLTALE_OFF, 0x00U },
	{0,0,INPUT_6_E,	 	USER_BUTTON_RELEASED_E, 0x00U, TELLTALE_SOLID_ON, 	 TELLTALE_OFF, 0x00U },
	{0,0,INPUT_7_E,	 	USER_BUTTON_RELEASED_E, 0x00U, TELLTALE_SOLID_ON, 	 TELLTALE_OFF, 0x00U },
	{0,0,INPUT_8_E,	 	USER_BUTTON_RELEASED_E, 0x00U, IGNITION_ON, TELLTALE_OFF, 0x00U },
	{0,0,INPUT_9_E,	 	USER_BUTTON_RELEASED_E, 0x00U, TELLTALE_SOLID_ON, 	  	 IGNITION_OFF, 0x00U },
	{0,0,INPUT_10_E,	USER_BUTTON_RELEASED_E, 0x00U, TELLTALE_BLINK, TELLTALE_OFF, 0x00U },
	{0,0,INPUT_11_E,	USER_BUTTON_RELEASED_E, 0x00U, TELLTALE_BLINK, TELLTALE_OFF, 0x00U },
#if(SPEED_PULSE_COUNTER == FALSE)
	{0,0,INPUT_12_E,	USER_BUTTON_RELEASED_E, 0x00U, TELLTALE_BLINK, 	 TELLTALE_OFF, 0x00U }
#endif
};

IgnitionInput_En_t	IgnitionInput_En = IGNITION_GPIO_E; //todo:configurable
uint8_t Pwmpulse_u8 = 0;
/***********************************************************************************************************************
* Function Name: ReadClusterInput
* Description  : This function reads the user inputs.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ReadClusterInput(void)
{
	#if(HMI_CAN_WITH_IO == TRUE)
	uint16_t InSigCount_u16 = 0;
	
	UserInputSigConf_St[INPUT_1_E].UserInputVal_u8 	= P2_bit.no6; 		/*ANI5*/		/* Neutral */		
	UserInputSigConf_St[INPUT_2_E].UserInputVal_u8 	= P2_bit.no1; 		/*ANI1*/		/* Right indicator*/
	UserInputSigConf_St[INPUT_3_E].UserInputVal_u8 	= P12_bit.no3; 		/*P124*/		/* Left indicator*/	
	UserInputSigConf_St[INPUT_4_E].UserInputVal_u8 	= P2_bit.no0; 		/*ANI0*/		/* Low beam */
	UserInputSigConf_St[INPUT_5_E].UserInputVal_u8 	= P13_bit.no7; 		/*INTP5/P137*/  /* High beam */			
	UserInputSigConf_St[INPUT_6_E].UserInputVal_u8 	= P2_bit.no2; 		/*ANI2*/		/* ECO */	
	UserInputSigConf_St[INPUT_7_E].UserInputVal_u8 	= P2_bit.no3; 		/*ANI3*/		/* Sport */
	UserInputSigConf_St[INPUT_8_E].UserInputVal_u8 	= P2_bit.no4; 		/*ANI4*/		/* Ignition */
	UserInputSigConf_St[INPUT_9_E].UserInputVal_u8 	= P2_bit.no5; 		/*ANI6*/		/* Reverse */
	UserInputSigConf_St[INPUT_10_E].UserInputVal_u8 = P2_bit.no7; 		/*ANI7*/		/* Brake fault mapped with service reminder*/
	UserInputSigConf_St[INPUT_11_E].UserInputVal_u8 = P12_bit.no4;		/*P123*/		/* Hazard ind mapped with warning Ind */
#if(SPEED_PULSE_COUNTER == FALSE)
	UserInputSigConf_St[INPUT_12_E].UserInputVal_u8 = P9_bit.no4; 		/*PWM-IN/P94*/ 	/* Speed pulse Counter*/	
#endif
	
	for(InSigCount_u16 = 0; InSigCount_u16 < TOTAL_USER_INPUT_SIG_E; InSigCount_u16++)
	{
		if(true == UserInputSigConf_St[InSigCount_u16].UserInputVal_u8)
		{
			// if(UserInputSigConf_St[InSigCount_u16].UserButtonState_En == USER_BUTTON_RELEASED_E)
			// {
			// 	UserInputSigConf_St[InSigCount_u16].UserButtonState_En = USER_BUTTON_PRESSED_E;
			// 	UserInputSigConf_St[InSigCount_u16].CaptureTime_u32 = GET_TIME_MS();
			// }
			// else if()
			// {
			// 	;	
			// }
			// UserInputSigConf_St[InSigCount_u16].TimeDiffrence_u16 = GET_TIME_MS() - UserInputSigConf_St[InSigCount_u16].CaptureTime_u32;
			UserInputSigConf_St[InSigCount_u16].SignalState_u8 = UserInputSigConf_St[InSigCount_u16].SigTurnOnVal_u8;
		}
		else if(false == UserInputSigConf_St[InSigCount_u16].UserInputVal_u8)
		{
			// if(UserInputSigConf_St[InSigCount_u16].UserButtonState_En == USER_BUTTON_PRESSED_E && 
			//    (UserInputSigConf_St[InSigCount_u16].ButtonLongPressed_b == false))
			// {
			// 	UserInputSigConf_St[InSigCount_u16].UserButtonState_En = USER_BUTTON_RELEASED_E;
			// 	UserInputSigConf_St[InSigCount_u16].SignalState_u8 = UserInputSigConf_St[InSigCount_u16].SigTurnOnVal_u8;
			// }
			// else if (UserInputSigConf_St[InSigCount_u16].UserButtonState_En == USER_BUTTON_PRESSED_E && (UserInputSigConf_St[InSigCount_u16].ButtonLongPressed_b == true))
			// {
			// 	UserInputSigConf_St[InSigCount_u16].UserButtonState_En = USER_BUTTON_RELEASED_E;
			// 	UserInputSigConf_St[InSigCount_u16].ButtonLongPressed_b = false;
			// 	UserInputSigConf_St[InSigCount_u16].TimeDiffrence_u16 = 0;
			// 	UserInputSigConf_St[InSigCount_u16].CaptureTime_u32 = 0;	
			// }
			// else if(UserInputSigConf_St[InSigCount_u16].UserButtonState_En == USER_BUTTON_RELEASED_E)
			// {
			// 	UserInputSigConf_St[InSigCount_u16].SignalState_u8 = UserInputSigConf_St[InSigCount_u16].SigTurnOffVal_u8;
			// }
			// else
			// {
			// 	;
			// }
			UserInputSigConf_St[InSigCount_u16].SignalState_u8 = UserInputSigConf_St[InSigCount_u16].SigTurnOffVal_u8;
		}
		else
		{
			;	
		}
	}
	#endif

	if(IgnitionInput_En == IGNITION_GPIO_E)
	{
		ClusterSignals_St.IgnitionSig_u8 = UserInputSigConf_St[INPUT_8_E].SignalState_u8;
	}
	else if(IgnitionInput_En == IGNITION_CAN_E)
	{
		ClusterSignals_St.IgnitionSig_u8 = 0;
	}
	else if(IgnitionInput_En == IGNITION_NONE_E)
	{
		ClusterSignals_St.IgnitionSig_u8 = 0x01U; //Set Ignition to ON
	}
	else
	{
		;
	}
	Pwmpulse_u8 = TCR01;
	return;
}


/***********************************************************************************************************************
* Function Name: UpdateSigToDataBank
* Description  : This function updates the user input signals and signals received over CAN to the Data Bank.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void UpdateSigToDataBank(void)
{
	ClusterSignals_St.SpeedPulseCount_u32		= ReadPulseCount();
	
	ClusterSignals_St.ODOmeterSig_u32 			= GET_PRESENT_ODOMETER();
	
	ClusterSignals_St.PowerConsumBarsSig_u16 	= GET_VEHICLE_SPEED()/10;	
	
	ClusterSignals_St.RangeKmSig_u16 			= GET_PRESENT_RANGEKM();
	
	ClusterSignals_St.VehicleSpeedSig_u8 		= GET_VEHICLE_SPEED();
	
	ClusterSignals_St.SOCSig_u8 				= GET_SOC();
	
	ClusterSignals_St.MileageSig_u8 			= GET_PRESENT_WHKM();
	
	ClusterSignals_St.SafeModeSig_u8 			= 0;
	
	ClusterSignals_St.BatteryTextSig_u8 		= 0;
	
	ClusterSignals_St.MotorTextSig_u8 			= 0;
	
	ClusterSignals_St.ServiceReminderSig_u8 	= UserInputSigConf_St[INPUT_10_E].SignalState_u8;
	
	ClusterSignals_St.WarningSig_u8 			= UserInputSigConf_St[INPUT_11_E].SignalState_u8;
	
	ClusterSignals_St.RegenSig_u8 				= RegenState_u8;
	
	ClusterSignals_St.BLE_Icon_Sig_u8 			= 0;

	ClusterSignals_St.ReverseModeSig_u8 		= UserInputSigConf_St[INPUT_9_E].SignalState_u8;
	ClusterSignals_St.EcoModeSig_u8 			= UserInputSigConf_St[INPUT_6_E].SignalState_u8;
	ClusterSignals_St.SportsModeSig_u8  		= UserInputSigConf_St[INPUT_7_E].SignalState_u8;
	
	ClusterSignals_St.NeutralModeSig_u8 		= UserInputSigConf_St[INPUT_1_E].SignalState_u8;
	
	ClusterSignals_St.ChargingStatus_u8 		= BatteryState_u8;     

	ClusterSignals_St.KillSwitchSig_u8 			= 0;

	ClusterSignals_St.LeftIndicatorSig_u8 		= UserInputSigConf_St[INPUT_3_E].SignalState_u8;

	ClusterSignals_St.RightIndicatorSig_u8 		= UserInputSigConf_St[INPUT_2_E].SignalState_u8;

	ClusterSignals_St.HighBeamSig_u8 			= UserInputSigConf_St[INPUT_5_E].SignalState_u8;

	ClusterSignals_St.LowBeamSig_u8 			= UserInputSigConf_St[INPUT_4_E].SignalState_u8;

	ClusterSignals_St.SideStandSig_u8 			= 0;//UserInputSigConf_St[INPUT_3_E].SignalState_u8;         
	
	/*Update Present time to the data-bank*/
	if(TimeUpdateChnnl_En == TIME_UPDATE_TIMER_E)
	{
		ClusterSignals_St.HoursTimeSig_u16 		= HoursTime_u16;
		ClusterSignals_St.MinutesTimeSig_u16 		= MinutesTime_u16;
	}
	else if(TimeUpdateChnnl_En == TIME_UPDATE_CAN_E)
	{
		/*update the time received over CAN*/
		ClusterSignals_St.HoursTimeSig_u16 		= 0;
	    	ClusterSignals_St.MinutesTimeSig_u16		= 0;
		/*Note : If the Time Signals are not available over CAN, assign zero here*/
	}
	else
	{
		;
	}

	SET_SPEED_PULSE_COUNT(ClusterSignals_St.SpeedPulseCount_u32);
	
	return;
}


/***********************************************************************************************************************
* Function Name: ResetTheUserInput
* Description  : This function resets the user input signals state.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheUserInput(void)
{
	uint16_t InputCount_u16 = 0;
	
	for(InputCount_u16 = 0; InputCount_u16 < TOTAL_USER_INPUT_SIG_E; InputCount_u16++)
	{
		UserInputSigConf_St[InputCount_u16].SignalState_u8 = 0x00U;
	}
	return;
}
/********************************************************EOF***********************************************************/