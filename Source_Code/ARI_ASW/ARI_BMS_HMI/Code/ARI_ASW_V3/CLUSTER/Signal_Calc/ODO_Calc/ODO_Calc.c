/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           :ODO_Calc.c
|    Project        : OHM Automotive
|    Description    : The file contains the ODO calulation .
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 16/12/2022       Jeevan Jestin N             Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/


/*******************************************************************************
 *  HEADER FILE INCLUDES
 ******************************************************************************/
#include"ODO_Calc.h"
#include "pfdl_user.h"
#include "Communicator.h"
#include "hmi_config_can.h"
/*******************************************************************************
 *  MACRO DEFINITION
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL VARIABLES DEFNITION 
 ******************************************************************************/

uint32_t TripA_ODO_u32 = 0;
uint32_t TripB_ODO_u32 = 0;

uint32_t TripA_Reset_Value_u32 = 0;
uint32_t TripB_Reset_Value_u32 = 0;

uint16_t DistancePerPulse_mm_u16 	= 0;
uint32_t UsedPulsesCounter_u32		= 0;
bool OdoInitFlag_b = false;
uint32_t PresentPulseCounter_u32 	= 0;
/*******************************************************************************
 *  STRUCTURE AND ENUM DEFNITION 
 ******************************************************************************/
ODOmeter_St_t	ODOmeter_St;
/*******************************************************************************
 *  STATIC FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : NONE
*   Description   : NONE
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void Calculate_ODOmeter(void)
{
	uint32_t DistanceTravelled_in_mm_u32	= 0;
	//uint16_t WheelRotationsDone_u16 		= 0;
	uint16_t PulseCountDone_u16 			= 0;

	if(false == OdoInitFlag_b)
	{
		OdoInitFlag_b = true;
		UsedPulsesCounter_u32 = GET_SPEED_PULSE_COUNT();
	}
	
	PresentPulseCounter_u32 = GET_SPEED_PULSE_COUNT(); /*Get the Total Speed Pulse Count*/

	if(PresentPulseCounter_u32 < UsedPulsesCounter_u32)
	{
		PulseCountDone_u16 = ( SPEED_PULSE_OVERFLOW_VALUE - UsedPulsesCounter_u32 ) + PresentPulseCounter_u32;
	}
	else
	{
		PulseCountDone_u16 	= (uint16_t)(PresentPulseCounter_u32 - UsedPulsesCounter_u32);
	}

	if(PulseCountDone_u16 > 0)
	{
		DistanceTravelled_in_mm_u32 = (uint32_t)(PulseCountDone_u16 * DistancePerPulse_mm_u16/*45*/);	
								/*Wheel 1 Rotation = 26 Pulses = 1171 mm Distance Travelled
									So, 1 Pulse = 1171 / 26 = 45 mm Distance travelled*/
		UsedPulsesCounter_u32 += PulseCountDone_u16;
		
		PresentODO_mm_u32 = PresentODO_mm_u32 + DistanceTravelled_in_mm_u32;
		
//		PresentODOmeter_u32 += Update_ODOmeter(&PresentODO_mm_u32);

//        ODOmeter_St.Main_ODOmeter_u32 += Update_ODOmeter(&PresentODO_mm_u32);
//        ODOmeter_St.TripA_ODOmeter_u32 += Update_ODOmeter(&PresentODO_mm_u32);
//        ODOmeter_St.TripB_ODOmeter_u32 += Update_ODOmeter(&PresentODO_mm_u32);

		PresentODOmeter_u32 += (PresentODO_mm_u32/1000);

	        ODOmeter_St.Main_ODOmeter_u32 += (PresentODO_mm_u32/1000);
	        ODOmeter_St.TripA_ODOmeter_u32 += (PresentODO_mm_u32/1000);
	        ODOmeter_St.TripB_ODOmeter_u32 += (PresentODO_mm_u32/1000);
		PresentODO_mm_u32 = (PresentODO_mm_u32%1000);
	}
	else
	{
		;
	}

	return;
}

/***********************************************************************************************************************
* Function Name: Update_ODOmeter
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint32_t Update_ODOmeter(uint32_t *ODOmeter_mm_pu32)
{
	uint32_t ODOmeter_in_m_u32 = 0;
	
	ODOmeter_in_m_u32 = (*ODOmeter_mm_pu32 / 1000);
	
	if(ODOmeter_in_m_u32 > 0)
	{
		*ODOmeter_mm_pu32 = (*ODOmeter_mm_pu32 % 1000);
	}
	else
	{
		;
	}
	
	return ODOmeter_in_m_u32;
}

/***********************************************************************************************************************
* Function Name: Set_ODOmeter
* Description  : 
* Arguments    : uint32_t SetODOmeter_u32
* Return Value : None
***********************************************************************************************************************/
void Set_ODOmeter(uint32_t SetODOmeter_u32)
{
	PresentODOmeter_u32 = SetODOmeter_u32;
	return; 
}


/***********************************************************************************************************************
* Function Name: Get_ODOmeter
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint32_t Get_ODOmeter(void)
{
	return ODOmeter_St.Main_ODOmeter_u32;
}


/***********************************************************************************************************************
* Function Name: Set_ODOmeter_in_mm
* Description  : 
* Arguments    : uint32_t SetODOin_mm_u32
* Return Value : None
***********************************************************************************************************************/
void Set_ODOmeter_in_mm(uint32_t SetODOin_mm_u32)
{
	PresentODO_mm_u32 = SetODOin_mm_u32;
	return;  
}


/***********************************************************************************************************************
* Function Name: Get_ODOmeter_in_mm
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint32_t Get_ODOmeter_in_mm(void)
{
	return PresentODO_mm_u32;
}
/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ResetTripA
*   Description   : NONE
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void ResetTripA(uint32_t Main_ODO_u32)
{
    TripA_Reset_Value_u32 = Main_ODO_u32;
    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ResetTripB
*   Description   : NONE
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void ResetTripB(uint32_t Main_ODO_u32)
{
    TripB_Reset_Value_u32 = Main_ODO_u32;
    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ResetTripB
*   Description   : NONE
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
uint32_t Get_TripA(void)
{
    return TripA_ODO_u32;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ResetTripB
*   Description   : NONE
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
uint32_t Get_TripB(void)
{
    return TripB_ODO_u32; 
}

/***********************************************************************************************************************
* Function Name: Get_Distance_for_1_Pulse
* Description  : This function calculates the equivalent distance in mm for 1 Pulse Count.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Get_Distance_for_1_Pulse(void)
{
	DistancePerPulse_mm_u16 = (uint16_t)((GET_WHEEL_DIA()*PIE) / GET_PULSE_PER_ROTATION());
	return;
}

/***********************************************************************************************************************
* Function Name: ResetTheODOmeterData
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheODOmeterData(void)
{
	//PresentPulseCounter_u32 = RESET;
	UsedPulsesCounter_u32 	= RESET;
	DistancePerPulse_mm_u16 = 0;
	OdoInitFlag_b = false;
	return;
}

/***********************************************************************************************************************
* Function Name: StoreODOtoFlash
* Description  : The function Stores the Main ODO, Trip_A ODO, Trip_B ODO.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void StoreODOtoFlash(void)
{
	uint8_t ODOmeterSize_u8 = 0;
	uint8_t ByteCount_u8 = 0;
	uint8_t *ODOmeterData_pu8 = 0;

	ODOmeterSize_u8 = sizeof(ODOmeter_St);
	ODOmeterData_pu8 = (uint8_t *)&ODOmeter_St;

	FDL_Erase(ODO_FLASH_BLOCK,ODO_TOTAL_FLSHA_BLOCK);

	for(ByteCount_u8 = 0; ByteCount_u8<ODOmeterSize_u8;ByteCount_u8++)
	{
		dubWriteBuffer[ByteCount_u8] = ODOmeterData_pu8[ByteCount_u8];
	}

	FDL_Write(ODO_FLASH_BLOCK, ODO_START_POS, ODOmeterSize_u8);

    return;
}

/***********************************************************************************************************************
* Function Name: RestoreODOfromFlash
* Description  : The function Restores the Main ODO, Trip_A ODO, Trip_B ODO.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void RestoreODOfromFlash(void)
{
	uint8_t ODOmeterSize_u8 = 0;
	uint8_t ByteCount_u8 = 0;
	uint8_t *ODOmeterData_pu8 = 0;

	ODOmeterSize_u8 = sizeof(ODOmeter_St);
	ODOmeterData_pu8 = (uint8_t *)&ODOmeter_St;

	FDL_Read(ODO_FLASH_BLOCK, ODO_START_POS, ODOmeterSize_u8);

	for(ByteCount_u8 = 0; ByteCount_u8<ODOmeterSize_u8;ByteCount_u8++)
	{
		ODOmeterData_pu8[ByteCount_u8] = dubReadBuffer[ByteCount_u8];
	}
	
	if(ODOmeter_St.Main_ODOmeter_u32 == 0xFFFFFFFF)
	{
		ODOmeter_St.Main_ODOmeter_u32 = 0;
	}
	if(ODOmeter_St.TripA_ODOmeter_u32 == 0xFFFFFFFF)
	{
		ODOmeter_St.TripA_ODOmeter_u32 = 0;
	}
	if(ODOmeter_St.TripB_ODOmeter_u32 == 0xFFFFFFFF)
	{
		ODOmeter_St.TripB_ODOmeter_u32 = 0;
	}

	PresentODOmeter_u32 = ODOmeter_St.Main_ODOmeter_u32;
}
/*---------------------- End of File -----------------------------------------*/