/***********************************************************************************************************************
* File Name    : cluster_main.c
* Version      : 01
* Description  : This file implements the cluster display main module.
* Created By   : Dileepa B S
* Creation Date: 05/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "cluster_main.h"
#include "Cluster_Conf.h"
#include "delay_flags.h"
#include "TelltaleDisp.h"
#include "SpeedDisp.h"
#include "MileageDisp.h"
#include "ODO_Disp.h"
#include "SOC_Disp.h"
#include "PowerConsum_Disp.h"
#include "RangeKmDisp.h"
#include "TimeDisp.h"
#include "GenConfig.h"
#include "timer_user.h"
#include "DataBank.h"
#include "UserButtuonIn.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
BatteryState_En_t	BatteryState_En = NO_LOAD_E;

/***********************************************************************************************************************
* Function Name: Display_Cluster_Data
* Description  : This function displays all the cluster signals on the LCD, based on the Cluster Data recevied 
                 over CAN
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Display_Cluster_Data(void)
{
	if(ClusterSignals_St.ChargingStatus_u8 == CHARGING_MODE)
	{
		/*Battery_Status_u8 = 1 : charging mode*/
		BatteryState_En = CHARGING_MODE_E;
	}
	else
	{
		/*Battery_Status_u8 = 0 : dis-charging mode*/
		BatteryState_En = DISCHARGING_MODE_E;
	}
	
	switch(BatteryState_En)
	{
		case CHARGING_MODE_E:
		{
			if(SetSpeedToZero_b == false)
			{
				/*Set when switching from discharging mode to charging mode*/
				
				/*Set Vehicle Speed and Power Consumption to 0 here*/
				ClusterSignals_St.VehicleSpeedSig_u8 		= 0;
				ClusterSignals_St.PowerConsumBarsSig_u16 	= 0;
				SetSpeedToZero((uint16_t)ClusterSignals_St.VehicleSpeedSig_u8, SPEED_E);
				Display_PowerConsum(ClusterSignals_St.PowerConsumBarsSig_u16, POWER_CONSUMP_E);
				
				InitialSpeed_Disp_b = false; /*Set this flag to false, so to display the speed once 
									after the charging completes*/
				SetSpeedToZero_b = true;
			}
			else
			{
				;
			}
			
			DisplayChargingInfo((uint16_t)ClusterSignals_St.SOCSig_u8, SPEED_E);
							/*Display CH and SOC% in Digit on the cluster using Speed segments */
			
			break;
		}
		case DISCHARGING_MODE_E:
		{
			if(SetSpeedToZero_b == true)
			{
				/*Reset when switching from charging mode to discharging mode*/
				SetSpeedToZero_b 		= false; 
							/*Reset to false, so to display the speed as 0, when charging starts*/
							
				/*Reset the One second delay parameters used to display CH and SOC on speed place*/
				TimeTick1ms_u32 		= 0;
				OneSecDelay_b 			= true; 
				
				CH_SOC_Disp_En 			= DISP_NONE_E;
							/*Reset the enum to display none, so CH and SOC display on speed place is not required
								during discharging mode*/
								
				/*Set Vehicle Speed and Power Consumption to 0 here*/
				ClusterSignals_St.VehicleSpeedSig_u8 		= 0;
				ClusterSignals_St.PowerConsumBarsSig_u16 	= 0;
			}
			else
			{
				;
			}
			
			Display_Speed((uint16_t)ClusterSignals_St.VehicleSpeedSig_u8, SPEED_E);
							/*Display the present vehicle speed*/
			Display_PowerConsum(ClusterSignals_St.PowerConsumBarsSig_u16, POWER_CONSUMP_E);
							/*Display the power consumption bars*/
			Display_ODO(ClusterSignals_St.ODOmeterSig_u32, ODO_E);
							/*Display the ODO*/
			Display_Mileage((uint16_t)ClusterSignals_St.MileageSig_u8, MILEAGE_E);
							/*Display the Mileage Wh/Km*/
			break;
		}
		default:
		{
			;
		}
	}
	
	Display_SOC((uint16_t)ClusterSignals_St.SOCSig_u8, ClusterSignals_St.ChargingStatus_u8,BATT_SOC_E);
				/*Discharging mode : Display Present SOC,
				  Charging mode : SOC Bars ANimation*/
	
	Display_RangeKm(ClusterSignals_St.RangeKmSig_u16, RANGE_KM_E);
				/*Display Range Km in both Charging and Discharging Mode*/
	if(TimeSetting_En == SET_TIME_END_E)
	{
		Display_Time(ClusterSignals_St.MinutesTimeSig_u16, ClusterSignals_St.HoursTimeSig_u16, MINUTES_TIME_E, HOURS_TIME_E);
					/*Display Real Time*/
		Blink_Time_Colon(TIME_COLON_E);
				/*Blink the time colon at 500ms rate*/
	}

	Indicate_Telltales();
			/*Indicate telltales ON/OFF/BLINK in with synchronization*/
	
	return;
}


/********************************************************EOF***********************************************************/