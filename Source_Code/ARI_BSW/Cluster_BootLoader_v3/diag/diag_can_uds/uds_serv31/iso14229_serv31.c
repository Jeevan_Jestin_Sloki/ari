/*
 * iso14229_serv31.c
 *
 *  Created on: 29-Jan-2020
 *      Author: Lakshmi
 */

/* ************************************************************************** */
/** Descriptive File Name

   @Company
 	 Sloki Technologies Pvt Ltd.

  	@File Name
    	iso14229_serv31.c

   @Summary
 	 This File contains macros, typedefs, extern definitions and
 	 user defined datatypes required by iso14229_serv34.c
 */


/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include <stdbool.h>
#include <stddef.h>
#include "main.h"
#include "crc_util.h"
#include "iso14229_serv31.h"
#include "fee_adapt.h"
/* ************************************************************************** */

/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */


uint32_t Addr31CheckSum_u32 =0u;
uint32_t Size31CheckSum_u32 =0u;
uint32_t flashAdd_u32 = 0;
uint32_t count_u32 = 0;
uint32_t i = 0;
uint32_t j = 0;
uint8_t CallBCK = 0U ;
uint8_t ActiveBlockIndex_u8 = 0x00u ;
int8_t resp_length_u8 = 0U ;
FlashState_En_t Flash_State_En = SERVICE_IDLE_E ;
ST_MACHINE_ERASE_En_t   ST_MACHINE_ERASE = IDLE_STATE_E;
bool EraseFlash_b = false;
bool sub_fun_not_support_b = false ;
bool sequence_error_b   = false ;
bool found_RID_b        = false ;
bool invalid_length_b   = false ;
bool general_programming_failure_b = false ;
bool req_out_of_range_b = false;
bool checksum_valid_b = false;
Asw_block_En_t Asw_block_En = ASW1;

bool CheckSumValidFlags[TOTAL_ASW_BLOCKS];

static uint32_t CalCrc32(void);
/* ************************************************************************** */
/* ************************************************************************** */

/**
	*  FUNCTION NAME : EraseMemory
	*  FILENAME      : iso14229_serv31.c
	*  @param        : buffPu8 - ASW block to br erase
	*  @brief        : This function will Erase flash memory from defined start address to end address
	*  @return       : int8_t status
*/
int8_t EraseMemory(uint8_t *buffPu8)
{
    uint16_t block_u16 = buffPu8[0];
    Asw_block_En_t Asw_Eraseblock_En;
    uint32_t startAddr_u32 = MemBlockConf_aSt[block_u16].StartAddr_u32;
    uint32_t mem_size_u32 = MemBlockConf_aSt[block_u16].EndAddr;
    uint32_t address = startAddr_u32;
    uint32_t size = mem_size_u32;
    bool status_b = false;
    block_u16 = buffPu8[0] ;

    if((Asw_block_En < ASW1) || (Asw_block_En > TOTAL_ASW_E))
    {
    	req_out_of_range_b = true;
    }
    else
    {
	    for(Asw_Eraseblock_En = ASW1; Asw_Eraseblock_En <= TOTAL_ASW_E;Asw_Eraseblock_En++)
	    {
		    if(Asw_block_En == Asw_Eraseblock_En)
		    {
			    status_b = Flash_Erase(address,size);
		            if(status_b == false)
		            {
		                general_programming_failure_b = true ;
		            }
		            else if(status_b == true)
		            {
		                EraseFlash_b = true;
		            }
		            ActiveBlockIndex_u8 = (Asw_Eraseblock_En-ASW1);
		            Flash_State_En = SERVICE_IDLE_E ;
		            CheckSumValidFlags[Asw_Eraseblock_En-ASW1] = false;
			    if(Asw_Eraseblock_En == TOTAL_ASW_E)
			    {
				  Asw_block_En =  ASW1; 
			    }
			    else
			    {
				Asw_block_En++;    
			    }
		            break;
		    }
	    }
    }
    return 0;
}


/**
	*  FUNCTION NAME : CalCrc32
	*  FILENAME      : iso14229_serv31.c
	*  @param        : none
	*  @brief        : This function will calculate CRC32 over flash memory from defined start address to end address
	*  @return       : UINT32 - The calculated crc32 value
*/
static uint32_t CalCrc32(void)
{
	uint32_t flashStartAdd_u32 = Addr31CheckSum_u32;  //UDS_Server_St.driveFlashStartAddress_u32;
//	uint32_t flashEndAdd_u32 =  0x00010000;//APP_END_ADDRESS;// UDS_Server_St.driveFlashEndAddress_u32;
	uint32_t flashReadData_u32 = 0;
    
    uint32_t Crc32Valueu32 = CRC_RESET_VALUE;
    uint16_t  i = 0;
    uint16_t  j = 0;
    uint8_t CrcDataBuff_au8[4] = { 0 };
    flashAdd_u32 = flashStartAdd_u32;
 
    count_u32 = Size31CheckSum_u32;//flashEndAdd_u32 - flashStartAdd_u32;
    for( i = 0; i < count_u32/4 ; i++)
    {
        flashReadData_u32 =  *(__far uint32_t *)flashAdd_u32;
        for(j = 0; j < 4 ; j++)
        {
		CrcDataBuff_au8[j] = ( flashReadData_u32 >> (8 * j) );

        }
        Crc32Valueu32 = Crc_CalculateCRC32((&CrcDataBuff_au8[0]), 4, Crc32Valueu32);
	flashAdd_u32 += 0x04;

    }
//    while(flashAdd_u32 <= flashEndAdd_u32)
//    {
//    	Crc32Valueu32 = (Crc32Valueu32 ^ (*(uint16_t*)flashAdd_u32));
//    	flashAdd_u32 = flashAdd_u32 + 4;
//    }
    return Crc32Valueu32;
}

/**
	*  FUNCTION NAME : CalculateCheckSum
	*  FILENAME      : iso14229_serv31.c
	*  @param        : none
	*  @brief        : This function will calculate CRC32 over flash memory from defined start address to end address
	*  @return       : int8_t - status
*/
int8_t CalculateCheckSum(uint8_t *buffPu8) 
{
	uint8_t i = 0;
	uint32_t actual_check_sum_u32 ;
	uint32_t req_check_sum_u32 = ((uint32_t)buffPu8[0] << 24) | ((uint32_t)buffPu8[1]<< 16) |
                                               ((uint32_t)buffPu8[2] << 8) | ((uint32_t)buffPu8[3]);
	actual_check_sum_u32 = CalCrc32();
	if(req_check_sum_u32 == actual_check_sum_u32)
	{
		CheckSumValidFlags[ActiveBlockIndex_u8] = true;
		

		for(i = 0; i <= (TOTAL_ASW_E-ASW1);i++)
		{
			if (CheckSumValidFlags[i])
			{
				if(i == (TOTAL_ASW_E-ASW1))
				{
					Flash_Pattern_Write(DRIVE_VALIDATION_START_ADDRESS, APP_PATTERN);
				}
			}
			else
			{
				break;
			}
		}
		checksum_valid_b = true;
		Flash_State_En = SERVICE_IDLE_E;
	}
	else
	{
		checksum_valid_b = false;
	}

	return 0;

}

/* ************************************************************************** */


// Section: Local function
/* ************************************************************************** */
/* ************************************************************************** */


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */



/* ************************************************************************** */

/**
*  FUNCTION NAME : iso14229_serv31
*  FILENAME      : iso14229_serv31.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_31 requests
*  @return       : Type of response.
*/

UDS_Serv_resptype_En_t iso14229_serv31(UDS_Serv_St_t*  UDS_Serv_pSt){
	UDS_Serv_resptype_En_t serv_31_resp_En;
    //uint32_t   i;
    //uint32_t   j;
    uint16_t   num_req_bytes_u16 = 0U;
    uint16_t   req_RID_u16 = 0x00U;
    uint8_t    Req_Subfun_id_u8 = 0U;
    
    static UINT8 buffer[5]={0U} ;
	num_req_bytes_u16 = UDS_Serv_pSt->RxLen_u16;
	Req_Subfun_id_u8 = UDS_Serv_pSt->RxBuff_pu8[1];
	req_RID_u16 = UDS_Serv_pSt->RxBuff_pu8[2];
	req_RID_u16 = (req_RID_u16 << 8) | UDS_Serv_pSt->RxBuff_pu8[3];
	//Flash_State_En = SERVICE_IDLE_E ;
    if(!CallBCK) {
        
        if(num_req_bytes_u16 < SERV31_MIN_LEN) {
        	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
        	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ROUTINE_CONTROL;
        	UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
        	UDS_Serv_pSt->TxLen_u16 = 3;
            serv_31_resp_En = UDS_SERV_RESP_NEG_E;
        }
        else if(!ISO14229_SA_L1_Gained_b) {
        	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
        	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ROUTINE_CONTROL;
        	UDS_Serv_pSt->TxBuff_pu8[2] = SECURITY_ACCESS_DENIED;
        	UDS_Serv_pSt->TxLen_u16 = 3;
        	serv_31_resp_En = UDS_SERV_RESP_NEG_E;
        }
        else
        {
            switch(Req_Subfun_id_u8)
            {
                case RC_START:
                {
                    for(i=0;i<TOTAL_RID;i++)
                    {
                        if(RIDConf_aSt[i].RID == req_RID_u16)
                        {
                            if(RIDConf_aSt[i].RIDreqLength == num_req_bytes_u16)
                            {
                                if(RIDConf_aSt[i].past_state == Flash_State_En)
                                {
                                    for(j = 0U; j<(RIDConf_aSt[i].RIDreqLength - 4U); j++)
                                    {
                                    	buffer[j] = UDS_Serv_pSt->RxBuff_pu8[j + 4U] ;
                                    }
                                    for(j = 1U; j < 4U ; j++)
                                    {
                                    	UDS_Serv_pSt->TxBuff_pu8[j] = UDS_Serv_pSt->RxBuff_pu8[j] ;
                                    }
                                    resp_length_u8 = RIDConf_aSt[i].callbackptr(buffer);

                                    if( (ST_MACHINE_ERASE == WAIT_PENDING_E)&&(resp_length_u8 == 0U) )
                                    {
                                        CallBCK=1;
                                        serv_31_resp_En = UDS_SERV_RESP_WAITPEND_E;               /* Wait response */
                                    }
                                    else if(req_out_of_range_b)
                                    {
                                    	CallBCK=0;
                                    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
                                    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ROUTINE_CONTROL;
                                    	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;
                                    	UDS_Serv_pSt->TxLen_u16 = 3;
                                        serv_31_resp_En = UDS_SERV_RESP_NEG_E;
                                        break;
                                    }
                                    else if(sequence_error_b)
                                    {
                                    	CallBCK=0;
                                    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
                                    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ROUTINE_CONTROL;
                                    	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_SEQUENCE_ERR;
                                    	UDS_Serv_pSt->TxLen_u16 = 3;
                                        serv_31_resp_En = UDS_SERV_RESP_NEG_E;
                                        break;
                                    }
                                    else if(general_programming_failure_b)
                                    {
                                    	CallBCK=0;
                                    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
                                    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ROUTINE_CONTROL;
                                    	UDS_Serv_pSt->TxBuff_pu8[2] = GEN_PROG_FAILURE;
                                    	UDS_Serv_pSt->TxLen_u16 = 3;
                                        serv_31_resp_En = UDS_SERV_RESP_NEG_E;
                                        break;
                                    }
                                    else
                                    {
                                    	UDS_Serv_pSt->TxLen_u16 = RIDConf_aSt[i].responselength ;
                                        if( MINUS_FOUR != resp_length_u8){
                                            Flash_State_En = RIDConf_aSt[i].present_state ;

                                        }
                                        if(req_RID_u16 == RID_CHECKSUM) {
                                        if(checksum_valid_b == false) {
                                        	UDS_Serv_pSt->TxBuff_pu8[1] = 0x01;
                                        	UDS_Serv_pSt->TxBuff_pu8[2] = 0xFF;
                                        	UDS_Serv_pSt->TxBuff_pu8[3] = 0x01;
                                        	UDS_Serv_pSt->TxBuff_pu8[4] = 0xFF;

                                        }
                                        else if(checksum_valid_b == true) {
                                        	UDS_Serv_pSt->TxBuff_pu8[1] = 0x01;
                                        	UDS_Serv_pSt->TxBuff_pu8[2] = 0xFF;
                                        	UDS_Serv_pSt->TxBuff_pu8[3] = 0x01;
                                        	UDS_Serv_pSt->TxBuff_pu8[4] = 0x00;

                                        }
                                        }
                                        serv_31_resp_En = UDS_SERV_RESP_POS_E ;
                                        CallBCK=0;
                                        sequence_error_b = false;
                                  
                                        break ;
                                    }
                                }
                                else {
                                	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
                                	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ROUTINE_CONTROL;
                                	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_SEQUENCE_ERR;
                                	UDS_Serv_pSt->TxLen_u16 = 3;
                                    serv_31_resp_En = UDS_SERV_RESP_NEG_E;
                                    break;
                                }
                            }
                            else {
                            	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
                            	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ROUTINE_CONTROL;
                            	UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
                            	UDS_Serv_pSt->TxLen_u16 = 3;
                                serv_31_resp_En = UDS_SERV_RESP_NEG_E;
                                break;
                            }
                        }
                        else {
                        	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
                        	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ROUTINE_CONTROL;
                        	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;
                        	UDS_Serv_pSt->TxLen_u16 = 3;
                            serv_31_resp_En = UDS_SERV_RESP_NEG_E;
                            //break;
                        }
                    }
                    break;
                }
                default: {
                	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
                	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ROUTINE_CONTROL;
                	UDS_Serv_pSt->TxBuff_pu8[2] = SUB_FUNC_NOT_SUPPORTED;
                	UDS_Serv_pSt->TxLen_u16 = 3;
                    serv_31_resp_En = UDS_SERV_RESP_NEG_E;
                    break;
                }
            }
        }
    }
    else
    {
        //RIDConf_aSt[1].callbackptr(buffer);//todo temp
        if(ST_MACHINE_ERASE == WAIT_PENDING_E)
        {
            serv_31_resp_En = UDS_SERV_RESP_WAITPEND_E ;
        }
        else if (ST_MACHINE_ERASE == IDLE_STATE_E )
        {
        	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
        	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ROUTINE_CONTROL;
        	UDS_Serv_pSt->TxBuff_pu8[2] = GEN_PROG_FAILURE ;
            UDS_Serv_pSt->TxLen_u16 = 3;
            serv_31_resp_En = UDS_SERV_RESP_NEG_E;
        }
        else
        {
            CallBCK= 0U ;
            UDS_Serv_pSt->TxLen_u16 = 3U ;
            UDS_Serv_pSt->TxBuff_pu8[1] = 0x01U ;
            UDS_Serv_pSt->TxBuff_pu8[2] = 0xFFU ;
            UDS_Serv_pSt->TxBuff_pu8[3] = 0x00U ;
            Flash_State_En = SERVICE31_ERASE_E ;
            serv_31_resp_En = UDS_SERV_RESP_POS_E ;
        }
    }
    return serv_31_resp_En;

}

/* *****************************************************************************
 End of File
 */
