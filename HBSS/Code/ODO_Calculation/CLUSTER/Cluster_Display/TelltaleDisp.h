/***********************************************************************************************************************
* File Name    : TelltaleDisp.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 08/11/2021
***********************************************************************************************************************/

#ifndef TELLTALE_DISP_H
#define TELLTALE_DISP_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "GenConfig.h"
#include "App_typedefs.h" 
#include "Cluster_Conf.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define _250MS		250u
#define _500MS		500u
#define _3000MS		3000u
#define _300MS		300u
#define _150MS		150u
#define _1000MS		1000u
#define _50MS			50

#define	TEXT_A_INDX			0	
#define	TEXT_B_INDX			1
#define	LEFT_IND_INDX			2
#define	ENGINE_FLT_INDX			3
#define	WARNING_INDX			4
#define	KILL_SWITCH_INDX		5
#define	MOTOR_FLT_INDX			6
#define	SER_REM_INDX			7
#define	RIGHT_IND_INDX			8
#define	POWER_ICON_INDX			9
#define	TOP_TEXT_INDX			10
#define	CH_STATE_INDX			11
#define	DRV_NEUTRAL_INDX		12
#define	DRV_ECO_INDX			13
#define	DRV_SPORTS_INDX			14
#define	DRV_REVERSE_INDX		15
#define	SIDE_STAND_INDX			16
#define	NETWORK_INDX			17
#define BLUETOOTH_INDX			18
#define	HIGH_BEAM_INDX			19

#define AM_TEXT_INDX			20
#define PM_TEXT_INDX			21
#define	ODO_TEXT_INDX			22
#define	TRIP_TEXT_INDX			23
#define COMMON_SIG_INDX			24


#define TOTAL_TELLTALE			(COMMON_SIG_INDX+1)

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	TEXT_A_TT_E ,
	TEXT_B_TT_E ,
	LEFT_IND_TT_E ,
	ENGINE_FLT_TT_E ,
	WARNING_IND_TT_E ,
	KILL_SWITCH_TT_E ,
	MOTOR_FLT_TT_E ,
	SER_REM_TT_E ,
	RIGHT_IND_TT_E ,
	POWER_ICON_IND_TT_E ,
	TOP_TEXT_IND_TT_E ,
	CH_STATE_IND_TT_E,
	NEUTRAL_IND_TT_E ,
	ECO_MODE_TT_E ,
	SPORTS_MODE_TT_E ,
	REVERSE_MODE_TT_E ,
	SIDE_STAND_TT_E ,
	NETWORK_TT_E ,
	BLUETOOTH_TT_E ,
	HIGH_BEAM_TT_E ,
	AM_TEXT_TT_E ,
	PM_TEXT_TT_E ,
	ODO_TEXT_TT_E ,
	TRIP_TEXT_TT_E,
	COMMON_TEXT_TT_E,
	TIME_COLON_TT_E,
	TOTAL_TELLTALE_E,
}Telltales_En_t;

#pragma pack
typedef struct
{
	Telltales_En_t	Telltales_En;          	/*Signal Enum*/
	ClusterSignals_En_t TellTaleConf_Sig_En;
	uint16_t 		TelltaleBlinkFreq_u16;  /*Frequency of blink*/
	uint16_t		TellatleOntime_u16;
	uint16_t		TelltaleOntimeCount_u16;
	uint8_t 	 	Telltale_Value_u8;		/*Signal Present Value*/
	uint8_t 	 	Telltale_PrevValue_u8;		/*Signal Previous Value*/
	bool		 	TelltaleBlink_b;               /*Signal blinking state*/
	bool			TelltaleState_b;			/*TellBlink State*/
}Telltales_Conf_St_t;

#pragma unpack
/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Indicate_Telltales(void); 
void Turn_ON_Telltales(void);          
void Turn_OFF_Telltales(void);
void Blink_Telltales(void);
extern void ClrTelltalesStates(void);

#endif /* TELLTALE_DISP_H */


