/***********************************************************************************************************************
* File Name    : DataBank.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 01/9/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "DataBank.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

ClusterSignals_St_t	ClusterSignals_St = 
{
	0,	/*ODO meters*/
	0,	/*Power Consumption [Watts/Bars]*/
	0,	/*Vehicle Speed Kmph*/
	0,	/*Navigation Distance meters*/
	0,	/*Range Km*/
	0U,	/*ODO Text*/
	0U,	/*TRIP Text*/
	0U,	/*A Text*/
	0U,	/*B Text*/
	0U,	/*Left Indicator*/
	0U,	/*Engine Fault*/
	0U,	/*Warning Symbol*/
	0U,	/*Kill Switch*/
	0U,	/*Motor Fault*/
	0U,	/*Service Reminder*/
	0U,	/*Right Indicator*/
	0U,	/*Navigation Direction*/
	0U,	/*Battery SOC*/
	0U,	/*Power Icon*/
	0U,	/*TOP Text*/
	0U,	/*Charging Indicator*/
	0U,	/*Neutral Mode*/
	0U,	/*Economy Mode*/
	0U,	/*Sports Mode*/
	0U,	/*Reverse Mode*/
	0U,	/*Side Stand*/
	0U,	/*Network Connection*/
	0U,	/*BLE Icon*/
	0U,	/*High Beam*/
	0U,	/*Hours Time*/
	0U,	/*Minutes Time*/
	0U,	/*AM Text*//*todo:jeevan*/
	0U,	/*PM Text*/
	0U,	/*Time Colon*/
	0U,	/*Navigation status*/
	0U,
};


ClusterInput_St_t	ClusterInput_St = 
{
	0U,	/*Ignition Status*/
	0U,	/*Charging Status*/
};


/***********************************************************************************************************************
* Function Name: Clear_Data_Bank
* Description  : This function clears the signals in the data-bank.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Clear_Data_Bank(void)
{
	ClusterSignals_St.ODOmeter_u32 			= 0;
	ClusterSignals_St.PowerConsumption_u16 		= 0;
	ClusterSignals_St.VehicleSpeed_u16 		= 0;
	ClusterSignals_St.NavigationDistance_u16 	= 0;
	ClusterSignals_St.RangeKm_u16 			= 0;
	ClusterSignals_St.ODOText_u8 			= 0;
	ClusterSignals_St.TRIPText_u8 			= 0;
	ClusterSignals_St.AText_u8 			= 0;
	ClusterSignals_St.BText_u8 			= 0;
	ClusterSignals_St.LeftIndicator_u8 		= 0;
	ClusterSignals_St.EngineFault_u8 		= 0;
	ClusterSignals_St.Warning_u8 			= 0;
	ClusterSignals_St.KillSwitch_u8 		= 0;
	ClusterSignals_St.MotorFault_u8 		= 0;
	ClusterSignals_St.ServiceReminder_u8 		= 0;
	ClusterSignals_St.RightIndicator_u8 		= 0;
	ClusterSignals_St.NavigationDirection_u8 	= 0;
	ClusterSignals_St.BatterySOC_u8 		= 0;
	ClusterSignals_St.PowerIcon_u8 			= 0;
	ClusterSignals_St.TOPText_u8 			= 0;
	ClusterSignals_St.ChargingIndicator_u8 		= 0;
	ClusterSignals_St.NeutralMode_u8 		= 0;
	ClusterSignals_St.EconomyMode_u8 		= 0;
	ClusterSignals_St.SportsMode_u8 		= 0;
	ClusterSignals_St.ReverseMode_u8 		= 0;
	ClusterSignals_St.SideStand_u8 			= 0;
	ClusterSignals_St.NetworkConnection_u8 		= 0;
	ClusterSignals_St.BLEIcon_u8 			= 0;
	ClusterSignals_St.HighBeam_u8 			= 0;
	ClusterSignals_St.HoursTime_u8 			= 0;
	ClusterSignals_St.MinutesTime_u8 		= 0;
	ClusterSignals_St.AMText_u8 			= 0;
	ClusterSignals_St.PMText_u8 			= 0;
	ClusterSignals_St.TimeColon_u8 			= 0;
	ClusterSignals_St.Commonsig_u8			= 0;
	return;
}


/********************************************************EOF***********************************************************/