﻿/***********************************************************************************************************************
* File Name    : mcu_can.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 26/05/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "mcu_can.h"

/***********************************************************************************************************************
MACRO directive
***********************************************************************************************************************/

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
MCU_MSG_1_St_t_t  MCU_MSG_1_St_t;
MCU_MSG_2_St_t_t  MCU_MSG_2_St_t;

/***********************************************************************************************************************
* Function Name: Deserialize_MCU_MSG_1_St_t
* Description  : The function Deserialize the MCU Message 1.
* Arguments    : MCU_MSG_1_St_t_t
* Return Value : MCU_MSG_1_ST_T_ID
***********************************************************************************************************************/
 uint32_t Deserialize_MCU_MSG_1_St_t(MCU_MSG_1_St_t_t* message, const uint8_t* data)
{
  message->SpeedMode_u8 = (((data[6] >> MCU_MSG_1_ST_T_SPEEDMODE_U8_MASK0) & (SIGNLE_READ_Mask2))) + MCU_MSG_1_ST_T_CANID_SPEEDMODE_U8_OFFSET;
   return MCU_MSG_1_ST_T_ID; 
}

/***********************************************************************************************************************
* Function Name: Serialize_MCU_MSG_1_St_t
* Description  : The function serialize the MCU Message 1.
* Arguments    : MCU_MSG_1_St_t_t
* Return Value : MCU_MSG_1_ST_T_ID
***********************************************************************************************************************/


 uint32_t Serialize_MCU_MSG_1_St_t(MCU_MSG_1_St_t_t* message, uint8_t* data)
{
  message->SpeedMode_u8 = (message->SpeedMode_u8  - MCU_MSG_1_ST_T_CANID_SPEEDMODE_U8_OFFSET);
  data[6] = ((message->SpeedMode_u8 & (SIGNLE_READ_Mask2)) << MCU_MSG_1_ST_T_SPEEDMODE_U8_MASK0) ;
   return MCU_MSG_1_ST_T_ID; 
}

/***********************************************************************************************************************
* Function Name: Deserialize_MCU_MSG_1_St_t
* Description  : The function Deserialize the MCU Message 1.
* Arguments    : MCU_MSG_2_St_t_t
* Return Value : MCU_MSG_2_ST_T_ID
***********************************************************************************************************************/
 uint32_t Deserialize_MCU_MSG_2_St_t(MCU_MSG_2_St_t_t* message, const uint8_t* data)
{
  message->MotorRPM_u16 = (((data[1] & (SIGNLE_READ_Mask8)) << MCU_MSG_2_ST_T_MOTORRPM_U16_MASK0) | (data[0] & (SIGNLE_READ_Mask8))) + MCU_MSG_2_ST_T_CANID_MOTORRPM_U16_OFFSET;
  message->MotorTemperature_u16 = (((data[5] & (SIGNLE_READ_Mask8)) << MCU_MSG_2_ST_T_MOTORTEMPERATURE_U16_MASK0) | (data[4] & (SIGNLE_READ_Mask8))) + MCU_MSG_2_ST_T_CANID_MOTORTEMPERATURE_U16_OFFSET;
   return MCU_MSG_2_ST_T_ID; 
}
/***********************************************************************************************************************
* Function Name: Serialize_MCU_MSG_1_St_t
* Description  : The function serialize the MCU Message 2.
* Arguments    : MCU_MSG_2_St_t_t
* Return Value : MCU_MSG_2_ST_T_ID
***********************************************************************************************************************/

 uint32_t Serialize_MCU_MSG_2_St_t(MCU_MSG_2_St_t_t* message, uint8_t* data)
{
  message->MotorRPM_u16 = (message->MotorRPM_u16  - MCU_MSG_2_ST_T_CANID_MOTORRPM_U16_OFFSET);
  message->MotorTemperature_u16 = (message->MotorTemperature_u16  - MCU_MSG_2_ST_T_CANID_MOTORTEMPERATURE_U16_OFFSET);
  data[0] = (message->MotorRPM_u16 & (SIGNLE_READ_Mask8)) ;
  data[1] = ((message->MotorRPM_u16 >> MCU_MSG_2_ST_T_MOTORRPM_U16_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->MotorTemperature_u16 & (SIGNLE_READ_Mask8)) ;
  data[5] = ((message->MotorTemperature_u16 >> MCU_MSG_2_ST_T_MOTORTEMPERATURE_U16_MASK0) & (SIGNLE_READ_Mask8)) ;
   return MCU_MSG_2_ST_T_ID; 
}

/***********************************************************************************************************************
* Function Name: ResetMCU_MSG_1_Data
* Description  : The function Clear the MCU message 1.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetMCU_MSG_1_Data(void)
{
  MCU_MSG_1_St_t.SpeedMode_u8 = CLEAR;
  return;
}

/***********************************************************************************************************************
* Function Name: ResetMCU_MSG_2_Data
* Description  : The function Clear the MCU message 2.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetMCU_MSG_2_Data(void)
{
  MCU_MSG_2_St_t.MotorRPM_u16 = CLEAR;
  MCU_MSG_2_St_t.MotorTemperature_u16 = CLEAR;
  return;
}

