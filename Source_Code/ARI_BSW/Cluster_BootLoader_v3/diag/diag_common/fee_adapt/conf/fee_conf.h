
/******************************************************************************
 *    FILENAME    : fee_conf.h
 *    DESCRIPTION : EEPROM configuration 
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki     18/01/2017		   Initial version
 ******************************************************************************
*/ 

#ifndef _FEE_CONF_H_
#define _FEE_CONF_H_

#include "diag_typedefs.h"


/* Structure declaration for EEPROM layout */

//typedef const struct
//{
//    UINT32      FEE_MsgName_u32;    /* Message Name */
//    UINT32      StartAddress_u32;   /* Starting address of the message */
//    UINT16      Size_u16;           /* Length of the data to be stored in memory */
//}FEE_AddrConfig_St_t;


typedef enum
{
  FEE_FM_L2_ENTRY1,
  FEE_FM_L2_ENTRY2,
  FEE_FM_L2_ENTRY3,
  FEE_FM_L2_ENTRY4,
  FEE_FM_L2_ENTRY5,
  FEE_FM_L2_ENTRY6,
  FEE_FM_L2_ENTRY7,
  FEE_FM_L2_ENTRY8,
  FEE_FM_L2_ENTRY9,
  FEE_FM_L2_ENTRY10,
  FEE_FM_COMMON_DATA,
  FEE_FM_RDYRESULTS,
  FEE_FM_TFSLC,
  TOTAL_FEE_SIGNAL_E 
}FEE_EntryName_En_t;


//extern const FEE_AddrConfig_St_t FEE_AddrConfig_aSt[TOTAL_FEE_SIGNAL_E];


#endif
