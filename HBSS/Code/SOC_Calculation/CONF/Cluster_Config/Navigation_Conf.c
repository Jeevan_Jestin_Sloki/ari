/***********************************************************************************************************************
* File Name    : Navigation_Conf.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 03/12/2021
***********************************************************************************************************************/


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Navigation_Conf.h"
/***********************************************************************************************************************
***********************************************************************************************************************/

/*************************************************************************************************************************/
/*
			Navigation direction signal
*/

 const SignalsValue_St_t		Navig_SignalStraight_aSt[STRAIGHT_SIG_LEN]=
 {
	{ZERO_E,	THREE_E,		Navigation_ST_sigconf_ast}, 
 };
 
 const SignalsValue_St_t		Navig_SignalLeft_aSt[LEFT_TURN_SIG_LEN]=
 {
	{ZERO_E,	TWO_E,		Navigation_LT_sigconf_ast}, 
 };
 
 const SignalsValue_St_t		Navig_SignalRight_aSt[RIGHT_TURN_SIG_LEN]=
 {
	{ZERO_E,	TWO_E,		Navigation_RT_sigconf_ast}, 
 };
 
 const SignalsValue_St_t		Navig_SignalLeftUTurn_aSt[LEFT_U_TURN_SIG_LEN]=
 {
	{ZERO_E,	TWO_E,		Navigation_LUT_sigconf_ast}, 
 };
 
 const SignalsValue_St_t		Navig_SignalRightUTURN_aSt[RIGHT_U_TURN_SIG_LEN]=
 {
	{ZERO_E,	TWO_E,		Navigation_RUT_sigconf_ast}, 
 };
 
 
// const SignalsValue_St_t		Navigation_SignalsValue_aSt[NAVIG_DIR_SIG_LEN]=
//{
//	/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
//	{ZERO_E,	TWO_E,		Navigation_ST_sigconf_ast},	// Straight
//	{ONE_E,		TWO_E,		Navigation_LT_sigconf_ast},	// Left Turn
//	{TWO_E,		TWO_E,		Navigation_RT_sigconf_ast},	// Right Turn
//	{THREE_E,	TWO_E,		Navigation_LUT_sigconf_ast},	// Left U Turn
//	{FOUR_E,	TWO_E,		Navigation_RUT_sigconf_ast},	// Right U Turn

//};

/***************const SignalConfig_St_t******************************/

 const SignalConfig_St_t 		Navigation_LT_sigconf_ast[TWO_E]=
{
/*PositionSignalValue  SegmentsRequired  SegmentsConfiguration*/
	{OFF_E,		THREE_E,		Navigation_LT_OFF_segconf_ast},
	{ON_E,		THREE_E,		Navigation_LT_ON_segconf_ast},
	
};

 const SignalConfig_St_t 		Navigation_LUT_sigconf_ast[TWO_E]=
{
	{OFF_E,		THREE_E,		Navigation_LUT_OFF_segconf_ast},
	{ON_E,		THREE_E,		Navigation_LUT_ON_segconf_ast},
	
};

  const SignalConfig_St_t 		Navigation_ST_sigconf_ast[THREE_E]=
{
	{OFF_E,		THREE_E,		Navigation_ST_OFF_segconf_ast},
	{ON_E,		THREE_E,		Navigation_ST_ON_segconf_ast},
	{SEVEN_E,	THREE_E,		Navigation_ST_5_segconf_ast},
	
};


 const SignalConfig_St_t 		Navigation_RT_sigconf_ast[TWO_E]=
{
	{OFF_E,		THREE_E,		Navigation_RT_OFF_segconf_ast},
	{ON_E,		THREE_E,		Navigation_RT_ON_segconf_ast},
	
};

 const SignalConfig_St_t 		Navigation_RUT_sigconf_ast[TWO_E]=
{
	{OFF_E,		THREE_E,		Navigation_RUT_OFF_segconf_ast},
	{ON_E,		THREE_E,		Navigation_RUT_ON_segconf_ast},
	
};

/*************************const SegConfig_St_t*************/

 const SegConfig_St_t			Navigation_LT_OFF_segconf_ast[THREE_E]=
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	{&SEG31,	0x01U,		0x00U},
	{&SEG32,	0x03U,		0x00U},
	{&SEG33,	0x01U,		0x00U},
};

 const SegConfig_St_t			Navigation_LT_ON_segconf_ast[THREE_E]=
{
	{&SEG31,	0x01U,		0x00U},
	{&SEG32,	0x03U,		0x04U},
	{&SEG33,	0x01U,		0x0CU},
};






 const SegConfig_St_t			Navigation_LUT_OFF_segconf_ast[THREE_E]=
{
	{&SEG31,	0x01U,		0x00U},
	{&SEG32,	0x03U,		0x00U},
	{&SEG33,	0x01U,		0x00U},
};

 const SegConfig_St_t			Navigation_LUT_ON_segconf_ast[THREE_E]=
{
	{&SEG31,	0x01U,		0x00U},
	{&SEG32,	0x03U,		0x04U},
	{&SEG33,	0x01U,		0x06U},
};






 const SegConfig_St_t			Navigation_ST_OFF_segconf_ast[THREE_E]=
{
	{&SEG31,	0x01U,		0x00U},
	{&SEG32,	0x03U,		0x00U},
	{&SEG33,	0x01U,		0x00U},
};

 const SegConfig_St_t			Navigation_ST_ON_segconf_ast[THREE_E]=
{
	{&SEG31,	0x01U,		0x00U},
	{&SEG32,	0x03U,		0x0CU},
	{&SEG33,	0x01U,		0x00U},
};

const SegConfig_St_t			Navigation_ST_5_segconf_ast[THREE_E]=
{
	{&SEG31,	0x01U,		0x0EU},
	{&SEG32,	0x03U,		0x0CU},
	{&SEG33,	0x01U,		0x0EU},
};


 const SegConfig_St_t			Navigation_RT_OFF_segconf_ast[THREE_E]=
{
	{&SEG31,	0x01U,		0x00U},
	{&SEG32,	0x03U,		0x00U},
	{&SEG33,	0x01U,		0x00U},
};

 const SegConfig_St_t			Navigation_RT_ON_segconf_ast[THREE_E]=
{
	{&SEG31,	0x01U,		0x0CU},
	{&SEG32,	0x03U,		0x04U},
	{&SEG33,	0x01U,		0x00U},
};




 const SegConfig_St_t			Navigation_RUT_OFF_segconf_ast[THREE_E]=
{
	{&SEG31,	0x01U,		0x00U},
	{&SEG32,	0x03U,		0x00U},
	{&SEG33,	0x01U,		0x00U},
};

 const SegConfig_St_t			Navigation_RUT_ON_segconf_ast[THREE_E]=
{
	{&SEG31,	0x01U,		0x06U},
	{&SEG32,	0x03U,		0x04U},
	{&SEG33,	0x01U,		0x00U},
};

/*
	Naviagtion Distance Configration
*/


 const SignalsValue_St_t			Navig_dist_SignalsValue_aSt[NAVIG_DIST_SIG_LEN]=
{
	{ZERO_E,	TWO_E,		Navig_dist_Sign1conf_aSt},
	{ONE_E,		TWO_E,		Navig_dist_Sign2conf_aSt},
	{TWO_E,		ELEVEN_E,	Navig_dist_Sign3conf_aSt},
};
 const SignalConfig_St_t 		Navig_dist_Sign1conf_aSt[TWO_E]=
{	
	{ON_E,		ONE_SEG_E,		Navig_dist_seg1ON_segconf_ast},
	{OFF_E, 	ONE_SEG_E,		Navig_dist_seg1OFF_segconf_ast},
};

 const SignalConfig_St_t 		Navig_dist_Sign2conf_aSt[TWO_E]=
{
	{ON_E,		ONE_SEG_E,		Navig_dist_seg2ON_segconf_ast},
	{OFF_DIGIT, 	ONE_SEG_E,		Navig_dist_seg2OFF_segconf_ast},
};

 const SignalConfig_St_t 		Navig_dist_Sign3conf_aSt[ELEVEN_E]=
{
	{ ZERO_E,	TWO_SEG_E,	Navig_dist_Segn3_0_SegConf_aSt	},
	{ ONE_E,	TWO_SEG_E,	Navig_dist_Segn3_1_SegConf_aSt	},
	{ TWO_E,	TWO_SEG_E,	Navig_dist_Segn3_2_SegConf_aSt	},
	{ THREE_E,	TWO_SEG_E,	Navig_dist_Segn3_3_SegConf_aSt	},
	{ FOUR_E,	TWO_SEG_E,	Navig_dist_Segn3_4_SegConf_aSt	},
	{ FIVE_E,	TWO_SEG_E,	Navig_dist_Segn3_5_SegConf_aSt	},
	{ SIX_E,	TWO_SEG_E,	Navig_dist_Segn3_6_SegConf_aSt	},
	{ SEVEN_E,	TWO_SEG_E,	Navig_dist_Segn3_7_SegConf_aSt	},
	{ EIGHT_E,	TWO_SEG_E,	Navig_dist_Segn3_8_SegConf_aSt	},
	{ NINE_E,	TWO_SEG_E,	Navig_dist_Segn3_9_SegConf_aSt	},
	{ OFF_DIGIT,	TWO_SEG_E,	Navig_dist_Segn3_OFF_SegConf_aSt},
};




 const SegConfig_St_t 			Navig_dist_seg1ON_segconf_ast[ONE_SEG_E]=
{
	{&SEG32,	0x0DU, 		0x02},
};

 const SegConfig_St_t 			Navig_dist_seg1OFF_segconf_ast[ONE_SEG_E]=
{
	{&SEG32,	0x0DU, 		0x00},
};



 const SegConfig_St_t 			Navig_dist_seg2ON_segconf_ast[ONE_SEG_E]=
{
	{&SEG33,	0x0EU, 		0x01},
};

 const SegConfig_St_t 			Navig_dist_seg2OFF_segconf_ast[ONE_SEG_E]=
{
	{&SEG33,	0x0EU, 		0x00},
};




 const SegConfig_St_t 			Navig_dist_Segn3_0_SegConf_aSt[TWO_E]=
{
	{&SEG35,	0x00U,		0x0FU},
	{&SEG34,	0x01U,		0x0AU},
};

 const SegConfig_St_t 			Navig_dist_Segn3_1_SegConf_aSt[TWO_E]=
{
	{&SEG35,	0x00U,		0x00U},
	{&SEG34,	0x01U,		0x0AU},
};

 const SegConfig_St_t 			Navig_dist_Segn3_2_SegConf_aSt[TWO_E]=
{
	{&SEG35,	0x00U,		0x0BU},
	{&SEG34,	0x01U,		0x0CU},
};

 const SegConfig_St_t 			Navig_dist_Segn3_3_SegConf_aSt[TWO_E]=
{
	{&SEG35,	0x00U,		0x09U},
	{&SEG34,	0x01U,		0x0EU},
};

 const SegConfig_St_t 			Navig_dist_Segn3_4_SegConf_aSt[TWO_E]=
{
	{&SEG35,	0x00U,		0x04U},
	{&SEG34,	0x01U,		0x0EU},
};

 const SegConfig_St_t 			Navig_dist_Segn3_5_SegConf_aSt[TWO_E]=
{
	{&SEG35,	0x00U,		0x0DU},
	{&SEG34,	0x01U,		0x06U},
};

 const SegConfig_St_t 			Navig_dist_Segn3_6_SegConf_aSt[TWO_E]=
{
	{&SEG35,	0x00U,		0x0FU},
	{&SEG34,	0x01U,		0x06U},
};

 const SegConfig_St_t 			Navig_dist_Segn3_7_SegConf_aSt[TWO_E]=
{
	{&SEG35,	0x00U,		0x08U},
	{&SEG34,	0x01U,		0x0AU},
};

 const SegConfig_St_t 			Navig_dist_Segn3_8_SegConf_aSt[TWO_E]=
{
	{&SEG35,	0x00U,		0x0FU},
	{&SEG34,	0x01U,		0x0EU},
};


 const SegConfig_St_t 			Navig_dist_Segn3_9_SegConf_aSt[TWO_E]=
{
	{&SEG35,	0x00U,		0x0DU},
	{&SEG34,	0x01U,		0x0EU},
};
 const SegConfig_St_t 			Navig_dist_Segn3_OFF_SegConf_aSt[TWO_E]=
{
	{&SEG35,	0x00U,		0x00U},
	{&SEG34,	0x01U,		0x00U},
};




	
	
