/***********************************************************************************************************************
* File Name    : DataAquire.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/09/2021
***********************************************************************************************************************/

#ifndef DATA_AQUIRE_H
#define DATA_AQUIRE_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"
/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define SPEED_PULSE_COUNTER					TRUE  /* true false*/

#define TELLTALE_SOLID_ON					0x03U
#define TELLTALE_BLINK						0x01U
#define TELLTALE_OFF						0x00U

#define IGNITION_ON							0x01U
#define IGNITION_OFF						0x00U

#define GPIO_ACTIVEHIGH						0x00U
#define GPIO_ACTIVELOW						0x01U

#define MAX_MODE_COUNTER					(0x02)
#define MIN_MODE_COUNTER					(0x01u)

#define MAX_ASSIST_LEVEL					(0x03)
#define MIN_ASSIST_LEVEL					(0x00)

#define ECO_MODE							0x00
#define CITY_MODE							0x01
#define SPORTS_MODE							0x02

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	USER_INPUT_SIG_START_E,
	INPUT_1_E = USER_INPUT_SIG_START_E,
	INPUT_2_E,
	INPUT_3_E,
	INPUT_4_E,
	INPUT_5_E,
	INPUT_6_E,
	INPUT_7_E,
	INPUT_8_E,
	INPUT_9_E,
	INPUT_10_E,
	INPUT_11_E,
#if(SPEED_PULSE_COUNTER == FALSE)
	INPUT_12_E,
#endif
	TOTAL_USER_INPUT_SIG_E,
	USER_INPUT_SIG_END_E = TOTAL_USER_INPUT_SIG_E,
}UserInputSig_En_t;


typedef enum
{
	ACTIVE_NONE_E,
	ACTIVE_HIGH_E,
	ACTIVE_LOW_E,
}InputActiveState_En_t;

typedef enum
{
	USER_BUTTON_PRESSED_E,
	USER_BUTTON_RELEASED_E,
	USER_BUTTON_IDLE_E,
}UserButtonState_En_t;

typedef struct
{
	uint32_t				CaptureTime_u32;
	uint16_t				TimeDiffrence_u16;
	UserInputSig_En_t		UserInputSig_En;
	UserButtonState_En_t	UserButtonState_En;
	uint8_t 				UserInputVal_u8;
	uint8_t 				SigTurnOnVal_u8;
	uint8_t 				SigTurnOffVal_u8;
	uint8_t 				SignalState_u8;
	bool					ButtonLongPressed_b;
}UserInputSigConf_St_t;


typedef enum
{
	IGNITION_NONE_E,
	IGNITION_CAN_E,
	IGNITION_GPIO_E,
}IgnitionInput_En_t;

/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern UserInputSigConf_St_t	UserInputSigConf_St[TOTAL_USER_INPUT_SIG_E];

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void ReadClusterInput(void);
extern void UpdateSigToDataBank(void);
extern void ResetTheUserInput(void);
extern void UserInterfaceSet(void);
void UserIntefaceButton(void);
void ToggleWalkAssist(void);
void ToggleCruiseControl(void);

#endif /* DATA_AQUIRE_H
*********************************************************End_Of_File**************************************************/