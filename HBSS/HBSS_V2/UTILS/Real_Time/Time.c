/***********************************************************************************************************************
* File Name    : Time.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 05/01/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Time.h"
#include "timer_user.h"
#include "DataBank.h"
#include "GenConfig.h"
#include "can_driver.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
uint8_t 		Minute_Counter_u8 = 00; /*todo:jeevan*/
uint8_t 		Hours_Counter_u8 = 00; // set minute and hour to 0
uint8_t			MinutesTime_u8 = 0;
uint8_t			HoursTime_u8 = 0;
bool			Meridian_Flag_b = false;/* The flag is to shift the time meridian after 12 oclock */
uint16_t 		Second_Counter_u8 = 0;

/***********************************************************************************************************************
* Function Name: Set_Real_Time
* Description  : This function Sets the Real Time on Ignition-ON.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Set_Real_Time(uint8_t Hour_time_u8, uint8_t Minute_time_u8, uint8_t meridian_u8)
{
	Minute_Counter_u8 = Minute_time_u8;	 
	Hours_Counter_u8 = Hour_time_u8;
	Second_Counter_u8 = 0; /* reset the second counter */
	switch(meridian_u8)
	{
		case 0: 
		{
			ClusterSignals_St.AMText_u8 = SOLID_TELLTALE;
			ClusterSignals_St.PMText_u8 = TELLTALE_OFF;
			break;
		}
		case 1: 
		{
			ClusterSignals_St.PMText_u8 = SOLID_TELLTALE;
			ClusterSignals_St.AMText_u8 = TELLTALE_OFF;
			break;
		}
		default: 
		{
			ClusterSignals_St.AMText_u8 = SOLID_TELLTALE;
			ClusterSignals_St.PMText_u8 = TELLTALE_OFF;
			break;
		}
	}
	/*todo:jeevan*/ 
	//implement for updating the Time meridian
	if(HOUR_COUNT_LIMIT == Hours_Counter_u8)
	{	
		/* if the time is set to 12 am/pm by externally 
				no need to shift the time meridian*/
		Meridian_Flag_b = true;	
	}
	return;
}


/***********************************************************************************************************************
* Function Name: Update_Time
* Description  : This function Updates the Minutes and Hours based on the interval timer interrupt.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Update_Time(void)
{
	//static uint16_t Second_Counter_u8 = 0;
	   
	//Set the Second counter for every 1 second.
	if(_1Second_Flag_b)
	{
		_1Second_Flag_b = false;
		Second_Counter_u8++;
	}
	else
	{
		;
	}
	
	/*Set the Minute counter for every 1 Minute (When second counter overflows)
	        	and resets the second counter to 0. */
	if(SEC_COUNT_LIMIT == Second_Counter_u8)
	{
		Second_Counter_u8 = 0;
		Minute_Counter_u8++;
		
		
	}
	else
	{
		;
	}
	
	/* Set the Hour counter for every 1 Hour (When Minute counter overflows )
			and resets the Minute counter to 0. */
	if(MIN_COUNT_LIMIT == Minute_Counter_u8)
	{
		Minute_Counter_u8 = 0;
		Hours_Counter_u8++;
		if(HOUR_COUNT_OVERFLOW == Hours_Counter_u8)
		{
			/* Starts the hourcounter from _1_OCLOCK after 12:59 */
			Hours_Counter_u8 = _1_OCLOCK;
			/* Reset the flag to shift the pm after 12 oclock*/
			Meridian_Flag_b = false;
		}
		if(HOUR_COUNT_LIMIT == Hours_Counter_u8 &&  Meridian_Flag_b == true)
		{
			Meridian_Flag_b = false;	
		}
	}
	else
	{
		;
	}
	
	
	if((HOUR_COUNT_LIMIT == Hours_Counter_u8) && (Meridian_Flag_b == false))
	{
		/* to avoid shiting the time meridian continously, when Hours_Counter_u16 = 12.
				Hours_Counter_u16 will be in 12 upto 1hour duration*/
		Meridian_Flag_b = true;
		/* Shift the time meridian after 11:59 */
		if(ClusterSignals_St.AMText_u8 == SOLID_TELLTALE || ClusterSignals_St.AMText_u8 == BLINK_TELLTALE)
		{
			ClusterSignals_St.AMText_u8 = TELLTALE_OFF;
			ClusterSignals_St.PMText_u8 = SOLID_TELLTALE;
		}
		else if( ( ClusterSignals_St.PMText_u8 == SOLID_TELLTALE ) || ( ClusterSignals_St.PMText_u8 == BLINK_TELLTALE ) )
		{
			ClusterSignals_St.AMText_u8 = SOLID_TELLTALE;
			ClusterSignals_St.PMText_u8 = TELLTALE_OFF;
		}
		else
		{
			ClusterSignals_St.AMText_u8 = SOLID_TELLTALE;
			ClusterSignals_St.PMText_u8 = TELLTALE_OFF;
		}
	}
	else
	{
		;
	}
	
	MinutesTime_u8 = Minute_Counter_u8;
	HoursTime_u8 = Hours_Counter_u8;
	
	return;
}

/********************************************************EOF***********************************************************/