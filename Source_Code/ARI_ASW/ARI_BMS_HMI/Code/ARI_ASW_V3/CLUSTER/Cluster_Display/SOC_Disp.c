/***********************************************************************************************************************
* File Name    : SOC_Disp.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 16/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "SOC_Disp.h"
#include "delay_flags.h"
#include "SegDispWrite.h"
#include "GenConfig.h"
#include "digits_utils.h"
#include "DataBank.h"
#include "Communicator.h"
#include "timer_user.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
uint32_t	     	SOC_BAR_Value_u32 	= 0;
uint32_t	     	SOC_Bars_u32 		= 0;
uint16_t	     	SOC_Bars_Count_u16 	= 0;
bool 	     	     	InitialSOC_Disp_b 	= false;
bool			RedBarDisp_b		= false;
SOC_BAR_POS_En_t     	SOC_BAR_POS_En 		= SOC_BAR_START_E;
Charge_State_En_t	Charge_State_En 	= CHARGING_END_E;
SOC_BarDispState_En_t	SOC_BarDispState_En	= SOC_GREEN_BAR_DISP_E;
CH_SOC_Disp_En_t	CH_SOC_Disp_En		= DISP_NONE_E;
bool 			InitialPercent_Disp_b   = false;


/***********************************************************************************************************************
* Function Name: Disp_BattPer
* Description  : The function displays the Battery percentage
* Arguments    : uint16_t SOC_Per_u16, ClusterSignals_En_t SOC_PER_E
* Return Value : none
***********************************************************************************************************************/
void Disp_BattPer(uint16_t SOC_Per_u16, ClusterSignals_En_t SOC_PER_E) 
{
	uint16_t	     ValidPercent_u16 	= 0;
	static uint16_t      PrevSOCper_u16 	= 0;
	
	SocPerDispState_En_t  SocPerDispState_En = SOC_PER_DISP_HOLD_E;
	
	
	if(PrevSOCper_u16 == SOC_Per_u16)
	{
		SocPerDispState_En = SOC_PER_DISP_HOLD_E;
		/*if present and previous SOC percentage is equal, No need to update the percentage data in HMI */
		if(false == InitialPercent_Disp_b)
		{
			/* If Percentage is updating First time after ignition , Updat the Percentage data*/
			SocPerDispState_En = SOC_PER_DISPLAY_E;
			InitialPercent_Disp_b = true;
		}
	}
	else
	{	/* If present and previous SOC percentage is not equal, Update the Percentage data*/
		SocPerDispState_En = SOC_PER_DISPLAY_E;
		PrevSOCper_u16 = SOC_Per_u16;
	}
	switch(SocPerDispState_En)
	{
		case SOC_PER_DISPLAY_E:
		{
			ValidPercent_u16 = ValidatePercentage(SOC_Per_u16);
			Write_SEG(SOC_PER_E, (uint32_t)ValidPercent_u16,DECIMAL); 
			break;
		}
		case SOC_PER_DISP_HOLD_E:
		{
			break;
		}
		default:
		{
			;	
		}
	}
	return;
}


/***********************************************************************************************************************
* Function Name: ValidatePercentage
* Description  : This function validates the SOC percentage value received.
* Arguments    : uint16_t  Percent_Check_u16
* Return Value : uint16_t  Percent_Check_u16
***********************************************************************************************************************/
uint16_t ValidatePercentage(uint16_t Percent_Check_u16)
{
	if(Percent_Check_u16 > MAX_SOC_PER)
	{
		/* if SOC percentage is greater than the MAX_SOC_PER, Re-assign the SOC percentage to MAX_SOC_PER */
		Percent_Check_u16 = MAX_SOC_PER;
	}
	else
	{
		;
	}
	return Percent_Check_u16;
}


/***********************************************************************************************************************
* Function Name: Display_SOC
* Description  : The function display the Battery percentage and SOC bars based on charging and discharging
* Arguments    : uint16_t SOC_u16, uint8_t BattStatus_u8, ClusterSignals_En_t SOC_BAR_E , ClusterSignals_En_t SOC_DIGIT_E
* Return Value : None
***********************************************************************************************************************/
void Display_SOC(uint16_t SOC_u16, uint8_t BattStatus_u8, ClusterSignals_En_t SOC_BAR_E , ClusterSignals_En_t SOC_DIGIT_E)
{
	Disp_BattPer( SOC_u16,  SOC_DIGIT_E);
	if(BattStatus_u8 == DISCHARGING_MODE)
	{
		/*Display SOC During Discharging Mode*/
		if((Charge_State_En == CHARGING_E) || (Charge_State_En == CHARGING_START_E))
		{
			/*If Battery is under charging, Change the Battery Charging state to CHARGING_END*/
			InitialSOC_Disp_b = false;
			Charge_State_En = CHARGING_END_E;
			//ClusterSignals_St.ChargingIndicator_u8 = 0;
			SET_CHARGER_PLUG_IN_STATE(0);
		}
		else
		{
			;
		}
		
		Display_DischargeSOC(SOC_u16,SOC_BAR_E); /*Display the Present SOC of the Battery-Pack during 
								Discharging-Mode*/
	}
	else
	{
		/*Display SOC During Charging Mode*/
		if((Charge_State_En == CHARGING_END_E)&&(SOC_u16 < MAX_SOC_RANGE))
		{
			/*Set the Battery charge state to CHARGING_START When the battery status is switched from 
				Discharging state to the Charging state and turn on the charging indicator*/
			Charge_State_En = CHARGING_START_E; 
			//ClusterSignals_St.ChargingIndicator_u8 = 3;
			SET_CHARGER_PLUG_IN_STATE(3);
		}
		else
		{
			;
		}
		
		if(SOC_BarDispState_En == SOC_RED_BAR_DISP_E)
		{
			/*During charging mode, set the SOC BAR display state to GREEN-BAR Display*/
			SOC_BarDispState_En = SOC_GREEN_BAR_DISP_E;
		}
		
		Display_ChargeSOC(SOC_u16,SOC_BAR_E); /*Display the Present SOC of the Battery-Pack during 
								Charging-Mode*/
	}
	return;
}



/***********************************************************************************************************************
* Function Name: Display_DischargeSOC
* Description  : This function Displays the Battery SOC Level during discharging-Mode.
* Arguments    : uint16_t SOC_u16, ClusterSignals_En_t SOC_BAR_E
* Return Value : None
***********************************************************************************************************************/
void Display_DischargeSOC(uint16_t SOC_u16, ClusterSignals_En_t SOC_BAR_E)
{
	uint16_t             Valid_SOC_u16 = 0;
	static uint16_t      PrevSOC_u16 = 0;
	
	SOC_DispState_En_t  	SOC_DispState_En   = SOC_DISP_HOLD_E; /*Initialzing SOC Display state to HOLD*/
	
	if(PrevSOC_u16 == SOC_u16)
	{
		/*If present SOC is equal to the Previous SOC, Set SOC Display state to Hold*/
		SOC_DispState_En = SOC_DISP_HOLD_E;
		
		if(false == InitialSOC_Disp_b)
		{
			/*If SOC is displaying for the first time (After the Ignition-ON), Set SOC Display 
				state to DISPLAY*/
			SOC_DispState_En = SOC_DISPLAY_E;
			InitialSOC_Disp_b = true; /*Set Initial SOC Display Flag*/
		}
		else if(true == RedBarDisp_b)
		{
			SOC_BarDispState_En = SOC_RED_BAR_DISP_E;
			/*Keep the SOC Display State to DISPLAY, when the Discharge SOC at RedZone*/
			SOC_DispState_En = SOC_DISPLAY_E;
		}
		else
		{
			;
		}
	}
	else
	{
		/*If the Present SOC is not equal to the Previous SOC, Set the SOC display state to DISPLAY*/
		SOC_DispState_En = SOC_DISPLAY_E;
		
		PrevSOC_u16 = SOC_u16;  /*Assign Present SOC value to the Previous SOC value*/
	}
	
	switch(SOC_DispState_En)
	{
		case SOC_DISPLAY_E:
		{
			Valid_SOC_u16 = Validate_SOC(SOC_u16); /*Validate the Battery SOC received*/
			
			/*Get the SOC Bars Value, to display the bars on the cluster corresponding to the SOC*/
			SOC_BAR_Value_u32 = Get_SOC_Bars(Valid_SOC_u16,(uint8_t)DISCHARGING_MODE);
			
			if(Valid_SOC_u16 >= SAFE_MODE_DISCHARGE_START)
			{
				SOC_BarDispState_En = SOC_GREEN_BAR_DISP_E;
				/*Display Discharge SOC during Safe Mode Discahege [GREEN-ZONE]*/
				Write_SEG(SOC_BAR_E,SOC_BAR_Value_u32,DECIMAL);
				RedBarDisp_b = false;
				SET_CHARGER_PLUG_IN_STATE(0);
				//ClusterSignals_St.ChargingIndicator_u8 = 0;
			}
			else
			{
				/*Displaying of SOC during discharge mode @ red-zone is handle by the Telltales Display
					module. [To blink the respective Red-Bar]*/
				SOC_BarDispState_En = SOC_RED_BAR_DISP_E;
				RedBarDisp_b = true;
				//ClusterSignals_St.ChargingIndicator_u8 = 1;
				SET_CHARGER_PLUG_IN_STATE(1);
			}
			break;
		}
		case SOC_DISP_HOLD_E:
		{
			break;
		}
		default:
		{
			;	
		}
	}
	
	return;
}



/***********************************************************************************************************************
* Function Name: Display_ChargeSOC
* Description  : This function Displays the Battery SOC Level during charging-state and animates the remaining bars in 
			increasing order.
* Arguments    : uint16_t SOC_u16, ClusterSignals_En_t SOC_BAR_E
* Return Value : None
***********************************************************************************************************************/
void Display_ChargeSOC(uint16_t SOC_u16, ClusterSignals_En_t SOC_BAR_E)
{
	if(Charge_State_En == CHARGING_START_E)
	{
		/*When the Battery state is switched from the Discharging state to the charging state,
			Get the SOC Bars and Equvalent Bars Value*/
		Get_Bar_PosAndValue(SOC_u16); 
	}
	else
	{
		;
	}
	
	if(ChargeBars_500ms_b) /*Condtion will be true, for evry 500 m-sec*/
	{
		ChargeBars_500ms_b = false;
		
		switch(SOC_BAR_POS_En)
		{
			case SOC_BAR_START_E: 
			{
				Write_SEG(SOC_BAR_E,SOC_Bars_u32,DECIMAL); /*Write the SEG Registers*/
				SOC_Bars_u32 += Find_Power_Of_10(SOC_BAR_START_E); /*Get the SOC bars value to display 
											the next bar*/
				SOC_BAR_POS_En = SOC_BAR_1_E; /*Change the present SOC bar position to the Next Bar*/
				
				break;
			}
			case SOC_BAR_1_E: 
			{
				Write_SEG(SOC_BAR_E,SOC_Bars_u32,DECIMAL);
				SOC_Bars_u32 += Find_Power_Of_10(SOC_BAR_1_E);
				SOC_BAR_POS_En = SOC_BAR_2_E;
				break;
			}
			case SOC_BAR_2_E: 
			{
				Write_SEG(SOC_BAR_E,SOC_Bars_u32,DECIMAL);
				SOC_Bars_u32 += Find_Power_Of_10(SOC_BAR_2_E);
				SOC_BAR_POS_En = SOC_BAR_3_E;
				break;
			}
			case SOC_BAR_3_E: 
			{
				Write_SEG(SOC_BAR_E,SOC_Bars_u32,DECIMAL);
				SOC_Bars_u32 += Find_Power_Of_10(SOC_BAR_3_E);
				SOC_BAR_POS_En = SOC_BAR_4_E;
				break;
			}
			case SOC_BAR_4_E: 
			{
				Write_SEG(SOC_BAR_E,SOC_Bars_u32,DECIMAL);
				SOC_Bars_u32 += Find_Power_Of_10(SOC_BAR_4_E);
				SOC_BAR_POS_En = SOC_BAR_5_E;
				break;
			}
			case SOC_BAR_5_E: 
			{
				Write_SEG(SOC_BAR_E,SOC_Bars_u32,DECIMAL);
				SOC_Bars_u32 += Find_Power_Of_10(SOC_BAR_5_E);
				SOC_BAR_POS_En = (SOC_BAR_POS_En_t)SOC_Bars_Count_u16;/*Reload the SOC Bar position*/
				SOC_Bars_u32 = SOC_BAR_Value_u32;/*Reload the SOC Bars Count*/
				Get_Bar_PosAndValue(SOC_u16); /*Refresh the SOC Bars value,
									if there is change in SOC of 20%*/
				break;
			}
			default:
			{
				;
			}
		}
	}
	else
	{
		;
	}
	
	if((SOC_u16 >= MAX_SOC_RANGE)&&(Charge_State_En == CHARGING_E))
	{
		/* When Battery SOC reaches its Maximum range (100%), Change the battery charging state to 
			CHARGING_END and turn off the Charging indicator */
		Charge_State_En = CHARGING_END_E;
		ClusterSignals_St.ChargingIndicator_u8 = 0;
	}
	else
	{
		;
	}
	
	return;
}



/***********************************************************************************************************************
* Function Name: Validate_SOC
* Description  : This function validates the SOC value received.
* Arguments    : uint16_t SOC_u16
* Return Value : uint16_t SOC_u16
***********************************************************************************************************************/
uint16_t    Validate_SOC(uint16_t SOC_u16)
{
	if(SOC_u16 > MAX_SOC_RANGE)
	{
		/*If received SOC is more than the Max SOC range (100%), then change the received SOC
			with the Maximum SOC range (100%)*/
		SOC_u16 = MAX_SOC_RANGE;
	}
	return 	SOC_u16;
}

/***********************************************************************************************************************
* Function Name: Get_SOC_Bars
* Description  : This function Converts SOC value into equivalen bars and gives the equivalent value for the
		 bars count.
* Arguments    : uint16_t Valid_SOC_u16, uint8_t BattStatus_u8
* Return Value : uint32_t SOC_Bars_Value_u32
***********************************************************************************************************************/
uint32_t    Get_SOC_Bars(uint16_t Valid_SOC_u16, uint8_t BattStatus_u8)
{
	uint32_t	SOC_Bars_Value_u32 = 0;
	float 		SOC_Bars_Count = 0;
	
	if(DISCHARGING_MODE == BattStatus_u8)
	{
		/*Get the SOC bars count during discharging mode*/
		
		/*
			BAR = SOC/BAR_RANGE
		*/
		SOC_Bars_Count = ((float)Valid_SOC_u16/EACH_BAR_SOC_RANGE);
		
		SOC_Bars_Count_u16 = SOC_Bars_Count;
		
		if(SOC_Bars_Count > SOC_Bars_Count_u16)
		{
			SOC_Bars_Count_u16+=1;
		}
		if(SOC_Bars_Count_u16 > TOTAL_SOC_BARS)
		{
			/*If the SOC Bars count calculated is more than the Max bars (10), then reset the SOC bars
				count to the Max Bars (10)*/
			SOC_Bars_Count_u16 = TOTAL_SOC_BARS;
		}
		else
		{
			;
		}
	}
	else	
	{
		/*Get the SOC bars count during charging mode*/
		SOC_Bars_Count_u16 = Valid_SOC_u16/EACH_BAR_SOC_RANGE;
	}
	
	SOC_Bars_Value_u32 = Get_Equi_Bars_Value(SOC_Bars_Count_u16); /*Get the Equivalent bars value corersponding 
									to the bars count*/
	
	return SOC_Bars_Value_u32;
}



/***********************************************************************************************************************
* Function Name: Get_Bar_PosAndValue
* Description  : This function during charging mode, calculates the SOC bars Count and equivalent Bars value when
		 there is a increase in  10% of SOC.
* Arguments    : uint16_t SOC_u16
* Return Value : None
***********************************************************************************************************************/
void Get_Bar_PosAndValue(uint16_t SOC_u16)
{
	uint16_t             Valid_SOC_u16 	= 0;
	static uint16_t      PrevSOC_u16 	= 0;
	
	if((SOC_u16 != PrevSOC_u16)||(Charge_State_En == CHARGING_START_E))
	{
		if((((SOC_u16/20) - (PrevSOC_u16/20)) != 0) || ((Charge_State_En == CHARGING_START_E)))
		{
			/*If present SOC is not equal to the Previous SOC and also Increase in 10% SOC*/
			
			Valid_SOC_u16 = Validate_SOC(SOC_u16); /*Validates the Received SOC, Wether it is within the MIN and MAX Range*/
			
			/*Get the SOC Bars count and Equivalent bars value*/
			SOC_BAR_Value_u32 = Get_SOC_Bars(Valid_SOC_u16,(uint8_t)CHARGING_MODE);
			
			SOC_BAR_POS_En = (SOC_BAR_POS_En_t)SOC_Bars_Count_u16; /*Assign SOC bars count as a SOC bar Position*/
			SOC_Bars_u32 = SOC_BAR_Value_u32;
			
			PrevSOC_u16 = SOC_u16; /*Assign present SOC value to the previous SOC value*/
			
			Charge_State_En = CHARGING_E; /*Set the Battery Charging state to CHARGING*/
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
	return;
}


/***********************************************************************************************************************
* Function Name: DispUSM_DischargeSOC
* Description  : This function displays the SOC at Unsafe discharging mode [RED-ZONE] by blinking the respective BAR.
* Arguments    : ClusterSignals_En_t SOC_BAR_E, uint32_t SOC_BAR_Value_u32, bool Red_Zone_Bar_b
* Return Value : None
***********************************************************************************************************************/
void DispUSM_DischargeSOC(ClusterSignals_En_t SOC_BAR_E, uint32_t SOC_BAR_Value_u32, bool Red_Zone_Bar_b)
{
	/*NOTE : Red_Zone_Bar_b flag is synchronized with the Telltales State flag, In-Order to blink the red bar
			in synchronization with the other tell-tales*/
			
	if(!Red_Zone_Bar_b)
	{
		/*Turn-On SOC Bars corresponding to the present SOC : 
				Include the Present SOC Bar*/
		Write_SEG(SOC_BAR_E,SOC_BAR_Value_u32,DECIMAL);
		Red_Zone_Bar_b = ON;
	}
	else
	{
		/*Turn-On SOC Bars corresponding to the present SOC :
			Exclude the Present SOC Bar*/
		switch(SOC_BAR_Value_u32)
		{
			case RED_BAR_1_VALUE:
			{
				SOC_BAR_Value_u32 -= 1;
				Write_SEG(SOC_BAR_E,SOC_BAR_Value_u32,DECIMAL);
				break;
			}
			case RED_BAR_2_VALUE:
			{
				SOC_BAR_Value_u32 -= 10;
				Write_SEG(SOC_BAR_E,SOC_BAR_Value_u32,DECIMAL);
				break;
			}
			default:
			{
				;
			}
		}
	}
	return;
}


/********************************************************EOF***********************************************************/