﻿#include "App_typedefs.h" 

#ifndef MCU_CAN_H
#define MCU_CAN_H

#define MCU_MSG_1_ST_T_SPEEDMODE_U8_MASK0  1U


#define MCU_MSG_2_ST_T_MOTORRPM_U16_MASK0  8U
#define MCU_MSG_2_ST_T_MOTORTEMPERATURE_U16_MASK0  8U


#define SIGNLE_READ_Mask0     0U    
#define SIGNLE_READ_Mask1     0x01U 
#define SIGNLE_READ_Mask2     0x03U 
#define SIGNLE_READ_Mask3     0x07U 
#define SIGNLE_READ_Mask4     0x0FU 
#define SIGNLE_READ_Mask5     0x1FU 
#define SIGNLE_READ_Mask6     0x3FU 
#define SIGNLE_READ_Mask7     0x7FU 
#define SIGNLE_READ_Mask8     0xFFU 


#define SIGNLE_WRITE_Mask0    0x80U 
#define SIGNLE_WRITE_Mask1    0xC0U 
#define SIGNLE_WRITE_Mask2    0xE0U 
#define SIGNLE_WRITE_Mask3    0xF0U 
#define SIGNLE_WRITE_Mask4    0xF8U 
#define SIGNLE_WRITE_Mask5    0xFCU 
#define SIGNLE_WRITE_Mask6    0xFEU 
#define SIGNLE_WRITE_Mask7    0xFFU 
/* def @MCU_MSG_1_ST_T CAN Message                                   (419377153) */
#define MCU_MSG_1_ST_T_ID                                            (419377153U)
#define MCU_MSG_1_ST_T_IDE                                           (1U)
#define MCU_MSG_1_ST_T_DLC                                           (1U)


#define MCU_MSG_1_ST_T_SPEEDMODE_U8FACTOR                                   (1)
#define MCU_MSG_1_ST_T_CANID_SPEEDMODE_U8_STARTBIT                           (49)
#define MCU_MSG_1_ST_T_CANID_SPEEDMODE_U8_OFFSET                             (0)
#define MCU_MSG_1_ST_T_CANID_SPEEDMODE_U8_MIN                                (0)
#define MCU_MSG_1_ST_T_CANID_SPEEDMODE_U8_MAX                                (0)


typedef struct
{
  uint8_t SpeedMode_u8;
}
MCU_MSG_1_St_t_t;


/* def @MCU_MSG_2_ST_T CAN Message                                   (419377152) */
#define MCU_MSG_2_ST_T_ID                                            (419377152U)
#define MCU_MSG_2_ST_T_IDE                                           (1U)
#define MCU_MSG_2_ST_T_DLC                                           (4U)


#define MCU_MSG_2_ST_T_MOTORRPM_U16FACTOR                                   (1)
#define MCU_MSG_2_ST_T_CANID_MOTORRPM_U16_STARTBIT                           (0)
#define MCU_MSG_2_ST_T_CANID_MOTORRPM_U16_OFFSET                             (0)
#define MCU_MSG_2_ST_T_CANID_MOTORRPM_U16_MIN                                (0)
#define MCU_MSG_2_ST_T_CANID_MOTORRPM_U16_MAX                                (65535)
#define MCU_MSG_2_ST_T_MOTORTEMPERATURE_U16FACTOR                                   (1)
#define MCU_MSG_2_ST_T_CANID_MOTORTEMPERATURE_U16_STARTBIT                           (32)
#define MCU_MSG_2_ST_T_CANID_MOTORTEMPERATURE_U16_OFFSET                             (0)
#define MCU_MSG_2_ST_T_CANID_MOTORTEMPERATURE_U16_MIN                                (0)
#define MCU_MSG_2_ST_T_CANID_MOTORTEMPERATURE_U16_MAX                                (65535)


typedef struct
{
  uint16_t MotorRPM_u16;
  uint16_t MotorTemperature_u16;
}
MCU_MSG_2_St_t_t;

 extern MCU_MSG_1_St_t_t  MCU_MSG_1_St_t;
 extern MCU_MSG_2_St_t_t  MCU_MSG_2_St_t;

 extern uint32_t Deserialize_MCU_MSG_1_St_t(MCU_MSG_1_St_t_t* message, const uint8_t* data);
 extern uint32_t Serialize_MCU_MSG_1_St_t(MCU_MSG_1_St_t_t* message, uint8_t* data);
 extern uint32_t Deserialize_MCU_MSG_2_St_t(MCU_MSG_2_St_t_t* message, const uint8_t* data);
 extern uint32_t Serialize_MCU_MSG_2_St_t(MCU_MSG_2_St_t_t* message, uint8_t* data);
 void ResetMCU_MSG_1_Data(void);
 void ResetMCU_MSG_2_Data(void);
 
 #endif
