/***********************************************************************************************************************
* File Name    : TelltaleDisp.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 08/11/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "TelltaleDisp.h"
#include "SegDispWrite.h"
#include "delay_flags.h"
#include "DataBank.h"
#include "SOC_Disp.h"
#include "PowerConsum_Disp.h"
#include "task_scheduler.h"
#include "Communicator.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/	


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
Telltales_Conf_St_t	Telltales_Conf_St[TOTAL_TELLTALE_E] = 
{
/*TelltaleSignal Enum  		Conf Sig name		Tell Frequency  	TelltaleOntime   Ontime Count       SignalValue  SignalPrevValue  SignalBlinkState */
	{TEXT_A_TT_E,   		TEXT_A_E, 				_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{TEXT_B_TT_E,  			TEXT_B_E, 				_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{LEFT_IND_TT_E, 		LEFT_IND_E, 			_500MS , 			_250MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{ENGINE_FLT_TT_E,  		ENGINE_FAULT_E, 		_3000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{WARNING_IND_TT_E,  	WARNING_IND_E,			_500MS , 			_250MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{KILL_SWITCH_TT_E,  	KILL_SWITCH_E, 			_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{MOTOR_FLT_TT_E,   		MOTOR_FAULT_E,			_3000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{SER_REM_TT_E,   		SERV_REM_E, 			_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{RIGHT_IND_TT_E,   		RIGHT_IND_E,			_500MS , 			_250MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{POWER_ICON_IND_TT_E,  	POWER_W_IND_E, 			_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{TOP_TEXT_IND_TT_E,  	TOP_TEXT_E,				_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{CH_STATE_IND_TT_E,  	CHARGING_STATUS_E, 		_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{NEUTRAL_IND_TT_E,   	NEUTRAL_MODE_E,			_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{ECO_MODE_TT_E,  		ECO_MODE_E, 			_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{SPORTS_MODE_TT_E,  	SPORTS_MODE_E,			_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{REVERSE_MODE_TT_E,  	REVERSE_MODE_E,			_300MS , 			_150MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{SIDE_STAND_TT_E,  		SIDE_STAND_E,			_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{NETWORK_TT_E,  		NETWORK_E,				_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{BLUETOOTH_TT_E,  		BLE_E,					_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{HIGH_BEAM_TT_E, 		HIGH_BEAM_E,			_1000MS, 			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{AM_TEXT_TT_E,  		TEXT_AM_E,				_500MS , 			_250MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{PM_TEXT_TT_E,  		TEXT_PM_E,				_500MS , 			_250MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{ODO_TEXT_TT_E,  		ODO_TEXT_E, 			_1000MS,			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{TRIP_TEXT_TT_E, 		TRIP_TEXT_E, 			_1000MS,			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{COMMON_TEXT_TT_E,  	COMMON_SIG_E, 			_1000MS,			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
	{TIME_COLON_TT_E, 		TIME_COLON_E, 			_1000MS,			_500MS,   		0x00,  			0x00U,	0x00U,		FALSE , FALSE},
};

/*
	Present SOC and PowerConsumption value is updated to the variables 
*/
uint32_t TellTale_5ms_Count_u32 = 0;
uint32_t SOC_BarsVal_u32 	= 0;
uint16_t PwrConsum_Bars_u16 	= 0;

bool InitialDispTellTales_b 	= false;


/***********************************************************************************************************************
* Function Name: Indicate_Telltales
* Description  : This function Turns-ON, Turs-OFF and Blinks the Telltales.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Indicate_Telltales(void)
{
	/*Update the Present Value of Telltale Signals*/
	Telltales_Conf_St[TEXT_A_TT_E].Telltale_Value_u8 		= ClusterSignals_St.AText_u8;
	Telltales_Conf_St[TEXT_B_TT_E].Telltale_Value_u8 		= ClusterSignals_St.BText_u8;
	Telltales_Conf_St[LEFT_IND_TT_E].Telltale_Value_u8 		= ClusterSignals_St.LeftIndicator_u8;
	Telltales_Conf_St[ENGINE_FLT_TT_E].Telltale_Value_u8 	= ClusterSignals_St.EngineFault_u8;
	Telltales_Conf_St[WARNING_IND_TT_E].Telltale_Value_u8 		= ClusterSignals_St.Warning_u8;
	Telltales_Conf_St[KILL_SWITCH_TT_E].Telltale_Value_u8 	= ClusterSignals_St.KillSwitch_u8;
	Telltales_Conf_St[MOTOR_FLT_TT_E].Telltale_Value_u8 	= ClusterSignals_St.MotorFault_u8;
	Telltales_Conf_St[SER_REM_TT_E].Telltale_Value_u8 		= ClusterSignals_St.ServiceReminder_u8;
	Telltales_Conf_St[RIGHT_IND_TT_E].Telltale_Value_u8 	= ClusterSignals_St.RightIndicator_u8;
	Telltales_Conf_St[POWER_ICON_IND_TT_E].Telltale_Value_u8 	= ClusterSignals_St.PowerIcon_u8;
	Telltales_Conf_St[TOP_TEXT_IND_TT_E].Telltale_Value_u8 		= ClusterSignals_St.TOPText_u8;
	Telltales_Conf_St[CH_STATE_IND_TT_E].Telltale_Value_u8 		= ClusterInput_St.ChargingStatus_u8;
	Telltales_Conf_St[NEUTRAL_IND_TT_E].Telltale_Value_u8 	= ClusterSignals_St.NeutralMode_u8;
	Telltales_Conf_St[ECO_MODE_TT_E].Telltale_Value_u8 		= ClusterSignals_St.EconomyMode_u8;
	Telltales_Conf_St[SPORTS_MODE_TT_E].Telltale_Value_u8 	= ClusterSignals_St.SportsMode_u8;
	Telltales_Conf_St[REVERSE_MODE_TT_E].Telltale_Value_u8 	= ClusterSignals_St.ReverseMode_u8;
	Telltales_Conf_St[SIDE_STAND_TT_E].Telltale_Value_u8 	= ClusterSignals_St.SideStand_u8;
	Telltales_Conf_St[NETWORK_TT_E].Telltale_Value_u8 		= ClusterSignals_St.NetworkConnection_u8;
	Telltales_Conf_St[BLUETOOTH_TT_E].Telltale_Value_u8 	= ClusterSignals_St.BLEIcon_u8;
	Telltales_Conf_St[HIGH_BEAM_TT_E].Telltale_Value_u8 	= ClusterSignals_St.HighBeam_u8;
	Telltales_Conf_St[AM_TEXT_TT_E].Telltale_Value_u8 		= ClusterSignals_St.AMText_u8;
	Telltales_Conf_St[PM_TEXT_TT_E].Telltale_Value_u8 		= ClusterSignals_St.PMText_u8;
	Telltales_Conf_St[ODO_TEXT_TT_E].Telltale_Value_u8 		= ClusterSignals_St.ODOText_u8;
	Telltales_Conf_St[TRIP_TEXT_TT_E].Telltale_Value_u8 	= ClusterSignals_St.TRIPText_u8;
	Telltales_Conf_St[TIME_COLON_TT_E].Telltale_Value_u8	= ClusterSignals_St.TimeColon_u8;
	Telltales_Conf_St[COMMON_TEXT_TT_E].Telltale_Value_u8 	= ClusterSignals_St.Commonsig_u8;

	TellTale_5ms_Count_u32++;
	
	Turn_ON_Telltales();	/*Turn-OFF the Telltales*/
	Turn_OFF_Telltales();  	/*Turn-ON the Telltales*/
	Blink_Telltales();      /*Blink the Telltales*/


	return;
}


/***********************************************************************************************************************
* Function Name: Turn_ON_Telltales
* Description  : This function Turns-ON the Telltales.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Turn_ON_Telltales(void)
{
	uint16_t Telltales_Count_u16 = 0; /*To count Total telltale signals*/
	
	for(Telltales_Count_u16 = 0; Telltales_Count_u16 < TOTAL_TELLTALE_E; Telltales_Count_u16++)
	{
		if((Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8 != \
			Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8) || (InitialDispTellTales_b == false))
		{
			if(TURN_ON == Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8)
			{
				Telltales_Conf_St[Telltales_Count_u16].TelltaleBlink_b = FALSE; /*Clear signal blink state*/
				
				/*Write_SEG(SignalEnum, ON);*/
				Write_SEG(((ClusterSignals_En_t)Telltales_Conf_St[Telltales_Count_u16].TellTaleConf_Sig_En),(uint32_t)ON);
				
				/*Signal Previous value is assigned with signal present value*/
				Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8 = \
				Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8;
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}
	}
	return;
}

/***********************************************************************************************************************
* Function Name: Turn_OFF_Telltales
* Description  : This function Turns-OFF the Telltales.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Turn_OFF_Telltales(void)
{
	uint16_t Telltales_Count_u16 = 0; /*To count Total telltale signals*/
	
	for(Telltales_Count_u16 = 0; Telltales_Count_u16 < TOTAL_TELLTALE_E; Telltales_Count_u16++)
	{
		if((Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8 != \
			Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8) || (InitialDispTellTales_b == false))
		{
			if(TURN_OFF == Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8)
			{
				// if( TRUE == Telltales_Conf_St[Telltales_Count_u16].TelltaleState_b)
				// {
				// 	Telltales_Conf_St[Telltales_Count_u16].TelltaleOntimeCount_u16 = CLEAR;
				// 	Telltales_Conf_St[Telltales_Count_u16].TelltaleState_b = FALSE;
				// }

				Telltales_Conf_St[Telltales_Count_u16].TelltaleBlink_b = FALSE; /*Clear signal blink state*/
				
				/*Write_SEG(SignalEnum, OFF);*/
				Write_SEG(((ClusterSignals_En_t)Telltales_Conf_St[Telltales_Count_u16].TellTaleConf_Sig_En),(uint32_t)OFF);
				
				/*Signal Previous value is assigned with signal present value*/
				Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8 = \
					Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8;
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}
	}
	
	return;
}


/***********************************************************************************************************************
* Function Name: Blink_Telltales
* Description  : This function Blinks the Telltales.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Blink_Telltales(void)
{
	uint16_t Telltales_Count_u16 = 0; /*To count Total telltale signals*/
	
	
	static bool TelltaleState_b  = OFF;
	
	/*Condition is true for every 500ms*/
	// if(Delay_Flag_500ms_b)
	// {
		for(Telltales_Count_u16 = 0; Telltales_Count_u16 < TOTAL_TELLTALE_E; Telltales_Count_u16++)
		{
			/*Condition is true, if Signal presen value is not equal to the signal previous value, or 
		          if Signal blink state is true*/
			if((Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8 != \
				Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8)|| \
					(TRUE == Telltales_Conf_St[Telltales_Count_u16].TelltaleBlink_b))
			{
				/*Condition is true, if Signal value is equal to blink value or if 
				  Signal blink state is true*/
				if((BLINK == Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8) || \
					(TRUE == Telltales_Conf_St[Telltales_Count_u16].TelltaleBlink_b))
				{
					
					 /*Set signal blink state*/
					Telltales_Conf_St[Telltales_Count_u16].TelltaleBlink_b = TRUE; 

					if(ZERO_E == (TaskSchedulerCounter_u32 % Telltales_Conf_St[Telltales_Count_u16].TelltaleBlinkFreq_u16))
					{
						 if( FALSE == Telltales_Conf_St[Telltales_Count_u16].TelltaleState_b )
						{
							/* Turn on the telltale and load the Ontime of the telltale*/
							Telltales_Conf_St[Telltales_Count_u16].TelltaleState_b = TRUE;
							Telltales_Conf_St[Telltales_Count_u16].TelltaleOntimeCount_u16 = Telltales_Conf_St[Telltales_Count_u16].TellatleOntime_u16;
							Write_SEG(((ClusterSignals_En_t)Telltales_Conf_St[Telltales_Count_u16].TellTaleConf_Sig_En),(uint32_t)ON);
						}
					}

					if(( TRUE == Telltales_Conf_St[Telltales_Count_u16].TelltaleState_b) && 
					   ( ZERO_E == Telltales_Conf_St[Telltales_Count_u16].TelltaleOntimeCount_u16 ))
					{
						/* When ON time count becomes zero Turn of the Tell Tale*/
						Telltales_Conf_St[Telltales_Count_u16].TelltaleState_b = FALSE;
						Write_SEG(((ClusterSignals_En_t)Telltales_Conf_St[Telltales_Count_u16].TellTaleConf_Sig_En),(uint32_t)OFF);
					}
					else if(TRUE == Telltales_Conf_St[Telltales_Count_u16].TelltaleState_b)
					{
						/* " Blink_Telltales() "" is 50 millisecond scheduler , So decrement the Ontime count by 50 */
						Telltales_Conf_St[Telltales_Count_u16].TelltaleOntimeCount_u16 -= _50MS;
					}
					else
					{
						;
					}
					
					// if(!TelltaleState_b)
					// {
					// 	/*Write_SEG(SignalEnum, ON);*/
					// 	Write_SEG(((ClusterSignals_En_t)Telltales_Conf_St[Telltales_Count_u16].Telltales_En),(uint32_t)ON);
					// }
					// else
					// {
					// 	/*Write_SEG(SignalEnum, OFF);*/
					// 	Write_SEG(((ClusterSignals_En_t)Telltales_Conf_St[Telltales_Count_u16].Telltales_En),(uint32_t)OFF);
					// }
					
					/*Signal Previous value is assigned with signal present value*/
					Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8 = \
					Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8;
				}
				else
				{
					;
				}
			}
		}
		
		/*
			To Blink the Red Bar of Powerconsumption  
		*/
		
		if(PC_BarDispState_En == PC_RED_BAR_DISP_E)
		{
			PwrConsum_Bars_u16 = Valid_PowerConsum_u16;
						/*Present updated valid  Power Consumption value is taken from the 
							PowerConsump display module*/
			DispUSM_PwrConsum(POWER_CONSUMP_E,PwrConsum_Bars_u16,TelltaleState_b);
						/*Display the RED-ZONE power consumption*/
		}
		else
		{
			;
		}
		
		if(Delay_Flag_500ms_b)
		{
			/*
				To Blink the Red Bar of %SOC  
			*/
			if(SOC_BarDispState_En == SOC_RED_BAR_DISP_E )
			{
				SOC_BarsVal_u32 = SOC_BAR_Value_u32;
							/*Present updated Equivalent bars value for the corresponding SOC is 
								taken from the SOC display Module*/
				DispUSM_DischargeSOC(BATT_SOC_BAR_E, SOC_BarsVal_u32, TelltaleState_b);
							/*Display the RED-ZONE Battery SOC*/	
			}
			else
			{
				;
			}
			Delay_Flag_500ms_b = false;
			/*To Blink, Switching of telltale state to ON and OFF*/
			if(TelltaleState_b)
			{
				TelltaleState_b = OFF;
			}
			else
			{
				TelltaleState_b = ON;
			}
		}
		
		
		
		
		//Delay_Flag_500ms_b = false;
	//}
//	else
//	{
//		;
//	}
	return;
}


/***********************************************************************************************************************
* Function Name: ClrTelltalesStates
* Description  : This function Clears the Telltales Previos value and telltales blink state.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ClrTelltalesStates(void)
{
	uint16_t TelltalesCount_u16 = 0;
	
	for(TelltalesCount_u16 = 0; TelltalesCount_u16 < TOTAL_TELLTALE_E; TelltalesCount_u16++)
	{
		Telltales_Conf_St[TelltalesCount_u16].Telltale_PrevValue_u8 	= CLEAR;
		Telltales_Conf_St[TelltalesCount_u16].TelltaleBlink_b 		= FALSE;
		Telltales_Conf_St[TelltalesCount_u16].Telltale_Value_u8		= CLEAR;
		Telltales_Conf_St[TelltalesCount_u16].TelltaleOntimeCount_u16 = CLEAR;
	}
	InitialDispTellTales_b 	= false;
	return;
}



/********************************************************EOF***********************************************************/