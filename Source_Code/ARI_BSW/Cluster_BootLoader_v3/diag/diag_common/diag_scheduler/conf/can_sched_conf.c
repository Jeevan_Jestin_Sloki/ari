/******************************************************************************
 *    FILENAME    : can_sched.c
 *    DESCRIPTION : description of a can scheduler
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki     18/01/2017		   Initial version
 ******************************************************************************
*/ 



/* Section: Included Files                                                   */
#include "can_sched.h"
#include "can_sched_conf.h"
#include "cil_can_conf.h"
#include "math_util.h"
#if(TRUE == DIAG_J1939_SUPPORTED)
	#include "j1939.h"
#endif
#if(TRUE == DIAG_UDS_SUPPORTED)

#endif
#if(TRUE == DIAG_OBD2_SUPPORTED)

#endif

/**************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

/* ************************************************************************** */

/*
 * @summary:- can structure message buffer  
 */


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */
#if(TRUE == DIAG_J1939_SUPPORTED)

uint32_t CAN_CONFIG_SCHED_ACTIVE_RX_u32;
uint32_t CAN_CONFIG_SCHED_ACTIVE_TX_u32;

#endif


/* ************************************************************************** */


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

#if(TRUE == DIAG_J1939_SUPPORTED)

BOOL IsRXCanMessageActive(UINT32 BitPosCANMessage)
{
    BOOL ret_b = (BOOL)FALSE;
    
    if(BitPosCANMessage == 0xFFFFU)
    {
        ret_b = (BOOL)TRUE;        
    }
    else 
    {            
        ret_b = GETBIT(CAN_CONFIG_SCHED_ACTIVE_RX_u32, BitPosCANMessage);
    }
    
    return ret_b;
}

BOOL IsTXCanMessageActive(UINT32 BitPosCANMessage)
{
    BOOL ret_b = (BOOL)FALSE;
    
    if(BitPosCANMessage == 0xFFFFU)
    {
        ret_b = (BOOL)TRUE;        
    }
    else 
    {    
        ret_b = (BOOL)GETBIT(CAN_CONFIG_SCHED_ACTIVE_TX_u32, BitPosCANMessage);
    }
    
    return ret_b;
}
#endif
/*  A brief description of a section can be given directly below the section
    banner.
 */

// *****************************************************************************


/*
 * @summary:-    array of can Application structures    
 */
const CANSCHED_RX_Conf_St_t   CANSCHED_RX_Conf_aSt[]=
{
#if((TRUE == DIAG_UDS_SUPPORTED) && (TRUE == DIAG_CANTP_SUPPORTED))
	// {CIL_CANTP_REQ_IVN_RX_E,	        PERIODICITY_MS(5),	  CANSched_RxMsgCallback,	NULL},  //
	{CIL_CANTP_REQ_TESTER_RX_E,   	    PERIODICITY_MS(5),	  CANSched_RxMsgCallback, 	NULL},  //
	// {CIL_CANTP_REQ_FUNC_RX_E,   	    PERIODICITY_MS(5),	  CANSched_RxMsgCallback, 	NULL},  //
#endif
// #if(TRUE == DIAG_OBD2_SUPPORTED)
// #if(TRUE == DIAG_CANTP_SUPPORTED)
// 	{CIL_CANTP_REQ_OBD_TESTER_RX_E,   	    PERIODICITY_MS(5),	  CANSched_RxMsgCallback, 	NULL},  //
// #endif
// #endif

// #if(TRUE == DIAG_J1939_SUPPORTED)
//     /*1 */    {CIL_J1939_REQ_DA_RX_E,     PERIODICITY_MS(20U),     J1939_reqPgnClbk ,     J1939_rqPgnClbk_timeout},
//     /*1 */    {CIL_J1939_REQ_DA_BAM_RX_E,     PERIODICITY_MS(20U),     J1939_reqPgnClbk ,     J1939_rqPgnClbk_timeout},
    
    
//     /*2 */    {CIL_J1939_TPCM_RX_E,       PERIODICITY_MS(20U),     J1939_tpcmClbk,        NULL},
//     /*3 */    {CIL_J1939_TPCM_BAM_RX_E,   PERIODICITY_MS(20U),     J1939_tpcmClbk,        NULL},
//     /*4 */    {CIL_J1939_TPDT_RX_E,       PERIODICITY_MS(20U),     J1939_tpdtClbk,        NULL},
//     /*5 */    {CIL_J1939_DM22_RX_E,       PERIODICITY_MS(20U),     J1939_dm22Clbk,        NULL},
//     /*6 */    {CIL_J1939_TPDT_BAM_RX_E,   PERIODICITY_MS(20U),     J1939_tpdtClbk,        NULL},
//     /*7 */    {CIL_J1939_81_NMAC_RX_E,     NO_TIMEOUT,     J1939_81_RXNMAC_Callback,     NULL},
// #endif
};


const CANSCHED_TX_Conf_St_t   CANSCHED_TX_Conf_aSt[]=
{
    // CIL Sig name,			Cycle Time					Offset (Timeslice)			Call Back Function     
    { CIL_TEST3_TX_E,			PERIODICITY_MS(500), 		PERIODICITY_MS(10),   		NULL}, // todo harsh
// #if(TRUE == DIAG_J1939_SUPPORTED)
// 	{CIL_J1939_71_TEST1_TX_E,     PERIODICITY_MS(500U),      PERIODICITY_MS(500U),	  J1939_71_TX_TEST1},
//     {CIL_J1939_71_TEST2_TX_E,     PERIODICITY_MS(500U),      PERIODICITY_MS(500U), 	  J1939_71_TX_TEST2},
//     {CIL_J1939_71_TEST3_TX_E,     PERIODICITY_MS(500U),      PERIODICITY_MS(500U), 	  J1939_71_TX_TEST3},
//     {CIL_j1939_tpdt_TX_E,         PERIODICITY_MS(60U),     PERIODICITY_MS(25U),       J1939_tp_txDtManage},
// #endif

};




/* *****************************************************************************
 End of File
 */
