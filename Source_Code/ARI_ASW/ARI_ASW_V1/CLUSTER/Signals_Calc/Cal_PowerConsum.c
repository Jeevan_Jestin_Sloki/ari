/***********************************************************************************************************************
* File Name    : Cal_PowerConsum.c
* Version      : 01
* Description  : This file contains the definitions of the export data and functions related to the Power Consumption
					Watts Calculation.
* Created By   : Dileepa B S
* Creation Date: 06/01/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cal_PowerConsum.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
static uint16_t PowerConsumBars_u16 = 0;


/***********************************************************************************************************************
* Function Name: CalculatePowerConsumBars
* Description  : This function calculates the Power Consumption Bars Count based on the Power Consumption Watts.
* Arguments    : uint16_t PowerConsumWatts_u16
* Return Value : None
***********************************************************************************************************************/
void CalculatePowerConsumBars(uint16_t PowerConsumWatts_u16)
{
	#if(POWERCONSUM_IN_WATTS == TRUE)
		PowerConsumBars_u16 = Get_PwrConsum_Bars(PowerConsumWatts_u16);
	#elif(POWERCONSUM_IN_WATTS == FALSE)
		PowerConsumBars_u16 = PowerConsumWatts_u16;
	#endif
	return;
}


/***********************************************************************************************************************
* Function Name: Get_PwrConsum_Bars
* Description  : This function Converts Power Consumption value into equivalen bars. 
* Arguments    : uint16_t Valid_PowerConsum_u16
* Return Value : uint16_t PwrConBars_u16
***********************************************************************************************************************/
uint16_t Get_PwrConsum_Bars(uint16_t Valid_PowerConsum_u16)
{
	float		PwrConBars_Count 	= 0;
	uint16_t	PwrConBars_u16 		= 0;
	
	PwrConBars_Count = ((float)Valid_PowerConsum_u16/EACH_BAR_WATTS_RANGE);
	PwrConBars_u16 = PwrConBars_Count;
	if(PwrConBars_Count > PwrConBars_u16)
	{
		PwrConBars_u16 += 1;
	}
	else
	{
		;
	}
	
	return PwrConBars_u16;
}



/***********************************************************************************************************************
* Function Name: Get_PowerConsum_Bars
* Description  : This function returns the Power Consumption Bars Count.
* Arguments    : None
* Return Value : uint16_t PowerConsumBars_u16
***********************************************************************************************************************/
uint16_t Get_PowerConsum_Bars(void)
{
	return PowerConsumBars_u16;
}


/********************************************************EOF***********************************************************/