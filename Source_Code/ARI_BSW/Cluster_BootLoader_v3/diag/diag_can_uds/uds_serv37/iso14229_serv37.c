/*
 * iso14229_serv37.c
 *
 *  Created on: 29-Jan-2020
 *      Author: Lakshmi
 */

/* ************************************************************************** */

/** Descriptive File Name

  @Company
 	 Sloki Technologies Pvt Ltd.

  	@File Name
    	iso14229_serv37.c

   @Summary
 	 This File contains macros, typedefs, extern definitions and
 	 user defined datatypes required by iso14229_serv37.c
 */
/* ************************************************************************** */


/* Section: Included Files                                                    */
/* ************************************************************************** */
#include <stdbool.h>
#include <stddef.h>
#include "iso14229_serv37.h"
/* ************************************************************************** */


/* ************************************************************************** */

/**
*  FUNCTION NAME : iso14229_serv37
*  FILENAME      : iso14229_serv37.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_37 requests
*  @return       : Type of response.
*/

UDS_Serv_resptype_En_t iso14229_serv37(UDS_Serv_St_t*  UDS_Serv_pSt){
	UDS_Serv_resptype_En_t serv_37_resp_En;
//    uint32_t address = 0x11000;
//    uint8_t value_u8[1024] = {0};
    uint8_t num_of_bytes_req_u8 = UDS_Serv_pSt->RxLen_u16;
    if((SERV_37_MAX_LEN < num_of_bytes_req_u8) && (SERV_37_MAX_LEN == 0x00)) {
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_TRANSFER_DATA_EXIST;
    	UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
    	UDS_Serv_pSt->TxLen_u16 = 3;
        serv_37_resp_En = UDS_SERV_RESP_NEG_E;
    }
    else if(!ISO14229_SA_L1_Gained_b) {
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_TRANSFER_DATA_EXIST;
    	UDS_Serv_pSt->TxBuff_pu8[2] = SECURITY_ACCESS_DENIED;
    	UDS_Serv_pSt->TxLen_u16 = 3;
        serv_37_resp_En = UDS_SERV_RESP_NEG_E;
    }
    else if(Flash_State_En != SERVICE36_E ) {
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_TRANSFER_DATA_EXIST;
    	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_SEQUENCE_ERR;
    	UDS_Serv_pSt->TxLen_u16 = 3;
    	serv_37_resp_En = UDS_SERV_RESP_NEG_E;
    }
    else if(serv34_memory_size_u32 != (NumOf_Bytes_Programmed_u32))
    {
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_TRANSFER_DATA_EXIST;
    	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_SEQUENCE_ERR;
    	UDS_Serv_pSt->TxLen_u16 = 3;
    	serv_37_resp_En = UDS_SERV_RESP_NEG_E;
    }
    else {

    	rx_block_seq_number_u8 = 0x00;
        serv_37_resp_En = UDS_SERV_RESP_POS_E;
        Flash_State_En = SERVICE37_E;
    }

    return serv_37_resp_En;
}


/* *****************************************************************************
 End of File
 */

