/***********************************************************************************************************************
* File Name    : Communicator.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 05/01/2022
***********************************************************************************************************************/

#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	
#include "App_typedefs.h"
#include "pfdl_user.h"

/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/

extern uint32_t PresentODOmeter_u32;
extern uint32_t PresentODO_mm_u32;
extern uint32_t SpeedPulseCounter_u32;
extern uint16_t Brightness_u16;
extern uint16_t PresentSessionWHkm_u16;
extern uint16_t PresentSessionRangeKm_u16;
extern uint16_t ActualSpeed_u16;
extern uint8_t  BatteryState_u8;
extern uint8_t  RegenState_u8; 
extern uint8_t SocPercentage_u8;
/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define FLASH_BLOCK_0                           0x00
#define ERASE_1_BLOCK                           0x01
#define FLASH_BLOCK0_POS                              0x00
#define NOMINAL_RANGEKM				170

#define GET_PRESENT_ODOMETER()					(PresentODOmeter_u32)
#define GET_PRESENT_ODO_MM()					(PresentODO_mm_u32)
#define GET_SPEED_PULSE_COUNT()                 (SpeedPulseCounter_u32)
#define GET_PRESENT_BRIGHTNESS()				(Brightness_u16)
#define GET_PRESENT_WHKM()                      (PresentSessionWHkm_u16)
#define GET_PRESENT_RANGEKM()                   (PresentSessionRangeKm_u16)
#define GET_VEHICLE_SPEED()                     (ActualSpeed_u16)
#define GET_SOC()                               (SocPercentage_u8)

#define SET_SPEED_PULSE_COUNT(x)                (SpeedPulseCounter_u32 = x)
#define SET_PRESENT_ODOMETER(x)					(PresentODOmeter_u32 = x)
#define SET_PRESENT_ODO_MM(x)					(PresentODO_mm_u32 = x)
#define SET_PRESENT_BRIGHTNESS(x)				(Brightness_u16 = x)
#define SET_PRESENT_WHKM(x)                     (PresentSessionWHkm_u16 = x)
#define SET_PRESENT_RANGEKM(x)                  (PresentSessionRangeKm_u16 = x)
#define SET_VIHICLE_SPEED(x)                    (ActualSpeed_u16 = x)
#define SET_SOC(x)                              (SocPercentage_u8 = x)
/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
#pragma pack
typedef struct 
{
    uint32_t OdoMeter_u32;
    uint32_t UnusedOdoMilliMeter_u32;
    uint16_t PreviousSessionWHkm_u16;
    uint16_t PreviousRangeKm_u16;
    uint8_t  BackLightBrightness_u8;
}FlashBlock0Data_St_t;
#pragma unpack
/***********************************************************************************************************************
Export Variable
***********************************************************************************************************************/
extern FlashBlock0Data_St_t FlashBlock0Data_St;

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void UploadDataToFlash(void);
extern void DownloadDataFromFlash(void);
extern void ResetCommunicatorData(void);

#endif /* COMMUNICATOR_H */


