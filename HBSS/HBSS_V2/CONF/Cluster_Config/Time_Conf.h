/***********************************************************************************************************************
* File Name    : Time_Conf.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 28/11/2021
***********************************************************************************************************************/

#ifndef TIME_CONF_H
#define TIME_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
/*
				Minute time digit
*/

//extern  const SignalsValue_St_t		MinutesTime_SignalsValue_aSt[MINUTES_TIME_SIG_LEN];
extern  const SignalsValue_St_t		MinutesTimeHB_SignalsValue_aSt[MINUTE_HBLB_SIG_LEN];
extern  const SignalsValue_St_t		MinutesTimeLB_SignalsValue_aSt[MINUTE_HBLB_SIG_LEN];

extern  const SignalConfig_St_t		MinutesTime_Sig1Conf_ast[SIXTEEN_E];
extern  const SignalConfig_St_t		MinutesTime_Sig2Conf_ast[SIXTEEN_E];

extern  const SegConfig_St_t			MinutesTime_S1_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_Off_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_A_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_B_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_C_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_D_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_E_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_F_SegConf_aSt[TWO_SEG_E];

extern  const SegConfig_St_t			MinutesTime_S2_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_Off_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_A_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_B_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_C_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_D_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_E_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_F_SegConf_aSt[TWO_SEG_E];

/*
			Hours time digit
*/


extern  const SignalsValue_St_t		HoursTime_SignalsValue_aSt[HOURS_TIME_SIG_LEN];

extern  const SignalConfig_St_t		HoursTime_Sig1Conf_ast[TWELVE_E];
extern  const SignalConfig_St_t		HoursTime_Sig2Conf_ast[TWO_E];

extern  const SegConfig_St_t			HoursTime_S1_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_Off_SegConf_aSt[TWO_SEG_E];
extern const  SegConfig_St_t			HoursTime_S1_T_SegConf_aSt[TWO_SEG_E];

extern  const SegConfig_St_t			HoursTime_S2_ON_SegConf_aSt[ONE_E];
extern  const SegConfig_St_t			HoursTime_S2_OFF_SegConf_aSt[ONE_E];


/*
  			 Time COLON teltail Configuration 
*/
extern  const SignalsValue_St_t		Colon_SignalsValue_aSt[TIME_COLON_SIG_LEN];

extern  const SignalConfig_St_t		ColonSigConf_ast[TWO_E];

extern  const SegConfig_St_t			Colon_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			Colon_Off_SegConf_aSt[ONE_SEG_E];

/*
 			Ante/Post Meridiem teltail Congfigration
*/

extern  const SignalsValue_St_t		Text_AM_SignalsValue_aSt[TEXT_AM_SIG_LEN]; 

extern  const SignalConfig_St_t		AM_SigConf_ast[TWO_E];

extern  const SegConfig_St_t 			AM_off_Segconf_ast[ONE_SEG_E];
extern  const SegConfig_St_t 			AM_on_Segconf_ast[ONE_SEG_E];

extern  const SignalsValue_St_t		Text_PM_SignalsValue_aSt[TEXT_PM_SIG_LEN];

extern  const SignalConfig_St_t		PM_SigConf_ast[TWO_E];

extern  const SegConfig_St_t 			PM_off_Segconf_ast[ONE_SEG_E];
extern  const SegConfig_St_t 			PM_on_Segconf_ast[ONE_SEG_E];



#endif /* TIME_CONF_H */


