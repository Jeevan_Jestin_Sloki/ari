/***********************************************************************************************************************
* File Name    : ODO_Conf.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 03/12/2021
***********************************************************************************************************************/

#ifndef ODO_CONF_H
#define ODO_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
/*
			ODO text configration
*/

extern  const SignalsValue_St_t		ODO_TEXT_SignalsValue_aSt[ODO_TEXT_SIG_LEN];

extern  const SignalConfig_St_t		ODO_Text_sigconf_ast[TWO_E];

extern  const SegConfig_St_t			ODO_Texton_SegConf_ast[ONE_SEG_E];
extern  const SegConfig_St_t			ODO_Textoff_SegConf_ast[ONE_SEG_E];


/*
		TRIP text configration
*/

extern  const SignalsValue_St_t		TRIP_Text_SignalsValue_aSt[TRIP_TEXT_SIG_LEN];

extern  const SignalConfig_St_t		Trip_Text_SigConf_ast[TWO_E];

extern  const SegConfig_St_t			Trip_Texton_SegConf_ast[ONE_SEG_E];
extern  const SegConfig_St_t			Trip_Textoff_SegConf_ast[ONE_SEG_E];
/*
		 Text A configration
*/
extern const SignalsValue_St_t		Text_A_SignalsValue_aSt[TEXT_A_SIG_LEN];

extern  const SignalConfig_St_t		Text_A_SigConf_ast[TWO_E];

extern  const SegConfig_St_t			Text_Aon_SegConf_ast[ONE_SEG_E];
extern  const SegConfig_St_t			Text_Aoff_SegConf_ast[ONE_SEG_E];

/*
		 Text B configration
*/

extern const SignalsValue_St_t		Text_B_SignalsValue_aSt[TEXT_B_SIG_LEN];

extern  const SignalConfig_St_t		Text_B_SigConf_ast[TWO_E];

extern  const SegConfig_St_t			Text_Bon_SegConf_ast[ONE_SEG_E];
extern  const SegConfig_St_t			Text_Boff_SegConf_ast[ONE_SEG_E];


/*
		ODO digits  configration
*/
extern  const SignalsValue_St_t		ODO_SignalsValue_aSt[ODO_SIG_LEN];


extern  const SignalConfig_St_t		ODOSig1Conf_ast[ELEVEN_E];
extern  const SignalConfig_St_t		ODOSig2Conf_ast[ELEVEN_E];
extern  const SignalConfig_St_t		ODOSig3Conf_ast[ELEVEN_E];
extern  const SignalConfig_St_t		ODOSig4Conf_ast[ELEVEN_E];
extern  const SignalConfig_St_t		ODOSig5Conf_ast[ELEVEN_E];
extern  const SignalConfig_St_t		ODOSig6Conf_ast[ELEVEN_E];


/*
				Digit 1 Configration
*/

extern  const SegConfig_St_t			ODO_S1_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S1_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S1_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S1_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S1_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S1_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S1_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S1_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S1_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S1_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S1_off_SegConf_aSt[TWO_SEG_E];   


/*
				Digit 2 Configration
*/

extern  const SegConfig_St_t			ODO_S2_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S2_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S2_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S2_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S2_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S2_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S2_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S2_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S2_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S2_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S2_off_SegConf_aSt[TWO_SEG_E];

/*
				Digit 3 Configration
*/

extern  const SegConfig_St_t			ODO_S3_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S3_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S3_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S3_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S3_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S3_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S3_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S3_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S3_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S3_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S3_off_SegConf_aSt[TWO_SEG_E];

/*
				Digit 4 Configration
*/

extern  const SegConfig_St_t			ODO_S4_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S4_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S4_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S4_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S4_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S4_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S4_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S4_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S4_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S4_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S4_off_SegConf_aSt[TWO_SEG_E];

/*
				Digit 5 Configration
*/

extern  const SegConfig_St_t			ODO_S5_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S5_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S5_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S5_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S5_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S5_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S5_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S5_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S5_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S5_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S5_off_SegConf_aSt[TWO_SEG_E];

/*
				Digit 6 Configration
*/

extern  const SegConfig_St_t			ODO_S6_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S6_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S6_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S6_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S6_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S6_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S6_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S6_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S6_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S6_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			ODO_S6_off_SegConf_aSt[TWO_SEG_E];


#endif /* ODO_CONF_H */


