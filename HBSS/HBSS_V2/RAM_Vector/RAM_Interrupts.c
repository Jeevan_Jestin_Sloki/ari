/***********************************************************************************************************************
* File Name    : RAM_Interrupts.c
* Version      : 01
* Description  : This file contains the interrupts which points to RAM ISR.
* Created By   : Sandeep K Y
* Creation Date: 01/02/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 
#include "RAM_Vector.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
#pragma interrupt 	Watchdog_Interrupt(vect=INTWDTI)
#pragma interrupt 	INTLVI_Interrupt(vect=INTLVI)
#pragma interrupt 	INTP0_Interrupt(vect=INTP0)
#pragma interrupt 	INTP1_Interrupt(vect=INTP1)
#pragma interrupt 	INTP2_Interrupt(vect=INTP2)
#pragma interrupt 	INTP3_Interrupt(vect=INTP3)
#pragma interrupt 	INTP4_Interrupt(vect=INTP4)
#pragma interrupt 	INTP5_Interrupt(vect=INTP5)
#pragma interrupt 	INTCLM_Interrupt(vect=INTCLM)
#pragma interrupt 	INTCSI00_Interrupt(vect=INTCSI00)
#pragma interrupt 	INTCSI01_Interrupt(vect=INTCSI01)
#pragma interrupt 	INTDMA0_Interrupt(vect=INTDMA0)
#pragma interrupt 	INTDMA1_Interrupt(vect=INTDMA1)
#pragma interrupt 	INTRTC_Interrupt(vect=INTRTC)
#pragma interrupt 	INTIT_Interrupt(vect=INTIT)
#pragma interrupt 	INTLT0_Interrupt(vect=INTLT0)
#pragma interrupt 	INTLR0_Interrupt(vect=INTLR0)
#pragma interrupt 	INTLS0_Interrupt(vect=INTLS0)
#pragma interrupt 	INTSG_Interrupt(vect=INTSG)
#pragma interrupt 	Timer_Interrupt(vect=INTTM00)
#pragma interrupt 	INTTM01_Interrupt(vect=INTTM01)
#pragma interrupt 	INTTM02_Interrupt(vect=INTTM02)
#pragma interrupt 	INTTM03_Interrupt(vect=INTTM03)
#pragma interrupt 	INTAD_Interrupt(vect=INTAD)
#pragma interrupt 	INTLT1_Interrupt(vect=INTLT1)
#pragma interrupt 	INTLR1_Interrupt(vect=INTLR1)
#pragma interrupt 	INTLS1_Interrupt(vect=INTLS1)
#pragma interrupt 	INTPLR1_Interrupt(vect=INTPLR1)
#pragma interrupt 	INTIIC11_Interrupt(vect=INTIIC11)
#pragma interrupt 	INTTM04_Interrupt(vect=INTTM04)
#pragma interrupt 	INTTM05_Interrupt(vect=INTTM05)
#pragma interrupt 	INTTM06_Interrupt(vect=INTTM06)
#pragma interrupt 	INTTM07_Interrupt(vect=INTTM07)
#pragma interrupt 	INTC0ERR_Interrupt(vect=INTC0ERR)
#pragma interrupt 	INTC0WUP_Interrupt(vect=INTC0WUP)
#pragma interrupt 	INTC0REC_Interrupt(vect=INTC0REC)
#pragma interrupt 	INTC0TRX_Interrupt(vect=INTC0TRX)
#pragma interrupt 	INTTM10_Interrupt(vect=INTTM10)
#pragma interrupt 	INTTM11_Interrupt(vect=INTTM11)
#pragma interrupt 	INTTM12_Interrupt(vect=INTTM12)
#pragma interrupt 	INTTM13_Interrupt(vect=INTTM13)
#pragma interrupt 	INTMD_Interrupt(vect=INTMD)
#pragma interrupt 	INTFL_Interrupt(vect=INTFL)
#pragma interrupt 	INTTM14_Interrupt(vect=INTTM14)
#pragma interrupt 	INTTM15_Interrupt(vect=INTTM15)
#pragma interrupt 	INTTM16_Interrupt(vect=INTTM16)
#pragma interrupt 	INTTM17_Interrupt(vect=INTTM17)
#pragma interrupt 	INTTM20_Interrupt(vect=INTTM20)
#pragma interrupt 	INTTM21_Interrupt(vect=INTTM21)
#pragma interrupt 	INTTM22_Interrupt(vect=INTTM22)
#pragma interrupt 	INTTM23_Interrupt(vect=INTTM23)
#pragma interrupt 	INTTM24_Interrupt(vect=INTTM24)
#pragma interrupt 	INTTM26_Interrupt(vect=INTTM26)


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

static void __near Watchdog_Interrupt(void)
{
	
    (*RAM_INTWDTI_ISR)();
    
}

static void __near INTLVI_Interrupt(void)
{
	
    (*RAM_INTLVI_ISR)();
    
}

static void __near INTP0_Interrupt(void)
{
	
    (*RAM_INTP0_ISR)();
    
}
static void __near INTP1_Interrupt(void)
{
	
    (*RAM_INTP1_ISR)();
    
}
static void __near INTP2_Interrupt(void)
{
    
    (*RAM_INTP2_ISR)();
    
}
static void __near INTP3_Interrupt(void)
{
    
    (*RAM_INTP3_ISR)();
    
}
static void __near INTP4_Interrupt(void)
{
    
    (*RAM_INTP4_ISR)();
    
}
static void __near INTP5_Interrupt(void)
{
    
    (*RAM_INTP5_ISR)();
    
}
static void __near INTCLM_Interrupt(void)
{
    
    (*RAM_INTCLM_ISR)();
    
}
static void __near INTCSI00_Interrupt(void)
{
    
    (*RAM_INTCSI00_ISR)();
    
}
static void __near INTCSI01_Interrupt(void)
{
    
    (*RAM_INTCSI01_ISR)();
    
}
static void __near INTDMA0_Interrupt(void)
{
    
    (*RAM_INTDMA0_ISR)();
    
}
static void __near INTDMA1_Interrupt(void)
{
    
    (*RAM_INTDMA1_ISR)();
    
}
static void __near INTRTC_Interrupt(void)
{
    
    (*RAM_INTRTC_ISR)();
    
}
static void __near INTIT_Interrupt(void)
{
    
    (*RAM_INTIT_ISR)();
    
}
static void __near INTLT0_Interrupt(void)
{
    
    (*RAM_INTLT0_ISR)();
    
}
static void __near INTLR0_Interrupt(void)
{
    
    (*RAM_INTLR0_ISR)();
    
}
static void __near INTLS0_Interrupt(void)
{
    
    (*RAM_INTLS0_ISR)();
    
}
static void __near INTSG_Interrupt(void)
{
    
    (*RAM_INTSG_ISR)();
    
}
static void __near Timer_Interrupt(void)
{
    
    (*RAM_INTTM00_ISR)();
    
}
static void __near INTTM01_Interrupt(void)
{
    
    (*RAM_INTTM01_ISR)();
    
}
static void __near INTTM02_Interrupt(void)
{
    
    (*RAM_INTTM02_ISR)();
    
}
static void __near INTTM03_Interrupt(void)
{
    
    (*RAM_INTTM03_ISR)();
    
}
static void __near INTAD_Interrupt(void)
{
    
    (*RAM_INTAD_ISR)();
    
}
static void __near INTLT1_Interrupt(void)
{
    
    (*RAM_INTLT1_ISR)();
    
}
static void __near INTLR1_Interrupt(void)
{
    
    (*RAM_INTLR1_ISR)();
    
}
static void __near INTLS1_Interrupt(void)
{
    
    (*RAM_INTLS1_ISR)();
    
}
static void __near INTPLR1_Interrupt(void)
{
    
    (*RAM_INTPLR1_ISR)();
    
}
static void __near INTIIC11_Interrupt(void)
{
    
    (*RAM_INTIIC11_ISR)();
    
}
static void __near INTTM04_Interrupt(void)
{
    
    (*RAM_INTTM04_ISR)();
    
}
static void __near INTTM05_Interrupt(void)
{
    
    (*RAM_INTTM05_ISR)();
    
}
static void __near INTTM06_Interrupt(void)
{
    
    (*RAM_INTTM06_ISR)();
    
}
static void __near INTTM07_Interrupt(void)
{
    
    (*RAM_INTTM07_ISR)();
    
}
static void __near INTC0ERR_Interrupt(void)
{
    
    (*RAM_INTC0ERR_ISR)();
    
}
static void __near INTC0WUP_Interrupt(void)
{
    
    (*RAM_INTC0WUP_ISR)();
    
}
static void __near INTC0REC_Interrupt(void)
{
    
    (*RAM_INTC0REC_ISR)();
    
}
static void __near INTC0TRX_Interrupt(void)
{
    
    (*RAM_INTC0TRX_ISR)();
    
}
static void __near INTTM10_Interrupt(void)
{
    
    (*RAM_INTTM10_ISR)();
    
}
static void __near INTTM11_Interrupt(void)
{
    
    (*RAM_INTTM11_ISR)();
    
}
static void __near INTTM12_Interrupt(void)
{
    
    (*RAM_INTTM12_ISR)();
    
}
static void __near INTTM13_Interrupt(void)
{
    
    (*RAM_INTTM13_ISR)();
    
}
static void __near INTMD_Interrupt(void)
{
    
    (*RAM_INTMD_ISR)();
    
}
static void __near INTFL_Interrupt(void)
{
    
    (*RAM_INTFL_ISR)();
    
}
static void __near INTTM14_Interrupt(void)
{
    
    (*RAM_INTTM14_ISR)();
    
}
static void __near INTTM15_Interrupt(void)
{
    
    (*RAM_INTTM15_ISR)();
    
}
static void __near INTTM16_Interrupt(void)
{
    
    (*RAM_INTTM16_ISR)();
    
}
static void __near INTTM17_Interrupt(void)
{
    
    (*RAM_INTTM17_ISR)();
    
}
static void __near INTTM20_Interrupt(void)
{
    
    (*RAM_INTTM20_ISR)();
    
}
static void __near INTTM21_Interrupt(void)
{
    
    (*RAM_INTTM21_ISR)();
    
}

static void __near INTTM22_Interrupt(void)
{
    
    (*RAM_INTTM22_ISR)();
    
}
static void __near INTTM23_Interrupt(void)
{
    
    (*RAM_INTTM23_ISR)();
    
}

static void __near INTTM24_Interrupt(void)
{
    
    (*RAM_INTTM24_ISR)();
    
}
static void __near INTTM26_Interrupt(void)
{
    
    (*RAM_INTTM26_ISR)();
    
}


/********************************************************EOF***********************************************************/
