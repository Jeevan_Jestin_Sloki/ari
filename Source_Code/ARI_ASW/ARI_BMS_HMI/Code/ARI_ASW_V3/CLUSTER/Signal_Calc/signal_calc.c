/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : signal_calc.h
|    Project        : Ohm Automotive.
|    Description    : The file contains main function for the s
                        ignal vale calculation.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date             Name                      Company
| --------     ---------------------     ---------------------------------------
| 15/12/2022     Jeevan Jestin N           Sloki Software Technologies LLP.
|-------------------------------------------------------------------------------
|******************************************************************************/


/*******************************************************************************
 *  HEADER FILE INCLUDES
 ******************************************************************************/
#include"signal_calc.h"
#include "ari_bms_can.h"
#include "cluster_main.h"
#include"TimeDisp.h"
#include "hmi_config_can.h"
#include"ODO_Calc.h"
#include "pfdl_user.h"
#include "Speed_Calc.h"
#include"Range_WhKmCalc.h"
#include "ari_bms_can.h"
#include "Communicator.h"
/*******************************************************************************
 *  MACRO DEFINITION
 ******************************************************************************/
#define TEMPERATURE_THRSHLD     137
#define _2_SEC                  2000 //2000 milli sec
#define ODO_DISP    0x00
#define TRIP_A_DISP 0x01
#define TRIP_B_DISP 0x02
/*******************************************************************************
 *  GLOBAL VARIABLES DEFNITION 
 ******************************************************************************/
uint32_t Time_Seg_Holder_Time_u32 = 0;
uint8_t Temperature_u8 = 0;
bool Temper_Disp_Inprog = false;
uint8_t Tempt_Thrshld_u8 = 0;
Flash_Param_En_t Flash_Param_En = FLASH_NONE_E;
FlashParam_St_t FlashParam_St;
uint8_t FlashBuff_au8[sizeof(FlashParam_St_t)];
bool BattCharging_b = false;
/*******************************************************************************
 *  STRUCTURE AND ENUM DEFNITION 
 ******************************************************************************/
Time_SegHolder_En_t Time_SegHolder_En = TIME_SEG_HOLD_NONE_E;
/*******************************************************************************
 *  STATIC FUNCTION PROTOTYPES
 ******************************************************************************/
void Time_Seg_Holder(void);
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Signal_Calc
*   Description   : The function calculate the HMI signal value
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void Signal_Calc(void)
{

    Calculate_ODOmeter();
    Calculate_VehicleSpeed();
    
    
    
    
    UpdateTime();

    if((true == Volt_Recv_b) &&(true == Ah_Recv_b))
    {
//	    if(0 == (GET_TIME_MS()%5000))
//	    {
//		    PresentODOmeter_u32 += 100;
//	    }
        EstimateWhKm(BMS_INFO_MSG_1_St.Vol_Bat,BMS_INFO_MSG_1_St.Display_SOC,BMS_INFO_MSG_5_St.Pack_TotalCap);
        UpdateRangeKm(BMS_INFO_MSG_1_St.Vol_Bat,BMS_INFO_MSG_1_St.Display_SOC,BMS_INFO_MSG_5_St.Pack_TotalCap);
    }
   
   Time_Seg_Holder();

   if((1 == BMS_INFO_MSG_2_St.BMS_ChargeStatus)&&(false == BattCharging_b))
   {
        SET_BATTPACK_STATUS(1);
        if(false == BattCharging_b)
        {
            BattCharging_b = true;
        }
   }
   else if((1 != BMS_INFO_MSG_2_St.BMS_ChargeStatus)&&(true == BattCharging_b))
   {
        SET_BATTPACK_STATUS(0);
        BattCharging_b = false;
        WhEstimateOnstart_b = false;
   }
   else
   {

   }


    //WhEstimateOnstart_b

//    Temperature_u8 = HMI_MSG_3_St.Temprature_u8;

//    if(true == Set_Tempt_Thrsld)
//    {
//        Set_Tempt_Thrsld = false;
//        Tempt_Thrshld_u8 = Get_TemptThreshold();
//        StoreFlashParam(FLASH_TEMPERATURE);
//    }

//    if(true == Reset_Trip_A)
//    {
//        Reset_Trip_A = false;
//        ResetTripA(ODO_Resetvalue_u32);
//        StoreFlashParam(FLASH_TRIP_A);
//    }
//    if(true == Reset_Trip_B)
//    {
//        Reset_Trip_B = false;
//        ResetTripB(ODO_Resetvalue_u32);
//        StoreFlashParam(FLASH_TRIP_B);
//    }
//	Calculate_ODO(HMI_MSG_2_St.ODO_u32);
    

//    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Signal_Calc
*   Description   : The function calculate the HMI signal value
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void Time_Seg_Holder(void)
{
   if( 0 != BMS_INFO_MSG_3_St.Alarm_Code)
   {
       if(false == Temper_Disp_Inprog)
       {
           Temper_Disp_Inprog = true;
           SET_BATT_FAULT_STATE(1);
           Time_Seg_Holder_Time_u32 = GET_TIME_MS();
       }
       else
       {
           if(GET_TIME_MS() > (Time_Seg_Holder_Time_u32+_2_SEC))
           {
               if(DISPLAY_TIME_E == Time_Disp_State_En)
               {
                   Time_Disp_State_En = DISPLAY_TEMP_E;
                   Time_Seg_Holder_Time_u32 = GET_TIME_MS();
                   Clear_Time_InitFlags();
               }
               else
               {
                   Time_Disp_State_En = DISPLAY_TIME_E;
                   Time_Seg_Holder_Time_u32 = GET_TIME_MS();
                   Clear_Time_InitFlags();

               }

           }
       }

   }
   else
   {
       if(true == Temper_Disp_Inprog)
       {
           Temper_Disp_Inprog = false;
           Time_Disp_State_En = DISPLAY_TIME_E;
           SET_BATT_FAULT_STATE(0);
           Clear_Time_InitFlags();
       }  
   }

   return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : ODOtoDisp
*   Description   : The function calculate the HMI signal value
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
//uint32_t ODOtoDisp(void)
//{
//    uint32_t Disp_ODOmeter_u32 = 0;

//    switch(HMI_MSG_2_St.Trip_Meter)
//    {
//        case ODO_DISP:
//        {
//            Disp_ODOmeter_u32 = HMI_MSG_2_St.ODO_u32;
//            break;   
//        }
//        case TRIP_A_DISP:
//        {
//            Disp_ODOmeter_u32 = Get_TripA();
//            break;
//        }
//        case TRIP_B_DISP:
//        {
//            Disp_ODOmeter_u32 = Get_TripB();
//            break;
//        }
//        default:
//        {

//        }
//    }
//    return Disp_ODOmeter_u32;
//}

void UpdateTime(void)
{
    uint8_t Hr_time_u8;
    if(BMS_INFO_MSG_4_St.System_Hour > 12)
    {
        Hr_time_u8 = BMS_INFO_MSG_4_St.System_Hour%12;
       SET_AM_STATE(0);
        SET_PM_STATE(3);
        
    }
    else
    {
        Hr_time_u8 = BMS_INFO_MSG_4_St.System_Hour;
        SET_AM_STATE(3);
        SET_PM_STATE(0);
    }

    SET_HR_TIME(Hr_time_u8);
    return;
}
/*---------------------- End of File -----------------------------------------*/