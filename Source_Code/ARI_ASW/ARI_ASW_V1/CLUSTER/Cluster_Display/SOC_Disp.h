/***********************************************************************************************************************
* File Name    : SOC_Disp.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 24/01/2021
***********************************************************************************************************************/

#ifndef SOC_DISP_H
#define SOC_DISP_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 
#include "Cluster_Conf.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define TOTAL_SOC_BARS				10
#define DISCHARGING_MODE			0x00U
#define CHARGING_MODE				0x01U
#define SAFE_MODE_DISCHARGE_START		31

#define RED_BAR_1_VALUE				1
#define RED_BAR_2_VALUE				11
#define RED_BAR_3_VALUE				111

#define VALUE_TO_DISP_CH			(67+72)	/*'C'+'H' = CH*/

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	SOC_DISPLAY_E,
	SOC_DISP_HOLD_E,
}SOC_DispState_En_t;


typedef enum
{
	SOC_BAR_START_E = 0,
	SOC_BAR_1_E,
	SOC_BAR_2_E,
	SOC_BAR_3_E,
	SOC_BAR_4_E,
	SOC_BAR_5_E,
	SOC_BAR_6_E,
	SOC_BAR_7_E,
	SOC_BAR_8_E,
	SOC_BAR_9_E,
	SOC_BAR_10_E,
	SOC_TOTAL_BARS_E = SOC_BAR_10_E,
}SOC_BAR_POS_En_t;


typedef enum
{
	CHARGING_START_E,
	CHARGING_E,
	CHARGING_END_E,
}Charge_State_En_t;

typedef enum
{
	SOC_GREEN_BAR_DISP_E,
	SOC_RED_BAR_DISP_E,
}SOC_BarDispState_En_t;

typedef enum
{
	DISP_NONE_E,
	DISP_CH_TEXT_E,
	DISP_SOC_NUM_E,
}CH_SOC_Disp_En_t;

/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern uint32_t	     		SOC_BAR_Value_u32;
extern bool 			InitialSOC_Disp_b;
extern Charge_State_En_t	Charge_State_En;
extern SOC_BarDispState_En_t	SOC_BarDispState_En;
extern CH_SOC_Disp_En_t		CH_SOC_Disp_En;

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
uint16_t    	Validate_SOC(uint16_t);
extern uint32_t Get_SOC_Bars(uint16_t,uint8_t);
void 		Display_ChargeSOC(uint16_t, ClusterSignals_En_t);
void 		Display_DischargeSOC(uint16_t, ClusterSignals_En_t);
extern void 	Display_SOC(uint16_t, uint8_t, ClusterSignals_En_t );
void		Get_Bar_PosAndValue(uint16_t);
void 		DispUSM_DischargeSOC(ClusterSignals_En_t, uint32_t, bool);  /*To display the SOC @ Unsafe mode
									(when SOC <= 30% --> REDZONE)*/
									
extern void DisplayChargingInfo(uint16_t, ClusterSignals_En_t);

extern void Control_BatteryIcon(bool);

#endif /* SOC_DISP_H */


