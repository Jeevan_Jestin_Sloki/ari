/*
 * iso14229_serv34.h
 *
 *  Created on: 29-Jan-2020
 *      Author: Lakshmi
 */
/* ************************************************************************** */
/** Descriptive File Name

  @Company
 Sloki Technologies Pvt Ltd.

  @File Name
    iso14229_serv34.h

  @Summary
 This File contains macros, typedefs, extern definitions and
 * user defined datatypes required by iso14229_serv34.c

 */
/* ************************************************************************** */

#ifndef ISO14229_SERV34_H_
#define ISO14229_SERV34_H_

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
#include <stdlib.h>
#include "uds_conf.h"
#include "iso14229_serv31.h"
/* ************************************************************************** */


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


    /* ************************************************************************** */
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
		#define SERV34_MIN_LENGTH                           0x08
		#define SERV34_ADD_LEN_FORMAT_ID                    0x44
		//#define DATA_PER_BLOCK                              0x402
    /* ************************************************************************** */



    // *****************************************************************************
    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************
		//extern MemBlockConf_St_t   MemBlockConf_aSt[] ;
		extern FlashState_En_t Flash_State_En;
		extern uint32_t Addr31CheckSum_u32 ;
		extern uint32_t Size31CheckSum_u32 ;
		extern uint32_t number_of_bytes_2b_write_u32;
		extern uint32_t NumOf_Bytes_Programmed_u32 ;
		extern uint32_t serv34_memory_address_u32;
		extern uint32_t serv34_memory_size_u32;
		extern uint8_t ActiveBlockIndex_u8 ;
		extern uint8_t Number_of_Blocks_u8;
    // *****************************************************************************

/**
*  FUNCTION NAME : iso14229_serv34
*  FILENAME      : iso14229_serv34.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_34 requests
*  @return       : Type of response.
*/


UDS_Serv_resptype_En_t iso14229_serv34(UDS_Serv_St_t*  UDS_Serv_pSt);


    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* ISO14229_SERV34_H_ */

/* *****************************************************************************
 End of File
 */




