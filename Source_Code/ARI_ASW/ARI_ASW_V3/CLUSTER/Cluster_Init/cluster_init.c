/***********************************************************************************************************************
* File Name    : cluster_init.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 04/01/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_lcd.h"
#include "cluster_init.h"
#include "BackLightCtrl.h"
#include "can_driver.h"
#include "r_cg_timer.h"
#include "task_scheduler.h"
#include "GenConfig.h"
#include "can_tranceiver.h"
#include "DataBank.h"
#include "DataAquire.h"
#include "SegDispWrite.h"
#include "ClusterAnimation.h"
#include "DataAquire.h"
#include "ari_bms_can.h"
#include"signal_calc.h"
#include"ODO_Calc.h"
#include"Range_WhKmCalc.h"
#include "timer_user.h"
#include "Speed_Calc.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool 		Cluster_Init_Success_b = false;
bool 		Cluster_Init_Flag_b = false;
uint8_t 	Default_tripState   = 0;

/***********************************************************************************************************************
* Function Name: Cluster_Init
* Description  : This function Init/Deinit the cluster as per the Ignition_state
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Cluster_Init(void)
{
	if(CLUSTER_INIT == ClusterSignals_St.IgnitionStatus_u8)
	{
		Cluster_Display_Init();
	}
	else if(CLUSTER_DE_INIT == ClusterSignals_St.IgnitionStatus_u8)
	{
		Cluster_Display_Deinit();
	}
	else
	{
		;
	}
        return;
}


/***********************************************************************************************************************
* Function Name: Cluster_Display_Init
* Description  : This function Initialzes the cluster display on ignition-ON.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Cluster_Display_Init(void)
{
	if(false == Cluster_Init_Flag_b)
	{
#if(HMI_CAN_ONLY == TRUE)
		R_TAU0_Channel0_Start();
#endif
		RestoreODOfromFlash();
		RestoreRangeKmParam();

		R_TAU2_Channel0_Start();	/*Starts the PWM-2 TAU2 channel0-Master*/
		
//		Release_STBY_Mode();		  /*Release CAN Tranceiver device from Stand-By-Mode [Power-Save Mode]*/
//		CAN_SleepMode_Release();          /*Releases the CAN Module from the Sleep mode*/
		
		Turn_ON_BackLight();		/*Turn-On BackLight*/		
		
		LCD_PowerSaveRelease();    	/*Release the LCD Driver from the power save mode*/
		
		R_LCD_VoltageOn();         	/*Voltage select for Common Pins*/
		
		R_LCD_Start();             	/*Voltage select for Segment Pins*/
		
		update_ODOtrip(Default_tripState);

		Get_Distance_for_1_Pulse();

		SetPulseCount();

		R_TAU0_Channel1_Start();

		WhEstimateOnstart_b = false;

		Cluster_State_En 	= DISPLAY_ANIMATION_E;   	/*Set the cluster display state to Animation*/
		Cluster_Init_Flag_b 	= true;
		Cluster_Init_Success_b 	= true;
	}
	return;
}


/***********************************************************************************************************************
* Function Name: Cluster_Display_Deinit
* Description  : This function De-Initialzes the cluster display on ignition-OFF.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Cluster_Display_Deinit(void)
{
	if(true == Cluster_Init_Flag_b)
	{
		
		Cluster_State_En = DISPLAY_HOLD_E;      	/*Set the Cluster Display state to Hold*/
		StoreODOtoFlash();
		StoreRangekmParam();
		
		R_TAU0_Channel1_Stop();

		Turn_OFF_BackLight();        			/*Turn-ON BackLight 1 and 2*/
		
		Turn_OnOff_AllSegments(CLEAR_SEG_REGISTER);   	/*Clear the data present in all the Segment registers*/
		
		ClrBrightnessCtrlData();			/*Clear/Reset the all the status flags and data related to 
									auto brightness adjustment*/

		ResetTheUserInput(); /*Reset the User Input Signals State*/
		
		Cluster_Init_Flag_b 	= false;
		Cluster_Init_Success_b 	= false;
		
		ResetAnimationData();
		ResetTheSpeedData();
		ResetTheODOmeterData();
		ResetPulseCountData();
		
		PowerSaveMode();             		/*CAN Driver Sleep mode  and LCD driver is in Power save mode*/
	}
	return;
}


/***********************************************************************************************************************
* Function Name: PowerSaveMode
* Description  : This function enables the CAN module Sleep mode and puts the CPU in the power save mode.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void PowerSaveMode(void)
{
	R_LCD_Stop();                 /*Voltage deselect for Segment Pins*/
	
	R_LCD_VoltageOff();           /*Voltage deselect for Common Pins*/
#if(HMI_CAN_ONLY == TRUE)
	R_TAU0_Channel0_Stop();
#endif
	
	R_TAU2_Channel0_Stop();	      /*Stop the PWM-2 TAU2 channel0-Master*/
	
	//CAN_SleepMode_Setting();      /*CAN Driver Sleep mode setting*/
	
	LCD_PowerSave();              /*LCD Driver Power save mode setting*/
	
	#if(CAN_TRANCEIVER_2 == TRUE)
		//Set_STBY_Mode();	      /*Set CAN Tranceiver device to Stand-By-Mode [Power-Save Mode]*/
	#endif
	
	/*
		NOTE : STOP() instruction should be call;
			1) After setting all the peripherals [Of your choice] to power-save mode or
			   Sleep-Mode.
			2) After perfoming the required de-initializations.
	*/
	
	STOP();			      /*STOP instruction execution sets the STOP mode (Ultra-Low Power Consumption mode). 
					In the STOP mode, the High-Speed OCO is stop, stopping the whole system, there by considerably reducing the CPU 
					operating current.
					Note : STOP mode is cleared by an interrupt request*/
					
	return;
}


/********************************************************EOF***********************************************************/