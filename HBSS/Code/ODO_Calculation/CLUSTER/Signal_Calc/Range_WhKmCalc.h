/***********************************************************************************************************************
* File Name    : ODO_Calc.h
* Version      : 01
* Description  : This file contains the macro definition related to main
* Created By   : Dileepa B S
* Creation Date: 06/10/2021
***********************************************************************************************************************/
#ifndef RANGE_WHKM_H
#define RANGE_WHKM_H
/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"
#include"Flash_Param.h"
/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define REC_LEN					        5
#define RANGE_FLASH_SIZE                4
#define RANGE_FLASH_BLOCK               1
#define TOTAL_RANGE_FLASH_BLOCK         1
#define RANGE_FLASH_BLKPOS              0
/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/

typedef enum
{
	REC_VOLTAGE_E,
	CAL_AVG_VOLTAGE_E,
}BattVolRecFun_En_t;

typedef struct 
{
    uint32_t BattVoltage_au16[REC_LEN];
    uint32_t AvgBattVoltage_u32;
    uint32_t PreviousOdoMeter_u32;
    uint16_t PresentWatts_u16;
    uint16_t PreviousWatts_u16;
    uint16_t WattsConsumed_u16;
}WhRangeKm_St_t;

/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern bool    WhEstimateOnstart_b;
/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void ResetRangeKmParam(void);
extern void StoreRangekmParam(void);
extern void RestoreRangeKmParam(void);
extern void ReadWHkm(Flash_Param_St_t* Flash_Param_pSt);
extern void WriteWHkm(Flash_Param_St_t* Flash_Param_pSt);

void EstimateWhKm(uint16_t VoltagemV_u16, uint16_t SocPercentage_u16, uint16_t TotalCapacityAh_u16);
void UpdateRangeKm(uint16_t VoltagemV_u16, uint16_t SocPercentage_u16, uint16_t TotalCapacityAh_u16);
void Record_BatteryVoltage(BattVolRecFun_En_t BattVolRecFun_En,uint16_t BatteryVoltage_u16);
#endif