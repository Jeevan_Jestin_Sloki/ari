﻿

#include "ari_bms_can.h"

BMS_INFO_MSG_1_t BMS_INFO_MSG_1_St;
BMS_INFO_MSG_2_t BMS_INFO_MSG_2_St;
BMS_INFO_MSG_3_t BMS_INFO_MSG_3_St;
BMS_INFO_MSG_4_t BMS_INFO_MSG_4_St;
BMS_INFO_MSG_5_t BMS_INFO_MSG_5_St;

bool Ah_Recv_b = false;
bool SOC_Recv_b = false;
bool Volt_Recv_b = false;

 uint32_t Deserialize_BMS_INFO_MSG_2(BMS_INFO_MSG_2_t* message, const uint8_t* data)
{
  message->BMS_ChargeStatus = (((data[0] >> BMS_INFO_MSG_2_BMS_CHARGESTATUS_MASK0) & (SIGNLE_READ_Mask2))) + BMS_INFO_MSG_2_CANID_BMS_CHARGESTATUS_OFFSET;
   return BMS_INFO_MSG_2_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_BMS_INFO_MSG_2(BMS_INFO_MSG_2_t* message, uint8_t* data)
{
  message->BMS_ChargeStatus = (message->BMS_ChargeStatus  - BMS_INFO_MSG_2_CANID_BMS_CHARGESTATUS_OFFSET);
  data[0] = ((message->BMS_ChargeStatus & (SIGNLE_READ_Mask2)) << BMS_INFO_MSG_2_BMS_CHARGESTATUS_MASK0) ;
   return BMS_INFO_MSG_2_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_BMS_INFO_MSG_3(BMS_INFO_MSG_3_t* message, const uint8_t* data)
{
  message->Alarm_Code = ((data[0] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_3_CANID_ALARM_CODE_OFFSET;
  message->Alarm_Level = ((data[1] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_3_CANID_ALARM_LEVEL_OFFSET;
   return BMS_INFO_MSG_3_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_BMS_INFO_MSG_3(BMS_INFO_MSG_3_t* message, uint8_t* data)
{
  message->Alarm_Code = (message->Alarm_Code  - BMS_INFO_MSG_3_CANID_ALARM_CODE_OFFSET);
  message->Alarm_Level = (message->Alarm_Level  - BMS_INFO_MSG_3_CANID_ALARM_LEVEL_OFFSET);
  data[0] = (message->Alarm_Code & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->Alarm_Level & (SIGNLE_READ_Mask8)) ;
   return BMS_INFO_MSG_3_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_BMS_INFO_MSG_4(BMS_INFO_MSG_4_t* message, const uint8_t* data)
{
  message->System_Year = (((data[0] & (SIGNLE_READ_Mask8)) << BMS_INFO_MSG_4_SYSTEM_YEAR_MASK0) | (data[1] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_4_CANID_SYSTEM_YEAR_OFFSET;
  message->System_Month = ((data[2] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_4_CANID_SYSTEM_MONTH_OFFSET;
  message->System_Day = ((data[3] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_4_CANID_SYSTEM_DAY_OFFSET;
  message->System_Hour = ((data[4] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_4_CANID_SYSTEM_HOUR_OFFSET;
  message->System_Minute = ((data[5] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_4_CANID_SYSTEM_MINUTE_OFFSET;
  message->System_Seconds = ((data[6] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_4_CANID_SYSTEM_SECONDS_OFFSET;
   return BMS_INFO_MSG_4_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_BMS_INFO_MSG_4(BMS_INFO_MSG_4_t* message, uint8_t* data)
{
  message->System_Year = (message->System_Year  - BMS_INFO_MSG_4_CANID_SYSTEM_YEAR_OFFSET);
  message->System_Month = (message->System_Month  - BMS_INFO_MSG_4_CANID_SYSTEM_MONTH_OFFSET);
  message->System_Day = (message->System_Day  - BMS_INFO_MSG_4_CANID_SYSTEM_DAY_OFFSET);
  message->System_Hour = (message->System_Hour  - BMS_INFO_MSG_4_CANID_SYSTEM_HOUR_OFFSET);
  message->System_Minute = (message->System_Minute  - BMS_INFO_MSG_4_CANID_SYSTEM_MINUTE_OFFSET);
  message->System_Seconds = (message->System_Seconds  - BMS_INFO_MSG_4_CANID_SYSTEM_SECONDS_OFFSET);
  data[0] = ((message->System_Year >> BMS_INFO_MSG_4_SYSTEM_YEAR_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->System_Year & (SIGNLE_READ_Mask8)) ;
  data[2] = (message->System_Month & (SIGNLE_READ_Mask8)) ;
  data[3] = (message->System_Day & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->System_Hour & (SIGNLE_READ_Mask8)) ;
  data[5] = (message->System_Minute & (SIGNLE_READ_Mask8)) ;
  data[6] = (message->System_Seconds & (SIGNLE_READ_Mask8)) ;
   return BMS_INFO_MSG_4_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_BMS_INFO_MSG_5(BMS_INFO_MSG_5_t* message, const uint8_t* data)
{
  message->Pack_TotalCap = (((data[0] & (SIGNLE_READ_Mask8)) << BMS_INFO_MSG_5_PACK_TOTALCAP_MASK0) | (data[1] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_5_CANID_PACK_TOTALCAP_OFFSET;
  message->Pack_LeftCap = (((data[2] & (SIGNLE_READ_Mask8)) << BMS_INFO_MSG_5_PACK_LEFTCAP_MASK0) | (data[3] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_5_CANID_PACK_LEFTCAP_OFFSET;
  message->Charge_Time = (((data[4] & (SIGNLE_READ_Mask8)) << BMS_INFO_MSG_5_CHARGE_TIME_MASK0) | (data[5] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_5_CANID_CHARGE_TIME_OFFSET;
  message->SOH = (((data[6] & (SIGNLE_READ_Mask8)) << BMS_INFO_MSG_5_SOH_MASK0) | (data[7] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_5_CANID_SOH_OFFSET;
   return BMS_INFO_MSG_5_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_BMS_INFO_MSG_5(BMS_INFO_MSG_5_t* message, uint8_t* data)
{
  message->Pack_TotalCap = (message->Pack_TotalCap  - BMS_INFO_MSG_5_CANID_PACK_TOTALCAP_OFFSET);
  message->Pack_LeftCap = (message->Pack_LeftCap  - BMS_INFO_MSG_5_CANID_PACK_LEFTCAP_OFFSET);
  message->Charge_Time = (message->Charge_Time  - BMS_INFO_MSG_5_CANID_CHARGE_TIME_OFFSET);
  message->SOH = (message->SOH  - BMS_INFO_MSG_5_CANID_SOH_OFFSET);
  data[0] = ((message->Pack_TotalCap >> BMS_INFO_MSG_5_PACK_TOTALCAP_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->Pack_TotalCap & (SIGNLE_READ_Mask8)) ;
  data[2] = ((message->Pack_LeftCap >> BMS_INFO_MSG_5_PACK_LEFTCAP_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[3] = (message->Pack_LeftCap & (SIGNLE_READ_Mask8)) ;
  data[4] = ((message->Charge_Time >> BMS_INFO_MSG_5_CHARGE_TIME_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[5] = (message->Charge_Time & (SIGNLE_READ_Mask8)) ;
  data[6] = ((message->SOH >> BMS_INFO_MSG_5_SOH_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[7] = (message->SOH & (SIGNLE_READ_Mask8)) ;
   return BMS_INFO_MSG_5_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_BMS_INFO_MSG_1(BMS_INFO_MSG_1_t* message, const uint8_t* data)
{
  message->Vol_Bat = (((data[0] & (SIGNLE_READ_Mask8)) << BMS_INFO_MSG_1_VOL_BAT_MASK0) | (data[1] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_1_CANID_VOL_BAT_OFFSET;
  message->Cur_Bat = (((data[2] & (SIGNLE_READ_Mask8)) << BMS_INFO_MSG_1_CUR_BAT_MASK0) | (data[3] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_1_CANID_CUR_BAT_OFFSET;
  message->SOC = (((data[4] & (SIGNLE_READ_Mask8)) << BMS_INFO_MSG_1_SOC_MASK0) | (data[5] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_1_CANID_SOC_OFFSET;
  message->Display_SOC = (((data[6] & (SIGNLE_READ_Mask8)) << BMS_INFO_MSG_1_DISPLAY_SOC_MASK0) | (data[7] & (SIGNLE_READ_Mask8))) + BMS_INFO_MSG_1_CANID_DISPLAY_SOC_OFFSET;
   return BMS_INFO_MSG_1_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_BMS_INFO_MSG_1(BMS_INFO_MSG_1_t* message, uint8_t* data)
{
  message->Vol_Bat = (message->Vol_Bat  - BMS_INFO_MSG_1_CANID_VOL_BAT_OFFSET);
  message->Cur_Bat = (message->Cur_Bat  - BMS_INFO_MSG_1_CANID_CUR_BAT_OFFSET);
  message->SOC = (message->SOC  - BMS_INFO_MSG_1_CANID_SOC_OFFSET);
  message->Display_SOC = (message->Display_SOC  - BMS_INFO_MSG_1_CANID_DISPLAY_SOC_OFFSET);
  data[0] = ((message->Vol_Bat >> BMS_INFO_MSG_1_VOL_BAT_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->Vol_Bat & (SIGNLE_READ_Mask8)) ;
  data[2] = ((message->Cur_Bat >> BMS_INFO_MSG_1_CUR_BAT_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[3] = (message->Cur_Bat & (SIGNLE_READ_Mask8)) ;
  data[4] = ((message->SOC >> BMS_INFO_MSG_1_SOC_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[5] = (message->SOC & (SIGNLE_READ_Mask8)) ;
  data[6] = ((message->Display_SOC >> BMS_INFO_MSG_1_DISPLAY_SOC_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[7] = (message->Display_SOC & (SIGNLE_READ_Mask8)) ;
   return BMS_INFO_MSG_1_ID; 
}
