/***********************************************************************************************************************
* File Name    : RangeKm_Conf.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 05/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "RangeKm_Conf.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/
 const SignalsValue_St_t		Range_SignalsValue_aSt[RANGE_SIG_LEN] =
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	{ ZERO_E,	ELEVEN_E,		RangeSig1Conf_ast },
	{ ONE_E,	ELEVEN_E,		RangeSig2Conf_ast },
	{ TWO_E,	ELEVEN_E,		RangeSig3Conf_ast }
};

 const SignalConfig_St_t		RangeSig1Conf_ast[ELEVEN_E] = 
{
/*PositionSignalValue  SegmentsRequired  SegmentsConfiguration*/
	{ ZERO_E,	TWO_SEG_E,	Range_S1_0_SegConf_aSt	},
	{ ONE_E,	TWO_SEG_E,	Range_S1_1_SegConf_aSt	},
	{ TWO_E,	TWO_SEG_E,	Range_S1_2_SegConf_aSt	},
	{ THREE_E,	TWO_SEG_E,	Range_S1_3_SegConf_aSt	},
	{ FOUR_E,	TWO_SEG_E,	Range_S1_4_SegConf_aSt	},
	{ FIVE_E,	TWO_SEG_E,	Range_S1_5_SegConf_aSt	},
	{ SIX_E,	TWO_SEG_E,	Range_S1_6_SegConf_aSt	},
	{ SEVEN_E,	TWO_SEG_E,	Range_S1_7_SegConf_aSt	},
	{ EIGHT_E,	TWO_SEG_E,	Range_S1_8_SegConf_aSt	},
	{ NINE_E,	TWO_SEG_E,	Range_S1_9_SegConf_aSt	},
	{ OFF_DIGIT,	TWO_SEG_E,	Range_S1_Off_SegConf_aSt}
};

 const SignalConfig_St_t		RangeSig2Conf_ast[ELEVEN_E] = 
{
	{ ZERO_E,	TWO_SEG_E,	Range_S2_0_SegConf_aSt	},
	{ ONE_E,	TWO_SEG_E,	Range_S2_1_SegConf_aSt	},
	{ TWO_E,	TWO_SEG_E,	Range_S2_2_SegConf_aSt	},
	{ THREE_E,	TWO_SEG_E,	Range_S2_3_SegConf_aSt	},
	{ FOUR_E,	TWO_SEG_E,	Range_S2_4_SegConf_aSt	},
	{ FIVE_E,	TWO_SEG_E,	Range_S2_5_SegConf_aSt	},
	{ SIX_E,	TWO_SEG_E,	Range_S2_6_SegConf_aSt	},
	{ SEVEN_E,	TWO_SEG_E,	Range_S2_7_SegConf_aSt	},
	{ EIGHT_E,	TWO_SEG_E,	Range_S2_8_SegConf_aSt	},
	{ NINE_E,	TWO_SEG_E,	Range_S2_9_SegConf_aSt	},
	{ OFF_DIGIT,	TWO_SEG_E,	Range_S2_Off_SegConf_aSt}
};

 const SignalConfig_St_t		RangeSig3Conf_ast[ELEVEN_E] = 
{
	{ ZERO_E,	TWO_SEG_E,	Range_S3_0_SegConf_aSt	},
	{ ONE_E,	TWO_SEG_E,	Range_S3_1_SegConf_aSt	},
	{ TWO_E,	TWO_SEG_E,	Range_S3_2_SegConf_aSt	},
	{ THREE_E,	TWO_SEG_E,	Range_S3_3_SegConf_aSt	},
	{ FOUR_E,	TWO_SEG_E,	Range_S3_4_SegConf_aSt	},
	{ FIVE_E,	TWO_SEG_E,	Range_S3_5_SegConf_aSt	},
	{ SIX_E,	TWO_SEG_E,	Range_S3_6_SegConf_aSt	},
	{ SEVEN_E,	TWO_SEG_E,	Range_S3_7_SegConf_aSt	},
	{ EIGHT_E,	TWO_SEG_E,	Range_S3_8_SegConf_aSt	},
	{ NINE_E,	TWO_SEG_E,	Range_S3_9_SegConf_aSt	},
	{ OFF_DIGIT,	TWO_SEG_E,	Range_S3_Off_SegConf_aSt}
};


 const SegConfig_St_t	Range_S1_0_SegConf_aSt[TWO_SEG_E] = 
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	{&SEG47,	0x01U,		0x0AU},
	{&SEG46,	0x00U,		0x0FU}
};

 const SegConfig_St_t	Range_S1_1_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG47,	0x01U,		0x0AU},
	{&SEG46,	0x00U,		0x00U}
};

 const SegConfig_St_t	Range_S1_2_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG47,	0x01U,		0x06U},
	{&SEG46,	0x00U,		0x0DU}
};

 const SegConfig_St_t	Range_S1_3_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG47,	0x01U,		0x0EU},
	{&SEG46,	0x00U,		0x09U}
};

 const SegConfig_St_t	Range_S1_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG47,	0x01U,		0x0EU},
	{&SEG46,	0x00U,		0x02U}
};

 const SegConfig_St_t	Range_S1_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG47,	0x01U,		0x0CU},
	{&SEG46,	0x00U,		0x0BU}
};

 const SegConfig_St_t	Range_S1_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG47,	0x01U,	0x0CU},
	{&SEG46,	0x00U,	0x0FU}
};


 const SegConfig_St_t	Range_S1_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG47,	0x01U,		0x0AU},
	{&SEG46,	0x00U,		0x01U}
};


 const SegConfig_St_t	Range_S1_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG47,	0x01U,		0x0EU},
	{&SEG46,	0x00U,		0x0FU},
};


 const SegConfig_St_t	Range_S1_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG47,	0x01U,		0x0EU},
	{&SEG46,	0x00U,		0x0BU}
};

 const SegConfig_St_t	Range_S1_Off_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG47,	0x01U,		0x00U},
	{&SEG46,	0x00U,		0x00U}
};

 const SegConfig_St_t	Range_S2_0_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG45,	0x01U,		0x0AU},
	{&SEG43,	0x00U,		0x0FU}
};

 const SegConfig_St_t	Range_S2_1_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG45,	0x01U,		0x0AU},
	{&SEG43,	0x00U,		0x00U}
};

 const SegConfig_St_t	Range_S2_2_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG45,	0x01U,		0x06U},
	{&SEG43,	0x00U,		0x0DU}
};

 const SegConfig_St_t	Range_S2_3_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG45,	0x01U,		0x0EU},
	{&SEG43,	0x00U,		0x09U}
};

 const SegConfig_St_t	Range_S2_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG45,	0x01U,		0x0EU},
	{&SEG43,	0x00U,		0x02U}
};

 const SegConfig_St_t	Range_S2_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG45,	0x01U,		0x0CU},
	{&SEG43,	0x00U,		0x0BU}
};

 const SegConfig_St_t	Range_S2_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG45,	0x01U,		0x0CU},
	{&SEG43,	0x00U,		0x0FU}
};


 const SegConfig_St_t	Range_S2_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG45,	0x01U,		0x0AU},
	{&SEG43,	0x00U,		0x01U}
};


 const SegConfig_St_t	Range_S2_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG45,	0x01U,		0x0EU},
	{&SEG43,	0x00U,		0x0FU}
};


 const SegConfig_St_t	Range_S2_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG45,	0x01U,		0x0EU},
	{&SEG43,	0x00U,		0x0BU}
};

 const SegConfig_St_t	Range_S2_Off_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG45,	0x01U,		0x00U},
	{&SEG43,	0x00U,		0x00U}
};

 const SegConfig_St_t	Range_S3_0_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG42,	0x01U,		0x0AU},
	{&SEG41,	0x00U,		0x0FU}

};

 const SegConfig_St_t	Range_S3_1_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG42,	0x01U,		0x0AU},
	{&SEG41,	0x00U,		0x00U}
};

 const SegConfig_St_t	Range_S3_2_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG42,	0x01U,		0x06U},
	{&SEG41,	0x00U,		0x0DU}
};

 const SegConfig_St_t	Range_S3_3_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG42,	0x01U,		0x0EU},
	{&SEG41,	0x00U,		0x09U}
};

 const SegConfig_St_t	Range_S3_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG42,	0x01U,		0x0EU},
	{&SEG41,	0x00U,		0x02U}
};

 const SegConfig_St_t	Range_S3_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG42,	0x01U,		0x0CU},
	{&SEG41,	0x00U,		0x0BU}
};

 const SegConfig_St_t	Range_S3_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG42,	0x01U,		0x0CU},
	{&SEG41,	0x00U,		0x0FU}
};


 const SegConfig_St_t	Range_S3_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG42,	0x01U,		0x0AU},
	{&SEG41,	0x00U,		0x01U}
};


 const SegConfig_St_t	Range_S3_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG42,	0x01U,		0x0EU},
	{&SEG41,	0x00U,		0x0FU}
};


 const SegConfig_St_t	Range_S3_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG42,	0x01U,		0x0EU},
	{&SEG41,	0x00U,		0x0BU}
};

 const SegConfig_St_t	Range_S3_Off_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG42,	0x01U,		0x00U},
	{&SEG41,	0x00U,		0x00U}
};


/********************************************************EOF***********************************************************/