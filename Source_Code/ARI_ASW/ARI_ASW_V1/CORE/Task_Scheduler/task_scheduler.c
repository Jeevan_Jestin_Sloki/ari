/***********************************************************************************************************************
* File Name    : task_scheduler.c
* Version      : 01
* Description  : This file implements the Task_Scheduler
* Created By   : Dileepa B S
* Creation Date: 19/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "task_scheduler.h"
#include "timer_user.h"
#include "can_driver.h"
#include "r_cg_wdt.h"
#include "cluster_init.h"
#include "cluster_main.h"
#include "delay_flags.h"
#include "Time.h"
#include "ClusterAnimation.h"
#include "BackLightCtrl.h"
#include "GenConfig.h"
#include "r_cg_timer.h"
#include "com_tasksched.h"
#include "App_typedefs.h"
#include "diag_sys_conf.h"
#include "iso14229_serv11.h"
#include "DataAquire.h"
#include "UserButtuonIn.h"
#include "Signals_Calc.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
Cluster_State_En_t	Cluster_State_En 	= DISPLAY_HOLD_E;
bool   				TS_Exit_b 			= false;


/***********************************************************************************************************************
* Function Name: _TS_Start
* Description  : This function schedules the tasks for every 5-ms
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void TS_Start(void)
{
 	while(!TS_Exit_b)
	{
		while(_5ms_TS_Flag_b)
		{
			_5ms_TS_Flag_b = false;
			
			R_WDT_Restart();    /*Refersh the Watch-Dog-Timer within 34.13 m-sec*/
			
			Diag_TS_Proc_5ms();
			
			ReadClusterInput();
				/*Read the Rider Inputs to the cluster through GPIO/ADC/INTC*/
			
			Cluster_Init(); /*Init or Deinit the Cluster*/	
			
			Update_Time(); /* Update the Real-time */
			
			if(true == Cluster_Init_Success_b)
			{
				if(Cluster_State_En == DISPLAY_ANIMATION_E)
				{
					AnimateOnStart();
				}
				
				#if(CAL_CLUSTER_SIG == TRUE)
					CalculateClusterSignals(); /*Calculate Required Cluster Signals*/
				#endif
				
				Set_Delay_Flags();
				
				UpdateSigToDataBank();
					/*Update the Data Bank with the user input data and CAN data*/
					
				if(Cluster_State_En == DISPLAY_DATA_E)
				{
					if((BackLightCtrl_En == BACKLIGHT_CTRL_ALS_E) || (BackLightCtrl_En == BACKLIGHT_CTRL_CAN_E))
					{
						/*Schedule the Brightness filter @30ms rate*/
						if(0 == (Time_Tick_u32 % _30MS_DIVISOR_30))
						{
							AdjustLightIntensity(BackLightCtrl_En);
						}
					}
					
					if(0 == (Time_Tick_u32 % _100MS_DIVISOR_100))
					{
						Display_Cluster_Data();
					}
				}
				
				#if(READ_UIB_INPUT == TRUE)
					Read_UserInterfaceButton(); 
				#endif
			}
			
			TS_Exit();
			
		}
	}
	return;
}


/***********************************************************************************************************************
* Function Name: TS_Exit
* Description  : This functoion stops the Task Scheduler.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void TS_Exit(void)
{
	static uint32_t Counter_reset_u32 	= 0;
	
	if(true == reset_b)
	{
		Counter_reset_u32++;
		
		if(Counter_reset_u32 == RESET_DEBOUNCE_COUNT)
		{
			TS_Exit_b 			= true;
			Counter_reset_u32 	= 0;
		}
	}
	return;
}

/***********************************************************************************************************************
* Function Name: TS_Stop
* Description  : This functoion Restarts the MCU.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void TS_Stop(void)
{
	int8_t __far* a;            // Create a far-Pointer
	IAWCTL|=0x80;               // switch IAWEN on (defalut off)
	a=(int8_t __far*) 0x00000;  
	*a=0; 
	return;
}

/********************************************************EOF***********************************************************/