/***********************************************************************************************************************
* File Name    : Cluster_Reset.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 04/01/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Reset.h" 
#include "ClusterAnimation.h"
#include "BackLightCtrl.h"
#include "DataAquire.h"
#include "delay_flags.h"
#include "Time.h"
#include "GenConfig.h"
#include "SpeedDisp.h"
#include "SOC_Disp.h"
#include "cluster_main.h"
#include "timer_user.h"
#include "SegDispWrite.h"
#include "hmi_config_can.h"
#include "Speed_Calc.h"
#include "ODOmeter_Calc.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


/***********************************************************************************************************************
* Function Name: Reset_Cluster
* Description  : This function clears all segment registers (SEG0 - SEG47) except unused segments.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Reset_Cluster(void)
{
	SetOrClearSegmentReg(SEG_REG_CLR_ALL);	/*Clear the data present in all the Segment registers*/
	
	ResetAnimationData(); /*Clear or Reset Animation Data*/
	
	ClrBrightnessCtrlData();	/*Clear/Reset the all the status flags and data related to 
										auto brightness adjustment*/
										
					/*Reset the HMI CAN Signals*/
	//Clear_HMIConf_CANSignals();	/*Reset the HMI-CONF CAN Signals*/
	
	ResetTheUserInput();	/*Reset the User Input Signals State*/
	
	Reset_DelayFlagStates(); /*Reset data/states used for delay flags*/
	
	ResetRealTimeCounters(); /*Reset the data used for the Real-Time Set and Update*/

	ResetPulseCountData();

	ResetTheSpeedData();

	ResetTheODOmeterData();
	
	ResetCommunicatorData();
	
	BatteryState_En 	= NO_LOAD_E;	/*Reset the Battery State to No-Load*/
	SetSpeedToZero_b 	= false;
	TimeTick1ms_u32 	= 0;
	OneSecDelay_b 		= true; 
	CH_SOC_Disp_En 		= DISP_NONE_E;
	Charge_State_En 	= CHARGING_END_E;
		
	return;
}


/********************************************************EOF***********************************************************/