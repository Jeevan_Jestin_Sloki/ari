/***********************************************************************************************************************
* File Name    : board_init.c
* Version      : 01
* Description  : This file contains the functions to initialize the Hardware and Software components required by the 
                 HMI Cluster Software.
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "board_init.h"
#include "r_cg_macrodriver.h"
#include "r_cg_cgc.h"
#include "r_cg_port.h"
#include "r_cg_timer.h"
#include "can_driver.h"
#include "com_tasksched.h"
#include "r_cg_wdt.h"
#include "MY_ram_vector.h"
#include "PFlash.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/ 

/***********************************************************************************************************************
* Function Name: Hardware_Init
* Description  : This function initializes the Hardware peripherals.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Hardware_Init(void)
{
	volatile uint32_t w_count;
	DI();
	R_CGC_Get_ResetSource();  /*Get the reset flag status of the MCU*/
        R_CGC_Create();           /*Initialize the clock generator module*/
        R_PORT_Create();          /*Initialize the GPIO module*/
	R_WDT_Create();           /*Initialize the Watch Dog Timer Module*/
        R_TAU0_Create();          /*Initialize the Interval timer module*/
	/* ----------------------------------------------*/
	/*
		CAN Driver Initialization
	*/
	CAN_Init();
	PFlash_Init();
	SetCAN_STBY_Pin();  /*Set the CAN-Tranceiver Stand-By Pin to 5V*/
	
	/* ----------------------------------------------*/
	
        IAWCTL = 0x00U;
        GUARD = 0x00U;	
	
	/* Start data flash control */
    	DFLEN = 1U;
    	for (w_count = 0U; w_count < 13U; w_count++)
    	{
        	NOP();
    	}
    	/* End data flash control */
	
	return;
}


/***********************************************************************************************************************
* Function Name: Software_Init
* Description  : This function initailizes the Software components and starts the hardware peripherals.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Software_Init(void)
{
	EI();
	RAM_INTWDTI_ISR = &WDT_ISR;
	RAM_INTTM00_ISR = &IntervalTimerISR;
	RAM_INTC0REC_ISR = &ISR_Msgbuf_Receive;
	RAM_INTC0ERR_ISR = &CAN_Error_Process;
	R_TAU0_Channel0_Start();   /*Start the Interval Timer*/
	Diag_TS_Init();            /*Initialize the CIL-CAN Layer*/
	return;
}

/********************************************************EOF***********************************************************/
