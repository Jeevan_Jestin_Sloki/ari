/***********************************************************************************************************************
* File Name    : ClusterCallback.c
* Version      : 01
* Description  : This file implements Callback function of HMI cluster.
* Created By   : Jeevan Jestin N
* Creation Date: 27/06/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include"ClusterCallback.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
BattInfoCmd_En_t BattInfoCmd_En = BATT_GENERAL_INFO_E;
bool SendRequest_b = false;
/***********************************************************************************************************************
* Function Name: Display_Cluster_Data
* Description  : This function displays all the cluster signals on the LCD.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void RequestBmsFrame(void)
{
    uint8_t CanDataBuff_au8[8] = {0};
    if(true == SendRequest_b)
    {
        if(BATT_GENERAL_INFO_E == BattInfoCmd_En)
        {
            CanDataBuff_au8[0] = BATT_ID;
            CanDataBuff_au8[1] = BATT_GENERAL_INFO_ID;
            CanDataBuff_au8[7] = BATT_DMUUMY_DATA;
        BattInfoCmd_En = BATT_CAPACITY_INFO_E;
        }
        else if(BATT_CAPACITY_INFO_E == BattInfoCmd_En)
        {
            CanDataBuff_au8[0] = BATT_ID;
            CanDataBuff_au8[1] = BATT_CAPACITY_INFO_ID;
            CanDataBuff_au8[7] = BATT_DMUUMY_DATA;
        BattInfoCmd_En = BATT_GENERAL_INFO_E;
        }
        else
        {

        }
	Tx_MsgBuf_Processing(REQ_BMS_BUFF_NO, BMS_DATA_LEN,&CanDataBuff_au8[0]);
    }
    
    return;
}