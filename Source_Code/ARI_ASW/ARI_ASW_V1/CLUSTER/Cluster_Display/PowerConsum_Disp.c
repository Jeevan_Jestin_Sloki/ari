/***********************************************************************************************************************
* File Name    : PowerConsum_Disp.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 16/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "PowerConsum_Disp.h"
#include "SegDispWrite.h"
#include "GenConfig.h"
#include "digits_utils.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
static uint32_t	     	PwrConsum_Bars_Value_u32 	= 0;
bool 		     		InitialPowerConsum_Disp_b 	= false;
bool		     		RedPC_Bar_Disb_b 			= false;
uint16_t             	Valid_PowerConsum_u16 		= 0;
PC_BarDispState_En_t	PC_BarDispState_En			= PC_GREEN_BAR_DISP_E;


/***********************************************************************************************************************
* Function Name: Display_PowerConsum
* Description  : This function validates and displays the Power Consumption.
* Arguments    : uint16_t PowerConsum_u16, ClusterSignals_En_t POWER_CONSUM_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void Display_PowerConsum(uint16_t PowerConsum_u16, 
				ClusterSignals_En_t POWER_CONSUM_ENUM_E)
{
	static uint16_t      PrevPowerConsum_u16 = 0;
	
	PwrConsum_DispState_En_t  PwrConsum_DispState_En = PWR_CONSUM_HOLD_E;
	
	if(PrevPowerConsum_u16 == PowerConsum_u16)
	{
		PwrConsum_DispState_En = PWR_CONSUM_HOLD_E;
		
		if(false == InitialPowerConsum_Disp_b)
		{
			PwrConsum_DispState_En = PWR_CONSUM_DISPLAY_E;
			InitialPowerConsum_Disp_b = true;
		}
		else if(true == RedPC_Bar_Disb_b)
		{
			PC_BarDispState_En = PC_RED_BAR_DISP_E;
			PwrConsum_DispState_En = PWR_CONSUM_DISPLAY_E;
		}
		else
		{
			;	
		}
	}
	else
	{
		PwrConsum_DispState_En = PWR_CONSUM_DISPLAY_E;
		PrevPowerConsum_u16 = PowerConsum_u16;
	}	
	switch(PwrConsum_DispState_En)
	{
		case PWR_CONSUM_DISPLAY_E:
		{
			Valid_PowerConsum_u16 = Validate_PowerConsum(PowerConsum_u16);
			
			PwrConsum_Bars_Value_u32 = Get_Equi_Bars_Value(Valid_PowerConsum_u16);
			
			if(Valid_PowerConsum_u16 <= SAFE_MODE_PWR_CONSUM_RANGE)
			{
				PC_BarDispState_En = PC_GREEN_BAR_DISP_E;
				
				Write_SEG(POWER_CONSUM_ENUM_E,PwrConsum_Bars_Value_u32);
				
				RedPC_Bar_Disb_b = false;
			}
			else
			{
				/*Displaying the power consumption @ red-zone area is handled by the Telltales Display
					module [To blink the respective red-bar]*/
				PC_BarDispState_En = PC_RED_BAR_DISP_E;
				RedPC_Bar_Disb_b = true;
			}
			break;
		}
		case PWR_CONSUM_HOLD_E:
		{
			break;
		}
		default:
		{
			;	
		}
	}
	return;
}


/***********************************************************************************************************************
* Function Name: Validate_PowerConsum
* Description  : This function validates the Power consumption value received.
* Arguments    : uint16_t PowerConsum_u16
* Return Value : uint16_t PowerConsum_u16
***********************************************************************************************************************/
uint16_t    Validate_PowerConsum(uint16_t PowerConsum_u16)
{
	if(PowerConsum_u16 > MAX_PWR_CONSUM_BARS)
	{
		PowerConsum_u16 = MAX_PWR_CONSUM_BARS;
	}
	else
	{
		;	
	}
	return 	PowerConsum_u16;
}


/***********************************************************************************************************************
* Function Name: DispUSM_PwrConsum
* Description  : This function displays the Power consumption bars [in red zone] by blinking the present power
			consumption bar.
* Arguments    : ClusterSignals_En_t POWER_CONSUM_ENUM_E, uint32_t PC_BarsValue_u32, uint16_t PC_Bar_Count_u16,
		 	bool RedPC_Bar_b
* Return Value : None
***********************************************************************************************************************/
void DispUSM_PwrConsum(ClusterSignals_En_t POWER_CONSUM_ENUM_E, uint16_t PwrConsumBars_u16, bool RedPC_Bar_b)
{
	/*NOTE : RedPC_Bar_b flag is synchronized with the Telltales State flag, In-Order to blink the red bar
			in synchronization with the other tell-tales*/
	
	uint32_t	PC_BarsValue_u32 = 0;	
	
	if(!RedPC_Bar_b)
	{
		PC_BarsValue_u32 = Get_Equi_Bars_Value(PwrConsumBars_u16);
		/*Turn-On Power-Consumption Bars corresponding to the present Power-Consumption : 
				Include the Present Power-Consumption Bar*/
		Write_SEG(POWER_CONSUM_ENUM_E,PC_BarsValue_u32);
		RedPC_Bar_b = ON;
	}
	else
	{
		/*Turn-On Power-Consumption Bars corresponding to the present Power-Consumption : 
				Exclude the Present Power-Consumption Bar*/
		switch(PwrConsumBars_u16)
		{
			case PC_RED_BAR_1_POS:
			{
				PC_BarsValue_u32 = Get_Equi_Bars_Value((PC_RED_BAR_1_POS - 1));
				Write_SEG(POWER_CONSUM_ENUM_E,PC_BarsValue_u32);
				break;
			}
			case PC_RED_BAR_2_POS:
			{
				PC_BarsValue_u32 = Get_Equi_Bars_Value((PC_RED_BAR_2_POS - 1));
				Write_SEG(POWER_CONSUM_ENUM_E,PC_BarsValue_u32);
				break;
			}
			case PC_RED_BAR_3_POS:
			{
				PC_BarsValue_u32 = Get_Equi_Bars_Value((PC_RED_BAR_3_POS - 1));
				Write_SEG(POWER_CONSUM_ENUM_E,PC_BarsValue_u32);
				break;
			}
			default:
			{
				;
			}
		}
		RedPC_Bar_b = OFF;
	}
	
	return;
}


/***********************************************************************************************************************
* Function Name: Control_PowerIcon
* Description  : This function Turns-ON / Turns-OFF the Power Icon.
* Arguments    : bool SignalState_b
* Return Value : None
***********************************************************************************************************************/
void Control_PowerIcon(bool SignalState_b)
{
	Write_SEG(POWER_IND_E, SignalState_b);
}


/********************************************************EOF***********************************************************/