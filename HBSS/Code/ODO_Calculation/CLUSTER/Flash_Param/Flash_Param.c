/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           : DataAcquire.c
|    Project        : IDAN Motor VCU
|    Description    : The file Acquire the Data from Hardware.
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 30/06/2022       Name             Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/


/*******************************************************************************
 *  HEADER FILE INCLUDES
 ******************************************************************************/
#include"Flash_Param.h"
#include "pfdl_user.h"
#include"ODO_Calc.h"
#include"Range_WhKmCalc.h"
#include "can_driver.h"
#include "r_cg_wdt.h"
#include"flash_param_cfg.h"
#include "crc_util.h" 
/*******************************************************************************
 *  MACRO DEFINITION
 ******************************************************************************/

#if(0x03 >= DATA_BLOCK_SIZE)
#error  DATA_BLOCK_SIZE should be greater than 0x03  
#endif

#if(0x00 == SIZE_OF_ONE_SECTOR)
#error  DATA_BLOCK_SIZE cannot be Zero/Null  
#endif

// #if(FDL_SECTOR_START < TOTAL_FDL_SECTOR)
// #error  DATA_BLOCK_SIZE cannot be Zero/Null 
// #endif

#define FLASH_BLOCK_0           0
#define BLANK_CHECK_SIZE        4
#define BLANK_VALUE             0xFF
#define READ_POS                0
#define FLASH_BLOCK_1           1
#define FLASH_PARAM_SIZE        16
#define FLASH_PARAM_BLOCK       128 // 2 flash block used (1024*2)/FLASH_PARAM_SIZE
                                // 1 flash block is 1024
#define SIZE_OF_1_FLS_BLCK      1024 // Size of one flash blcok
#define FLASH_PARAM_1_BLOCK_SIZE 64



#define FLASH_PATTERN           0x5A5A5A5Au
#define FDL_PATTERN_START_POS   0x00
#define PATTERN_SIZE            0x04

#define FDL_CRC_SIZE            1
#define BLOCK_PATTERN           0x5A /* Pattern to indicate data has written succesfully*/
#define BLOCK_PATTERN_SIZE      2 /* Size of the block pattern */

#define SIZE_OF_PATTERN         16 // Total 4 pattern each of 4 bytes (4*4 = 16))
#define SIZE_OF_1_BLOCK         (DATA_BLOCK_SIZE + BLOCK_PATTERN_SIZE + FDL_CRC_SIZE)

#define TOTAL_DATA_BLOCK        ((SIZE_OF_ONE_SECTOR - SIZE_OF_PATTERN)/SIZE_OF_1_BLOCK)


typedef void (*Flash_RW_fptr)(uint16_t Sector_u16,uint16_t WritePos_u16,uint16_t WriteSize_u16, uint8_t* Flashbuff_pu8);
typedef void (*Flash_E_fptr)(uint16_t Sector_u16);

/*******************************************************************************
 *  GLOBAL VARIABLES DEFNITION 
 ******************************************************************************/
 const uint32_t FlashPattern_u32 = FLASH_PATTERN;
bool Flash_Wrt_En_b = false;
uint16_t Write_Pos_u16 = 0;
uint8_t Write_Block_u8 = 0;
uint8_t Can_Data_au8[8] = {0};

uint8_t FDL_Data_au8[SIZE_OF_1_BLOCK] = {0};

Flash_RW_fptr DFlash_Read = NULL;
Flash_RW_fptr DFlash_Write = NULL;
Flash_E_fptr DFlash_Erase = NULL;

/*******************************************************************************
 *  STRUCTURE AND ENUM DEFNITION 
 ******************************************************************************/
Flash_Param_St_t Flash_Param_St;
Flash_Param_St_t Flash_Prev_St;

FdlBlockState_En_t FdlBlockState_En = FDL_BLOCK_IDLE_E;
FdlStatePattern_St_t FdlStatePattern_St[TOTAL_FDL_SECTOR];

const uint8_t FlashParamSize_u8 = sizeof(FdlStatePattern_St_t);
/*******************************************************************************
 *  STATIC FUNCTION PROTOTYPES
 ******************************************************************************/
void ReadDataFlash(uint16_t Sector_u16,uint16_t Position, uint16_t Size_u16, uint8_t* DataBuff_pu8);
void WriteDataFlash(uint16_t Sector_u16,uint16_t Position, uint16_t Size_u16, uint8_t* DataBuff_pu8);
void EraseDataSector(uint16_t Sector_u16);
void DFlashBlockInit(Flash_RW_fptr Read_fptr,Flash_RW_fptr Write_fptr,Flash_E_fptr Erase_fptr);

void ReadFdlState(void);
void FillPatternToFdlBuff(void);
void WriteToNextSector(uint16_t SourceSector_u16, uint16_t DestinationSector_u16,uint16_t Pos_u16);
void GetFilledSectorCopy(uint8_t* CopyBuff_pu8);
void WriteFlashParam(uint8_t FdlSector_u8,uint16_t FdlPos_u16,uint8_t* Flash_Param_pu8);
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Read_Flash_Param
*   Description   : NONE
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void Read_Flash_Param(void)
{
    Flash_Param_St_t *Flash_Param_pSt;
    uint16_t ReadPos_u16 = 0;
    uint8_t ReadBlock_u8 = 0;
    uint8_t i_Count_u8 = 0;
    uint8_t j=0;

    for(i_Count_u8 = 0;i_Count_u8<FLASH_PARAM_BLOCK;i_Count_u8++)
    {
        ReadPos_u16 = (FLASH_PARAM_SIZE*i_Count_u8)%SIZE_OF_1_FLS_BLCK;
        ReadBlock_u8 = (FLASH_PARAM_SIZE*i_Count_u8)/SIZE_OF_1_FLS_BLCK;
        FDL_Read(ReadBlock_u8,ReadPos_u16,FLASH_PARAM_SIZE);
        Flash_Param_pSt = (Flash_Param_St_t*)dubReadBuffer;
        if(0xFFFFFFFF == Flash_Param_pSt->ODO_Meter_u32)
        {
            Write_Block_u8 = ReadBlock_u8;
            Write_Pos_u16 = ReadPos_u16;
            break;
        }
        Flash_Param_St = *Flash_Param_pSt;
        if(i_Count_u8 == (FLASH_PARAM_BLOCK - 1))
        {
            Write_Block_u8 = 0;
            Write_Pos_u16 = 0;
            FDL_Erase(FLASH_BLOCK_0,1);
            for (j = 0; j < FLASH_PARAM_SIZE; j++)
            {
                dubWriteBuffer[j] = dubReadBuffer[j];
            }
            FDL_Write(Write_Block_u8,Write_Pos_u16,FLASH_PARAM_SIZE);
            Write_Pos_u16 = FLASH_PARAM_SIZE;
            FDL_Erase(FLASH_BLOCK_1,1);
        }
    }

    Write_OdoMeter(&Flash_Param_St);
    WriteWHkm(&Flash_Param_St);
    //Write_Flash_Param();
    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : Write_Flash_Param
*   Description   : NONE
*   Parameters    : None
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void Write_Flash_Param(void)
{
    uint16_t i = 0;
    uint8_t *Flash_Param_pu8 = (uint8_t*)&Flash_Param_St;
    uint16_t ParamSize_u16 = sizeof(Flash_Param_St);

    Read_OdoMeter(&Flash_Param_St);
    ReadWHkm(&Flash_Param_St);
    
//    Flash_Param_St.ODO_Meter_u32 = 1375000;
//    Flash_Param_St.TripA_Meter_u32 = 1375000;
//    Flash_Param_St.TripB_Meter_u32 = 1375000;

    for(i=0;i<ParamSize_u16;i++)
    {
        dubWriteBuffer[i] = Flash_Param_pu8[i];
    }

//        FDL_Erase(FLASH_BLOCK_0,2);
//        FDL_Erase(1,2);
    
    
//	for(i=0;i<1024;i += 16)
//    {
//	    R_WDT_Restart(); 
//	FDL_Write(0,i,FLASH_PARAM_SIZE);
//    }
    
    
//    NOP();
//    NOP();
//    NOP();
//    NOP();
//    Flash_Param_St.ODO_Meter_u32 = 1275000;
//    Flash_Param_St.TripA_Meter_u32 = 1275000;
//    Flash_Param_St.TripB_Meter_u32 = 1275000;
//    for(i=0;i<ParamSize_u16;i++)
//    {
//        dubWriteBuffer[i] = Flash_Param_pu8[i];
//    }
//    for(i=0;i<1024;i += 16)
//    {
//	    R_WDT_Restart(); 
//	FDL_Write(1,i,FLASH_PARAM_SIZE);
//    }
    
//    NOP();
//    NOP();

	FDL_Write(Write_Block_u8,Write_Pos_u16,FLASH_PARAM_SIZE);
    
    return;
}

void DownLoadHmiDataFromFlash(uint8_t * const DataBuff_pu8)
{
    bool ExitParsing_b = false;
    bool DataReadSuccfly = false;
    bool FullSectorAval_b = false;
    uint8_t FdlReadSector_u8 = FDL_SECTOR_START;
    uint16_t Sctr_Cnt_u16 = 0;
    uint16_t i_u16 = 0;
    uint16_t j_u16 = 0;
    uint16_t ReadPos_u16 = 0;
    uint16_t WritePos_u16 = 0;
    uint8_t FullFdlSect_u8 = 0;
    Flash_Param_St_t *Flash_Param_pSt;
    
    DFlashBlockInit(NULL,NULL,NULL);
    ReadFdlState();

    for(Sctr_Cnt_u16 = 0;Sctr_Cnt_u16<TOTAL_FDL_SECTOR;Sctr_Cnt_u16++)
    {
        if(FLASH_PATTERN != FdlStatePattern_St[Sctr_Cnt_u16].ErasePattern_u32)
        {
            DFlash_Erase(FdlReadSector_u8);
            FillPatternToFdlBuff();
            DFlash_Write(FdlReadSector_u8,0,PATTERN_SIZE,&FDL_Data_au8[0]);
        }

        if((FLASH_PATTERN == FdlStatePattern_St[Sctr_Cnt_u16].CopyingPattern_u32) &&
                (FLASH_PATTERN != FdlStatePattern_St[Sctr_Cnt_u16].ActivePattern_u32))
        {
            if(true == FullSectorAval_b)
            {
                WritePos_u16 = ((TOTAL_DATA_BLOCK-1)*SIZE_OF_1_BLOCK) + SIZE_OF_PATTERN;
                WriteToNextSector(FullFdlSect_u8,FdlReadSector_u8,WritePos_u16);
                // DFlash_Erase(FullFdlSect_u8);
                // FillPatternToFdlBuff();
                // DFlash_Write(FullFdlSect_u8,0,PATTERN_SIZE,&FDL_Data_au8[0]);
            }
            else
            {
                DFlash_Erase(FdlReadSector_u8);
                FillPatternToFdlBuff();
                DFlash_Write(FdlReadSector_u8,0,PATTERN_SIZE,&FDL_Data_au8[0]);
                Write_Pos_u16 = SIZE_OF_PATTERN;
                Write_Block_u8 = 0;
                for(j_u16 = 0;j_u16<DATA_BLOCK_SIZE;j_u16++)
                {
                    DataBuff_pu8[j_u16] = 0x00;
                }
            }
            DataReadSuccfly = true;
        }
        else if((FLASH_PATTERN == FdlStatePattern_St[Sctr_Cnt_u16].ActivePattern_u32)&&
                (FLASH_PATTERN != FdlStatePattern_St[Sctr_Cnt_u16].FullPattern_u32))
        {
            for(i_u16 = 0;i_u16<TOTAL_DATA_BLOCK;i_u16++)
            {  
                ReadPos_u16 = (i_u16*SIZE_OF_1_BLOCK)+SIZE_OF_PATTERN;
                DFlash_Read(FdlReadSector_u8,ReadPos_u16,SIZE_OF_1_BLOCK,&FDL_Data_au8[0]);
                if((0xFF == FDL_Data_au8[0]) &&
                   (0xFF == FDL_Data_au8[SIZE_OF_1_BLOCK - 1]))
                {
                    Write_Block_u8 = FdlReadSector_u8;
                    Write_Pos_u16 = ReadPos_u16;

                    DFlash_Read(FdlReadSector_u8,(ReadPos_u16 - SIZE_OF_1_BLOCK),SIZE_OF_1_BLOCK,&FDL_Data_au8[0]);

                    for(j_u16 = 0;j_u16<DATA_BLOCK_SIZE;j_u16++)
                    {
                        DataBuff_pu8[j_u16] = FDL_Data_au8[j_u16+1];
                    }

                    ExitParsing_b = true;
                }
                else if((BLOCK_PATTERN == FDL_Data_au8[0]) &&
                        (0xFF == FDL_Data_au8[SIZE_OF_1_BLOCK - 1]))
                {
                    WritePos_u16 = ((TOTAL_DATA_BLOCK-1)*SIZE_OF_1_BLOCK) + SIZE_OF_PATTERN;
                    WriteToNextSector(FdlReadSector_u8,FdlReadSector_u8+1,WritePos_u16);
                    ExitParsing_b = true;
                }
                else
                {
                    ;
                }
                
                if(true == ExitParsing_b)
                {
                    DataReadSuccfly = true;
                    break; /* Exit parsing */
                }
                else if(i_u16 == (TOTAL_DATA_BLOCK - 1))
                {
                    for(j_u16 = 0;j_u16<DATA_BLOCK_SIZE;j_u16++)
                    {
                        DataBuff_pu8[j_u16] = FDL_Data_au8[j_u16+1];
                    }

                    FillPatternToFdlBuff();
                    DFlash_Write(FdlReadSector_u8,12,PATTERN_SIZE,&FDL_Data_au8[0]);
                    WritePos_u16 = ((TOTAL_DATA_BLOCK-1)*SIZE_OF_1_BLOCK) + SIZE_OF_PATTERN;
                    WriteToNextSector(FdlReadSector_u8,FdlReadSector_u8+1,WritePos_u16);
                    DFlash_Erase(FdlReadSector_u8);
                    DFlash_Write(FdlReadSector_u8,0,PATTERN_SIZE,&FDL_Data_au8[0]);
                    DataReadSuccfly = true;
                }
                else
                {

                }
            }
            break;
        }
        else if((FLASH_PATTERN == FdlStatePattern_St[Sctr_Cnt_u16].ActivePattern_u32)&&
                (FLASH_PATTERN == FdlStatePattern_St[Sctr_Cnt_u16].FullPattern_u32))
        {
            FullFdlSect_u8 = FdlReadSector_u8;
            if(false == FullSectorAval_b)
            {
                FullSectorAval_b = true;
            }
            
        }
        else
        {

        }

        if(true == DataReadSuccfly)
        {
            break;
        }

        if((TOTAL_FDL_SECTOR - 1) == Sctr_Cnt_u16)
        {
            if(true == FullSectorAval_b)
            {
                WritePos_u16 = ((TOTAL_DATA_BLOCK-1)*SIZE_OF_1_BLOCK) + SIZE_OF_PATTERN;
                WriteToNextSector(FullFdlSect_u8,FullFdlSect_u8+1,WritePos_u16);
            }
            else
            {
                for(j_u16 = 0;j_u16<DATA_BLOCK_SIZE;j_u16++)
                {
                    DataBuff_pu8[j_u16] = 0x00;
                }

                Write_Block_u8 = 0;
                Write_Pos_u16 = SIZE_OF_PATTERN;
                FillPatternToFdlBuff();
                DFlash_Write(FDL_SECTOR_START,4,PATTERN_SIZE,&FDL_Data_au8[0]);
                UploadHmiDataToFlash(DATA_BLOCK_SIZE,DataBuff_pu8);
		        Write_Pos_u16 = SIZE_OF_PATTERN+SIZE_OF_1_BLOCK;
		        FillPatternToFdlBuff();
                DFlash_Write(FDL_SECTOR_START,8,PATTERN_SIZE,&FDL_Data_au8[0]);
            }  
        }

        FdlReadSector_u8++;
    }

    return;
}

void ReadFdlState(void)
{
    uint8_t i = 0;
    uint8_t FdlSector_u8 = 0;
    uint8_t* FlashPattern_pau8 = (uint8_t*)FdlStatePattern_St;
    
    for(FdlSector_u8 = 0;FdlSector_u8<TOTAL_FDL_SECTOR;FdlSector_u8++)
    {
        //FDL_Read(FdlSector_u8,FDL_PATTERN_START_POS,sizeof(FdlStatePattern_St_t));
        DFlash_Read((FdlSector_u8+FDL_SECTOR_START),FDL_PATTERN_START_POS,SIZE_OF_PATTERN,&FDL_Data_au8[0]);
        for(i=0;i<sizeof(FdlStatePattern_St_t);i++)
        {
            *(FlashPattern_pau8++) = FDL_Data_au8[i];
        }
    }

    return;
}

void FillPatternToFdlBuff(void)
{
    FDL_Data_au8[0] = FlashPattern_u32 & 0xFF;
    FDL_Data_au8[1] = (FlashPattern_u32 >> 8) & 0xFF;
    FDL_Data_au8[2] = (FlashPattern_u32 >> 16) & 0xFF;
    FDL_Data_au8[3] = (FlashPattern_u32 >> 24) & 0xFF;
    return;
}

void WriteToNextSector(uint16_t SourceSector_u16, uint16_t DestinationSector_u16,uint16_t Pos_u16)
{
 
    if(DestinationSector_u16 >= (FDL_SECTOR_START+TOTAL_FDL_SECTOR))
    {
        DestinationSector_u16 = FDL_SECTOR_START;
    }
    
    if((FLASH_PATTERN != FdlStatePattern_St[DestinationSector_u16].ErasePattern_u32)||
       (0xFFFFFFFF != FdlStatePattern_St[DestinationSector_u16].CopyingPattern_u32)||
       (0xFFFFFFFF != FdlStatePattern_St[DestinationSector_u16].ActivePattern_u32)||
       (0xFFFFFFFF != FdlStatePattern_St[DestinationSector_u16].FullPattern_u32))  
    {
        DFlash_Erase(DestinationSector_u16);
        FillPatternToFdlBuff();
        DFlash_Write(DestinationSector_u16,0,PATTERN_SIZE,&FDL_Data_au8[0]);
    }
    
    FillPatternToFdlBuff();
    DFlash_Write(DestinationSector_u16,4,PATTERN_SIZE,&FDL_Data_au8[0]);
    DFlash_Read(SourceSector_u16,Pos_u16,SIZE_OF_1_BLOCK,&FDL_Data_au8[0]);
    
    DFlash_Write(DestinationSector_u16,SIZE_OF_PATTERN,SIZE_OF_1_BLOCK,&FDL_Data_au8[0]);
    FillPatternToFdlBuff();
    DFlash_Write(DestinationSector_u16,8,PATTERN_SIZE,&FDL_Data_au8[0]);

    Write_Block_u8 = DestinationSector_u16;
    Write_Pos_u16 = SIZE_OF_PATTERN+SIZE_OF_1_BLOCK;
}

void GetFilledSectorCopy(uint8_t* CopyBuff_pu8)
{
    uint8_t FilledSect_u8 = 0;
    uint16_t Pos_u16 = 0;

    Pos_u16 = ((TOTAL_DATA_BLOCK - 1)*SIZE_OF_1_BLOCK)+SIZE_OF_PATTERN;
    for(FilledSect_u8 = FDL_SECTOR_START;FilledSect_u8<(FDL_SECTOR_START+TOTAL_FDL_SECTOR);FilledSect_u8++)
    {
        if(FLASH_PATTERN == FdlStatePattern_St[FilledSect_u8].FullPattern_u32)
        {
            //FDL_Read(FilledSect_u8,Pos_u16,SIZE_OF_1_BLOCK);
            DFlash_Read(FilledSect_u8,Pos_u16,SIZE_OF_1_BLOCK,CopyBuff_pu8);
            break;
        }
    }

    return;
}

void GetErasedSector(void)
{
    uint8_t ErasedSect_u8 = 0;

    for(ErasedSect_u8 = FDL_SECTOR_START;ErasedSect_u8<(FDL_SECTOR_START+TOTAL_FDL_SECTOR);ErasedSect_u8++)
    {
        if((FLASH_PATTERN == FdlStatePattern_St[ErasedSect_u8].ErasePattern_u32)||
           (FLASH_PATTERN != FdlStatePattern_St[ErasedSect_u8].CopyingPattern_u32)||
           (FLASH_PATTERN != FdlStatePattern_St[ErasedSect_u8].ErasePattern_u32)||
           (FLASH_PATTERN != FdlStatePattern_St[ErasedSect_u8].ErasePattern_u32))
        {
            break;
        }
    }
    return;
}


void StoreHmiData(void)
{
    Read_OdoMeter(&Flash_Param_St);
    ReadWHkm(&Flash_Param_St);
    WriteFlashParam(Write_Block_u8,Write_Pos_u16,(uint8_t*)&Flash_Param_St);
    return;
}

void WriteFlashParam(uint8_t FdlSector_u8,uint16_t FdlPos_u16,uint8_t* Flash_Param_pu8)
{
    uint16_t i = 0;
   
    for(i=0;i<FlashParamSize_u8;i++)
    {
        dubWriteBuffer[i] = Flash_Param_pu8[i];
    }

    FDL_Write(FdlSector_u8,FdlPos_u16,FlashParamSize_u8);
    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : DFlashBlockInit
*   Description   : The function Initialize the Flash Function pointer to read , write, erase
*   Parameters    : Read_fptr : Pointer to the data flash read function.
*                   Write_fptr : Pointer to the data flash write function.
*                   Erase_fptr : pointer to the data flash erase function.
*   Return Value  : None
*  ---------------------------------------------------------------------------*/
void DFlashBlockInit(Flash_RW_fptr Read_fptr,Flash_RW_fptr Write_fptr,Flash_E_fptr Erase_fptr)
{
    DFlash_Read = ReadDataFlash;
    DFlash_Write = WriteDataFlash;
    DFlash_Erase = EraseDataSector;
    return;
}

/* -----------------------------------------------------------------------------
*  FUNCTION DESCRIPTION
*  -----------------------------------------------------------------------------
*   Function Name : UploadHmiDataToFlash
*   Description   : The file upload the HMI data to the data flash(emmulated eeprom)
*   Parameters    : DataSize_u16 : Size of the data to be written to the data flash
*                   DataBuff_pu8 : Pointer to the data buffer  
*   Return Value  : true -> Successfullt written. 
*                   false -> Failed to write.
*  ---------------------------------------------------------------------------*/
bool UploadHmiDataToFlash(uint16_t DataSize_u16, uint8_t* DataBuff_pu8)
{
    uint16_t i = 0;

    if(DataSize_u16 > (DATA_BLOCK_SIZE))
    {
        /* Data size must be less than or equal to the 
        configured data block(DATA_BLOCK_SIZE) size*/
        return false;
    }

    FDL_Data_au8[0] = BLOCK_PATTERN;

    for(i = 0;i<DataSize_u16;i++)
    {
        FDL_Data_au8[i+1] = DataBuff_pu8[i];
    }

    FDL_Data_au8[SIZE_OF_1_BLOCK - 1] = BLOCK_PATTERN;
    /* Last byte of the Data block contains pattern 0x5A to indicate
    Data has written succesfully */

    FDL_Data_au8[(SIZE_OF_1_BLOCK-2)] = Crc_CalculateCRC8(&FDL_Data_au8[1],(DATA_BLOCK_SIZE-2));
    /* Last second byte contains CRC of data block */
    
    if(NULL == DFlash_Write)
    {
        return false;
    }

    DFlash_Write(Write_Block_u8,Write_Pos_u16,SIZE_OF_1_BLOCK,&FDL_Data_au8[0]);

    return true;
}

void ReadDataFlash(uint16_t Sector_u16,uint16_t Position_u16, uint16_t Size_u16, uint8_t* DataBuff_pu8)
{
    uint16_t ByteCnt_u16 = 0;
    FDL_Read(Sector_u16,Position_u16,Size_u16);

    for(ByteCnt_u16 = 0;ByteCnt_u16<Size_u16;ByteCnt_u16++)
    {
        DataBuff_pu8[ByteCnt_u16] = dubReadBuffer[ByteCnt_u16];
    }

    return;
}

void WriteDataFlash(uint16_t Sector_u16,uint16_t Position_u16, uint16_t Size_u16, uint8_t* DataBuff_pu8)
{
    uint16_t ByteCnt_u16 = 0;
    for(ByteCnt_u16 = 0;ByteCnt_u16<Size_u16;ByteCnt_u16++)
    {
        dubWriteBuffer[ByteCnt_u16] = DataBuff_pu8[ByteCnt_u16];
    }
    FDL_Write(Sector_u16,Position_u16,Size_u16);
    return;
}

void EraseDataSector(uint16_t Sector_u16)
{
    FDL_Erase(Sector_u16,1);
    return;
}
/*---------------------- End of File -----------------------------------------*/
/*---------------------- End of File -----------------------------------------*/