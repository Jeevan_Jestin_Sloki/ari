/***********************************************************************************************************************
* File Name    : can_driver_conf.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 04/01/2022
***********************************************************************************************************************/

#ifndef CAN_DRIVER_CONF_H
#define CAN_DRIVER_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	
#include "App_typedefs.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define ENABLE_CAN_SLEEP_MODE				FALSE //todo:configurable	/* TRUE OR FALSE*/

/*
	CAN TX-MSG ID's
*/
#define TX_ID_7F1	                      	0x07F1 /*UDS-Response*/

#define TX_ID_7EB	                      	0x07EB /*TX Test Frame*/

#define TX_ID_19EF0085						0x19EF0085
	
/*	
	CAN RX-MSG ID's	
*/	
#define RX_ID7F0	                      	0x07F0 /*UDS-Request*/
	
#define RX_ID601							0x601
#define RX_ID602							0x602
#define RX_ID603							0X603
#define RX_ID1E1                       0X1E1
#define RX_ID1E4                       0X1E4
#define RX_ID1F5                       0X1F5
#define RX_ID202                       0X202
#define RX_ID205                       0X205




#define RX_ID7EA	                      	0x07EA

#define RX_ID_19EF008E						0x19EF008E

#define DUMMY_RX_ID							0U

/*
	DLC
*/

#define MAXIMUM_RTC_CAN_ID                      0x06 /* 6 CAN ID's are allowed for the User to configure the signal*/
#define RTC_CAN_ID_START_INDEX					0x05 /* The position of RTC CAN ID's in the " CAN_Msg_Conf_St" table*/
#define MSG_LENGTH                              0x08

#define TOTAL_CAN_TX_MSG						(1)
#define TOTAL_CAN_RX_MSG						(7) /* 6 RTC CAN ID's , 1 UDS, 1 Signal Config, HMI config ID */
#define TOTAL
#define TOTAL_CAN_MSGS							( TOTAL_CAN_TX_MSG + TOTAL_CAN_RX_MSG )


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	CAN_BAUD_NONE_E,
	CAN_BAUD_125KBPS_E,
	CAN_BAUD_250KBPS_E,
	CAN_BAUD_500KBPS_E,
	CAN_BAUD_1MBPS_E,
}CAN_Baud_Sel_En_t;


typedef enum
{
	CAN_TX_E,
	CAN_RX_E,
}CAN_MsgType_En_t;

typedef enum
{
	CAN_STD_E,
	CAN_EXT_E,
	CAN_NONE_E,
}CAN_Frame_En_t;


typedef struct
{
	CAN_MsgType_En_t	CAN_MsgType_En;
	CAN_Frame_En_t		CAN_Frame_En;
	uint32_t 			ID_u32;
	uint8_t  			DLC_u8;
	uint8_t  			BufferNum_u8;
}CAN_Msg_Conf_St_t;


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern 	CAN_Baud_Sel_En_t	CAN_Baud_Sel_En;
extern  CAN_Msg_Conf_St_t	CAN_Msg_Conf_St[TOTAL_CAN_MSGS];


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/


#endif /* CAN_DRIVER_CONF_H */


