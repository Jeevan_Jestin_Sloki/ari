/***********************************************************************************************************************
* File Name    : Speed_Conf.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 28/11/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Speed_Conf.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/
/*
		TOP text configration
*/
 const SignalsValue_St_t		TOP_text_SiganlValue_ast[TOP_TEXT_SIG_LEN]=
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	ZERO_E, 	TWO_E,		TOP_text_Sig_ast
};

 const SignalConfig_St_t		TOP_text_Sig_ast[TWO_E]=
{
/*PositionSignalValue  SegmentsRequired  SegmentsConfiguration*/
	{ON_E, 		ONE_SEG_E,	TOP_text_on_Sig_ast},
	{OFF_E, 	ONE_SEG_E,	TOP_text_off_Sig_ast},
};

 const SegConfig_St_t		TOP_text_on_Sig_ast[ONE_SEG_E]=
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	&SEG14, 	0x0BU,		0x04U
};
 const SegConfig_St_t		TOP_text_off_Sig_ast[ONE_SEG_E]=
{
	&SEG14, 	0x0BU,		0x00U
};
/*
		Speed didgit configration
*/
 const SignalsValue_St_t		Speed_SignalsValue_aSt[SPEED_SIG_LEN] =
{
	{ZERO_E, 	TWELVE_E,	SpeedSig1Conf_ast},
	{ONE_E, 	THIRTEEN_E,	SpeedSig2Conf_ast},
	{TWO_E, 	TWO_E,		SpeedSig3Conf_ast},
};



 const SignalConfig_St_t			SpeedSig1Conf_ast[TWELVE_E] = 
{
	{ZERO_E,	FOUR_SEG_E,	Speed_S1_0_SegConf_aSt  },
	{ONE_E,		FOUR_SEG_E,	Speed_S1_1_SegConf_aSt  },
	{TWO_E,		FOUR_SEG_E,	Speed_S1_2_SegConf_aSt  },
	{THREE_E,	FOUR_SEG_E,	Speed_S1_3_SegConf_aSt  },
	{FOUR_E,	FOUR_SEG_E,	Speed_S1_4_SegConf_aSt  },
	{FIVE_E,	FOUR_SEG_E,	Speed_S1_5_SegConf_aSt  },
	{SIX_E,		FOUR_SEG_E,	Speed_S1_6_SegConf_aSt  },
	{SEVEN_E,	FOUR_SEG_E,	Speed_S1_7_SegConf_aSt  },
	{EIGHT_E,	FOUR_SEG_E,	Speed_S1_8_SegConf_aSt  },
	{NINE_E,	FOUR_SEG_E,	Speed_S1_9_SegConf_aSt  },
	{OFF_DIGIT,	FOUR_SEG_E,	Speed_S1_Off_SegConf_aSt},
	{DISPLAY_H,	FOUR_SEG_E,	Speed_S1_H_SegConf_aSt  },
};

 const SignalConfig_St_t			SpeedSig2Conf_ast[THIRTEEN_E] = 
{
	{ZERO_E,	FOUR_SEG_E,	Speed_S2_0_SegConf_aSt},
	{ONE_E,		FOUR_SEG_E,	Speed_S2_1_SegConf_aSt},
	{TWO_E,		FOUR_SEG_E,	Speed_S2_2_SegConf_aSt},
	{THREE_E,	FOUR_SEG_E,	Speed_S2_3_SegConf_aSt},
	{FOUR_E,	FOUR_SEG_E,	Speed_S2_4_SegConf_aSt},
	{FIVE_E,	FOUR_SEG_E,	Speed_S2_5_SegConf_aSt},
	{SIX_E,		FOUR_SEG_E,	Speed_S2_6_SegConf_aSt},
	{SEVEN_E,	FOUR_SEG_E,	Speed_S2_7_SegConf_aSt},
	{EIGHT_E,	FOUR_SEG_E,	Speed_S2_8_SegConf_aSt},
	{NINE_E,	FOUR_SEG_E,	Speed_S2_9_SegConf_aSt},
	{OFF_DIGIT,	FOUR_SEG_E,	Speed_S2_Off_SegConf_aSt},
	{DISPLAY_G,	FOUR_SEG_E,	Speed_S2_G_SegConf_aSt},
	{DISPLAY_C,	FOUR_SEG_E,	Speed_S2_C_SegConf_aSt},
};

 const SignalConfig_St_t			SpeedSig3Conf_ast[TWO_E] = 
{
	{OFF_DIGIT,	ONE_SEG_E,	Speed_S3_Off_SegConf_aSt},
	{ONE_E,		ONE_SEG_E,	Speed_S3_1_SegConf_aSt},
};


 const SegConfig_St_t			Speed_S1_0_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x00U,		0x0FU},
	{&SEG9,		0x00U,		0x0BU},
	{&SEG8,		0x00U,		0x0FU},
	{&SEG7,		0x0DU,		0x02U},
	
};

 const SegConfig_St_t			Speed_S1_1_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x00U,		0x00U},
	{&SEG9,		0x00U,		0x00U},
	{&SEG8,		0x00U,		0x0FU},
	{&SEG7,		0x0DU,		0x02U},
};

 const SegConfig_St_t			Speed_S1_2_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x00U,		0x0BU},
	{&SEG9,		0x00U,		0x0FU},
	{&SEG8,		0x00U,		0x0EU},
	{&SEG7,		0x0DU,		0x02U},
};

 const SegConfig_St_t			Speed_S1_3_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x00U,		0x0AU},
	{&SEG9,		0x00U,		0x0FU},
	{&SEG8, 	0x00U,		0x0FU},
	{&SEG7,		0x0DU,		0x02U},
};

 const SegConfig_St_t			Speed_S1_4_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x00U,		0x0EU},
	{&SEG9,		0x00U,		0x04U},
	{&SEG8,		0x00U,		0x0FU},
	{&SEG7,		0x0DU,		0x02U},
};

 const SegConfig_St_t			Speed_S1_5_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x00U,		0x0EU},
	{&SEG9,		0x00U,		0x0FU},
	{&SEG8,		0x00U,		0x0BU},
	{&SEG7,		0x0DU,		0x02U},
};

 const SegConfig_St_t			Speed_S1_6_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x0DU,		0x0FU},
	{&SEG9,		0x00U,		0x0FU},
	{&SEG8,		0x00U,		0x0BU},
	{&SEG7,		0x0DU,		0x02U},
};


 const SegConfig_St_t			Speed_S1_7_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x00U,		0x08U},
	{&SEG9,		0x00U,		0x08U},
	{&SEG8,		0x00U,		0x0FU},
	{&SEG7,		0x0DU,		0x02U},
};


 const SegConfig_St_t			Speed_S1_8_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x00U,		0x0FU},
	{&SEG9,		0x00U,		0x0FU},
	{&SEG8,		0x00U,		0x0FU},
	{&SEG7,		0x0DU,		0x02U},
};


 const SegConfig_St_t			Speed_S1_9_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x00U,		0x0EU},
	{&SEG9,		0x00U,		0x0FU},
	{&SEG8,		0x00U,		0x0FU},
	{&SEG7,		0x0DU,		0x02U},
};

 const SegConfig_St_t			Speed_S1_Off_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x00U,		0x00U},
	{&SEG9,		0x00U,		0x00U},
	{&SEG8,		0x00U,		0x00U},
	{&SEG7,		0x0DU,		0x00U},
};

 const SegConfig_St_t			Speed_S1_H_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG10,	0x00U,		0x0FU},
	{&SEG9,		0x00U,		0x05U},
	{&SEG8,		0x00U,		0x0FU},
	{&SEG7,		0x0DU,		0x02U},
};

 const SegConfig_St_t			Speed_S2_0_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x0FU},
	{&SEG12,	0x00U,		0x0BU},
	{&SEG11,	0x00U,		0x0FU},
	{&SEG7,		0x0EU,		0x01U},
};

 const SegConfig_St_t			Speed_S2_1_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x00U},
	{&SEG12,	0x00U,		0x00U},
	{&SEG11,	0x00U,		0x0FU},
	{&SEG7,		0x0EU,		0x01U},
};

 const SegConfig_St_t			Speed_S2_2_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x0BU},
	{&SEG12,	0x00U,		0x0FU},
	{&SEG11,	0x00U,		0x0EU},
	{&SEG7,		0x0EU,		0x01U},
	
};

 const SegConfig_St_t			Speed_S2_3_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x0AU},
	{&SEG12,	0x00U,		0x0FU},
	{&SEG11,	0x00U,		0x0FU},
	{&SEG7,		0x0EU,		0x01U},
	
};

 const SegConfig_St_t			Speed_S2_4_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x0EU},
	{&SEG12,	0x00U,		0x04U},
	{&SEG11,	0x00U,		0x0FU},
	{&SEG7,		0x0EU,		0x01U},
};

 const SegConfig_St_t			Speed_S2_5_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x0EU},
	{&SEG12,	0x00U,		0x0FU},
	{&SEG11,	0x00U,		0x0BU},
	{&SEG7,		0x0EU,		0x01U},
};

 const SegConfig_St_t			Speed_S2_6_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x0FU},
	{&SEG12,	0x00U,		0x0FU},
	{&SEG11,	0x00U,		0x0BU},
	{&SEG7,		0x0EU,		0x01U},
};


 const SegConfig_St_t			Speed_S2_7_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x08U},
	{&SEG12,	0x00U,		0x08U},
	{&SEG11,	0x00U,		0x0FU,},
	{&SEG7,		0x0EU,		0x01U},
	
};


 const SegConfig_St_t			Speed_S2_8_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x0FU,},
	{&SEG12,	0x00U,		0x0FU,},
	{&SEG11,	0x00U,		0x0FU,},
	{&SEG7,		0x0EU,		0x01U,},
	
};


 const SegConfig_St_t			Speed_S2_9_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x0EU,},
	{&SEG12,	0x00U,		0x0FU,},
	{&SEG11,	0x00U,		0x0FU,},
	{&SEG7,		0x0EU,		0x01U,},
	
};

 const SegConfig_St_t			Speed_S2_Off_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x00U},
	{&SEG12,	0x00U,		0x00U},
	{&SEG11,	0x00U,		0x00U},
	{&SEG7,		0x0EU,		0x00U},
	
};


 const SegConfig_St_t			Speed_S2_G_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x0FU},
	{&SEG12,	0x00U,		0x0BU},
	{&SEG11,	0x00U,		0x0BU,},
	{&SEG7,		0x0EU,		0x01U},
	
};

 const SegConfig_St_t			Speed_S2_C_SegConf_aSt[FOUR_SEG_E] = 
{
	{&SEG13,	0x00U,		0x0FU},
	{&SEG12,	0x00U,		0x0BU},
	{&SEG11,	0x00U,		0x08U},
	{&SEG7,		0x0EU,		0x01U},
	
};


 const SegConfig_St_t			Speed_S3_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG14,	0x07U,		0x00U},
};

 const SegConfig_St_t			Speed_S3_1_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG14,	0x07U,		0x08U},
};