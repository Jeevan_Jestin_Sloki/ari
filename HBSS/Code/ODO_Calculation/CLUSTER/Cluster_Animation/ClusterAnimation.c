/***********************************************************************************************************************
* File Name    : ClusterAnimation.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 10/01/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "ClusterAnimation.h"
#include "SegDispWrite.h"
#include "GenConfig.h"
#include "digits_utils.h"
#include "DataBank.h"
#include "Navigation_Disp.h"
#include "ODO_Disp.h"
#include "SOC_Disp.h"
#include "SpeedDisp.h"
#include "TelltaleDisp.h"
#include "PowerConsum_Disp.h"
#include "TimeDisp.h"
#include "RangeKmDisp.h"
#include "timer_user.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool 		AnimationDone_b 	= false;
bool		CommonSeg_On_b 		= false;
uint32_t 	Animation5msCounter_u32 = 0;
uint8_t 	AN_dir_count		= 0;

SpeedAnimation_En_t	SpeedAnimation_En = SPEED_NO_CHANGE_E;
AnimationData_St_t	AnimationData_St  = 
{
	0,
	0,
	0,
	0,
	0,
	10,
};

Animation_dir_St_t	Animation_dir_St[MAX_NAVIG_DIR_SIGNAL] = 
{
	LEFT_U_TURN_AN_E,
	LEFT_TURN_AN_E,
	STRAIGHT_AN_E,
	RIGHT_TURN_AN_E,
	RIGHT_U_TURN_AN_E
};

/***********************************************************************************************************************
* Function Name: AnimateOnStart
* Description  : This function schedules the Anmation tasks at different rates.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void AnimateOnStart(void)
{
	Animation5msCounter_u32++;
	if(CommonSeg_On_b == false)
	{
		Turn_On_Common_Signals();
		CommonSeg_On_b = true;
	}
	if(((Animation5msCounter_u32 % 80) == 0)&&(SpeedAnimation_En == SPEED_NO_CHANGE_E))
	{
		/*To Provide 400ms delay after Logo-On*/
		SpeedAnimation_En = SPEED_ANIMATE_START_E;
	}
	if((Animation5msCounter_u32 % 9) == 0)
	{
		/*Schedule 45ms Animation Tasks here*/
		if((SpeedAnimation_En == SPEED_ANIMATE_START_E)||(SpeedAnimation_En == INCREASE_SPEED_E))
		{
			/*Start Animating the vehicle speed*/
			AnimateIncreaseVehicleSpeed();
		}
		
	}
	
	if((Animation5msCounter_u32 % 8) == 0)
	{
		/*Schedule 40ms Animation Tasks here*/
		if(SpeedAnimation_En == DECREASE_SPEED_E)
		{
			/*Start Animating the vehicle speed*/
			AnimateDecreaseVehicleSpeed();
		}
		
	}
	
	if(SpeedAnimation_En == INTERMEDIATE_DELAY_E)
	{
		AnimationData_St.SpeedDelayCounter_u16++; 
			/*Increments by 1 for each 5ms (Starts after Speed reaches 100) */
		if(0 == (AnimationData_St.SpeedDelayCounter_u16 % 80)) /*Provide Intermediate delay of 400ms*/
		{
			SpeedAnimation_En = DECREASE_SPEED_E;
			AnimationData_St.SpeedDelayCounter_u16 = 0;
			
			SetTopRowTelltales(SET_SIG_ACTUAL_E);
			Write_SEG((ClusterSignals_En_t)STRAIGHT_AN_E, (uint32_t)0);
			Write_SEG(ODO_E, ZERO_E );
//			Set_ODO_and_Time(SET_SIG_ACTUAL_E);
//			Set_CentreTelltales(SET_SIG_ACTUAL_E);
			SetBarsActualValue();
		}
	}
	return;
}


/***********************************************************************************************************************
* Function Name: Turn_On_Common_Signals
* Description  : This function turns-on the common signals on the cluster dispaly.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Turn_On_Common_Signals(void)
{
	//Turn_OnOff_AllSegments(SET_SEG_REGISTER); /*todo:dileepabs*/
	Write_SEG(COMMON_SIG_E, 0X01);
	return;
}

void AnimateIncreaseVehicleSpeed(void)
{
	if(SpeedAnimation_En == SPEED_ANIMATE_START_E)
	{
		SpeedAnimation_En = INCREASE_SPEED_E;
		SetDrivingModeRings(TURN_ON_SIG_E);
	}
	
	Write_SEG(SPEED_E, (uint32_t)AnimationData_St.VehicleSpeed_s16);
	
	Write_SEG(BATT_SOC_BAR_E, AnimationData_St.BarsCountValue_u32);
	Write_SEG(POWER_CONSUMP_E, AnimationData_St.BarsCountValue_u32);
	if(SpeedAnimation_En == INCREASE_SPEED_E)
	{
		AnimationData_St.VehicleSpeed_s16 += INCREASE_IN_SPEED;
		
		if(0 == (AnimationData_St.VehicleSpeed_s16 % 20))
		{
			AnimationData_St.BarsCount_u16 = (AnimationData_St.VehicleSpeed_s16 / 20); 
			AnimationData_St.BarsCountValue_u32 += Find_Power_Of_10((AnimationData_St.BarsCount_u16 - 1));
			//Write_SEG((ClusterSignals_En_t)(Animation_dir_St[AN_dir_count].Navig_dir_AN_En), 0x01);
			//AN_dir_count++;
		}
		
//		if(0 == (AnimationData_St.VehicleSpeed_s16 % 35))
//		{
//			Set_CentreTelltales(TURN_ON_SIG_E);
//		}

		if(0 == (AnimationData_St.VehicleSpeed_s16 % 10))
		{
			//Display_Navig_dist(AnimationData_St.NavigDist_value_u16, NAVIG_DIST_E);
			Write_SEG(BATT_SOC_DIGIT_E, (uint32_t)AnimationData_St.SOCpercentage_u16);
			
			AnimationData_St.NavigDist_value_u16 += INCREASE_IN_NAVIG_DIST;
			AnimationData_St.SOCpercentage_u16   += INCREASE_IN_SOC_PER;
			
		}
		
		if(AnimationData_St.VehicleSpeed_s16 == (MAXIMUM_SPEED + INCREASE_IN_SPEED))
		{
			AnimationData_St.VehicleSpeed_s16 = MAXIMUM_SPEED;
			
			SetTopRowTelltales(TURN_ON_SIG_E);
//			Set_ODO_and_Time(TURN_ON_SIG_E);
			Write_SEG(ODO_E, (uint32_t)ODO_DIGIT_FOR_ANIMATION);
//			Write_SEG((ClusterSignals_En_t)STRAIGHT_AN_E, (uint32_t)7);
			AN_dir_count = 0;
			SpeedAnimation_En = INTERMEDIATE_DELAY_E;
		}
	}
	else
	{
		;
	}
	return;
}

void AnimateDecreaseVehicleSpeed(void)
{
	Write_SEG(SPEED_E, (uint32_t)AnimationData_St.VehicleSpeed_s16);
	Navig_previous_state_b = true;
	Display_Navigation(ClusterSignals_St.NavigationDistance_u16, ClusterSignals_St.NavigationDirection_u8,
			   NAVIG_DIST_E, ClusterSignals_St.Navigation_Status_u8 );
	
	AnimationData_St.VehicleSpeed_s16 -= DECREASE_IN_SPEED;
			
	if(AnimationData_St.VehicleSpeed_s16 == (MINIMUM_SPEED - DECREASE_IN_SPEED))
	{
		AnimationData_St.VehicleSpeed_s16 = MINIMUM_SPEED;
		SetBottomRowTelltales(TURN_ON_SIG_E);
//		Set_Range_and_Mileage(TURN_ON_SIG_E);
//		SetDrivingModeRings(TURN_ON_SIG_E);
		Write_SEG(RANGE_KM_E,     (uint32_t)RANGEKM_FOR_ANIMATE);
//		Write_SEG(HOURS_TIME_E,   (uint32_t)HOURS_TM_FOR_ANIMATION);
//		Write_SEG(MINUTES_TIME_HB_E, (uint32_t)8);
//		Write_SEG(MINUTES_TIME_LB_E, (uint32_t)8);
		SpeedAnimation_En = SPEED_ANIMATE_END_E;
	}
	else
	{
		;
	}
	return;
}

void SetTopRowTelltales(SignalState_En_t SignalStateEn)
{
	switch(SignalStateEn)
	{
		case TURN_ON_SIG_E:
		{
			Write_SEG(ODO_TEXT_E,       (uint32_t)ON);
			Write_SEG(TRIP_TEXT_E,      (uint32_t)ON);
			Write_SEG(TEXT_A_E,         (uint32_t)ON);
			Write_SEG(TEXT_B_E,         (uint32_t)ON);
			Write_SEG(LEFT_IND_E,       (uint32_t)ON);
			Write_SEG(ENGINE_FAULT_E,   (uint32_t)ON);
			Write_SEG(WARNING_IND_E,    (uint32_t)ON);
			Write_SEG(KILL_SWITCH_E,    (uint32_t)ON);
			Write_SEG(MOTOR_FAULT_E,    (uint32_t)ON);
			Write_SEG(SERV_REM_E,       (uint32_t)ON);
			Write_SEG(RIGHT_IND_E,      (uint32_t)ON);
			Write_SEG(TOP_TEXT_E,       (uint32_t)ON);
			Write_SEG(POWER_W_IND_E,    (uint32_t)ON);
			break;	
		}
		case SET_SIG_ACTUAL_E:
		{
			if(OdoTrip_Sel_En == ODO_DISP_E)
			{
				Write_SEG(TRIP_TEXT_E, (uint32_t)OFF);
				Write_SEG(TEXT_B_E,    (uint32_t)OFF);
				Write_SEG(TEXT_A_E,    (uint32_t)OFF);
			}
			if(( OdoTrip_Sel_En == TRIP_DISP_E ) && ( TripAB_Sel_En == DISP_TRIP_A_E ))
			{
				Write_SEG(ODO_TEXT_E,  (uint32_t)OFF);
				Write_SEG(TEXT_B_E,    (uint32_t)OFF);
			}
			if(( OdoTrip_Sel_En == TRIP_DISP_E ) && ( TripAB_Sel_En == DISP_TRIP_B_E ))
			{
				Write_SEG(ODO_TEXT_E,  (uint32_t)OFF);
				Write_SEG(TEXT_A_E,    (uint32_t)OFF);
			}
			if(ClusterSignals_St.LeftIndicator_u8 == 0x00U)
			{
				Write_SEG(LEFT_IND_E,  (uint32_t)OFF);
			}
			if(ClusterSignals_St.EngineFault_u8 == 0x00U)
			{
				Write_SEG(ENGINE_FAULT_E,  (uint32_t)OFF);
			}
			if(ClusterSignals_St.Warning_u8 == 0x00U)
			{
				Write_SEG(WARNING_IND_E,     (uint32_t)OFF);
			}
			if(ClusterSignals_St.KillSwitch_u8 == 0x00U)
			{
				Write_SEG(KILL_SWITCH_E,    (uint32_t)OFF);
			}
			if(ClusterSignals_St.MotorFault_u8 == 0x00U)
			{
				Write_SEG(MOTOR_FAULT_E,   (uint32_t)OFF);
			}
			if(ClusterSignals_St.ServiceReminder_u8 == 0x00U)
			{
				Write_SEG(SERV_REM_E,   (uint32_t)OFF);
			}
			if(ClusterSignals_St.RightIndicator_u8 == 0x00U)
			{
				Write_SEG(RIGHT_IND_E,   (uint32_t)OFF);
			}
			if(ClusterSignals_St.TOPText_u8  == 0x00U)
			{
				Write_SEG(TOP_TEXT_E,   (uint32_t)OFF);
			}
			if(ClusterSignals_St.PowerIcon_u8 == 0x00U)
			{
				Write_SEG(POWER_W_IND_E,   (uint32_t)OFF);
			}
			break;	
		}
		default:
		{
			;	
		}
	}
	return;
}


void SetBottomRowTelltales(SignalState_En_t SignalStateEn)
{
	switch(SignalStateEn)
	{
		case TURN_ON_SIG_E:
		{
			Write_SEG(SIDE_STAND_E,     (uint32_t)ON);
			Write_SEG(NETWORK_E,        (uint32_t)ON);
			Write_SEG(BLE_E,            (uint32_t)ON);
			Write_SEG(HIGH_BEAM_E,      (uint32_t)ON);
//			Write_SEG(TEXT_AM_E,        (uint32_t)ON);
//			Write_SEG(TEXT_PM_E,	    (uint32_t)ON);
			Write_SEG(CHARGING_STATUS_E,(uint32_t)ON);
			break;	
		}
		case SET_SIG_ACTUAL_E:
		{
			/*todo:animation*/
			/*Signals Actual state/Value will be set from DisplayClusterData task*/
			break;	
		}
		default:
		{
			;	
		}
	}
	return;
}

void SetDrivingModeRings(SignalState_En_t SignalStateEn)
{
	switch(SignalStateEn)
	{
		case TURN_ON_SIG_E:
		{
			Write_SEG(REVERSE_MODE_E, (uint32_t)ON);
			//Write_SEG(NEUTRAL_MODE_E, (uint32_t)ON);
			Write_SEG(ECO_MODE_E,     (uint32_t)ON);
			//Write_SEG(SPORTS_MODE_E,  (uint32_t)ON); /* Todo: commented to remove perm*/
			break;	
		}
		case SET_SIG_ACTUAL_E:
		{
			/*todo:animation*/
			/*Signals Actual state/Value will be set from DisplayClusterData task*/
			break;	
		}
		default:
		{
			;	
		}
	}
	return;
}

void SetBarsActualValue(void)
{
	uint32_t BarCountValue_u32	= 0;
	uint16_t PowrConsump_u16 	= 0;
	uint16_t  BattSOC_u16 		= 0;
	
	PowrConsump_u16 = ClusterSignals_St.PowerConsumption_u16;
	BattSOC_u16 	= (uint16_t)ClusterSignals_St.BatterySOC_u8;
	BarCountValue_u32 = Get_Equi_Bars_Value(PowrConsump_u16);
	Write_SEG(POWER_CONSUMP_E, BarCountValue_u32);
				/*Display Actual Power Consumption*/
	BarCountValue_u32 = Get_SOC_Bars(BattSOC_u16, 0x0U);
	Write_SEG(BATT_SOC_BAR_E, BarCountValue_u32);
	return;
}

/***********************************************************************************************************************
* Function Name: ResetAnimationData
* Description  : This function resets the data /enums used for Cluster Animation.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetAnimationData(void)
{
	/* Reset flags related to the cluster display data*/
	AnimationDone_b 	= false;
	CommonSeg_On_b 		= false;
	Animation5msCounter_u32 = 0;
	AN_dir_count		= 0;
	SpeedAnimation_En 	= SPEED_NO_CHANGE_E;
	
	AnimationData_St.BarsCountValue_u32  	= 0;
	AnimationData_St.BarsCount_u16 	   	= 0;
	AnimationData_St.VehicleSpeed_s16	= 0;
	AnimationData_St.SpeedDelayCounter_u16 	= 0;
	AnimationData_St.NavigDist_value_u16   	= 0;
	AnimationData_St.SOCpercentage_u16    	= 10;
	
	_5ms_Counter_u16 		= RESET;
	
	
	InitialSpeed_Disp_b 		= false;
	SetSpeedToZero_b 		= false;
	
	InitialODO_Disp_b 		= false;
	OdoTrip_Sel_En 			= ODOTRIP_DISP_NONE_E;
	TripAB_Sel_En			= DISP_TRIP_NONE_E;
	
	InitialSOC_Disp_b 		= false;
	Charge_State_En 		= CHARGING_END_E;
	SOC_BarDispState_En		= SOC_GREEN_BAR_DISP_E;
	CH_SOC_Disp_En			= DISP_NONE_E;
	InitialPercent_Disp_b   	= false;
	
	InitialPowerConsum_Disp_b 	= false;
 	Valid_PowerConsum_u16 		= 0;
	PC_BarDispState_En		= PC_GREEN_BAR_DISP_E;
	
 	Initial_Minutes_Disp_b 		= false;
	Initial_Hours_Disp_b 		= false;

	InitialRangeKm_Disp_b 		= false;
	
	Init_navig_dist_b		= false;
	Init_navig_dir_b  		= false;
	Navig_previous_state_b 		= false;
	
	ClrTelltalesStates();
}
/********************************************************EOF***********************************************************/