
/******************************************************************************
 *    FILENAME    : fee_conf.c
 *    DESCRIPTION : EEPROM configuration 
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki     18/01/2017		   Initial version
 ******************************************************************************
*/ 

#include "fee_conf.h"


/* Maintain Size as multiple of 4 for proper backupram write */
/* The Address is MCU dependent. Size is L2 config table dependent 
*  (mainly depends on number of parameters in Freeze frame table.
*/
//const FEE_AddrConfig_St_t FEE_AddrConfig_aSt[TOTAL_FEE_SIGNAL_E] = 
//{
//  {FEE_FM_L2_ENTRY1, 	0x02001008,     48},
//  {FEE_FM_L2_ENTRY2, 	0x0200103A,     48},
//  {FEE_FM_L2_ENTRY3, 	0x0200106C,     48},
//  {FEE_FM_L2_ENTRY4, 	0x0200109E,     48},
//  {FEE_FM_L2_ENTRY5, 	0x020010D0,     48},
//  {FEE_FM_L2_ENTRY6, 	0x02001102,     48},
//  {FEE_FM_L2_ENTRY7, 	0x02001134,     48},
//  {FEE_FM_L2_ENTRY8, 	0x02001166,     48},
//  {FEE_FM_L2_ENTRY9, 	0x02001198,     48},
//  {FEE_FM_L2_ENTRY10, 	0x020011CA,     48},
//  {FEE_FM_RDYRESULTS, 	0x0200120C,     8},
//  {FEE_FM_TFSLC, 	0x02001214,     0},
//};



