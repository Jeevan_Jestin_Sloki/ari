/***********************************************************************************************************************
* File Name    : SpeedDisp.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 28/01/2021
***********************************************************************************************************************/

#ifndef SPEED_DISP_H
#define SPEED_DISP_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 
#include "Cluster_Conf.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define _300MS_DIVISOR_3					3
#define TOTAL_GO_ANIMATION_VALUES				4

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	SPEED_DISPLAY_E,
	SPEED_DISP_HOLD_E,
}SpeedDispState_En_t;

/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern bool 	    			InitialSpeed_Disp_b;
extern bool SetSpeedToZero_b;

/***********************************************************************************************************************
Export  Functions
***********************************************************************************************************************/
extern void Display_Speed(uint16_t, ClusterSignals_En_t );
uint16_t ValidateSpeed(uint16_t);
extern void SetSpeedToZero(uint16_t, ClusterSignals_En_t);

#endif /* SPEED_DISP_H */


