/***********************************************************************************************************************
* File Name    : Time.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 06/02/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Time.h"
#include "timer_user.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
TimeSetChnnl_En_t		TimeSetChnnl_En		= TIME_SET_CAN_E;		//todo:configurable
TimeUpdateChnnl_En_t	TimeUpdateChnnl_En 	= TIME_UPDATE_TIMER_E;	//todo:configurable
uint16_t				MinutesTime_u16 	= 0;
uint16_t				HoursTime_u16 		= 0;
uint16_t 				Hours_Counter_u16 	= 0; 
uint16_t 				Minute_Counter_u16 	= 0; 
uint16_t 				Second_Counter_u16  = 0;
static uint16_t			PrevHoursTime_u16	= 0xFFFFU;
static uint16_t			PrevMinutesTime_u16	= 0xFFFFU;


/***********************************************************************************************************************
* Function Name: Set_Real_Time
* Description  : This function Sets the Real Time on Ignition-ON.
* Arguments    : uint16_t HoursTimeSet_u16, uint16_t MinutesTimeSet_u16, uint16_t SecondsTimeSet_u16
* Return Value : None
***********************************************************************************************************************/
void Set_Real_Time(uint16_t HoursTimeSet_u16, uint16_t MinutesTimeSet_u16, uint16_t SecondsTimeSet_u16)
{
	if((HoursTimeSet_u16 != PrevHoursTime_u16)||(MinutesTimeSet_u16 != PrevMinutesTime_u16))
	{
		SecondsTimeSet_u16 = 0;
		
		Hours_Counter_u16  = HoursTimeSet_u16;
		Minute_Counter_u16 = MinutesTimeSet_u16;
		Second_Counter_u16 = SecondsTimeSet_u16;
		
		PrevHoursTime_u16   = HoursTimeSet_u16;
		PrevMinutesTime_u16 = MinutesTimeSet_u16;
	}
	else
	{
		;
	}
	return;
}


/***********************************************************************************************************************
* Function Name: Update_Time
* Description  : This function Updates the Minutes and Hours based on the interval timer interrupt.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Update_Time(void)
{
	if(TimeUpdateChnnl_En == TIME_UPDATE_TIMER_E)
	{
		//Set the Second counter for every 1 second.
		if(_1Second_Flag_b)
		{
			_1Second_Flag_b = false;
			Second_Counter_u16++;
		}
		/*Set the Minute counter for every 1 Minute (When second counter overflows)
					and resets the second counter to 0. */
		if(SEC_COUNT_LIMIT == Second_Counter_u16)
		{
			Second_Counter_u16 = 0;
			Minute_Counter_u16++;
		}
		/* Set the Hour counter for every 1 Hour (When Minute counter overflows )
				and resets the Minute counter to 0. */
		if(MIN_COUNT_LIMIT == Minute_Counter_u16)
		{
			Minute_Counter_u16 = 0;
			Hours_Counter_u16++;
		}
		//Resets the Hours counter when it overflows (When it reaches 24 Hrs).
		if(HOUR_COUNT_LIMIT == Hours_Counter_u16)
		{
			Hours_Counter_u16 = 0;
		}
		
		MinutesTime_u16 = Minute_Counter_u16;
		HoursTime_u16 = Hours_Counter_u16;
	}
	else
	{
		;
	}
	return;
}


/***********************************************************************************************************************
* Function Name: ResetRealTimeCounters
* Description  : This function resets the Real Time counters/variables used to set and update the time.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetRealTimeCounters(void)
{
	PrevHoursTime_u16	= 0xFFFFU;
	PrevMinutesTime_u16	= 0xFFFFU;
	return;
}


/********************************************************EOF***********************************************************/