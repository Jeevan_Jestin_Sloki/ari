/***********************************************************************************************************************
* File Name    : UserButtuonIn.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 30/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "UserButtuonIn.h"
#include "intc_user.h"
#include "Time.h"
#include "SegDispWrite.h"
#include "delay_flags.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
UserSettingSession_En_t	UserSettingSession_En 		= USER_SETTING_NOP_E;
TimeSetting_En_t		TimeSetting_En				= SET_TIME_END_E;
volatile uint32_t 		TimeTick_ms_u32 			= 0;

uint32_t 				PrevTime_ms_u32				= 0;
uint16_t 				UIB_HoursTime_u16			= 0;
uint16_t 				UIB_MinutesTime_u16			= 0;
uint16_t 				BtnPressedDuration_ms_u16 	= 0;
uint16_t 				NopFreeTimeLimit_u16 		= 0;
uint8_t  				MenuBtnPin_u8				= 0x00U;
bool 					HoursTimeState_b 			= ON;



/***********************************************************************************************************************
* Function Name: Read_UserInterfaceButton
* Description  : This function Reads the user interface button inputs Menu and Set.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Read_UserInterfaceButton(void)
{
	if((UserSettingSession_En == USER_SETTING_NOP_E) || (UserSettingSession_En == USER_SETTING_STOP_E))
	{
		//Read Menu Button Status
		MenuBtnPin_u8 = (uint8_t)P2_bit.no0; //Read the Input-4 (Pin P20) State
		
		if(MenuBtnPin_u8 == MENU_BTN_PRESSED)
		{
			//Button Pressed
			if(PrevTime_ms_u32 == 0)
			{
				PrevTime_ms_u32 = GET_CURRENT_TIME_MS();
			}
			BtnPressedDuration_ms_u16 += (GET_CURRENT_TIME_MS() - PrevTime_ms_u32);
			PrevTime_ms_u32 = GET_CURRENT_TIME_MS();
			
			if(BtnPressedDuration_ms_u16 >= MENU_BTN_PRESS_DURATION)
			{
				UserSettingSession_En = USER_SETTING_START_E;
							//Start the User Setting Session
				BtnPressedDuration_ms_u16 = 0;
				PrevTime_ms_u32 = 0;
			}
			
		}
		else if(MenuBtnPin_u8 == MENU_BTN_RELEASED)
		{
			//Button Released
			BtnPressedDuration_ms_u16 = 0;
			PrevTime_ms_u32 = 0;
		}
		else
		{
			; //Button Released
		}
	}
	
	if(UserSettingSession_En == USER_SETTING_START_E)
	{
		UserSettingSession(UserSettingSession_En);
	}
	
	return;
}


/***********************************************************************************************************************
* Function Name: UserSettingSession
* Description  : 
* Arguments    : UserSettingSession_En_t UserSetSession_En
* Return Value : None
***********************************************************************************************************************/	 
void UserSettingSession(UserSettingSession_En_t UserSetSession_En)
{
	if(TimeSetting_En == SET_TIME_END_E)
	{
		TimeSetting_En = SET_TIME_START_E;
		if(0 == PrevTime_ms_u32)
		{
			PrevTime_ms_u32 = GET_CURRENT_TIME_MS();
		}
	}
	
	NopFreeTimeLimit_u16 += (GET_CURRENT_TIME_MS() - PrevTime_ms_u32);
	PrevTime_ms_u32 = GET_CURRENT_TIME_MS();
	
	Write_SEG(TIME_COLON_E, ON); /*Set Time Colon Solid-ON*/
	Set_Hours_Time();
	Set_Minutes_Time();
	
	if(NopFreeTimeLimit_u16 >= NOP_FREE_TIME_LIMIT )
	{
		NopFreeTimeLimit_u16 = 0;
		PrevTime_ms_u32	= 0;
//		Initial_Minutes_Disp_b = false;
//		Initial_Hours_Disp_b = false;
		UserSettingSession_En = USER_SETTING_STOP_E;
		TimeSetting_En = SET_TIME_END_E;
	}
	
	return;
}


void Set_Hours_Time(void)
{
	if(TimeSetting_En == SET_TIME_START_E)
	{
		TimeSetting_En = SET_HOURS_TIME_E;
		UIB_HoursTime_u16 = GET_HOURS_TIME_COUNTER();
	}
	
	if(TimeSetting_En == SET_HOURS_TIME_E)
	{
		if(true == DelayFlagUIB500ms_b)
		{
			DelayFlagUIB500ms_b = false;
			
			if(ON == HoursTimeState_b)
			{
				Write_SEG(HOURS_TIME_E, (MAX_HOURS_TIME + 1));
				HoursTimeState_b = OFF;
			}
			else
			{
				Write_SEG(HOURS_TIME_E, UIB_HoursTime_u16);
				HoursTimeState_b = ON;
			}
		}
	}
	else
	{
		;
	}
	return;
}

void Set_Minutes_Time(void)
{
	if(TimeSetting_En == SET_MINUTES_TIME_E)
	{
		; //Set Minutes Time 
	}
	else
	{
		;
	}
	return;
}

/********************************************************EOF***********************************************************/