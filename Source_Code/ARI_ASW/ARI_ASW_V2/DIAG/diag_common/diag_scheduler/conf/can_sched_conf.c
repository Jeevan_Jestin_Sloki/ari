/******************************************************************************
 *    FILENAME    : can_sched.c
 *    DESCRIPTION : description of a can scheduler
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki     18/01/2017		   Initial version
 ******************************************************************************
*/ 



/* Section: Included Files                                                   */
#include "can_sched.h"
#include "can_sched_conf.h"
#include "cil_can_conf.h"
#include "math_util.h"
#include "diag_appl_test.h"
#include "Deserialize.h"
#include "ari_bms_can.h"
/**************************************************************************** */

/*
 * @summary:-    array of can Application structures    
 */
const CANSCHED_RX_Conf_St_t   CANSCHED_RX_Conf_aSt[CAN_SCHED_CONF_TOTAL_RX_MSG]=
{
	{ CIL_CANTP_REQ_TESTER_RX_E,   	    PERIODICITY_MS(100),		CANSched_RxMsgCallback, 	  NULL },
	{ CIL_BMS_MSG_1_E,   	            PERIODICITY_MS(10000),		CANSched_HMI_RxMsgCallback, 	  ClearBmsMsg1 },
    	{ CIL_BMS_MSG_2_E,   	            PERIODICITY_MS(10000),		CANSched_HMI_RxMsgCallback, 	  ClearBmsMsg2 },
	{ CIL_HMI_CONF_MSG_E,        	    PERIODICITY_MS(100),        	CANSched_HMI_RxMsgCallback,       NULL }, 
};


const CANSCHED_TX_Conf_St_t   CANSCHED_TX_Conf_aSt[CAN_SCHED_CONF_TOTAL_TX_MSG]=
{
    // CIL Sig name,			Cycle Time					Offset (Timeslice)			Call Back Function     
    { CIL_HMI_TX_1_E,			PERIODICITY_MS(100), 		PERIODICITY_MS(5),   		NULL },
    { CIL_HMI_TX_1_E,			PERIODICITY_MS(150), 		PERIODICITY_MS(10),   		NULL },
};


/* *****************************************************************************
 End of File
 */
