/***********************************************************************************************************************
* File Name    : Speed_Calc.c
* Version      : 01
* Description  : The file implement the calculation of speed data
* Created By   : Jeevan Jestin N
* Creation Date: 26/05/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#ifndef UIB_H
#define UIB_H

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	
#include "App_typedefs.h" 
/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define MAX_HOURS_TIME  12
#define HOUR_RESET_TIME 1
#define MAX_HIGHERMIN_TIME 5
#define MAX_LOWERMIN_TIME   9
#define MIN_RESET_TIME  0

#define TIMESETTING_TIMEOUT()   (SetButtonIdleTime_u32+7000)
/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/

typedef enum
{
    UIB_TIME_SET_NONE_E,
    UIB_TIME_HH_E,
    UIB_TIME_HM_E,
    UIB_TIME_LM_E,
    UIB_TIME_AM_E,
    UIB_TIME_PM_E
}UserButtonTimeSetState_En_t;

typedef enum
{
	AM_SEL_E,
	PM_SEL_E,
    MERDIEN_NONE_E,
}MerdienSel_En_t;

/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern UserButtonTimeSetState_En_t UserButtonTimeSetState_En;
extern uint8_t UIB_Min_time_u8;
extern uint8_t UIB_Hour_time_u8;
extern uint8_t UIB_LowerMin_time_u8;
extern uint8_t UIB_HighMin_time_u8;
/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void UpdateUserButton(void);
extern void TimeSettingUpdate(void);
extern void ClearUIBdata(void);
void UserButtonTimeout(void);
void ValidateHourMinTime(void);
void ModeButton(void);
void SetButton(void);
#endif