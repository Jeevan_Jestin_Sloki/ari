/******************************************************************************
 *    FILENAME    : diag_appl_test.c
 *    DESCRIPTION : Application test interface file for DIAG Stacks.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 


#include "diag_adapt.h" 
#include "diag_appl_test.h"
#include "diag_sys_conf.h"
#if(TRUE == DIAG_FM_SUPPORTED)
	#include "fm.h"
#endif
#if(TRUE == DIAG_UDS_SUPPORTED)
	#include "uds_conf.h"
#endif
//#include "hal_can_drv.h"

/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */



/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

//static int16_t ECT_Deg_s16 = 0;    //variable which holds the Engine starting coolent temperature    

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */
CanSchedMsg_St_t CanTest_St [1] = {0};

extern void TS_StopScheduler (void);
//uint8_t    diag_fltpath_u16 = 0xFF;
  
uint8_t test_fault_input1_au8[1] = {0}; 
uint8_t test_fault_input2_au8[1] = {0};
uint8_t test_fault_input3_au8[1] = {0};
uint8_t test_fault_input4_au8[1] = {0};  
uint8_t test_fault_input5_au8[1] = {0};  
uint8_t test_fault_input6_au8[1] = {0};
uint8_t test_fault_input7_au8[1] = {0};
uint8_t test_fault_input8_au8[1] = {0};
uint8_t test_fault_input9_au8[1] = {0};
uint8_t test_fault_input10_au8[1] = {0};

uint8_t Rv_test_fault_input1_au8[1] = {0};
uint8_t Rv_test_fault_input2_au8[1] = {0}; 
uint8_t Rv_test_fault_input3_au8[1] = {0}; 
uint8_t Rv_test_fault_input4_au8[1] = {0}; 
uint8_t Rv_test_fault_input5_au8[1] = {0}; 
uint8_t Rv_test_fault_input6_au8[1] = {0}; 
uint8_t Rv_test_fault_input7_au8[1] = {0}; 
uint8_t Rv_test_fault_input8_au8[1] = {0}; 
uint8_t Rv_test_fault_input9_au8[1] = {0}; 
uint8_t Rv_test_fault_input10_au8[1] = {0}; 

 FM_Test_St_t FM_Test_St[10] = 
 {
     {TEST_FAULT_DID_1 , test_fault_input1_au8},
     {TEST_FAULT_DID_2 , test_fault_input2_au8},
     {TEST_FAULT_DID_3 , test_fault_input3_au8},
     {TEST_FAULT_DID_4 , test_fault_input4_au8},
     {TEST_FAULT_DID_5 , test_fault_input5_au8},
     {TEST_FAULT_DID_6 , test_fault_input6_au8},
     {TEST_FAULT_DID_7 , test_fault_input7_au8},
     {TEST_FAULT_DID_8 , test_fault_input8_au8},
     {TEST_FAULT_DID_9 , test_fault_input9_au8},
     {TEST_FAULT_DID_10, test_fault_input10_au8},
 };



void diag_appl_test_fm_proc_50ms(void)
{
  uint8_t i = 0;
  
  for(i=0;i<10;i++)
  {
       if (0 == *FM_Test_St[i].FM_test_input_pu8)
       {
#if(TRUE == DIAG_FM_SUPPORTED)
           FM_ReportFault(FM_FP_TEST1_E, FM_NO_ERR);
#endif
       }        
       else if (1 == *FM_Test_St[i].FM_test_input_pu8)
       {
#if(TRUE == DIAG_FM_SUPPORTED)
           FM_ReportFault(FM_FP_TEST1_E, FM_MAX_ERR);
#endif
       }
       *FM_Test_St[i].FM_test_input_pu8 = 0xFF;
  }
       
//  if(*FM_Test_St[i].FM_test_input_pu8)
//  {
//        TS_StopScheduler();
//  }
}


//void diag_appl_test_fm_write(uint32_t Addr, uint32_t data)
//{
//    uint32_t i = 0;
//    uint8_t shift_u8 = 24;
//    uint32_t mask_u32 = 0xFF000000;
//
//    HAL_CAN_TxMessage_St_t CAN_Message_St;
//
//    CAN_Message_St.DLC_u8 = 8U;
//    CAN_Message_St.ID_u32 = 0x512;
//
//    CAN_Message_St.IDE_u8 = STD_CAN_E;
//    CAN_Message_St.RTR_u8 = DATA_FRAME_E;
//
//
//    CAN_Message_St.IsOnShotTx = FALSE;
//    CAN_Message_St.Label_u16 = 0x10;
//    CAN_Message_St.IsTxHistEn = FALSE;
//    CAN_Message_St.DataBytes_au8[0] = 0xFF;//write commande
//    for (i=1; i<5; i++)
//    {
//        CAN_Message_St.DataBytes_au8[i] = ((Addr & mask_u32) >> shift_u8);
//        shift_u8 -= 8;
//        mask_u32 = mask_u32 >> 8;
//    }
//    //command to write with address
//    HAL_CAN_SendCANMsg((HAL_CAN_BufferId_En_t)TX_BUFFER_ID, &CAN_Message_St);
//    shift_u8 = 24;
//    mask_u32 = 0xFF000000;
//    for (i=0; i<4; i++)
//    {
//        CAN_Message_St.DataBytes_au8[i] = ((data & mask_u32) >> shift_u8);
//        shift_u8 -= 8;
//        mask_u32 = mask_u32 >> 8;
//    }
//    //cammand to write the data
//    HAL_CAN_SendCANMsg((HAL_CAN_BufferId_En_t)TX_BUFFER_ID, &CAN_Message_St);
//}
//uint32_t diag_appl_test_fm_read(uint32_t Addr)
//{
//    uint32_t Data_u32 = 0;
//    uint32_t i = 0;
//    uint8_t shift_u8 = 24;
//    uint32_t mask_u32 = 0xFF000000;
//
//    HAL_CAN_TxMessage_St_t CAN_Message_St;
//
//    CAN_Message_St.DLC_u8 = 8U;
//    CAN_Message_St.ID_u32 = 0x512;
//
//    CAN_Message_St.IDE_u8 = STD_CAN_E;
//    CAN_Message_St.RTR_u8 = DATA_FRAME_E;
//    CAN_Message_St.DataBytes_au8[0] = 0x00;  //command to read
//    for (i=1; i<5; i++)
//    {
//        CAN_Message_St.DataBytes_au8[i] = ((Addr & mask_u32) >> shift_u8);
//        CAN_Message_St.DataBytes_au8[i + 4] = 0xFF;
//        shift_u8 -= 8;
//        mask_u32 = mask_u32 >> 8;
//    }
//    CAN_Message_St.DataBytes_au8[5] = 0x00;
//    CAN_Message_St.DataBytes_au8[6] = 0x00;
//    CAN_Message_St.DataBytes_au8[7] = 0x00;
//    shift_u8 = 24;
//    mask_u32 = 0xFF000000;
//    CAN_Message_St.IsOnShotTx = FALSE;
//    CAN_Message_St.Label_u16 = 0x10;
//    CAN_Message_St.IsTxHistEn = FALSE;
// // sent a command to read the data
//    HAL_CAN_SendCANMsg((HAL_CAN_BufferId_En_t)TX_BUFFER_ID, &CAN_Message_St);
//    //wait for recieving the data
//    while(CanTest_St->rxFlag == false);
//    for (i=1; i<5; i++)
//    {
//        Data_u32 |= (uint32_t)((CanTest_St->msg.DataBytes_au8[i] << shift_u8) & mask_u32);
//        shift_u8 -= 8;
//        mask_u32 = mask_u32 >> 8;
//    }
//    CanTest_St->rxFlag = false;
//    return Data_u32;
//
//
//}





