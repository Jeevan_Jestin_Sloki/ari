/***********************************************************************************************************************
* File Name    : ClusterCallback.c
* Version      : 01
* Description  : This file implements Callback function of HMI cluster.
* Created By   : Jeevan Jestin N
* Creation Date: 02/12/2021
***********************************************************************************************************************/
#ifndef CLUSTER_CALLBACK
#define CLUSTER_CALLBACK
/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"
#include "can_driver.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
#define REQ_BMS_BUFF_NO                     0x02

#define BATT_ID                             0x16
#define BATT_GENERAL_INFO_ID                0xBB
#define BATT_CAPACITY_INFO_ID               0xA3
#define BATT_DMUUMY_DATA                    0x7E
#define BMS_DATA_LEN                        0x08
/***********************************************************************************************************************
Structure & Enum Declaration
***********************************************************************************************************************/
typedef enum
{
    BATT_GENERAL_INFO_E,
    BATT_CAPACITY_INFO_E,
}BattInfoCmd_En_t;

/***********************************************************************************************************************
Extern variable
***********************************************************************************************************************/
extern bool SendRequest_b;
/***********************************************************************************************************************
Extern function
***********************************************************************************************************************/
extern void RequestBmsFrame(void);
#endif