/***********************************************************************************************************************
* File Name    : TimeDisp.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 12/12/2021
***********************************************************************************************************************/

#ifndef TIME_DISP_H
#define TIME_DISP_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" /*TODO: Dileepa*/
#include "Cluster_Conf.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	MINUTES_DISPLAY_E,
	MINUTES_DISP_HOLD_E,
}MinutesDispState_En_t;


typedef enum
{
	HOURS_DISPLAY_E,
	HOURS_DISP_HOLD_E,
}HoursDispState_En_t;

/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern bool 	     Initial_Minutes_Disp_b;
extern bool 	     Initial_Hours_Disp_b;


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Display_Time(uint16_t, uint16_t, ClusterSignals_En_t, ClusterSignals_En_t,ClusterSignals_En_t );
void Display_Temperature(ClusterSignals_En_t TEMPERATURE_HB_VALUE_E, ClusterSignals_En_t TEMPERATURE_LB_VALUE_E,
				ClusterSignals_En_t TEMPERATURE_TXT_E, uint8_t Disp_Temperature_u8);
uint16_t ValidateMinutesTime(uint16_t);
uint16_t ValidateHoursTime(uint16_t);
extern void Blink_Time_Colon(ClusterSignals_En_t);
extern void Clear_Time_InitFlags(void);
#endif /* TIME_DISP_H */


