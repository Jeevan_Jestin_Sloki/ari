/***********************************************************************************************************************
* File Name    : ODO_Calc.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 06/10/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "ODOmeter_Calc.h"
#include "Communicator.h"
#include "Speed_Calc.h"
#include "hmi_config_can.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
ODOmeterCal_En_t ODOmeterCal_En 	= ODO_CAL_NONE_E;
uint32_t PresentPulseCounter_u32 	= 0;
uint32_t UsedPulsesCounter_u32		= 0;
uint32_t Present_ODO_in_mm_u32		= 0;
uint32_t Present_ODOmeter_u32		= 0;
uint16_t DistancePerPulse_mm_u16 	= 0;
bool OdoInitFlag_b = false;

/***********************************************************************************************************************
* Function Name: Calculate_ODOmeter
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Calculate_ODOmeter(void)
{
	uint32_t DistanceTravelled_in_mm_u32	= 0;
	//uint16_t WheelRotationsDone_u16 		= 0;
	uint16_t PulseCountDone_u16 			= 0;

	if(false == OdoInitFlag_b)
	{
		OdoInitFlag_b = true;
		UsedPulsesCounter_u32 = GET_SPEED_PULSE_COUNT();
	}
	
	PresentPulseCounter_u32 = GET_SPEED_PULSE_COUNT(); /*Get the Total Speed Pulse Count*/

	if(PresentPulseCounter_u32 < UsedPulsesCounter_u32)
	{
		PulseCountDone_u16 = ( SPEED_PULSE_OVERFLOW_VALUE - UsedPulsesCounter_u32 ) + PresentPulseCounter_u32;
	}
	else
	{
		PulseCountDone_u16 	= (uint16_t)(PresentPulseCounter_u32 - UsedPulsesCounter_u32);
	}

	if(PulseCountDone_u16 > 0)
	{
		DistanceTravelled_in_mm_u32 = (uint32_t)(PulseCountDone_u16 * DistancePerPulse_mm_u16/*45*/);	
								/*Wheel 1 Rotation = 26 Pulses = 1171 mm Distance Travelled
									So, 1 Pulse = 1171 / 26 = 45 mm Distance travelled*/
		UsedPulsesCounter_u32 += PulseCountDone_u16;
		
		PresentODO_mm_u32 = PresentODO_mm_u32 + DistanceTravelled_in_mm_u32;
		
		PresentODOmeter_u32 += Update_ODOmeter(&PresentODO_mm_u32);
	}
	else
	{
		;
	}

	return;
}


/***********************************************************************************************************************
* Function Name: Find_Wheel_Rotations
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint16_t Find_Wheel_Rotations(uint16_t SpeedPulseCount_u16)
{
	uint16_t WheelRotations_u16 = 0;
	
	WheelRotations_u16 = (SpeedPulseCount_u16 / PULSE_COUNT_PER_ROTATION);
	
	return WheelRotations_u16;
}


/***********************************************************************************************************************
* Function Name: Update_ODOmeter
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint32_t Update_ODOmeter(uint32_t *ODOmeter_mm_pu32)
{
	uint32_t ODOmeter_in_m_u32 = 0;
	
	ODOmeter_in_m_u32 = (*ODOmeter_mm_pu32 / 1000);
	
	if(ODOmeter_in_m_u32 > 0)
	{
		*ODOmeter_mm_pu32 = (*ODOmeter_mm_pu32 % 1000);
	}
	else
	{
		;
	}
	
	return ODOmeter_in_m_u32;
}

/***********************************************************************************************************************
* Function Name: Set_ODOmeter
* Description  : 
* Arguments    : uint32_t SetODOmeter_u32
* Return Value : None
***********************************************************************************************************************/
void Set_ODOmeter(uint32_t SetODOmeter_u32)
{
	PresentODOmeter_u32 = SetODOmeter_u32;
	return; 
}


/***********************************************************************************************************************
* Function Name: Get_ODOmeter
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint32_t Get_ODOmeter(void)
{
	return PresentODOmeter_u32;
}


/***********************************************************************************************************************
* Function Name: Set_ODOmeter_in_mm
* Description  : 
* Arguments    : uint32_t SetODOin_mm_u32
* Return Value : None
***********************************************************************************************************************/
void Set_ODOmeter_in_mm(uint32_t SetODOin_mm_u32)
{
	PresentODO_mm_u32 = SetODOin_mm_u32;
	return;  
}


/***********************************************************************************************************************
* Function Name: Get_ODOmeter_in_mm
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint32_t Get_ODOmeter_in_mm(void)
{
	return PresentODO_mm_u32;
}

/***********************************************************************************************************************
* Function Name: Get_Distance_for_1_Pulse
* Description  : This function calculates the equivalent distance in mm for 1 Pulse Count.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Get_Distance_for_1_Pulse(void)
{
	DistancePerPulse_mm_u16 = (uint16_t)((GET_WHEEL_DIA()*PIE) / GET_PULSE_PER_ROTATION());
	return;
}

/***********************************************************************************************************************
* Function Name: ResetTheODOmeterData
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheODOmeterData(void)
{
	//PresentPulseCounter_u32 = RESET;
	UsedPulsesCounter_u32 	= RESET;
	Present_ODO_in_mm_u32 	= RESET;
	Present_ODOmeter_u32 	= RESET;
	DistancePerPulse_mm_u16 = 0;
	OdoInitFlag_b = false;
	return;
}

/********************************************************EOF***********************************************************/