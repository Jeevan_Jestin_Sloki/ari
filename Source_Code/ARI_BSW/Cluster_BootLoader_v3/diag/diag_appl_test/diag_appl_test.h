/******************************************************************************
 *    FILENAME    : diag_appl_test.c
 *    DESCRIPTION : Application test interface file for DIAG Stacks.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 

/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#ifndef _DIAG_APPL_TEST_H_
#define _DIAG_APPL_TEST_H_

#include "can_if.h"
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */
#define TEST_DEMO_CODE    (TRUE)
#define TEST_FM_EEPROM_DEMO    (FALSE)


#define TEST_FAULT_DID_1 0xA100
#define TEST_FAULT_DID_2 0xA101
#define TEST_FAULT_DID_3 0xA102
#define TEST_FAULT_DID_4 0xA103
#define TEST_FAULT_DID_5 0xA104
#define TEST_FAULT_DID_6 0xA105
#define TEST_FAULT_DID_7 0xA106
#define TEST_FAULT_DID_8 0xA107
#define TEST_FAULT_DID_9 0xA108
#define TEST_FAULT_DID_10 0xA109

extern uint8_t test_fault_input1_au8[1];
extern uint8_t test_fault_input2_au8[1]; 
extern uint8_t test_fault_input3_au8[1]; 
extern uint8_t test_fault_input4_au8[1]; 
extern uint8_t test_fault_input5_au8[1]; 
extern uint8_t test_fault_input6_au8[1]; 
extern uint8_t test_fault_input7_au8[1]; 
extern uint8_t test_fault_input8_au8[1]; 
extern uint8_t test_fault_input9_au8[1]; 
extern uint8_t test_fault_input10_au8[1]; 

extern uint8_t Rv_test_fault_input1_au8[1];
extern uint8_t Rv_test_fault_input2_au8[1]; 
extern uint8_t Rv_test_fault_input3_au8[1]; 
extern uint8_t Rv_test_fault_input4_au8[1]; 
extern uint8_t Rv_test_fault_input5_au8[1]; 
extern uint8_t Rv_test_fault_input6_au8[1]; 
extern uint8_t Rv_test_fault_input7_au8[1]; 
extern uint8_t Rv_test_fault_input8_au8[1]; 
extern uint8_t Rv_test_fault_input9_au8[1]; 
extern uint8_t Rv_test_fault_input10_au8[1]; 




typedef struct {
	uint16_t        FM_DID_u8;
	uint8_t*         FM_test_input_pu8;
}FM_Test_St_t;

extern FM_Test_St_t FM_Test_St[10];
extern CanSchedMsg_St_t CanTest_St [1];

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

extern void diag_appl_test_fm_proc_50ms(void);

extern void diag_appl_test_fm_write(uint32_t Addr, uint32_t data);
extern uint32_t diag_appl_test_fm_read(uint32_t Addr);
//extern void diag_appl_test_fm_write(uint32_t Addr, uint32_t data);



#endif


