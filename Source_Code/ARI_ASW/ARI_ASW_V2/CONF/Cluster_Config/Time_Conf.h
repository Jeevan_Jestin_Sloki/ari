/***********************************************************************************************************************
* File Name    : Time_Conf.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 31/12/2020
***********************************************************************************************************************/

#ifndef TIME_CONF_H
#define TIME_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
extern  const SignalsValue_St_t			MinutesTime_SignalsValue_aSt[MINUTES_TIME_SIG_LEN];
 
extern  const SignalConfig_St_t			MinutesTime_Sig1Conf_ast[ELEVEN_E];
extern  const SignalConfig_St_t			MinutesTime_Sig2Conf_ast[EIGHT_E];
 
extern  const SegConfig_St_t			MinutesTime_S1_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S1_Off_SegConf_aSt[TWO_SEG_E];
 
extern  const SegConfig_St_t			MinutesTime_S2_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			MinutesTime_S2_Off_SegConf_aSt[TWO_SEG_E];

extern  const SignalsValue_St_t			HoursTime_SignalsValue_aSt[HOURS_TIME_SIG_LEN];
 
extern  const SignalConfig_St_t			HoursTime_Sig1Conf_ast[ELEVEN_E];
extern  const SignalConfig_St_t			HoursTime_Sig2Conf_ast[FIVE_E];
 
extern  const SegConfig_St_t			HoursTime_S1_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_3_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_4_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_5_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_6_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_7_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_9_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S1_Off_SegConf_aSt[TWO_SEG_E];
 
extern  const SegConfig_St_t			HoursTime_S2_0_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S2_1_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S2_2_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S2_8_SegConf_aSt[TWO_SEG_E];
extern  const SegConfig_St_t			HoursTime_S2_Off_SegConf_aSt[TWO_SEG_E];


/*
   Time COLON teltail Configuration 
*/
extern  const SignalsValue_St_t			Colon_SignalsValue_aSt[COLON_SIG_LEN];
extern  const SignalConfig_St_t			ColonSigConf_ast[TWO_E];
extern  const SegConfig_St_t			Colon_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			Colon_Off_SegConf_aSt[ONE_SEG_E];


#endif /* TIME_CONF_H */


