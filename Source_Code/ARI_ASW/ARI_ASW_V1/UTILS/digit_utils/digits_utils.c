/***********************************************************************************************************************
* File Name    : digits_utils.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 05/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "digits_utils.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


/***********************************************************************************************************************
* Function Name: GetDigitsFromNum
* Description  : This function Seperates the digits from the num and stores it into an char buffer.
* Arguments    : uint16_t Number_u16, uint8_t *DigArr_p8
* Return Value : None
***********************************************************************************************************************/
void GetDigitsFromNum(uint32_t Number_u32, uint8_t *DigArr_p8)
{
	uint8_t	    i = 0;
	while(Number_u32)
	{
		DigArr_p8[i] = (uint8_t)(Number_u32 % 10);
		Number_u32 = Number_u32/10;
		i++;
	}
	return;
}


/***********************************************************************************************************************
* Function Name: Get_Equi_Bars_Value
* Description  : This function Finds the Equivalent BarsValue corresponding to the input Number.
                 Example : if input = 4
		 Equivalent Bars Value = 1111
* Arguments    : uint16_t       ValueReceived_u16
* Return Value : uint32_t   	EquivalentBars_u32
***********************************************************************************************************************/
uint32_t Get_Equi_Bars_Value(uint16_t ValueReceived_u16)
{  
	uint32_t   	EquivalentBars_u32 = 0; 
	uint16_t        i = 0;
	for(i=0; i < ValueReceived_u16; i++)
	{
		EquivalentBars_u32 += Find_Power_Of_10(i);
	}
	return EquivalentBars_u32;
}

/***********************************************************************************************************************
* Function Name: Find_Power_Of_10
* Description  : This function Finds the power of a base number 10.
* Arguments    : uint16_t         Exponent_u16
* Return Value : uint32_t 	  PowerOfNum_u32
***********************************************************************************************************************/
uint32_t Find_Power_Of_10(uint16_t Exponent_u16)
{
	uint32_t 	  PowerOfNum_u32 = 0;
	const uint16_t	  Base_u16 = 10;
	uint16_t	  i = 0;
	
	PowerOfNum_u32 = 1;
	
	for(i = 1; i <= Exponent_u16; i++)
	{
		PowerOfNum_u32 *= Base_u16;
	}
	return PowerOfNum_u32;
}

/********************************************************EOF***********************************************************/