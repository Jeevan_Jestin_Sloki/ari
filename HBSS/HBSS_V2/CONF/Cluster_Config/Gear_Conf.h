/***********************************************************************************************************************
* File Name    : Gear_Conf.h
* Version      : 01
* Description  : 
* Created By   : Pramod Jagtap
* Creation Date: 09/11/2022
***********************************************************************************************************************/

#ifndef GEAR_CONF_H
#define GEAR_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/

/*
    Gear Text
*/

extern const SignalsValue_St_t			GearText_SignalsValue_aSt[GEAR_TEXT_SIG_LEN];
extern const SignalConfig_St_t			GearText_SigConf_ast[TWO_E];
extern const SegConfig_St_t			GearText_Off_SegConf_aSt[ONE_SEG_E];
extern const SegConfig_St_t			GearText_On_SegConf_aSt[ONE_SEG_E];


/*
Gear Digit Configuration
*/

extern const SignalsValue_St_t		GearDigit_SignalsValue_aSt[GEAR_DIGIT_SIG_LEN];
extern const SignalConfig_St_t		GearDigit_SigConf_ast[ELEVEN_E];
extern const SegConfig_St_t	GearDigit_0_SegConf_aSt[TWO_SEG_E];
extern const SegConfig_St_t	GearDigit_1_SegConf_aSt[TWO_SEG_E];
extern const SegConfig_St_t	GearDigit_2_SegConf_aSt[TWO_SEG_E];
extern const SegConfig_St_t	GearDigit_3_SegConf_aSt[TWO_SEG_E];
extern const SegConfig_St_t	GearDigit_4_SegConf_aSt[TWO_SEG_E];
extern const SegConfig_St_t	GearDigit_5_SegConf_aSt[TWO_SEG_E];
extern const SegConfig_St_t	GearDigit_6_SegConf_aSt[TWO_SEG_E];
extern const SegConfig_St_t	GearDigit_7_SegConf_aSt[TWO_SEG_E];
extern const SegConfig_St_t	GearDigit_8_SegConf_aSt[TWO_SEG_E];
extern const SegConfig_St_t	GearDigit_9_SegConf_aSt[TWO_SEG_E];
extern const SegConfig_St_t	GearDigit_Off_SegConf_aSt[TWO_SEG_E];



#endif










