/******************************************************************************
 *    FILENAME    : uds_conf.h
 *    DESCRIPTION : This file contains configuration data for UDS services.
 ******************************************************************************
 * Revision history
 *
 * Ver Author       Date               Description
 * 1   Sloki     18/01/2017		   Initial version
 ******************************************************************************
*/

#ifndef _UDS_CONF_H_
#define _UDS_CONF_H_
/*
 **************************************************************************************************
 *    Includes
 **************************************************************************************************
 */

#include <stdint.h>
#include "str_util.h"

//#include "uds_serv19.h"
#include "uds_session.h"
#include "diag_appl_test.h"
#include "diag_sys_conf.h"
#if(TRUE == DIAG_CANTP_SUPPORTED)
	#include "i15765.h"
#endif

 /*
  **************************************************************************************************
  *    Defines
  **************************************************************************************************
  */

#define	TOTAL_ASW_BLOCKS	    0x02u

#define  TOTAL_RID                  0x02
/* Session modes */

#define UDS_SESS_DEFAULT	        0x01u
#define UDS_SESS_PROG		        0x02u
#define UDS_SESS_EXTENDED	        0x04u
#define UDS_SESS_ENGG		        0x08u	//(optional)
#define UDS_SESS_SUPPL_EOL	        0x10u   //(optional)


/*UDS SID LIST*/

#define SID_TESTERPRESENT	        0x3E
#define SID_SESSIONCONTROL	        0x10
#define SID_RDBDID			0x22
#define SID_WDBDID			0x2E
#define SID_IOCBDID		        0x2F
#define SID_SA				0x27
#define SID_RMBA			0x23
#define SID_WMBA		        0x3D
#define SID_CDTC                      	0x14
#define SID_RDTC                      	0x19
#define SID_COMMUNICATIONCONTROL      	0x28
#define SID_ROUTINE_CONTROL      	0x31
#define SID_CONTROLDTC      		0x85
#define SID_ECURESET                   	0x11
#define SID_DDID                      	0x2C
#define SID_PDID                        0x2A
#define SID_ROUTINE_CONTROL                   0x31
#define SID_REQUEST_DOWNLOAD                  0x34
#define SID_TRANSFER_DATA                     0x36
#define SID_TRANSFER_DATA_EXIST               0x37



/*UDS NRC LIST*/
#define SERVICE_NOT_SUPPORTED           0x11
#define SUB_FUNC_NOT_SUPPORTED          0x12
#define INVALID_MESSAGE_LENGTH          0x13
#define RESP_LEN_EXCEEDED               0x14
#define CONDITION_NOT_CORRECT           0x22
#define REQUEST_SEQUENCE_ERR            0x24
#define REQUEST_OUT_OF_RANGE            0x31
#define SECURITY_ACCESS_DENIED          0x33
#define INVALID_KEY                     0x35
#define EXCEEDED_NUMBER_OF_ATTEMPTS     0x36    
#define REQ_TIMEDELAY_NOTEX             0x37
#define TRANSFER_DATA_SUSPENDED         0x71
#define GEN_PROG_FAILURE                0x72  
#define WRONG_BLOCK_SEQUENCE_CNTR       0x73
#define ERROR_REQ_OK_RESP_PENDING       0x78
#define SUBFUNC_NOSUPP_IN_ACTIVE_SESS   0x7E
#define SERVNOSUPP_IN_ACTIVE_SESS       0x7F
#define VOLTAGE_TOO_HIGH                0x92
#define VOLTAGE_TOO_LOW                 0x93

#define NEGATIVE_RESP                  	0x7F


#define UPLOAD_DOWNLOAD_NOT_ACCEPTED		  0x70

/*number of services defined in the service distributor table*/
#define ServDistNumEntries_u32 		7u

/*
 **************************************************************************************************
 *    Enum
 **************************************************************************************************
 */

/* @Summary : - sessions which is going to be supported in this service */
typedef enum
{
	DEFAULT_SESSION_SUB_ID_E = 1u,
	PROGRAMMING_SESSION_SUB_ID_E,
	EXTENDED_DIAG_SESSION_SUB_ID_E,
	SAFETY_SYSTEM_DIAG_SESSION_SUB_ID_E,		/* OEM EOL (optional) */
	TOTAL_SESS_E = 5u
}SESSION_SUBID_En_t;
/* state machine for boot loader software. */
typedef enum
{
	SERVICE_IDLE_E = 0U,
	SERVICE31_SET_BOOTFLAG_E ,
	SERVICE31_UPDATE_HISTORY_E ,
	SERVICE83_E,
	SERVICE31_ERASE_E ,
	SERVICE34_E ,
	SERVICE36_E ,
	SERVICE37_E ,
	SERVICE31_CHECKSUM_E ,
	SERVICE31_CLEAR_BOOTFLAG_E,
}FlashState_En_t;

/* state machine for erase */
typedef enum
{
	NOT_ERASABLE_E = 0U,
	ERASABLE_E,
}EraseState_En_t;

typedef enum
{
	ASW1 = 2U,
	ASW2,
	ASW3,
	ASW4,
	ASW5,
	ASW6,
	ASW7,
	ASW8,
	ASW9,
	ASW10,
	TOTAL_ASW_E = (TOTAL_ASW_BLOCKS+1),
}Asw_block_En_t;
typedef struct
{
	uint8_t           SectorID;
	uint32_t          StartAddr_u32;
	uint32_t          EndAddr;
	EraseState_En_t EraseState_En;
}MemBlockConf_St_t;

typedef int8_t (*funptr)(uint8_t *Pu8);

typedef struct
{
	uint16_t RID ;
	uint8_t  RIDreqLength ;
	funptr callbackptr ;
	FlashState_En_t past_state ;
	FlashState_En_t present_state ;
	uint8_t responselength ;
}RIDConf_St_t;
/* Read or Write enum*/
typedef enum
{
	READ_ONLY_E = 0,
	WRITE_ONLY_E,
	READ_WRITE_E,
	TOTAL_E,
}Memory_AccessType_En_t;

/*
 ****************************************************************************************
 *    Structure Definition 
 ****************************************************************************************
*/
/*
 **************************************************************************************************
 *    Export Variables
 **************************************************************************************************
 */

extern const ServiceDistUDS_St_t UDSServDist_apt[ServDistNumEntries_u32];
extern MemBlockConf_St_t   	MemBlockConf_aSt[TOTAL_ASW_BLOCKS];                                          //structure of arrays  for memory blocks
extern RIDConf_St_t 		RIDConf_aSt[TOTAL_RID];

#endif

/* *****************************************************************************
 End of File
 ******************************************************************************/
