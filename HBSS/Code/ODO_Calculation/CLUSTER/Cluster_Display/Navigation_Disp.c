/***********************************************************************************************************************
* File Name    : Navigation_Disp.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 15/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Navigation_Disp.h"
#include "SegDispWrite.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Global variables and function
***********************************************************************************************************************/
 bool Init_navig_dist_b		= false;
 bool Init_navig_dir_b  	= false;
 bool Navig_previous_state_b 	= false;
 ClusterSignals_En_t Navigation_dir_En = STRAIGHT_E;
/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/



/***********************************************************************************************************************
* Function Name: Display_Navigation
* Description  : The function display the navigation distance and direction
* Arguments    : uint16_t RangeKm_u16, ClusterSignals_En_t RANGE_KM_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void Display_Navigation(uint16_t Navig_dist_u16 , uint8_t Navig_dir_u8, ClusterSignals_En_t NAVIG_DIST_E, uint8_t Navig_status_u8)
{
	if (DISP_NAVIGATION == Navig_status_u8)
	{
		Display_direction(Navig_dir_u8);
		Display_Navig_dist(Navig_dist_u16 , NAVIG_DIST_E);
		Navig_previous_state_b = true;
	}
	else 
	{
		if(true == Navig_previous_state_b)
		{	
			Navig_previous_state_b 	= false;
			Init_navig_dist_b	= false;
	     		Init_navig_dir_b  	= false;  /*todo: jeevan*/ /* reset after deinit*/
			
			Write_SEG(STRAIGHT_E, (uint32_t)TURN_OFF_NAVIG);/* Turn off any one of the direction signal*/
			Write_SEG(NAVIG_DIST_E, (uint32_t)TURN_OFF_NAVIG);
		}
	}
	
}

/***********************************************************************************************************************
* Function Name: Display_direction
* Description  : The function display the Navigation direction
* Arguments    : uint8_t Navig_dir_u8, ClusterSignals_En_t NAVIG_DIR_E
* Return Value : None
***********************************************************************************************************************/
void Display_direction(uint8_t Navig_dir_u8)
{
	static uint8_t previous_dir_u8 = 0;
	uint8_t dir_value_u8 = 0;
	Navig_DispState_En_t  Navig_DispState_En = NAVIG_DISP_HOLD_E;
	//ClusterSignals_En_t Navigation_dir_En = STRAIGHT_E;
	
	if(previous_dir_u8 == Navig_dir_u8)
	{
		Navig_DispState_En = NAVIG_DISP_HOLD_E;
		if(false == Init_navig_dir_b)
		{
			Navig_DispState_En = NAVIG_DIR_DISPLAY_E;
			Init_navig_dir_b = true;
		}
		else
		{
			;	
		}
	}
	else
	{
		Navig_DispState_En = NAVIG_DIR_DISPLAY_E;
		previous_dir_u8 = Navig_dir_u8;
		
	}
	
	if(Navig_dir_u8 > 5 && Navig_DispState_En == NAVIG_DIR_DISPLAY_E)
	{
		Navig_DispState_En = NAVIG_DISP_HOLD_E;	
	}
	
	if(NAVIG_DIR_DISPLAY_E == Navig_DispState_En)
	{
		dir_value_u8 = 1;//previous_dir_u8;
		switch(Navig_dir_u8)
		{
			case 0: 
			{
				dir_value_u8 = 0;				/* Turn off all the signals*/
				break;
			}
			case 1: 
			{
				Navigation_dir_En = STRAIGHT_E;       		/* Straight */
				break;
			}
			case 2: 
			{
				Navigation_dir_En = LEFT_TURN_E;		/* Left Turn */
				break;
			}
			case 3:
			{
				Navigation_dir_En = RIGHT_TURN_E;		/* Right Turn */
				break;
			}
			case 4:
			{
				Navigation_dir_En = LEFT_U_TURN_E;		/* Left U Turn*/ 
				break;
			}
			case 5:
			{
				Navigation_dir_En = RIGHT_U_TURN_E;		/* Rigth U Turn*/ 
				break;
			}
			default:
			{
				//dir_value_u8 = 0;
			}
		}
		Write_SEG(Navigation_dir_En, (uint32_t)dir_value_u8);
	}
	else
	{
		;
	}
	
	
}
/***********************************************************************************************************************
* Function Name: Display_Navig_dist
* Description  : The function display the navigation distance and direction
* Arguments    : uint16_t RangeKm_u16, ClusterSignals_En_t RANGE_KM_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void Display_Navig_dist(uint16_t Navig_dist_u16, ClusterSignals_En_t NAVIG_DIST_E)
{
	static uint16_t Previous_dist = 0;
	uint16_t Valid_dist_u16 = 0;
	Navig_DispState_En_t  Navig_DispState_En = NAVIG_DISP_HOLD_E;
	
	
	if(Previous_dist == Navig_dist_u16)
	{
		Navig_DispState_En = NAVIG_DISP_HOLD_E;
		if(false == Init_navig_dist_b)
		{
			Navig_DispState_En = NAVIG_DIR_DISPLAY_E;
			Init_navig_dist_b = true;
		}
	}
	else
	{
		Navig_DispState_En = NAVIG_DIR_DISPLAY_E;
		Previous_dist = Navig_dist_u16;
	}
	if (NAVIG_DIR_DISPLAY_E == Navig_DispState_En)
	{
		if (Navig_dist_u16 <= 0)
		{
			Navig_dist_u16 = 0;
		}
		else if (Navig_dist_u16 < MIN_NAVIG_DIST)
		{
			Navig_dist_u16 = MIN_NAVIG_DIST;
		}
		else if( Navig_dist_u16 > MAX_NAVIG_DIST)
		{
			Navig_dist_u16 = MAX_NAVIG_DIST;
		}
		else
		{
			;
		}
		Valid_dist_u16 = Get_Valid_digit(Navig_dist_u16);
		Write_SEG(NAVIG_DIST_E, (uint32_t)Valid_dist_u16);
	}
	else
	{
		;
	}
}

uint16_t Get_Valid_digit(uint16_t Valid_dist_u16)
{
	/* only multiples of 10 can be displaced in Navigation distance*/
	uint16_t Valid_digit_u16 = 0;
	if(0 == Valid_dist_u16)
	{
		Valid_digit_u16 = 10;/* To display  '0' Valid_digit_u16 should be '10'*/
	}
	else if ( Valid_dist_u16 < 100 )
	{
		Valid_digit_u16 = Valid_dist_u16 / 10; /* to remove last digit*/
		Valid_digit_u16 = ( Valid_digit_u16 * 100 ) + 10; /* To turn on 2nd digit of display*/
	}
	else if (Valid_dist_u16 <= 900)
	{
		Valid_digit_u16 = Valid_dist_u16 / 100; /* to remove last two digit*/
		Valid_digit_u16 = ( Valid_digit_u16 * 100 ) + 11; /* To turn on 2nd and 3rd digit of display*/
	}
	return Valid_digit_u16;
}






















































