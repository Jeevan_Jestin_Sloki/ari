/***********************************************************************************************************************
* File Name    : Range_WhKmCalc.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 07/06/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include"Range_WhKmCalc.h"
#include "Communicator.h"
#include"ODO_Calc.h"
#include "pfdl_user.h"
/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
WhRangeKm_St_t WhRangeKm_St;
bool    	WhEstimateOnstart_b = false;
uint16_t 	DistanceTravelled_u16 = 0;
uint8_t Odo100MeterCount_u8 = 0;
float PresentBatteryAh = 0;
/***********************************************************************************************************************
* Function Name: EstimateWhKm
* Description  : The function estimate the Watts consumed per Km
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void EstimateWhKm(uint16_t VoltagemV_u16, uint16_t SocPercentage_u16, uint16_t FullChargeCapacity_u16)
{
	uint16_t Avl_Capacity_u16 = 0;
    //float PresentBatteryAh = 0;
    if(false == WhEstimateOnstart_b)
    {
		WhEstimateOnstart_b = true;
		Avl_Capacity_u16 = FullChargeCapacity_u16 *GET_ACC_SOC();
        WhRangeKm_St.PreviousWatts_u16 = (uint16_t)(( (uint32_t)VoltagemV_u16 *  (uint32_t)Avl_Capacity_u16)/100);  /* volatge and ah scale factor is 0.1, So divide by 100*/
        WhRangeKm_St.PreviousOdoMeter_u32 = ODOmeter_St.Main_ODOmeter_u32;
		Odo100MeterCount_u8 = 0;
    }
    else
    {
        DistanceTravelled_u16 = DistanceTravelled_u16 +  (uint16_t)( ODOmeter_St.Main_ODOmeter_u32 -  WhRangeKm_St.PreviousOdoMeter_u32 );
        WhRangeKm_St.PreviousOdoMeter_u32 = ODOmeter_St.Main_ODOmeter_u32;

        if(100 < DistanceTravelled_u16)
        {
            Odo100MeterCount_u8 ++;
            DistanceTravelled_u16 = DistanceTravelled_u16 - 100;
            Record_BatteryVoltage(REC_VOLTAGE_E,VoltagemV_u16);
        }

        if(5 == Odo100MeterCount_u8)
        {
            Odo100MeterCount_u8 = 0;
            Record_BatteryVoltage(CAL_AVG_VOLTAGE_E,VoltagemV_u16);
			Avl_Capacity_u16 = FullChargeCapacity_u16 *(GET_ACC_SOC());
            WhRangeKm_St.PresentWatts_u16 = (uint16_t)(( (uint32_t)VoltagemV_u16 *  (uint32_t)Avl_Capacity_u16)/100);  /* volatge and ah scale factor is 0.1, So divide by 100*/
			if(WhRangeKm_St.PresentWatts_u16 < WhRangeKm_St.PreviousWatts_u16)
			{
				WhRangeKm_St.WattsConsumed_u16 = WhRangeKm_St.PreviousWatts_u16 - WhRangeKm_St.PresentWatts_u16;
			}
			else
			{
				;
			}
           
            SET_WHKM(WhRangeKm_St.WattsConsumed_u16 * 2);
            WhRangeKm_St.PreviousWatts_u16 = WhRangeKm_St.PresentWatts_u16;
        }
    }

    return;
}

/***********************************************************************************************************************
* Function Name: Record_BatteryVoltage
* Description  : This function Records the Battery Voltage and Calculates the Average Battery Voltage.
* Arguments    : BattVolRecFun_En_t BattVolRecFun_En,uint32_t BatteryVoltage_u32
* Return Value : None
***********************************************************************************************************************/
void Record_BatteryVoltage(BattVolRecFun_En_t BattVolRecFun_En,uint16_t BatteryVoltage_u16)
{
	uint32_t SumOfBattVoltage_u32 = 0;
	
	static uint16_t RecIndexCount_u16 = 0;
	
	switch(BattVolRecFun_En)
	{
		case REC_VOLTAGE_E:
		{
			//Record the Battery Voltage
			if(RecIndexCount_u16 < REC_LEN)
			{
				WhRangeKm_St.BattVoltage_au16[RecIndexCount_u16] = BatteryVoltage_u16;
				RecIndexCount_u16++;
			}
			else
			{
				RecIndexCount_u16 = 0;
			}
			break;
		}
		case CAL_AVG_VOLTAGE_E:
		{
			//Calculate the Battery Average voltage from the recorded data
			for(RecIndexCount_u16 = 0; RecIndexCount_u16 < REC_LEN; RecIndexCount_u16++)
			{
				SumOfBattVoltage_u32 += WhRangeKm_St.BattVoltage_au16[RecIndexCount_u16];
			}
			RecIndexCount_u16 = 0;
			WhRangeKm_St.AvgBattVoltage_u32 = (SumOfBattVoltage_u32 / REC_LEN);
			
			break;
		}
		default:
		{
			;
		}
	}
	return;
}

/***********************************************************************************************************************
* Function Name: UpdateRangeKm
* Description  : This function updates the range Km.
* Arguments    : uint16_t VoltagemV_u16, uint16_t SocPercentage_u16, uint16_t AvailCapacityAh_u16
* Return Value : None
***********************************************************************************************************************/
void UpdateRangeKm(uint16_t VoltagemV_u16, uint16_t SocPercentage_u16, uint16_t AvailCapacityAh_u16)
{

    uint16_t TotalWatts_u16 = 0;
	uint16_t CalcRangeKm_u16 = 0;

    if( ( 0 != PresentSessionWHkm_u16 ) && ( 0 != AvailCapacityAh_u16) )
    {
	    TotalWatts_u16 = (uint16_t)(( (uint32_t)VoltagemV_u16 *  (uint32_t)AvailCapacityAh_u16)/100);  /* volatge and ah scale factor is 0.1, So divide by 100*/

	    CalcRangeKm_u16 = TotalWatts_u16 / PresentSessionWHkm_u16;
		SET_RANGEKM(CalcRangeKm_u16);
    }

    return;
}

/***********************************************************************************************************************
* Function Name: UpdateRangeKm
* Description  : This function updates the range Km.
* Arguments    : uint16_t VoltagemV_u16, uint16_t SocPercentage_u16, uint16_t AvailCapacityAh_u16
* Return Value : None
***********************************************************************************************************************/
void ResetRangeKmParam(void)
{
	WhEstimateOnstart_b = false;
	WhRangeKm_St.PreviousWatts_u16 = CLEAR;
	WhRangeKm_St.PreviousOdoMeter_u32 = CLEAR;
	return;
}
/***********************************************************************************************************************
* Function Name: StoreRangekmParam
* Description  : This function stores range km parameter to flash.
* Arguments    : none
* Return Value : None
***********************************************************************************************************************/
void StoreRangekmParam(void)
{
	uint8_t Flashblck1_au8[RANGE_FLASH_SIZE] = {0};
	uint8_t ByteCount_u8 = 0;
	Flashblck1_au8[0] = (GET_RANGEKM() >> 8) & 0xFF;
	Flashblck1_au8[1] = ( GET_RANGEKM()  & 0xFF);

	Flashblck1_au8[2] = ( GET_WHKM() >> 8) & 0xFF;
	Flashblck1_au8[3] = ( GET_WHKM()  & 0xFF);

	FDL_Erase(RANGE_FLASH_BLOCK,TOTAL_RANGE_FLASH_BLOCK);

	for(ByteCount_u8 = 0; ByteCount_u8<RANGE_FLASH_SIZE;ByteCount_u8++)
	{
		dubWriteBuffer[ByteCount_u8] = Flashblck1_au8[ByteCount_u8];
	}
	FDL_Write(RANGE_FLASH_BLOCK,RANGE_FLASH_BLKPOS,RANGE_FLASH_SIZE);
	return;
}

/***********************************************************************************************************************
* Function Name: RestoreRangeKmParam
* Description  : This function Restores range km parameter from flash.
* Arguments    : none
* Return Value : None
***********************************************************************************************************************/

void RestoreRangeKmParam(void)
{
	uint8_t Flashblck1_au8[RANGE_FLASH_SIZE] = {0};
	uint8_t ByteCount_u8 = 0;
	FDL_Read(RANGE_FLASH_BLOCK,RANGE_FLASH_BLKPOS,RANGE_FLASH_SIZE);

	for(ByteCount_u8 = 0; ByteCount_u8<RANGE_FLASH_SIZE;ByteCount_u8++)
	{
		Flashblck1_au8[ByteCount_u8] = dubReadBuffer[ByteCount_u8];
	}
	
	PresentSessionRangeKm_u16 = Flashblck1_au8[0];
	PresentSessionRangeKm_u16 = ( PresentSessionRangeKm_u16 << 8 ) |  Flashblck1_au8[1];

	PresentSessionWHkm_u16 = Flashblck1_au8[2];
	PresentSessionWHkm_u16 = ( PresentSessionWHkm_u16 << 8 ) | Flashblck1_au8[3];
	
	if(0xFFFF == PresentSessionWHkm_u16)
	{
		PresentSessionWHkm_u16 = 0;
	}

	if(0xFFFF == PresentSessionRangeKm_u16)
	{
		PresentSessionRangeKm_u16 = 80;
	}

	return;
}