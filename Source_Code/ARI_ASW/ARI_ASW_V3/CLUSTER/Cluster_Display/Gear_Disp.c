/***********************************************************************************************************************
* File Name    : Gear_Disp.c
* Version      : 01
* Description  : 
* Created By   : Pramod Jagtap
* Creation Date: 28/11/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Gear_Disp.h"
#include "SegDispWrite.h"
#include "GenConfig.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool InitialGear_Disp_b = false;
bool SetGearToZero_b = false;

/***********************************************************************************************************************
* Function Name: Display_Gear
* Description  : This function validate and displays the vehicle Gear 
* Arguments    : uint8_t Gear_u8, ClusterSignals_En_t Gear_ENUM_E
* Return Value : None
***********************************************************************************************************************/

void Display_Gear(uint8_t Gear_u8, ClusterSignals_En_t GEAR_ENUM_E) 
{
    uint8_t	     ValidGear_u8 	= 0;
	static uint8_t      PrevGear_u8 	= 0;

    GearDispState_En_t  GearDispState_En = GEAR_DISP_HOLD_E;


    if(PrevGear_u8 == Gear_u8)
    {
       GearDispState_En =  GEAR_DISP_HOLD_E;
       if(false == InitialGear_Disp_b) 
       {
            GearDispState_En = GEAR_DISPLAY_E;
			InitialGear_Disp_b = true;
       }
    }
    else
    {
        GearDispState_En = GEAR_DISPLAY_E;
		PrevGear_u8 = Gear_u8;
    }
    switch(GearDispState_En)
    {
        case GEAR_DISPLAY_E:
        {
            ValidGear_u8 = ValidateSpeed(Gear_u8);
            Write_SEG(GEAR_ENUM_E, (uint8_t)ValidGear_u8,DECIMAL); 
			break;
        }
        case GEAR_DISP_HOLD_E:
        {
            break;
        }
        default:
        {
            ;
        }
    }
    return;
}

/***********************************************************************************************************************
* Function Name: ValidateGear
* Description  : This function validates the Gear value received.
* Arguments    : uint8_t  GearCheck_u8
* Return Value : uint8_t  GearCheck_u8
***********************************************************************************************************************/
uint8_t validateGear(uint8_t GearCheck_u8)
{
    if(GearCheck_u8 > MAX_GEAR_RANGE)
    {
        GearCheck_u8 = MAX_GEAR_RANGE;
    }
    else
    {
        ;
    }
    return GearCheck_u8;
}


/***********************************************************************************************************************
* Function Name: SetGearToZero
* Description  : This function sets the speed to 0 during discharging mode and immediate after the animation.
* Arguments    : uint8_t SpeedZero_u8, ClusterSignals_En_t SPPED_ENUM_E
* Return Value : None
***********************************************************************************************************************/


void SetGearToZero(uint8_t GearZero_u8, ClusterSignals_En_t GEAR_ENUM_E)
{
    Write_SEG(GEAR_ENUM_E, (uint32_t)GearZero_u8,DECIMAL); 
			/*Set Gear to Zero*/
	return;

}
