/*
 * iso14229_serv34.c
 *
 *  Created on: 29-Jan-2020
 *      Author: Lakshmi
 */



/* ************************************************************************** */
/** Descriptive File Name

   @Company
 	 Sloki Technologies Pvt Ltd.

  	@File Name
    	iso14229_serv31.c

   @Summary
 	 This File contains macros, typedefs, extern definitions and
 	 user defined datatypes required by iso14229_serv34.c
 */


/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
#include <stdbool.h>
#include <stddef.h>
//#include "Cpu.h"
#include "iso14229_serv34.h"
/* ************************************************************************** */

/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
	uint32_t serv34_memory_address_u32        = 0u;
    uint32_t serv34_memory_size_u32           = 0u;
    uint32_t NumOf_Bytes_Programmed_u32      = 0u;
    uint8_t Number_of_Blocks_u8               = 0u;

    extern uint8_t   ActiveBlockIndex_u8;
    extern uint32_t  Serv36TotalRequested_u32;

/* ************************************************************************** */


/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */
    /**
    *  FUNCTION NAME : iso14229_serv34
    *  FILENAME      : iso14229_serv34.c
    *  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
    *  @brief        : This function will process the service_31 requests
    *  @return       : Type of response.
    */
    UDS_Serv_resptype_En_t iso14229_serv34(UDS_Serv_St_t*  UDS_Serv_pSt){
    	UDS_Serv_resptype_En_t serv_34_resp_En;
    uint8_t number_of_bytes_req_u8 = UDS_Serv_pSt->RxLen_u16;
    uint8_t data_format_id_u8 = UDS_Serv_pSt->RxBuff_pu8[1];
    uint8_t add_len_format_id = UDS_Serv_pSt->RxBuff_pu8[2];
    uint8_t num_bytes_address = (add_len_format_id >> 0x04);
    uint8_t num_bytes_memSize = (add_len_format_id & 0x0F);
    uint8_t i,j;
    for(i=0;i<num_bytes_address;i++)
    {
        serv34_memory_address_u32 = (serv34_memory_address_u32 << 0x08) | (UDS_Serv_pSt->RxBuff_pu8[i+3]);
    }

    for(j=0;j<num_bytes_memSize;j++)
    {
        serv34_memory_size_u32 = (serv34_memory_size_u32 << 0x08) | (UDS_Serv_pSt->RxBuff_pu8[j+(i+3)]);
    }

    if(SERV34_MIN_LENGTH >  number_of_bytes_req_u8)
    {
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_REQUEST_DOWNLOAD;
    	UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
    	UDS_Serv_pSt->TxLen_u16 = 3;
        serv_34_resp_En = UDS_SERV_RESP_NEG_E;
    }
    else if(data_format_id_u8 != 0x00)
    {
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_REQUEST_DOWNLOAD;
    	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;
    	UDS_Serv_pSt->TxLen_u16 = 3;
        serv_34_resp_En = UDS_SERV_RESP_NEG_E;
    }
    else if(add_len_format_id != SERV34_ADD_LEN_FORMAT_ID)
    {
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_REQUEST_DOWNLOAD;
    	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;
    	UDS_Serv_pSt->TxLen_u16 = 3;
        serv_34_resp_En = UDS_SERV_RESP_NEG_E;
    }
    else if( (Flash_State_En != SERVICE31_ERASE_E ) && ( !EraseFlash_b ) )
    {
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_REQUEST_DOWNLOAD;
    	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_SEQUENCE_ERR;
    	UDS_Serv_pSt->TxLen_u16 = 3;
    	serv_34_resp_En = UDS_SERV_RESP_NEG_E;
    }
    else if(!ISO14229_SA_L1_Gained_b)
    {
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_REQUEST_DOWNLOAD;
    	UDS_Serv_pSt->TxBuff_pu8[2] = SECURITY_ACCESS_DENIED;
    	UDS_Serv_pSt->TxLen_u16 = 3;
        serv_34_resp_En = UDS_SERV_RESP_NEG_E;
    }
    else
    {
        if(MemBlockConf_aSt[ActiveBlockIndex_u8].StartAddr_u32 <= serv34_memory_address_u32 && (MemBlockConf_aSt[ActiveBlockIndex_u8].StartAddr_u32 + MemBlockConf_aSt[ActiveBlockIndex_u8].EndAddr) >= serv34_memory_address_u32)
        {
            if(serv34_memory_address_u32 && MemBlockConf_aSt[ActiveBlockIndex_u8].EndAddr >= serv34_memory_size_u32)
            {
                if(MemBlockConf_aSt[ActiveBlockIndex_u8].EraseState_En)
                {
                    Number_of_Blocks_u8 = ((serv34_memory_size_u32 / 0x400) + (serv34_memory_size_u32 % 0x400 > 0));
                    UDS_Serv_pSt->TxBuff_pu8[1] = 0x20;
                    UDS_Serv_pSt->TxBuff_pu8[2] = 0x04;
                    UDS_Serv_pSt->TxBuff_pu8[3] = 0x02;
                    UDS_Serv_pSt->TxLen_u16  = 3;
                    serv_34_resp_En = UDS_SERV_RESP_POS_E;
                    Flash_State_En = SERVICE34_E;
    	            Addr31CheckSum_u32 = serv34_memory_address_u32 ;
    	            Size31CheckSum_u32 = serv34_memory_size_u32 ;
    	            Serv36TotalRequested_u32 = 0;
    	            NumOf_Bytes_Programmed_u32 = 0;
                }
                else
                {
                	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
                	UDS_Serv_pSt->TxBuff_pu8[1] = SID_REQUEST_DOWNLOAD;
                	UDS_Serv_pSt->TxBuff_pu8[2] = UPLOAD_DOWNLOAD_NOT_ACCEPTED;
                	UDS_Serv_pSt->TxLen_u16 = 3;
                    serv_34_resp_En = UDS_SERV_RESP_NEG_E;
                }
            }
            else
            {
            	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
            	UDS_Serv_pSt->TxBuff_pu8[1] = SID_REQUEST_DOWNLOAD;
            	UDS_Serv_pSt->TxBuff_pu8[2] = UPLOAD_DOWNLOAD_NOT_ACCEPTED;
            	UDS_Serv_pSt->TxLen_u16 = 3;
                serv_34_resp_En = UDS_SERV_RESP_NEG_E;
            }
        }
        else
        {
        	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
        	UDS_Serv_pSt->TxBuff_pu8[1] = SID_REQUEST_DOWNLOAD;
        	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_OUT_OF_RANGE;
        	UDS_Serv_pSt->TxLen_u16 = 3;
            serv_34_resp_En = UDS_SERV_RESP_NEG_E;
        }

    }

    return serv_34_resp_En;
}


/* *****************************************************************************
 End of File
 */
