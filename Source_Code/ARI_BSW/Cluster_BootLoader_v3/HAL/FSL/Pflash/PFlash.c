#include "PFlash.h"
#include "fsl.h"
#include "r_cg_wdt.h"

fsl_descriptor_t fsl_descr;
fsl_write_t my_fsl_write_str;
void MyErrorHandler(void)
{
	NOP();
}
uint32_t index = 0;
fsl_u08 fsl_erase_status;
uint32_t ADDRESS,SIZE;
bool PFlash_Erase(uint32_t address,uint32_t size)
{
    
   // uint32_t i = 0;
    //fsl_u08 fsl_erase_status;
    bool flag = false;
    ADDRESS = address;
    SIZE = size;
    DI();
    FSL_Open();
    FSL_PrepareFunctions();
    for(index = (address/0x400); index < ((address+size)/0x400); index++)
    {
	    //index = i;
    	fsl_erase_status = FSL_Erase(index);
     	while(fsl_erase_status == FSL_BUSY)
	{
		fsl_erase_status = FSL_StatusCheck();
	}
	if(fsl_erase_status == FSL_OK)
	{
            flag = true;
	    NOP();
			//PASS
	}
	else
	{
			//FAIL
		MyErrorHandler();
		NOP();
        	flag = false;
	}
    }
    FSL_Close();
    EI();
    return flag;
}
bool PFlash_Write(uint32_t address,uint32_t size,uint8_t* data_buff)
{
    bool flag = false;
    fsl_u08 my_fsl_status;
    uint32_t i = 0;
    uint32_t n = 0;
    uint32_t j = 0;
  //  uint32_t k = 0;
    uint32_t iverify_blk = 0;
    uint32_t remaining_bytes_u32 = size;
    uint8_t verify_block_cnt = 0;
    DI();
    FSL_Open();    	
    FSL_PrepareFunctions();	
    
    for(i = address; i < (address+size);)
    {
	if(remaining_bytes_u32 < 256)
	{
        	if(remaining_bytes_u32%4 == 0)
		{
			my_fsl_write_str.fsl_word_count_u08 = remaining_bytes_u32/4;
		}
		else
		{
			my_fsl_write_str.fsl_word_count_u08 = (remaining_bytes_u32/4)+1;
			for(n = 0; n<(4-(remaining_bytes_u32)%4);n++)
			{
				data_buff[size+n] = 0xFF;
			}
			
		}
	}
	else
	{
		my_fsl_write_str.fsl_word_count_u08 = 0x40;
	}
	my_fsl_write_str.fsl_data_buffer_p_u08 = (fsl_u08 *)&data_buff[j];
        my_fsl_write_str.fsl_destination_address_u32 = i;
	
        my_fsl_status = FSL_Write((__near fsl_write_t*)&my_fsl_write_str);
        if(my_fsl_status != FSL_OK) 
        {
            MyErrorHandler();
        }
        verify_block_cnt++;
        
        if(verify_block_cnt>=8)		//check if one block filled. If yes, verify.
        {
                my_fsl_status = FSL_IVerify(iverify_blk);
                iverify_blk++;
                verify_block_cnt = 0;
        }
        
        if(my_fsl_status == FSL_OK) 
        {
            flag = true;
	    remaining_bytes_u32 -= (my_fsl_write_str.fsl_word_count_u08*4);
            i += (my_fsl_write_str.fsl_word_count_u08*4);
            j += (my_fsl_write_str.fsl_word_count_u08*4);
        }
        else
        {
            flag = false;
            MyErrorHandler();
        }
    }
	FSL_Close();
	EI();
    return flag;
}
bool PFlash_Pattern_Write(uint32_t address, uint32_t data)
{
	bool flag = false;
	uint8_t data_buff_au8[8] = {0};
	uint8_t i = 0;
	flag = PFlash_Erase(address,0x400);
	if(false == flag)
	{
		return false;
	}
	for(i=0; i<4U; i++)
	{
		data_buff_au8[i] = (uint8_t)((data >> (i*8)) & 0xFF);
	}

	flag = PFlash_Write(address, 4U, data_buff_au8);
	if(false == flag)
	{
		return false;
	}
	
	return flag;
	
}
bool PFlash_Init(void)
{
    fsl_u08 my_fsl_status;
    fsl_descr.fsl_flash_voltage_u08 = 0x00;
    fsl_descr.fsl_frequency_u08 = 0x14;
    fsl_descr.fsl_auto_status_check_u08 = 0x01;

    my_fsl_status = FSL_Init((__far fsl_descriptor_t*)&fsl_descr);
    if(my_fsl_status != FSL_OK)
    {
    	MyErrorHandler();
        return false;
    }
    return true;

}