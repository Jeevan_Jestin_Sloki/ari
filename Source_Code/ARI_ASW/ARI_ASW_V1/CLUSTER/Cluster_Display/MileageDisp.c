/***********************************************************************************************************************
* File Name    : MileageDisp.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 11/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "MileageDisp.h"
#include "SegDispWrite.h"
#include "GenConfig.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool 	     InitialMileage_Disp_b = false;

/***********************************************************************************************************************
* Function Name: Display_Mileage
* Description  : This function validate and displays the Mileage from 00 - 99 WH/KM
* Arguments    : uint16_t Mileage_u16, ClusterSignals_En_t MILEAGE_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void Display_Mileage(uint16_t Mileage_u16, ClusterSignals_En_t MILEAGE_ENUM_E)
{
	uint16_t	     ValidMileage_u16 = 0;
	static uint16_t      PrevMileage_u16 = 0;
	
	MileageDispState_En_t  MileageDispState_En = MILEAGE_DISP_HOLD_E;
	
	if(PrevMileage_u16 == Mileage_u16)
	{
		MileageDispState_En = MILEAGE_DISP_HOLD_E;
		if(false == InitialMileage_Disp_b)
		{
			MileageDispState_En = MILEAGE_DISPLAY_E;
			InitialMileage_Disp_b = true;
		}
	}
	else
	{
		MileageDispState_En = MILEAGE_DISPLAY_E;
		PrevMileage_u16 = Mileage_u16;
	}
	switch(MileageDispState_En)
	{
		case MILEAGE_DISPLAY_E:
		{
			ValidMileage_u16 = ValidateMileage(Mileage_u16);
			Write_SEG(MILEAGE_ENUM_E, (uint32_t)ValidMileage_u16); 
			break;
		}
		case MILEAGE_DISP_HOLD_E:
		{
			break;
		}
		default:
		{
			;	
		}
	}
	return;
}


/***********************************************************************************************************************
* Function Name: ValidateMileage
* Description  : This function validates the Mileage value received.
* Arguments    : uint16_t  MileageCheck_u16
* Return Value : uint16_t  MileageCheck_u16
***********************************************************************************************************************/
uint16_t ValidateMileage(uint16_t MileageCheck_u16)
{
	if(MileageCheck_u16 > MAX_MILEAGE_RANGE)
	{
		MileageCheck_u16 = MAX_MILEAGE_RANGE;
	}
	else
	{
		;
	}
	return MileageCheck_u16;
}


/***********************************************************************************************************************
* Function Name: Control_WHKM_Text
* Description  : This function Turns-ON / Turns-OFF the WH/KM Text.
* Arguments    : bool SignalState_b
* Return Value : None
***********************************************************************************************************************/
void Control_WHKM_Text(bool SignalState_b)
{
		Write_SEG(WHKM_TEXT_E,	SignalState_b);
}


/********************************************************EOF***********************************************************/