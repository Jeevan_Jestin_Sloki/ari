/***********************************************************************************************************************
* File Name    : SegDispWrite.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 21/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "SegDispWrite.h"
#include "digits_utils.h"
#include "GenConfig.h"
#include "r_cg_lcd.h"
#include "BackLightCtrl.h"
#include "r_cg_timer.h"


/***********************************************************************************************************************
macro directive
***********************************************************************************************************************/
#define     REG_WRITE(i,j,k,l)   	(*(ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Seg_pu8) =  \
					(*(ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Seg_pu8) &  \
					ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Mask_u8) | 	  \
					ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Value_u8)	

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


/***********************************************************************************************************************
* Function Name: Write_SEG
* Description  : This function Writes Data into the Segment registers to display the signals on the cluster.
* Arguments    : ClusterSignals_En_t ClusterSignals_En, uint32_t Signal_Value_u32
* Return Value : None
***********************************************************************************************************************/
void Write_SEG(ClusterSignals_En_t ClusterSignals_En, uint32_t Signal_Value_u32)
{
	uint16_t	i = 0; 
	uint16_t	j = 0;
	uint16_t	k = 0;
	uint16_t	l = 0;
	uint8_t 	SignalVal_au8[MAX_LENGTH_SIGNAL] = {0x0F,0x0F,0x0F,0x0F,0x0F,0x0F};
	
	
	if(9 < Signal_Value_u32)
	{
		GetDigitsFromNum(Signal_Value_u32,SignalVal_au8);	
	}
	else
	{
		SignalVal_au8[0] = (uint8_t)Signal_Value_u32;	
	}

	if( ClusterSignals_En == HOURS_TIME_E && Signal_Value_u32 == 0x0F ) 
	{
		SignalVal_au8[0]=0x0F;
		SignalVal_au8[1]=0x0F;
	}
	if((( ClusterSignals_En == MINUTES_TIME_HB_E)&& Signal_Value_u32 == 60 ))
	{
		   SignalVal_au8[0]=0x09;
		   //SignalVal_au8[1]=0x09;
	}
	   
	   if(((ClusterSignals_En == MINUTES_TIME_LB_E )&& Signal_Value_u32 == 60 ))
	   {
		   SignalVal_au8[0]=0x0F;
		   //SignalVal_au8[1]=0x09;
	   }
	
	for(i=CLUSTER_SIG_START_E; i < TOTAL_SIGNALS_E; i++)
	{
		if(ClusterSigConf_aSt[i].ClusterSignals_En == ClusterSignals_En)
		{
			for(j=0; j < ClusterSigConf_aSt[i].SigLength_u8; j++)
			{
				for(k=0; k < ClusterSigConf_aSt[i].SignalsValue_pSt[j].SignalLen_En; k++)
				{
					if(SignalVal_au8[j] == ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SignalsValue_En)
					{
						for(l=0; l < ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].Seg_Count_u8; l++)
						{
							REG_WRITE(i,j,k,l);
						}
					}	
				}			
			}
			
		break;
		}
	}

	return;	
}	


/***********************************************************************************************************************
* Function Name: Turn_OnOff_AllSegments
* Description  : 
* Arguments    : uint8_t RegValue_u8
* Return Value : None
***********************************************************************************************************************/
void Turn_OnOff_AllSegments(uint8_t RegValue_u8)
{
	uint16_t   SegCount_u16 = 0;
	volatile __near uint8_t  	*Segment_pu8; 
	Segment_pu8 = &SEG0;           /* Assign address of the SEG0 Register to the pointer */
	
	if(RegValue_u8 == SET_SEG_REGISTER)
	{
		R_TAU2_Channel0_Start();
	
		R_LCD_VoltageOn();         	/*Voltage select for Common Pins*/
			
		R_LCD_Start();             	/*Voltage select for Segment Pins*/
		
		Turn_ON_BackLight();		/*Turn-ON the Back-Lights*/
	}
	else
	{
		;
	}
	
	for(SegCount_u16 = 0; SegCount_u16 < TOTAL_SEG_REGISTERS; SegCount_u16++)
	{
		if((SegCount_u16 != _23_UNUSED_SEGMENT)|| (SegCount_u16 != _24_UNUSED_SEGMENT) || \
					(SegCount_u16 != _44_UNUSED_SEGMENT))
		{
			*Segment_pu8 = RegValue_u8;  /*Write Set  value to the Segment Register*/
		}
		else
		{
			;
		}
		Segment_pu8++;         /*Point the Next Segment Register*/
	}
	
	return; 
}


/********************************************************EOF***********************************************************/