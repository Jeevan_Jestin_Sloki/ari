
/*********************************************************************************
 *    FILENAME    : diag_adapt.h                                                 *          
 *    DESCRIPTION : Application adapter interface file for DIAG Stacks.          *
 *********************************************************************************
 * Revision history                                                              *               
 *                                                                               *
 * Ver Author       Date               Description                               *
 * 1   Sloki     1/10/2019		   Initial version                           *
 *********************************************************************************
*/  

#ifndef _DIAG_ADAPT_H_
#define _DIAG_ADAPT_H_

#include "diag_typedefs.h"
#include <stdint.h>
#include <stdbool.h>


// < USER TO INCLUDE THE APPL HEADER FILES (for exporting APPL Variables) REQUIRED FOR STACK FILES >








/*nvm mgr*/

typedef	struct{				/* NVM operation result information 	*/
	U2	u2_data;		/* receiving data 			*/
	U1	u1_ret;			/* result information(normal or abnormal) */
}NVM_RET;

/*--------------------------------------------------------------------------*/
/*  macro definition                                                        */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*  maximum number of NVM request                                           */
/*--------------------------------------------------------------------------*/
#define u1_MAX_NVMREQ       ((U1)22)
#define u1_MAX_NVMREQ_DTC   ((U1)11)


/*---------------------------------------------------------------------------*/
/*  NVM operation request                                                    */
/*                                                                           */
/*  order of priority   low  MSB    <- - - ->   LSB  high                    */
/*---------------------------------------------------------------------------*/
#define u2_REQ_NVMPLS	 			(BIT00)				/* pulse number request	 		*/
#define u2_REQ_NVMODO	 			(BIT01)				/* odo data request				*/
#define u2_REQ_NVMTRPA	 			(BIT02)				/* trip A request				*/
#define u2_REQ_NVMTRPB	 			(BIT03)				/* trip B request				*/
#define u2_REQ_NVMDSP	 			(BIT04)				/* display mode request			*/
#define u2_REQ_NVMRSTODO 			(BIT05)			    /* Mainodo reset sequence   	*/
#define u2_REQ_NVMODOWRT   			(BIT06)				/* ODO rewrite with ECU data   	*/
#define u2_REQ_NVMODOLRN   			(BIT07)				/* ODO rewrite prev lrn status 	*/
#define u2_REQ_NVM_DEFAULT			(BIT08)				/* Default Configuration write request 	*/
#define u2_REQ_DIAGCONFIG_PARAM		(BIT09)
#define u2_REQ_DTCCONFIG_PARAM		(BIT10)
#define u2_REQ_NVM_ILLU				(BIT11)				/* Illumination Level rewrite request 	*/
#define u2_REQ_NVM_GRT				(BIT12)				/* Global Real Time rewrite request 	*/
#define u2_REQ_NVMCONFIG_PARAM		(BIT13)				/* for read/write request 		*/
#define u2_REQ_NVM_AUTOCONFIG		(BIT14)				/* AutoConfig rewrite request 	*/
#define u2_REQ_3D_PARAM				(BIT15)				/* for read/write request 		*/
#define u2_REQ_NVM_DTCBITMASK		(BIT16)				/* DTC BitMask Rewrite request	*/
#define u2_REQ_NVM_OAT_COOL			(BIT17)					/* OAT Raw Sensor  Cool value  */
#define u2_REQ_NVM_OAT_HOT			(BIT18)					/* OAT Raw Sensor  Hot value  */
#define u2_REQ_NVM_FUEL_LOW			(BIT19)					/* FUEL LOW Calibration   */
#define u2_REQ_NVM_FUEL_FULL		(BIT20)					/* FUEL FULL Calibration   */
#define u2_REQ_NVM_CHECKSUM			(BIT21)					/* EOL CHECK SUM   */



/*--------------------------------------------------------------------------*/
/*  maximum NVM address                                                     */
/*--------------------------------------------------------------------------*/
#define u1_MAX_ADR		((U2)4096)  /* maximum word address			*/

/*---------------------------------------------------------------------------*/
/*  definition of NVM address                                                */
/*---------------------------------------------------------------------------*/
#define u1_ADR_MAIN				    ((U1)0)				/* main memory 												*/
#define u1_ADR_SUBA				    ((U1)4)				/* sub memory A 											*/
#define u1_ADR_SUBB				    ((U1)6)				/* sub memory B 											*/
#define u1_ADR_SUBC				    ((U1)8)				/* sub memory C 											*/
#define u1_ADR_FLAG				    ((U1)10)			/* main memory erase flag 									*/
#define u1_ADR_FLG_SA			    ((U1)11)			/* sub memory A writing flag 								*/
#define u1_ADR_FLG_SB			    ((U1)12)			/* sub memory B writing flag 								*/
#define u1_ADR_FLG_SC			    ((U1)13)			/* sub memory C writing flag 								*/
#define u1_ADR_ODOPLS			    ((U1)14)			/* odo fraction 											*/
#define u1_ADR_TRIPA			    ((U1)17)			/* trip A 													*/
#define u1_ADR_TRIPB			    ((U1)26)			/* trip B 													*/
#define u1_ADR_ODOPREVIP		    ((U1)85)			/* Check Purpose in Odometer								*/
#define u1_ADR_ODORST			    ((U1)86)			/* Odo reset flag 											*/
#define u1_ADR_ODORSTCNT		    ((U1)87)			/* Odometer Reset Count 									*/
/*#define u1_ADR_ODORSTDIST		    ((U1)88)*/			/* Odometer Cumulative Distance Count for odometer Reset 	*/
								    
#define u2_ADR_GRT   			    ((U2)512)			/* 8 Bytes of GRT, RTC sleep time and clock mode			*/
								    
#define u2_ADR_DIAG_PARA   		    ((U2)824)			/* Diagnostics Address 										*/
								    
#define u2_ADR_CONFIG_PARA   	    ((U2)1536)			/* Supplier Configuration 									*/
								    
#define u2_ADR_AUTOCONFIG		    ((U2)1605)			/* Autoconfig Data											*/
								    
#define u2_ADR_ODOLRNFLAG		    ((U2)1616)			/* Flag to notify sussess of previous odometer learning 	*/
#define u2_ADR_DSP				    ((U2)1617)			/* display mode 											*/
#define u2_ADR_ILLULEV	   		    ((U2)1618)			/* Previous Illumination Level								*/
								    
#define u2_ADR_DEF_CHK			    ((U2)2047)			/* Check if default is written at first BON					*/
								    
#define u2_ADR_DTCBITMASK		    ((U2)2048)			/* DTC BitMask Data			 								*/
#define u2_ADR_DTC_PARA   		    ((U2)2052)			/* DTC and Snapshot Address 								*/
								    
#define ADR_OAT_COOL                ((U2)1564)			/*OAT COOL Calibration */
#define ADR_OAT_HOT                 ((U2)1565)			/*OAT HOT Calibration*/
#define ADR_FUEL_CALIB_LOW          ((U2)1566)			/*FUEL LOW Calibration*/
#define ADR_FUEL_CALIB_FULL         ((U2)1567)			/*FUEL FULL Calibration*/
#define ADR_CHECK_SUM	            ((U2)1568)			/*CHECK SUM */


/*********************************************************************************************/
/*              DTC List                                                                     */
/*********************************************************************************************/
/*****************************************************************************/
/*                                3K BANK - (128 to 191) ECUID's                */
/*****************************************************************************/

/*****************************************************************************
*                                4K BANK - (192 to 255) Supplier Reserved
*****************************************************************************/

/*---------------------------------------------------------------------------*/
/*  function declaration                                                     */
/*---------------------------------------------------------------------------*/
extern void        	NvmDrvInit(void);
extern void			NvmmgrInitial(void);	/* WKUP processing 			            */
extern U1		    u1_NvmmgrChkSlp(void);	/* SLP judge processing 		        */
extern void			NvmmgrRoutine(void);	/* NVM manager routine 			        */
extern void			NvmmgrSetReq(U4, U1);	/* NVM request setting/resetting proc.  */
extern void    		NvmmgrSetReqDTC(U2, U1);    /* DTC NVM request setting/resetting proc. */

extern NVM_RET		st_NvmLetWrite(void);	/* NVM writing enable 			        */
extern NVM_RET		st_NvmNonWrite(void);	/* NVM writing disable 			        */
extern NVM_RET		st_NvmRead(U2);			/* NVM reading				            */
extern NVM_RET		st_NvmErase(U2);		/* NVM erase 				            */
extern NVM_RET		st_NvmWrite(U2,U2);	/* NVM write 				            */
extern U1      		u1_Get_Nvm_Status(void);    /* NVM Status */
/*---------------------------------------------------------------------------*/
/*   data declaration ( for data acquisition macro)                    */
/*  (attention) do not access to data directly                               */
/*---------------------------------------------------------------------------*/


#define U1_READ_CMD   	(U1)0xA1
#define U1_WRITE_CMD   	(U1)0xA0

#define U1_CONFIG_SIZE 			(U1)(64)	/*Range Config-44bytes + SUPPLIER config 12 bytes*/
#define U2_DIAG_SIZE 			(U2)(922)	/*DID paratemetrs are total of 820 bytes*/
/*system mgr*/
#define u4_DTCL2ENTRY_BKUP_RAM_ADD			(U4)0x02001008		/* Address for DTC backup RAM - 134*4 Bytes			*/
#define u4_DTCBITMASK_BKUP_RAM_ADD			(U4)0x02001000		/* Address for DTC backup RAM - 2*4 Bytes			*/


/*EOL*/
enum {
	EOL_TTMGR_CHK = 0,
	EOL_ALLSEG_CHK,
	EOL_LCD_ALLSEG_CHK,
	EOL_LCD_NUM_CHK,
	EOL_CHIME_CHK,
	EOL_SET_CHK,
	EOL_MODE_CHK,
	EOL_FUNC_CHK,
	EOL_INSTFE_CHK,
	EOL_NVM_DEFAULT,
	EOL_VERSION_CHK,
	EOL_MAX_STEP
};

/*-----------------------------------------------------------------------------------------------------------------------------------*/
/*  Constant Variable s                                                                                                        */
/*-----------------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------------------------------------*/
/*  Function Prototypes                                                                                                              */
/*-----------------------------------------------------------------------------------------------------------------------------------*/
extern void EOL_BON_NVM_Def_Wrt(void);
extern void EOL_Initial(void);
extern void EOL_Routine(void);

extern void u1_Start_EOL(void);
extern void vd_EOL_Cmd(U2 u2_t_cmd);
extern void vd_Stop_EOL(void);

extern U1 u1_Get_EOL_sts(void);
extern U1 u1_Get_EOL_state(void);
extern U6 u6_Get_EOL_TTMGR_LED_Data(void);
extern U2 u2_Get_EOL_TTMGR_LCD_Data(void);
extern U1 u1_Get_EOL_Num_Chk(void);
extern U1 u1_Get_EOL_Real_Val(void);

extern U1 EOL_Key_NoTimeout(void);
extern void vd_EOL_NVM_Key(U4 u4_NVM_Key);
extern U1 Check_EOL_NVM_Security(void);

extern void AcsNVMDefault(void);
extern void  AcsOat_Cool_cnfg(void);
extern void AcsOat_Hot_cnfg(void);
extern void AcsFUEL_CALIB_LOW(void) ;
extern void AcsFUEL_CALIB_FULL(void) ;
extern void AcsCHECK_SUM(void);    

/** generic error codes */
typedef enum en_result
{
    Ok                          = 0,  ///< No error
    Error                       = 1,  ///< Non-specific error code
    ErrorAddressAlignment       = 2,  ///< Address alignment does not match
    ErrorAccessRights           = 3,  ///< Wrong mode (e.g. user/system) mode is set
    ErrorInvalidParameter       = 4,  ///< Provided parameter is not valid
    ErrorOperationInProgress    = 5,  ///< A conflicting or requested operation is still in progress
    ErrorInvalidMode            = 6,  ///< Operation not allowed in current mode
    ErrorUninitialized          = 7,  ///< Module (or part of it) was not initialized properly
    ErrorBufferFull             = 8,  ///< Circular buffer can not be written because the buffer is full
    ErrorTimeout                = 9   ///< A requested operation could not be completed
} en_result_t;


extern en_result_t Bkupram_en_Init(void);

/* Function to perform Read / Write Access from/to the BKUP RAM */
/* One word */
extern U4          Bkupram_u4_Read32 (U4);
extern en_result_t Bkupram_en_Write32 (U4, U4);



extern uint8_t PDID1_Buffer_au8[4];
extern uint8_t PDID2_Buffer_au8[4];

extern uint8_t Boot_Soft_Iden_au8	         	     	[25];
extern uint8_t Boot_Fp_Data_au8               	     	[31];
extern uint8_t App_Soft_Fp_Data_au8           	     	[31];
extern uint8_t APP_SwData_Date_au8       		     	[10];
extern uint8_t APP_SwData_TesterSerNo_au8       	 	[10];
extern uint8_t APP_SwData_RepairShop_au8       	 		[10];
extern uint8_t APP_SwData_ReprogSeq_au8       	     	[1 ];
extern uint8_t CONFIG_FingerPrintData_au8       	 	[31];
extern uint8_t CONFIG_Data_Date_au8       		     	[10];
extern uint8_t CONFIG_Data_TesterSerNo_au8     	 		[10];
extern uint8_t CONFIG_Data_RepairShop_au8      	 		[10];
extern uint8_t CONFIG_Data_ReprogSeq_au8       	 		[1 ];
extern uint8_t ECU_Serial_Number_au8				 	[24];
extern uint8_t REPROGM_SVN_u8                	     	[10];
extern uint8_t ACTIVE_DIAGNOSTIC_Sess_au8		     	[1 ];
extern uint8_t ECU_MFG_Date_au8                     	[10];
extern uint8_t APP_Soft_Status_au8          		 	[1 ];
extern uint8_t CALIBRATION_Soft_Status_au8		     	[1 ];
extern uint8_t CONFIG_Soft_Status_au8 			     	[1 ];
extern uint8_t ACTIVE_SoftwareComponent_au8    	 		[1 ];
extern uint8_t ECU_HW_Num_au8                       	[24];
extern uint8_t ECU_SW_Num_au8                       	[24];
extern uint8_t System_name_au8					     	[24];
extern uint8_t PROGRAM_Secret_Key_au8  	    	 		[4 ];
extern uint8_t EXTENDED_Secret_Key_au8          	 	[4 ];
extern uint8_t END_Of_Line_Secret_Key_au8       	 	[4 ];
extern uint8_t SERV_Record1_au8               	     	[14];
extern uint8_t SERV_Record2_au8                     	[14];
extern uint8_t SERV_Record3_au8               	     	[14];
extern uint8_t SERV_Record4_au8                     	[14];
extern uint8_t SERV_record5_au8               	     	[14];
extern uint8_t PBL_Reserved_au8			       	 		[17];
extern uint8_t App_Soft_Iden_au8              	     	[25];
extern uint8_t SUPPLIER_Part_Num_au8       	     		[17];
extern uint8_t ECU_PBL_SW_Num_au8                   	[17];
extern uint8_t ECU_APP_SW_Num_au8                   	[17];
extern uint8_t ECU_Config_Sw_Number_au8			 		[17];
extern uint8_t ECU_Assembly_Number_au8			     	[17];
extern uint8_t DTC_Mask_Iden_au8              	     	[4 ];
extern uint8_t SBL_VER_Num_au8                	     	[25];
extern uint8_t ECU_Spec_au8                   	     	[5 ];
extern uint8_t Engine_Warn_Ind_au8            	     	[6 ];
extern uint8_t SEAT_Belt_Warn_Chime_au8       	     	[2 ];
extern uint8_t SEAT_Belt_Warn_Ind_au8         	     	[5 ];
extern uint8_t GEAR_Mode_Ind_au8              	     	[6 ];
extern uint8_t METER_Display_au8              	     	[5 ];
extern uint8_t AFE_IFE_Display_au8            	     	[8 ];
extern uint8_t REV_GEAR_AUDIO_Display_au8     	     	[8 ];
extern uint8_t SERV_INTERVAL_Distance_au8     	     	[9 ];
extern uint8_t DISTANCE_Since_Last_Serv_au8   	     	[4 ];
extern uint8_t PARK_BRAKE_Warn_VEHICLE_Speed_au8    	[2 ];
extern uint8_t AMT_Warn_Indicator_au8               	[5 ];
extern uint8_t WIF_Warn_Indicator_au8               	[2 ];
extern uint8_t FUEL_Level_au8                 	     	[5 ];
extern uint8_t CLK_DISPLAY_Setting_au8        	     	[6 ];
extern uint8_t SET_Switches_au8           		     	[2 ];
extern uint8_t GLOW_PLUG_Warning_au8        	     	[7 ];
extern uint8_t MIN_DISTANCE_Inst_Fuel_au8     	     	[6 ];
extern uint8_t AVG_FUEL_Time_au8          		     	[6 ];
extern uint8_t OAT_Condition_Time_au8         	     	[4 ];
extern uint8_t TIRE_Size_au8                        	[7 ];
extern uint8_t FUEL_Tank_Size_au8             	     	[1 ];
extern uint8_t IC_Replacement_au8                   	[7 ];
extern uint8_t SEASON_Odometer_au8           	     	[7 ];
extern uint8_t SPEED_Limit_au8                      	[3 ];
extern uint8_t ABS_Fac_au8                          	[2 ];
extern uint8_t ILLUMINATION_Fine_Tunn_au8       	 	[10];
extern uint8_t CNTRL_MODULE_Config_Type_au8		 		[1 ];
extern uint8_t COMMUNICATION_Status_au8			 		[1 ];
extern uint8_t VEHICLE_Mode_au8             		 	[1 ];
extern uint8_t ILL_DIMMING_Lev_Output_au8		     	[1 ];
extern uint8_t GLOBAL_RT_Clear_DTC_au8			     	[3 ];
extern uint8_t DTC_SET_DUE_TO_DIAG_Routine_au8	     	[1 ];
extern uint8_t GLOBAL_Real_Time_au8				 		[3 ];
extern uint8_t TOTAL_Distance_Travelled_au8		 		[3 ];
extern uint8_t ECU_Supply_Voltage_au8			     	[1 ];
extern uint8_t VEHICLE_Interior_Temp_au8			 	[1 ];
extern uint8_t ABIENT_AIR_Temp_au8				     	[1 ];
extern uint8_t VEHICLE_Power_Mode_au8			     	[1 ];
extern uint8_t ENGINE_Speed_au8		        	 		[2 ];
extern uint8_t VEHICLE_Speed_au8					 	[1 ];
extern uint8_t ENGINE_Coolant_Temp_au8			     	[1 ];
extern uint8_t Throttle_Position_au8           	     	[1 ];
extern uint8_t VIN_M_au8                      	     	[5 ];
extern uint8_t AFE_Biasing_au8					     	[1 ];
extern uint8_t DIAG_GATEWAY_State_au8           	 	[1 ];
extern uint8_t NW_TOPOLOGY_Config_au8           	 	[1 ];
extern uint8_t PARK_Lamp_In_au8                 	 	[1 ];
extern uint8_t IGN_In_au8		            	     	[1 ];
extern uint8_t TURN_Warn_Indicator_au8          	 	[1 ];
extern uint8_t HAZARD_Warn_Indicator_au8        	 	[1 ];
extern uint8_t ABS_Warn_Indicator_au8           	 	[1 ];
extern uint8_t EPAS_PSC_Warn_Indicator_au8      	 	[1 ];
extern uint8_t HIGH_COOLANT_Temp_Warn_au8       	 	[1 ];
extern uint8_t BATTERY_CHARGE_Warn_Indicator_au8	 	[1 ];
extern uint8_t BRAKE_FLUIDE_Warn_Indicator_au8  	 	[1 ];
extern uint8_t PARK_BRAKE_Warn_Indicator_au8    	 	[1 ];
extern uint8_t KEY_In_Out_au8                   	 	[1 ];
extern uint8_t SPEED_Limit_Warn_Indicator_au8   	 	[1 ];
extern uint8_t GLOW_PLUG_Warn_Indicator_au8     	 	[1 ];
extern uint8_t TX_Fault_Indicator_au8           	 	[1 ];
extern uint8_t DRL_Indicator_au8                	 	[1 ];
extern uint8_t WELCOME_Strategy_au8             	 	[1 ];
extern uint8_t OAT_Config_au8                   	 	[1 ];
extern uint8_t TCU_Buzzer_Req_au8               	 	[1 ];
extern uint8_t DEBUG_Frame_au8                  	 	[1 ];
extern uint8_t VEHICLE_MFG_ECU_Soft_Num_au8         	[17];
extern uint8_t VIN_1_u8                      	     	[24];
extern uint8_t DIAG_Version_au8                 	 	[10];
extern uint8_t VCN_au8                        	     	[15];
extern uint8_t ECU_PARAM_SW_Num_au8                 	[17];
extern uint8_t CONFIG_Data_Version_au8       	     	[10];
extern uint8_t READ_Mask_DTCs_au8               	 	[20];
extern uint8_t POWER_Train_Type_au8				 		[1 ];
extern uint8_t Driver_SBR_Tone_Count_au8			 	[1 ];
extern uint8_t ECU_installation_date_au8			 	[10];
extern uint8_t Eco_Sport_City_Warn_au8			     	[1 ];
extern uint8_t EOL_SESSION_au8       			     	[4 ];
extern uint8_t Ecu_Sbl_SW_Num_au8       			    [17];
extern uint8_t System_Supplier_au8       			    [24];
extern uint8_t Electrical_Power_au8       			    [1 ];
extern uint8_t Ecu_Status_Sig_au8       			    [3 ];
extern uint8_t Veh_Manf_Ecu_Hw_Num_au8     			    [17];
extern uint8_t Frontfog_Warn_Indi_au8     			    [1 ];

extern uint8_t Sample_DID_1_Data_au8    			    [1 ];
extern uint8_t Sample_DID_2_Data_au8    			    [1 ];
extern uint8_t Sample_DID_3_Data_au8    			    [1 ];
extern uint8_t Sample_DID_4_Data_au8    			    [1 ];
extern uint8_t Sample_DID_5_Data_au8    			    [1 ];
extern uint8_t Sample_DID_6_Data_au8    			    [1 ];
extern uint8_t Sample_DID_7_Data_au8    			    [1 ];
extern uint8_t Sample_DID_8_Data_au8    			    [1 ];
extern uint8_t Sample_DID_9_Data_au8    			    [1 ];
extern uint8_t Sample_DID_10_Data_au8    			    [1 ];


extern uint16_t Dist_Drvn;

extern U1 u1_Diag_param_tbl[U2_DIAG_SIZE] ;
extern U1 u1_NVM_Config_step;
extern U1 u1_DTC_NVM_step;
extern U1 u1_Diag_Config_step;
extern U1 u1_3D_Serv_step;
//extern void DISABLE_INTERRUPTS();
//extern void RESTORE_INTERRUPTS();
U1 WriteNVM_3DReq(U4 u4_addr, U2 u2_data);
U1 Read23_Ser_NVM(U2 memoryAddress, U1 memorySize, U1* u1_ptr_data);
void vd_Load_ASCII_ToDID(U1* u1_valptr_DID, U4 u4_data, U1 u1_size);
#endif
