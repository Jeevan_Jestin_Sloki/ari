/***********************************************************************************************************************
* File Name    : main.c
* Version      : 01
* Description  : Application Software of the HMI CLUSTER.
* Created By   : Dileepa B S
* Creation Date: 01/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "main.h"
#include "board_init.h"
#include "task_scheduler.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


/***********************************************************************************************************************
* Function Name: CLUSTER_ASW_MAIN
* Description  : This function implements the application software for the HMI Cluster.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void CLUSTER_ASW_MAIN(void)
{
	Hardware_Init(); /*Initialize the Hardware Peripherals/Drivers*/
	
	Software_Init(); /*Initialize the Software Components and Starts the Peripherals/Drivers*/
	
	while(1U)
	{
		TS_Start();      /*Start the Task Scheduler*/
		TS_Stop();	 /*Stop the Task Scheduler*/
	}
}

/********************************************************EOF***********************************************************/