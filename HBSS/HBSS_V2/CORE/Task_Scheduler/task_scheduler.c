/***********************************************************************************************************************
* File Name    : task_scheduler.c
* Version      : 01
* Description  : This file implements the Task_Scheduler
* Created By   : Jeevan Jestin N
* Creation Date: 25/12/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "task_scheduler.h"
#include "timer_user.h"
#include "r_cg_wdt.h"
#include "cluster_init.h"
#include "delay_flags.h"
#include "Time.h"
#include "BackLightCtrl.h"
#include "com_tasksched.h"
#include "iso14229_serv11.h"
#include "DataAquire.h"
#include "cluster_main.h"
#include "ClusterAnimation.h"
#include "hmi_config_can.h"
#include"Cluster_test.h"
#include "SignalCalc_main.h"
#include "Communicator.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool   TS_Exit_b 	= false;
Cluster_State_En_t	Cluster_State_En = DISPLAY_HOLD_E;


/***********************************************************************************************************************
* Function Name: _TS_Start
* Description  : This function schedules the tasks for every 5-ms
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void TS_Start(void)
{
	static uint32_t Counter_5ms_u32   = 0;	
	static uint32_t Counter_reset_u32 = 0;
	
 	while(!TS_Exit_b)
	{
		while(_5ms_TS_Flag_b)
		{
			_5ms_TS_Flag_b = false;
			TaskSchedulerCounter_u32 += 5;
			Counter_5ms_u32++;  /* Counter is incremented by 1 for every 5 ms */
			
			R_WDT_Restart();    /*Refersh the Watch-Dog-Timer within 34.13 m-sec*/
			
			Diag_TS_Proc_5ms();
			
			 Set_500ms_Flags(Counter_5ms_u32);
			 Set_250ms_Flags(Counter_5ms_u32);
			
			 ReadClusterInput();
			 	/*Read the Rider Inputs to the cluster through GPIO/ADC/INTC*/
				
			 UpdateSigToDataBank();
			 	/*Update the Data Bank with the user input data and CAN data*/
			
			Cluster_Init(); /*Init or Deinit the Cluster*/	
			
			 //Update_Time(); /* Update the time */ 
			 CalculateSignal();
			
			 if(true == reset_b)
			 {
			 	Counter_reset_u32++;
			 	if(Counter_reset_u32 == 3)
			 	{
			 		TS_Exit_b = true;
			 	}
			 }
			
			 /*TODO  CLUSTER ANIMATION FOR 2.5 SECOND*/
			 if(Cluster_State_En == DISPLAY_ANIMATION_E)
			 {
			 	if(true == Cluster_Init_Success_b)
			 	{
			 		AnimateOnStart();
					BuzzerAnimation();
			 	}
			 	else
			 	{
			 		;
			 	}
			 }
			 else
			 {
			 	;
			 }
			
			
			/*TO DISPLAY CLUSTER DATA*/
 			if(Cluster_State_En == DISPLAY_DATA_E)
 			{
				if(0 == (Time_Tick_u32 % _30MS_DIVISOR_6)) 
				{
					if(BackLightCtrl_En == BACKLIGHT_CTRL_CAN_E)
					{
						//ControlBrightness0verCAN(HMI_CONF_MSG_1.Brightness_u8);
					}
#if(BACKLIGHT_CTRL_ALS_MODE == TRUE)
					else if( BackLightCtrl_En == BACKLIGHT_CTRL_ALS_E )
					{
						AdjustLightIntensity();
					}
#endif	
				}
				else
				{
					;
				}
				// if((TaskSchedulerCounter_u32%40) == 0)
				// {
				// 	TestMain();
				// }

				if(0 == (TaskSchedulerCounter_u32 % _50MS_DIVISOR)) 
				{     
					if(true == Cluster_Init_Success_b)
					{
						Display_Cluster_Data();
					}
					else
					{
						;	
					}
				}
				else
				{
					;
				}
 			}
 			else
 			{
 				;
 			}
		}
	}
	return;
}


/***********************************************************************************************************************
* Function Name: TS_Stop
* Description  : This functoion Restarts the MCU.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void TS_Stop(void)
{
	int8_t __far* a;            // Create a far-Pointer
	IAWCTL|=0x80;               // switch IAWEN on (defalut off)
	a=(int8_t __far*) 0x00000;  
	*a=0; 
	return;
}

/********************************************************EOF***********************************************************/