/***********************************************************************************************************************
* File Name    : Signals_Calc.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 05/01/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Signals_Calc.h"
#include "Communicator.h"
#include "ODOmeter_Calc.h"
#include "Speed_Calc.h"
#include "Range_WhKmCalc.h"
#include "ari_bms_can.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

/***********************************************************************************************************************
* Function Name: CalculateClusterSignals
* Description  : This function Calculates the Required Cluster Signals.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void CalculateClusterSignals(void)
{

	 Calculate_ODOmeter();	/*Calculate Total Distance Travelled in Meters*/
	
	 Calculate_VehicleSpeed();	/*Calculate Present Vehicle Speed in Km/Hr*/

	if(true == BMSframeReceived_b)
	{
		EstimateWhKm(BMS_MSG_1_St.BatteryVoltage_u32,BMS_MSG_1_St.BatterySOC_u16,BMS_MSG_1_St.BatteryTotalCapacity_u16);

		UpdateRangeKm(BMS_MSG_1_St.BatteryVoltage_u32, BMS_MSG_1_St.BatterySOC_u16,BMS_MSG_1_St.BatteryTotalCapacity_u16);
	}
	
	if(CHARGING_STATE == BMS_MSG_2_St.BatteryPackState_u8)
	{
		BatteryState_u8 = CHARGING_STATE;
	}
	else
	{
		BatteryState_u8 = DISCHARGING_STATE;
	}

	if(REGEN_THRESHOLD <= BMS_MSG_1_St.BatteryCurrent_s16)
	{
		RegenState_u8 = REGEN_STATE_ON;
	}
	else
	{
		RegenState_u8 = REGEN_STATE_OFF;
	}

	SET_SOC((uint8_t)BMS_MSG_1_St.BatterySOC_u16); 
	return;
}


/********************************************************EOF***********************************************************/