/******************************************************************************
 *    FILENAME    : diag_adapt.c
 *    DESCRIPTION : Application adapter interface file for DIAG Stacks.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 

/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "diag_adapt.h" 
#include "diag_sys_conf.h"
#if(TRUE == DIAG_UDS_SUPPORTED)
	#include "uds_conf.h"
#endif
#if(DIAG_INTERRUPTS == TRUE)
	#include "r_cg_macrodriver.h"
#endif




/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

//static int16_t ECT_Deg_s16 = 0;    //variable which holds the Engine starting coolent temperature    
int16_t Diff_ECT_s16 = 0;
uint16_t Dist_Drvn = 0;

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

void vd_Load_ASCII_ToDID(U1* u1_valptr_DID, U4 u4_data, U1 u1_size)
{
	while(u1_size) {
		*u1_valptr_DID = (U1)((0x0F & (u4_data>>(4*(u1_size-1)))) + 0x30);
		u1_valptr_DID++;
		u1_size--;
	}
}
void vd_EOL_Cmd(U2 u2_t_cmd)
{
	//STS_EOL_CMD_REC = TRUE;
	//u2_eol_cmd_data = u2_t_cmd;
}

void	NvmmgrSetReq(U4 u4_ch, U1 u1_mode)
{
//	if(u1_mode == ON){					/* if request exists 			*/
//		st_fnvm.lword |= u4_ch;				/* set request flag 			*/
//	}
//	else{
//		st_fnvm.lword &= ~u4_ch;				/* clear request flag 			*/
//	}
}

void vd_EOL_NVM_Key(U4 u4_NVM_Key)
{
//	if(NVM_EOL_KEY == u4_NVM_Key)
//	{
//		STS_EOL_KEY_ACCEPTED = TRUE;
//		STS_EOL_CMD_REC = FALSE;
//	}
}

U1 EOL_Key_NoTimeout(void)
{
//	if(u2_EOL_KeyAcceptCnt < NVM_KEY_MAX_TIME) {
//		return TRUE;
//	} else {
//		return FALSE;
//	}
  return 0;
}

U1 Check_EOL_NVM_Security(void)
{
	//return STS_EOL_KEY_ACCEPTED;
  return 0;
}

void u1_Start_EOL(void)
{	
//	EOL_Initial();
//	LCD_Wkup();
//	/*STS_EOL_BEGIN = TRUE;*/
//	STS_EOL_INPROGRESS = TRUE;
//	vd_SetTachHi();
//	vd_SetTempHi();
//	FuelctlSetHi();
//	vd_SetOatHi();
//	STS_EOL_KEY_ACCEPTED = FALSE;
//	u2_EOL_KeyAcceptCnt = 0;
}

U1 Read23_Ser_NVM(U2 memoryAddress, U1 memorySize, U1* u1_ptr_data)
{
//	U1 u1_index;
//	NVM_RET	st_ret;
//	
//	IRQ_DISABLE();
//	for(u1_index = 0; u1_index < (memorySize/2); u1_index++)
//	{
//		st_ret 	= st_NvmRead(memoryAddress);
//		if(st_ret.u1_ret == OK)
//		{
//			*(u1_ptr_data++) = (U1)((st_ret.u2_data & 0xFF00) >> 8);
//			*(u1_ptr_data++) = (U1)(st_ret.u2_data & 0x00FF);
//			memoryAddress++;
//		}
//	}
//	IRQ_ENABLE();
//	return TRUE;
  return 0;
}

U1 WriteNVM_3DReq(U4 u4_addr, U2 u2_data)
{
//	if(u1_NVM_3D_loop_index != 0) {			/* Check Busy	*/
//		return TRUE;
//	}
//	if(u1_NVM_3D_size >= NVM_3D_MAX_DATA) {		/* Max Limit of u2_NVM_3D_Data[]	*/
//		return TRUE;
//	}
//	if(!u1_NVM_3D_size) {
//		u2_NVM_3D_Addr = u4_addr;
//	} else if(u4_addr != u2_NVM_3D_Addr + u1_NVM_3D_size) {		/* Checking for consecutive data	*/
//		return TRUE;
//	}
//	u2_NVM_3D_Data[u1_NVM_3D_size] = u2_data;
//	u1_3D_Serv_step = WRITE_NVM;
//	u1_NVM_3D_size++;
//	return FALSE;
  return 0;
}










uint8_t PDID1_Buffer_au8[4];
uint8_t PDID2_Buffer_au8[4];

uint8_t Boot_Soft_Iden_au8	         	     	[25];
uint8_t Boot_Fp_Data_au8               	     	[31];
uint8_t App_Soft_Fp_Data_au8           	     	[31];
uint8_t APP_SwData_Date_au8       		     	[10];
uint8_t APP_SwData_TesterSerNo_au8       	 	[10];
uint8_t APP_SwData_RepairShop_au8       	 	[10];
uint8_t APP_SwData_ReprogSeq_au8       	     	[1 ];
uint8_t CONFIG_FingerPrintData_au8       	 	[31];
uint8_t CONFIG_Data_Date_au8       		     	[10];
uint8_t CONFIG_Data_TesterSerNo_au8     	 	[10];
uint8_t CONFIG_Data_RepairShop_au8      	 	[10];
uint8_t CONFIG_Data_ReprogSeq_au8       	 	[1 ];
uint8_t ECU_Serial_Number_au8				 	[24];
uint8_t REPROGM_SVN_u8                	     	[10];
uint8_t ACTIVE_DIAGNOSTIC_Sess_au8		     	[1 ];
uint8_t ECU_MFG_Date_au8                     	[10];
uint8_t APP_Soft_Status_au8          		 	[1 ];
uint8_t CALIBRATION_Soft_Status_au8		     	[1 ];
uint8_t CONFIG_Soft_Status_au8 			     	[1 ];
uint8_t ACTIVE_SoftwareComponent_au8    	 	[1 ];
uint8_t ECU_HW_Num_au8                       	[24];
uint8_t ECU_SW_Num_au8                       	[24];
uint8_t System_name_au8					     	[24];
uint8_t PROGRAM_Secret_Key_au8  	    	 	[4 ];
uint8_t EXTENDED_Secret_Key_au8          	 	[4 ];
uint8_t END_Of_Line_Secret_Key_au8       	 	[4 ];
uint8_t SERV_Record1_au8               	     	[14];
uint8_t SERV_Record2_au8                     	[14];
uint8_t SERV_Record3_au8               	     	[14];
uint8_t SERV_Record4_au8                     	[14];
uint8_t SERV_record5_au8               	     	[14];
uint8_t PBL_Reserved_au8			       	 	[17];
uint8_t App_Soft_Iden_au8              	     	[25];
uint8_t SUPPLIER_Part_Num_au8       	     	[17];
uint8_t ECU_PBL_SW_Num_au8                   	[17];
uint8_t ECU_APP_SW_Num_au8                   	[17];
uint8_t ECU_Config_Sw_Number_au8			 	[17];
uint8_t ECU_Assembly_Number_au8			     	[17];
uint8_t DTC_Mask_Iden_au8              	     	[4 ];
uint8_t SBL_VER_Num_au8                	     	[25];
uint8_t ECU_Spec_au8                   	     	[5 ];
uint8_t Engine_Warn_Ind_au8            	     	[6 ];
uint8_t SEAT_Belt_Warn_Chime_au8       	     	[2 ];
uint8_t SEAT_Belt_Warn_Ind_au8         	     	[5 ];
uint8_t GEAR_Mode_Ind_au8              	     	[6 ];
uint8_t METER_Display_au8              	     	[5 ];
uint8_t AFE_IFE_Display_au8            	     	[8 ];
uint8_t REV_GEAR_AUDIO_Display_au8     	     	[8 ];
uint8_t SERV_INTERVAL_Distance_au8     	     	[9 ];
uint8_t DISTANCE_Since_Last_Serv_au8   	     	[4 ];
uint8_t PARK_BRAKE_Warn_VEHICLE_Speed_au8    	[2 ];
uint8_t AMT_Warn_Indicator_au8               	[5 ];
uint8_t WIF_Warn_Indicator_au8               	[2 ];
uint8_t FUEL_Level_au8                 	     	[5 ];
uint8_t CLK_DISPLAY_Setting_au8        	     	[6 ];
uint8_t SET_Switches_au8           		     	[2 ];
uint8_t GLOW_PLUG_Warning_au8        	     	[7 ];
uint8_t MIN_DISTANCE_Inst_Fuel_au8     	     	[6 ];
uint8_t AVG_FUEL_Time_au8          		     	[6 ];
uint8_t OAT_Condition_Time_au8         	     	[4 ];
uint8_t TIRE_Size_au8                        	[7 ];
uint8_t FUEL_Tank_Size_au8             	     	[1 ];
uint8_t IC_Replacement_au8                   	[7 ];
uint8_t SEASON_Odometer_au8           	     	[7 ];
uint8_t SPEED_Limit_au8                      	[3 ];
uint8_t ABS_Fac_au8                          	[2 ];
uint8_t ILLUMINATION_Fine_Tunn_au8       	 	[10];
uint8_t CNTRL_MODULE_Config_Type_au8		 	[1 ];
uint8_t COMMUNICATION_Status_au8			 	[1 ];
uint8_t VEHICLE_Mode_au8             		 	[1 ];
uint8_t ILL_DIMMING_Lev_Output_au8		     	[1 ];
uint8_t GLOBAL_RT_Clear_DTC_au8			     	[3 ];
uint8_t DTC_SET_DUE_TO_DIAG_Routine_au8	     	[1 ];
uint8_t GLOBAL_Real_Time_au8				 	[3 ];
uint8_t TOTAL_Distance_Travelled_au8		 	[3 ];
uint8_t ECU_Supply_Voltage_au8			     	[1 ];
uint8_t VEHICLE_Interior_Temp_au8			 	[1 ];
uint8_t ABIENT_AIR_Temp_au8				     	[1 ];
uint8_t VEHICLE_Power_Mode_au8			     	[1 ];
uint8_t ENGINE_Speed_au8		        	 	[2 ];
uint8_t VEHICLE_Speed_au8					 	[1 ];
uint8_t ENGINE_Coolant_Temp_au8			     	[1 ];
uint8_t Throttle_Position_au8                  	[1 ];
uint8_t VIN_M_au8                      	     	[5 ];
uint8_t AFE_Biasing_au8					     	[1 ];
uint8_t DIAG_GATEWAY_State_au8           	 	[1 ];
uint8_t NW_TOPOLOGY_Config_au8           	 	[1 ];
uint8_t PARK_Lamp_In_au8                 	 	[1 ];
uint8_t IGN_In_au8		            	     	[1 ];
uint8_t TURN_Warn_Indicator_au8          	 	[1 ];
uint8_t HAZARD_Warn_Indicator_au8        	 	[1 ];
uint8_t ABS_Warn_Indicator_au8           	 	[1 ];
uint8_t EPAS_PSC_Warn_Indicator_au8      	 	[1 ];
uint8_t HIGH_COOLANT_Temp_Warn_au8       	 	[1 ];
uint8_t BATTERY_CHARGE_Warn_Indicator_au8	 	[1 ];
uint8_t BRAKE_FLUIDE_Warn_Indicator_au8  	 	[1 ];
uint8_t PARK_BRAKE_Warn_Indicator_au8    	 	[1 ];
uint8_t KEY_In_Out_au8                   	 	[1 ];
uint8_t SPEED_Limit_Warn_Indicator_au8   	 	[1 ];
uint8_t GLOW_PLUG_Warn_Indicator_au8     	 	[1 ];
uint8_t TX_Fault_Indicator_au8           	 	[1 ];
uint8_t DRL_Indicator_au8                	 	[1 ];
uint8_t WELCOME_Strategy_au8             	 	[1 ];
uint8_t OAT_Config_au8                   	 	[1 ];
uint8_t TCU_Buzzer_Req_au8               	 	[1 ];
uint8_t DEBUG_Frame_au8                  	 	[1 ];
uint8_t VEHICLE_MFG_ECU_Soft_Num_au8         	[17];
uint8_t VIN_1_u8                      	     	[24];
uint8_t DIAG_Version_au8                 	 	[10];
uint8_t VCN_au8                        	     	[15];
uint8_t ECU_PARAM_SW_Num_au8                 	[17];
uint8_t CONFIG_Data_Version_au8       	     	[10];
uint8_t READ_Mask_DTCs_au8               	 	[20];
uint8_t POWER_Train_Type_au8				 	[1 ];
uint8_t Driver_SBR_Tone_Count_au8			 	[1 ];
uint8_t ECU_installation_date_au8			 	[10];
uint8_t Eco_Sport_City_Warn_au8			     	[1 ];
uint8_t EOL_SESSION_au8       			     	[4 ];
uint8_t Ecu_Sbl_SW_Num_au8       			    [17];
uint8_t System_Supplier_au8       			    [24];
uint8_t Electrical_Power_au8       			    [1 ];
uint8_t Ecu_Status_Sig_au8       			    [3 ];
uint8_t Veh_Manf_Ecu_Hw_Num_au8    			    [17];
uint8_t Frontfog_Warn_Indi_au8    			    [1 ];

uint8_t Sample_DID_1_Data_au8    			    [1 ];
uint8_t Sample_DID_2_Data_au8    			    [1 ];
uint8_t Sample_DID_3_Data_au8    			    [1 ];
uint8_t Sample_DID_4_Data_au8    			    [1 ];
uint8_t Sample_DID_5_Data_au8    			    [1 ];
uint8_t Sample_DID_6_Data_au8    			    [1 ];
uint8_t Sample_DID_7_Data_au8    			    [1 ];
uint8_t Sample_DID_8_Data_au8    			    [1 ];
uint8_t Sample_DID_9_Data_au8    			    [1 ];
uint8_t Sample_DID_10_Data_au8    			    [1 ];



U1 u1_config_param_tbl[U1_CONFIG_SIZE] ;

U1 u1_Diag_param_tbl[U2_DIAG_SIZE] ;
U1 u1_NVM_Config_step;
U1 u1_DTC_NVM_step;
U1 u1_Diag_Config_step;
U1 u1_3D_Serv_step;


void Diag_Disable_Interrupts(void)
{
#if(DIAG_INTERRUPTS == TRUE)
	DI();
#endif
    //PLIB_INT_Disable(INT_ID_0);
}

void Diag_Restore_Interrupts(void)
{
#if(DIAG_INTERRUPTS == TRUE)
	EI();
#endif
    //PLIB_INT_Enable(INT_ID_0);
}
