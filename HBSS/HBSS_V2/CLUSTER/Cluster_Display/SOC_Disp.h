/***********************************************************************************************************************
* File Name    : SOC_Disp.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 16/12/2021
***********************************************************************************************************************/

#ifndef SOC_DISP_H
#define SOC_DISP_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 
#include "Cluster_Conf.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define TOTAL_SOC_BARS				5
#define DISCHARGING_MODE			0x00U
#define CHARGING_MODE				0x01U
#define SAFE_MODE_DISCHARGE_START		41

#define RED_BAR_1_VALUE				1
#define RED_BAR_2_VALUE				11

#define MAX_SOC_PER				100

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	SOC_DISPLAY_E,
	SOC_DISP_HOLD_E,
}SOC_DispState_En_t;


typedef enum
{
	SOC_BAR_START_E = 0,
	SOC_BAR_1_E,
	SOC_BAR_2_E,
	SOC_BAR_3_E,
	SOC_BAR_4_E,
	SOC_BAR_5_E,
	SOC_TOTAL_BARS_E = SOC_BAR_5_E,
}SOC_BAR_POS_En_t;


typedef enum
{
	CHARGING_START_E,
	CHARGING_E,
	CHARGING_END_E,
}Charge_State_En_t;

typedef enum
{
	SOC_GREEN_BAR_DISP_E,
	SOC_RED_BAR_DISP_E,
}SOC_BarDispState_En_t;

typedef enum
{
	DISP_NONE_E,
	DISP_CH_TEXT_E,
	DISP_SOC_NUM_E,
}CH_SOC_Disp_En_t;


typedef enum
{
	SOC_PER_DISPLAY_E,
	SOC_PER_DISP_HOLD_E,
}SocPerDispState_En_t;

/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern uint32_t	     		SOC_BAR_Value_u32;
extern bool 			InitialSOC_Disp_b;
extern Charge_State_En_t	Charge_State_En;
extern SOC_BarDispState_En_t	SOC_BarDispState_En;
extern CH_SOC_Disp_En_t		CH_SOC_Disp_En;

extern uint16_t 		SOC_PER_Value_u16;
extern bool			InitialPercent_Disp_b;

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
uint16_t    	Validate_SOC(uint16_t);
extern uint32_t Get_SOC_Bars(uint16_t,uint8_t);
void 		Display_ChargeSOC(uint16_t, ClusterSignals_En_t);
void 		Display_DischargeSOC(uint16_t, ClusterSignals_En_t);
extern void 	Display_SOC(uint16_t, uint8_t, ClusterSignals_En_t ,ClusterSignals_En_t );
void		Get_Bar_PosAndValue(uint16_t);
void 		DispUSM_DischargeSOC(ClusterSignals_En_t, uint32_t, bool);  /*To display the SOC @ Unsafe mode
									(when SOC <= 30% --> REDZONE)*/
								

extern void Disp_BattPer(uint16_t SOC_Per_u16, ClusterSignals_En_t SOC_PER_E);
uint16_t ValidatePercentage(uint16_t Percent_Check_u16);
#endif /* SOC_DISP_H */


