﻿
/***********************************************************************************************************************
* File Name    : bms_can.c
* Version      : 01
* Description  : The file implements the CAN data serealize/Deserealize.
* Created By   : Jeevan Jestin N
* Creation Date: 26/05/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "bms_can.h"
/***********************************************************************************************************************
MACRO directive
***********************************************************************************************************************/

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
BatteryGeneralInfo_0xBB_St_t BatteryGeneralInfo_0xBB_St;
CapacityInfo_0xA3_St_t CapacityInfo_0xA3_St;
RequestFrame_0x16_St_t RequestFrame_0x16_St;

bool BmsframeRecived_b = false;
bool CapacityInfoRec_b = false;
bool BattGeneralInfoRec_b = false;
/***********************************************************************************************************************
* Function Name: Deserialize_RequestFrame
* Description  : The function Deserialize the Request Frame.
* Arguments    : RequestFrame_0x16_St_t
* Return Value : CAN ID
***********************************************************************************************************************/
 uint32_t Deserialize_RequestFrame(RequestFrame_0x16_St_t* message, const uint8_t* data)
{
  message->BatteryID = ((data[0] & (SIGNLE_READ_Mask8))) + REQUESTFRAME_CANID_BATTERYID_OFFSET;
  message->CMD = ((data[1] & (SIGNLE_READ_Mask8))) + REQUESTFRAME_CANID_CMD_OFFSET;
   return REQUESTFRAME_ID; 
}


/*----------------------------------------------------------------------------*/

/***********************************************************************************************************************
* Function Name: Serialize_RequestFrame
* Description  : The function serialize the Request Frame.
* Arguments    : RequestFrame_0x16_St_t
* Return Value : CAN ID
***********************************************************************************************************************/
 uint32_t Serialize_RequestFrame(RequestFrame_0x16_St_t* message, uint8_t* data)
{
  message->BatteryID = (message->BatteryID  - REQUESTFRAME_CANID_BATTERYID_OFFSET);
  message->CMD = (message->CMD  - REQUESTFRAME_CANID_CMD_OFFSET);
  data[0] = (message->BatteryID & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->CMD & (SIGNLE_READ_Mask8)) ;
   return REQUESTFRAME_ID; 
}


/*----------------------------------------------------------------------------*/

/***********************************************************************************************************************
* Function Name: Deserialize_CapacityInfo
* Description  : The function Deserialize the Capacity Frame.
* Arguments    : CapacityInfo_0xA3_St_t
* Return Value : CAN ID
***********************************************************************************************************************/
 uint32_t Deserialize_CapacityInfo(CapacityInfo_0xA3_St_t* message, const uint8_t* data)
{
  message->AvailableCapacity = (((data[2] & (SIGNLE_READ_Mask8)) << CAPACITYINFO_AVAILABLECAPACITY_MASK0) | (data[3] & (SIGNLE_READ_Mask8))) + CAPACITYINFO_CANID_AVAILABLECAPACITY_OFFSET;
  message->FullChargeCapacity = (((data[4] & (SIGNLE_READ_Mask8)) << CAPACITYINFO_FULLCHARGECAPACITY_MASK0) | (data[5] & (SIGNLE_READ_Mask8))) + CAPACITYINFO_CANID_FULLCHARGECAPACITY_OFFSET;
   return CAPACITYINFO_ID; 
}


/*----------------------------------------------------------------------------*/
/***********************************************************************************************************************
* Function Name: serialize_CapacityInfo
* Description  : The function serialize the Capacity Frame.
* Arguments    : CapacityInfo_0xA3_St_t
* Return Value : CAN ID
***********************************************************************************************************************/

 uint32_t Serialize_CapacityInfo(CapacityInfo_0xA3_St_t* message, uint8_t* data)
{
  message->AvailableCapacity = (message->AvailableCapacity  - CAPACITYINFO_CANID_AVAILABLECAPACITY_OFFSET);
  message->FullChargeCapacity = (message->FullChargeCapacity  - CAPACITYINFO_CANID_FULLCHARGECAPACITY_OFFSET);
  data[2] = ((message->AvailableCapacity >> CAPACITYINFO_AVAILABLECAPACITY_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[3] = (message->AvailableCapacity & (SIGNLE_READ_Mask8)) ;
  data[4] = ((message->FullChargeCapacity >> CAPACITYINFO_FULLCHARGECAPACITY_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[5] = (message->FullChargeCapacity & (SIGNLE_READ_Mask8)) ;
   return CAPACITYINFO_ID; 
}


/*----------------------------------------------------------------------------*/

/***********************************************************************************************************************
* Function Name: Deserialize_BatteryGeneralInfo
* Description  : The function Deserialize the Battery General Frame.
* Arguments    : BatteryGeneralInfo_0xBB_St_t
* Return Value : CAN ID
***********************************************************************************************************************/
 uint32_t Deserialize_BatteryGeneralInfo(BatteryGeneralInfo_0xBB_St_t* message, const uint8_t* data)
{
  message->BattVoltage_u16 = (((data[0] & (SIGNLE_READ_Mask8)) << BATTERYGENERALINFO_BATTVOLTAGE_MASK0) | (data[1] & (SIGNLE_READ_Mask8))) + BATTERYGENERALINFO_CANID_BATTVOLTAGE_OFFSET;
  message->BatteryCurrent_u16 = (((data[2] & (SIGNLE_READ_Mask8)) << BATTERYGENERALINFO_BATTERYCURRENT_MASK0) | (data[3] & (SIGNLE_READ_Mask8))) + BATTERYGENERALINFO_CANID_BATTERYCURRENT_OFFSET;
  message->SOC = ((data[4] & (SIGNLE_READ_Mask8))) + BATTERYGENERALINFO_CANID_SOC_OFFSET;
  message->BatteryTemperture_u16 = ((data[5] & (SIGNLE_READ_Mask8))) - BATTERYGENERALINFO_CANID_BATTERYTEMPERTURE_U16_OFFSET;
  message->BattPackState_u8 = ((data[6] & (SIGNLE_READ_Mask8))) + BATTERYGENERALINFO_CANID_BATTPACKSTATE_U8_OFFSET;
   return BATTERYGENERALINFO_ID; 
}

/*----------------------------------------------------------------------------*/
/***********************************************************************************************************************
* Function Name: serialize_BatteryGeneralInfo
* Description  : The function serialize the Battery General Frame.
* Arguments    : BatteryGeneralInfo_0xBB_St_t
* Return Value : CAN ID
***********************************************************************************************************************/

 uint32_t Serialize_BatteryGeneralInfo(BatteryGeneralInfo_0xBB_St_t* message, uint8_t* data)
{
  message->BattVoltage_u16 = (message->BattVoltage_u16  - BATTERYGENERALINFO_CANID_BATTVOLTAGE_OFFSET);
  message->BatteryCurrent_u16 = (message->BatteryCurrent_u16  - BATTERYGENERALINFO_CANID_BATTERYCURRENT_OFFSET);
  message->SOC = (message->SOC  - BATTERYGENERALINFO_CANID_SOC_OFFSET);
  message->BatteryTemperture_u16 = (message->BatteryTemperture_u16  - BATTERYGENERALINFO_CANID_BATTERYTEMPERTURE_U16_OFFSET);
  message->BattPackState_u8 = (message->BattPackState_u8  - BATTERYGENERALINFO_CANID_BATTPACKSTATE_U8_OFFSET);
  data[0] = ((message->BattVoltage_u16 >> BATTERYGENERALINFO_BATTVOLTAGE_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[1] = (message->BattVoltage_u16 & (SIGNLE_READ_Mask8)) ;
  data[2] = ((message->BatteryCurrent_u16 >> BATTERYGENERALINFO_BATTERYCURRENT_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[3] = (message->BatteryCurrent_u16 & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->SOC & (SIGNLE_READ_Mask8)) ;
  data[5] = (message->BatteryTemperture_u16 & (SIGNLE_READ_Mask8)) ;
  data[6] = (message->BattPackState_u8 & (SIGNLE_READ_Mask8)) ;
   return BATTERYGENERALINFO_ID; 
}

/***********************************************************************************************************************
* Function Name: ResetBatteryGeneralInfo
* Description  : The function Clear the Battery general info data
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetBatteryGeneralInfo(void)
{
  BattGeneralInfoRec_b = false;
  BatteryGeneralInfo_0xBB_St.BatteryCurrent_u16 = CLEAR;
  BatteryGeneralInfo_0xBB_St.BatteryTemperture_u16 =CLEAR;
  BatteryGeneralInfo_0xBB_St.BattVoltage_u16 = CLEAR;
  BatteryGeneralInfo_0xBB_St.SOC = CLEAR;
  BatteryGeneralInfo_0xBB_St.BattPackState_u8 = CLEAR;
  return;
}

/***********************************************************************************************************************
* Function Name: ResetCapacityInfo
* Description  : The function Clear the Capacity Info
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetCapacityInfo(void)
{
  CapacityInfoRec_b = false;
  CapacityInfo_0xA3_St.AvailableCapacity = CLEAR;
  CapacityInfo_0xA3_St.FullChargeCapacity = CLEAR;
  return;
}

/***********************************************************************************************************************
* Function Name: ResetBmsData
* Description  : The function Clear bms data
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetBmsData(void)
{
  ResetBatteryGeneralInfo();
  ResetCapacityInfo();
}
/***********************************************EOL********************************************************************/