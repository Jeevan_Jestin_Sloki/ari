/***********************************************************************************************************************
* File Name    : Range_WhKmCalc.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 07/06/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include"Range_WhKmCalc.h"
#include "Communicator.h"
/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
WhRangeKm_St_t WhRangeKm_St;
bool    WhEstimateOnstart_b = false;
uint16_t DistanceTravelled_u16 = 0;
uint8_t Odo100MeterCount_u8 = 0;
float PresentBatteryAh = 0;
/***********************************************************************************************************************
* Function Name: EstimateWhKm
* Description  : The function estimate the Watts consumed per Km
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void EstimateWhKm(uint32_t VoltagemV_u32, uint16_t SocPercentage_u16, uint16_t TotalCapacityAh_u16)
{
    //float PresentBatteryAh = 0;
    if(false == WhEstimateOnstart_b)
    {
	WhEstimateOnstart_b = true;
        PresentBatteryAh = ((float)SocPercentage_u16/100) * TotalCapacityAh_u16;
        WhRangeKm_St.PreviousWatts_u16 = (uint16_t)(PresentBatteryAh * ( (float)VoltagemV_u32/1000));
        WhRangeKm_St.PreviousOdoMeter_u32 = GET_PRESENT_ODOMETER();
    }
    else
    {
        DistanceTravelled_u16 = DistanceTravelled_u16 +  (uint16_t)( GET_PRESENT_ODOMETER() -  WhRangeKm_St.PreviousOdoMeter_u32 );
        WhRangeKm_St.PreviousOdoMeter_u32 = GET_PRESENT_ODOMETER();

        if(100 < DistanceTravelled_u16)
        {
            Odo100MeterCount_u8 ++;
            DistanceTravelled_u16 = DistanceTravelled_u16 - 100;
            Record_BatteryVoltage(REC_VOLTAGE_E,VoltagemV_u32);
        }

        if(5 == Odo100MeterCount_u8)
        {
            Odo100MeterCount_u8 = 0;
            PresentBatteryAh = (((float)SocPercentage_u16/100) * (float)TotalCapacityAh_u16);
            Record_BatteryVoltage(CAL_AVG_VOLTAGE_E,VoltagemV_u32);
            WhRangeKm_St.PresentWatts_u16 = (uint16_t)( PresentBatteryAh * ( (float)WhRangeKm_St.AvgBattVoltage_u32/1000));
            WhRangeKm_St.WattsConsumed_u16 = WhRangeKm_St.PreviousWatts_u16 - WhRangeKm_St.PresentWatts_u16;
            PresentSessionWHkm_u16 =  WhRangeKm_St.WattsConsumed_u16 * 2;
            WhRangeKm_St.PreviousWatts_u16 = WhRangeKm_St.PresentWatts_u16;
        }
    }

    return;
}

/***********************************************************************************************************************
* Function Name: Record_BatteryVoltage
* Description  : This function Records the Battery Voltage and Calculates the Average Battery Voltage.
* Arguments    : BattVolRecFun_En_t BattVolRecFun_En,uint32_t BatteryVoltage_u32
* Return Value : None
***********************************************************************************************************************/
void Record_BatteryVoltage(BattVolRecFun_En_t BattVolRecFun_En,uint32_t BatteryVoltage_u32)
{
	uint32_t SumOfBattVoltage_u32 = 0;
	
	static uint16_t RecIndexCount_u16 = 0;
	
	switch(BattVolRecFun_En)
	{
		case REC_VOLTAGE_E:
		{
			//Record the Battery Voltage
			if(RecIndexCount_u16 < REC_LEN)
			{
				WhRangeKm_St.BattVoltage[RecIndexCount_u16] = BatteryVoltage_u32;
				RecIndexCount_u16++;
			}
			else
			{
				RecIndexCount_u16 = 0;
			}
			break;
		}
		case CAL_AVG_VOLTAGE_E:
		{
			//Calculate the Battery Average voltage from the recorded data
			for(RecIndexCount_u16 = 0; RecIndexCount_u16 < REC_LEN; RecIndexCount_u16++)
			{
				SumOfBattVoltage_u32 += WhRangeKm_St.BattVoltage[RecIndexCount_u16];
			}
			
			RecIndexCount_u16 = 0;
			
			WhRangeKm_St.AvgBattVoltage_u32 = (SumOfBattVoltage_u32 / REC_LEN);
			
			break;
		}
		default:
		{
			;
		}
	}
	return;
}

/***********************************************************************************************************************
* Function Name: Record_BatteryVoltage
* Description  : This function Records the Battery Voltage and Calculates the Average Battery Voltage.
* Arguments    : BattVolRecFun_En_t BattVolRecFun_En,uint32_t BatteryVoltage_u32
* Return Value : None
***********************************************************************************************************************/
void UpdateRangeKm(uint32_t VoltagemV_u32, uint16_t SocPercentage_u16, uint16_t TotalCapacityAh_u16)
{
    float BatteryAh = 0;
    uint16_t TotalWatts_u16 = 0;

    if( ( 0 != PresentSessionWHkm_u16 ) && ( 0 != TotalCapacityAh_u16) )
    {
	    BatteryAh = ((float)SocPercentage_u16/100) * (float)TotalCapacityAh_u16;
	    TotalWatts_u16 = (uint16_t)( BatteryAh *  (float)VoltagemV_u32/1000);

	    PresentSessionRangeKm_u16 = TotalWatts_u16 / PresentSessionWHkm_u16;
    }

    return;
}