/*
 * iso14229_serv36.c
 *
 *  Created on: 29-Jan-2020
 *      Author: Lakshmi
 */

/* ************************************************************************** */
/** Descriptive File Name

 @Company
 	 Sloki Technologies Pvt Ltd.

  	@File Name
    	iso14229_serv36.c

   @Summary
 	 This File contains macros, typedefs, extern definitions and
 	 user defined datatypes required by iso14229_serv34.c
 */

/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
#include <stdbool.h>
#include <stddef.h>
#include "iso14229_serv36.h"
#include "fee_adapt.h"
/* ************************************************************************** */


/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */

uint32_t Serv36TotalRequested_u32 = 0u;
uint32_t number_of_bytes_2b_write_u32;
uint32_t number_of_bytes_in_CF_u32 = 0x08;
uint32_t index_u32;
uint8_t block_seq_number_u8;
uint8_t rx_block_seq_number_u8 = 0u;
/* ************************************************************************** */

/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/**
    *  FUNCTION NAME : iso14229_serv36
    *  FILENAME      : iso14229_serv36.c
    *  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
    *  @brief        : This function will process the service_31 requests
    *  @return       : Type of response.
 */
UDS_Serv_resptype_En_t iso14229_serv36(UDS_Serv_St_t*  UDS_Serv_pSt){
	UDS_Serv_resptype_En_t serv_36_resp_En;
    uint16_t number_of_bytes_req_u16 = UDS_Serv_pSt->RxLen_u16;
    bool status_b = false;
	number_of_bytes_2b_write_u32 = number_of_bytes_req_u16 - 0x02 ;
	block_seq_number_u8 = UDS_Serv_pSt->RxBuff_pu8[1];

    if(SERV_36_MIN_LEN > number_of_bytes_req_u16 && SERV_36_MAX_LEN < number_of_bytes_req_u16) 
	{

    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_TRANSFER_DATA;
    	UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
    	UDS_Serv_pSt->TxLen_u16 = 3;
        serv_36_resp_En = UDS_SERV_RESP_NEG_E;
    }
    else if(!ISO14229_SA_L1_Gained_b) 
	{
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_TRANSFER_DATA;
    	UDS_Serv_pSt->TxBuff_pu8[2] = SECURITY_ACCESS_DENIED;
    	UDS_Serv_pSt->TxLen_u16 = 3;
        serv_36_resp_En = UDS_SERV_RESP_NEG_E;
    }
    else if(Flash_State_En != SERVICE34_E)
	{
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_TRANSFER_DATA;
    	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_SEQUENCE_ERR;
    	UDS_Serv_pSt->TxLen_u16 = 3;
        serv_36_resp_En = UDS_SERV_RESP_NEG_E;
    }

    else if(block_seq_number_u8 != (rx_block_seq_number_u8 )) 
	{
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_TRANSFER_DATA;
    	UDS_Serv_pSt->TxBuff_pu8[2] = WRONG_BLOCK_SEQUENCE_CNTR;
    	UDS_Serv_pSt->TxLen_u16 = 3;
        serv_36_resp_En = UDS_SERV_RESP_NEG_E;
        block_seq_number_u8++;
    }
    else
    {
    	status_b = Flash_Write(serv34_memory_address_u32, number_of_bytes_2b_write_u32, &(UDS_Serv_pSt->RxBuff_pu8[2]));
	status_b = true;
    	if(true == status_b)
    	{
			NumOf_Bytes_Programmed_u32 = NumOf_Bytes_Programmed_u32 + number_of_bytes_2b_write_u32;
			serv34_memory_address_u32 = serv34_memory_address_u32+number_of_bytes_2b_write_u32;
			Serv36TotalRequested_u32 = Serv36TotalRequested_u32 + (number_of_bytes_req_u16-2);
			rx_block_seq_number_u8++;
			UDS_Serv_pSt->TxBuff_pu8[1] = block_seq_number_u8;
			UDS_Serv_pSt->TxLen_u16  = 1;
			serv_36_resp_En = UDS_SERV_RESP_POS_E;
			Number_of_Blocks_u8--;
    	}
    	else
    	{
			UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
			UDS_Serv_pSt->TxBuff_pu8[1] = SID_TRANSFER_DATA;
			UDS_Serv_pSt->TxBuff_pu8[2] = TRANSFER_DATA_SUSPENDED;
			UDS_Serv_pSt->TxLen_u16 = 3;
			serv_36_resp_En = UDS_SERV_RESP_NEG_E;
    	}

    	if(!Number_of_Blocks_u8)
    	{
    		if(serv34_memory_size_u32 == Serv36TotalRequested_u32)
    		{
    			Flash_State_En = SERVICE36_E;

    		}
    		else
    		{
    			UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    			UDS_Serv_pSt->TxBuff_pu8[1] = SID_TRANSFER_DATA;
    			UDS_Serv_pSt->TxBuff_pu8[2] = TRANSFER_DATA_SUSPENDED;
    			UDS_Serv_pSt->TxLen_u16 = 3;
    			serv_36_resp_En = UDS_SERV_RESP_NEG_E;
    		}
    	}

    }

    return serv_36_resp_En;

}
/* *****************************************************************************
 End of File
 */

