/***********************************************************************************************************************
* File Name    : GearConf.c
* Version      : 01
* Description  : 
* Created By   : Pramod Jagtap
* Creation Date: 09/11/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Gear_Conf.h"

/***********************************************************************************************************************
Global variables
***********************************************************************************************************************/

/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/
/*
    Gear Text 
*/

const SignalsValue_St_t			GearText_SignalsValue_aSt[GEAR_TEXT_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		GearText_SigConf_ast},
};
const SignalConfig_St_t			GearText_SigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	GearText_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	GearText_On_SegConf_aSt},
};
const SegConfig_St_t			GearText_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG33,	0x0EU,		0x00U},
};
const SegConfig_St_t			GearText_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG33,	0x0EU,		0x01U},
};


/*
    Gear Digit 
*/

 const SignalsValue_St_t		GearDigit_SignalsValue_aSt[GEAR_DIGIT_SIG_LEN] =
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	{ ZERO_E,	ELEVEN_E,		GearDigit_SigConf_ast }
};

const SignalConfig_St_t		GearDigit_SigConf_ast[ELEVEN_E] = 
{
/*PositionSignalValue  SegmentsRequired  SegmentsConfiguration*/
	{ ZERO_E,	TWO_SEG_E,	GearDigit_0_SegConf_aSt	},
	{ ONE_E,	TWO_SEG_E,	GearDigit_1_SegConf_aSt	},
	{ TWO_E,	TWO_SEG_E,	GearDigit_2_SegConf_aSt	},
	{ THREE_E,	TWO_SEG_E,	GearDigit_3_SegConf_aSt	},
	{ FOUR_E,	TWO_SEG_E,	GearDigit_4_SegConf_aSt	},
	{ FIVE_E,	TWO_SEG_E,	GearDigit_5_SegConf_aSt	},
	{ SIX_E,	TWO_SEG_E,	GearDigit_6_SegConf_aSt	},
	{ SEVEN_E,	TWO_SEG_E,	GearDigit_7_SegConf_aSt	},
	{ EIGHT_E,	TWO_SEG_E,	GearDigit_8_SegConf_aSt	},
	{ NINE_E,	TWO_SEG_E,	GearDigit_9_SegConf_aSt	},
	{ OFF_DIGIT,	TWO_SEG_E,	GearDigit_Off_SegConf_aSt}
};

const SegConfig_St_t	GearDigit_0_SegConf_aSt[TWO_SEG_E] = 
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	{&SEG35,	0x00U,		0x0FU},
	{&SEG34,	0x01U,		0x0AU}
};
 const SegConfig_St_t	GearDigit_1_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x00U},
	{&SEG34,	0x01U,		0x0AU}
};
 const SegConfig_St_t	GearDigit_2_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x0BU},
	{&SEG34,	0x01U,		0x0CU}
};
 const SegConfig_St_t	GearDigit_3_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x09U},
	{&SEG34,	0x01U,		0x0EU}
};
 const SegConfig_St_t	GearDigit_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x04U},
	{&SEG34,	0x01U,		0x0EU}
};
 const SegConfig_St_t	GearDigit_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x0DU},
	{&SEG34,	0x01U,		0x06U}
};
 const SegConfig_St_t	GearDigit_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x0FU},
	{&SEG34,	0x01U,		0x06U}
};
 const SegConfig_St_t	GearDigit_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x08U},
	{&SEG34,	0x01U,		0x0AU}
};
 const SegConfig_St_t	GearDigit_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x0FU},
	{&SEG34,	0x01U,		0x0EU}
};
 const SegConfig_St_t	GearDigit_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x0DU},
	{&SEG34,	0x01U,		0x0EU}
};
 const SegConfig_St_t	GearDigit_Off_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x00U},
	{&SEG34,	0x01U,		0x00U}
};



/*
    Gear Digit Text 
*/
const SignalsValue_St_t		GearDigitText_SignalsValue_aSt[GEAR_DIGIT_SIG_LEN] =
{
	{ZERO_E, TWO_E,GearDigitText_SigConf_ast},
};

const SignalConfig_St_t		GearDigitText_SigConf_ast[TWO_E] = 
{
	{OFF_E,	TWO_SEG_E,	GearDigit_G_OffSegConf_aSt},
	{ON_E,	TWO_SEG_E,	GearDigit_G_SegConf_aSt},
};

 const SegConfig_St_t	GearDigit_G_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x0FU},
	{&SEG34,	0x01U,		0x02U}
};

 const SegConfig_St_t	GearDigit_G_OffSegConf_aSt[TWO_SEG_E] = 
{
	{&SEG35,	0x00U,		0x00U},
	{&SEG34,	0x01U,		0x00U}
};
/********************************************************EOF***********************************************************/
