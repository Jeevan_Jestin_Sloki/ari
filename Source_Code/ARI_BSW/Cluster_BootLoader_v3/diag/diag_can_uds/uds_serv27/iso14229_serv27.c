/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
#include "iso14229_serv27.h"
#include "main.h"
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */

//static uint32_t timer_delay_for_security_u32 = 0;
static uint8_t serv27_SA_handler = SA_NO_ACCESS_E;
uint8_t Total_number_of_attempts = 3;
BOOL ISO14229_SA_L1_Gained_b = FALSE;
static BOOL req_delay_not_expire_b = FALSE;
SPLIT_RAND_Un_t SPLIT_RAND_Un;
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */

/*
 *  FUNCTION NAME : GenerateKeyAndVerify
 *  FILENAME      : iso14229_serv27.c
 *  @param        : databuff_pu8 which is pointing to the key value buffer
 *  @brief        : This function is verify that received key is matched or not with calculated key     
 *  @return       : BOOL  
 */

BOOL GenerateKeyAndVerify(uint8_t * databuff_pu8)
{
    uint32_t i;
    SPLIT_RAND_Un_t key_data_Un;     
    key_data_Un.random_number = ~SPLIT_RAND_Un.random_number;
    //key_data_Un.random_number = 0x22222222;
    for(i=0 ;i<4;i++)
    {
        if(databuff_pu8[i+1]!=key_data_Un.byte_buff[i])
        {
            return FALSE;
        }
    }
    return TRUE;
}

/*
 *  FUNCTION NAME : Generate_Seed_Frame
 *  FILENAME      : iso14229_serv27.c
 *  @param        : databuff_pu8-> which is storing to the data into this buffer 
 *  @brief        : This function is made for the seed frame       
 *  @return       : void    
 */

void Generate_Seed_Frame(uint8_t * databuff_pu8)
{
    uint32_t random_input_u32 = TS_time_ms_u32;
    uint32_t i = 0;   
   
    do
    {
        srand(random_input_u32++); 
        SPLIT_RAND_Un.random_number = (uint32_t)rand(); // generating the random number
    }while((SPLIT_RAND_Un.random_number == 0) || (SPLIT_RAND_Un.random_number == 0xFFFFFFFF));
    
    for(i = 0; i < 4; i++)
    {
       databuff_pu8[i] = SPLIT_RAND_Un.byte_buff[i];  
    }
    return;     
}


/*
 *  FUNCTION NAME : Generate_NullSeed
 *  FILENAME      : iso14229_serv27.c
 *  @param        : databuff_pu8 -> storing into the null seed 
 *  @brief        : This function is made for the seed frame       
 *  @return       : void    
 */

void Generate_NullSeed(uint8_t * databuff_pu8)
{
    uint32_t i = 0;   
    for(i = 0; i < 4; i++)
    {
       databuff_pu8[i]= 0;
    }
    return;     
}
/* ************************************************************************** */


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */


/**
*  FUNCTION NAME : iso14229_serv27
*  FILENAME      : iso14229_serv27.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_27 requests
*  @return       : Type of response.                  
*/
UDS_Serv_resptype_En_t iso14229_serv27(UDS_Serv_St_t*  UDS_Serv_pSt) {
    uint16_t num_of_bytes_req_u16 = UDS_Serv_pSt->RxLen_u16;
    static uint8_t Number_of_Attempts_u8 = 0;
    uint8_t sa_sub_fun_u8 = UDS_Serv_pSt->RxBuff_pu8[1];
    UDS_Serv_resptype_En_t serv_27_resp_En;

    switch(sa_sub_fun_u8) {
        case SEED_REQ_E : 
        {
            if(num_of_bytes_req_u16 != SA_SEED_REQ_LEN) {
            	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
            	UDS_Serv_pSt->TxBuff_pu8[1] = SID_SA;
            	UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
            	UDS_Serv_pSt->TxLen_u16 = 3;
                 serv_27_resp_En = UDS_SERV_RESP_NEG_E;
            }
            else if (ISO14229_SA_L1_Gained_b) {            
            	UDS_Serv_pSt->TxBuff_pu8[1] = SEED_REQ_E;
                Generate_NullSeed(&UDS_Serv_pSt->TxBuff_pu8[2]);
                UDS_Serv_pSt->TxLen_u16 = 0x05;
                serv_27_resp_En = UDS_SERV_RESP_POS_E;
                serv27_SA_handler = SA_ACCESS_OK_E;
            }
            else {
            	UDS_Serv_pSt->TxBuff_pu8[1] = SEED_REQ_E;
                Generate_Seed_Frame(&UDS_Serv_pSt->TxBuff_pu8[2]);
                UDS_Serv_pSt->TxLen_u16 = 0x05;
                Number_of_Attempts_u8 = 0;
                serv_27_resp_En = UDS_SERV_RESP_POS_E;
                serv27_SA_handler = SA_RECEIVE_KEY_E;
            }
            break;
        }
        case KEY_SEND_E :
        {
            if(num_of_bytes_req_u16 != SA_KEY_REQ_LEN) {
            	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
            	UDS_Serv_pSt->TxBuff_pu8[1] = SID_SA;
            	UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
            	UDS_Serv_pSt->TxLen_u16 = 3;
                serv_27_resp_En = UDS_SERV_RESP_NEG_E;
                 Number_of_Attempts_u8++;
            }
            else if(SA_ACCESS_OK_E == serv27_SA_handler) 
			{
            	UDS_Serv_pSt->TxBuff_pu8[1] = KEY_SEND_E;
            	UDS_Serv_pSt->TxLen_u16 = 0x01;
                serv_27_resp_En = UDS_SERV_RESP_POS_E;
                Number_of_Attempts_u8 = 0;
                serv27_SA_handler = SA_ACCESS_OK_E;						
			}
            else if(SA_RECEIVE_KEY_E != serv27_SA_handler) {
            	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
            	UDS_Serv_pSt->TxBuff_pu8[1] = SID_SA;
            	UDS_Serv_pSt->TxBuff_pu8[2] = REQUEST_SEQUENCE_ERR;
            	UDS_Serv_pSt->TxLen_u16 = 3;
            	serv_27_resp_En = UDS_SERV_RESP_NEG_E;
            }
            else if(Total_number_of_attempts == Number_of_Attempts_u8) {
            	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
            	UDS_Serv_pSt->TxBuff_pu8[1] = SID_SA;
            	UDS_Serv_pSt->TxBuff_pu8[2] = EXCEEDED_NUMBER_OF_ATTEMPTS;
            	UDS_Serv_pSt->TxLen_u16 = 3;
            	serv_27_resp_En = UDS_SERV_RESP_NEG_E;
                //timer_delay_for_security_u32 = TS_time_ms_u32;
                req_delay_not_expire_b = TRUE;
                Number_of_Attempts_u8 = 0;
                serv27_SA_handler = SA_NO_ACCESS_E;
                
            }
            else {
                if(TRUE == GenerateKeyAndVerify(&UDS_Serv_pSt->RxBuff_pu8[1]))
                {
                	UDS_Serv_pSt->TxBuff_pu8[1] = KEY_SEND_E;
                	UDS_Serv_pSt->TxLen_u16 = 0x01;
                    Number_of_Attempts_u8 = 0;
                    serv_27_resp_En = UDS_SERV_RESP_POS_E;
                    serv27_SA_handler = SA_ACCESS_OK_E;	
                    ISO14229_SA_L1_Gained_b = TRUE;
                }
                else
                {   
                	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
                	UDS_Serv_pSt->TxBuff_pu8[1] = SID_SA;
                	UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_KEY;
                	UDS_Serv_pSt->TxLen_u16 = 3;
                    serv_27_resp_En = UDS_SERV_RESP_NEG_E;
                    Number_of_Attempts_u8++;
                }
            }
            break;
        }
        default :
        {
        	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
        	UDS_Serv_pSt->TxBuff_pu8[1] = SID_SA;
        	UDS_Serv_pSt->TxBuff_pu8[2] = SUB_FUNC_NOT_SUPPORTED;
        	UDS_Serv_pSt->TxLen_u16 = 3;
            serv_27_resp_En = UDS_SERV_RESP_NEG_E;
        }
    }
    return serv_27_resp_En;
}

 /**
*  FUNCTION NAME : iso14229_serv27_timeout
*  FILENAME      : iso14229_serv27.c
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.                     
*/
void iso14229_serv27_timeout(void)
{
    if(TRUE == req_delay_not_expire_b){ 
        //if((sioconn_config_data_Un.sioconn_config_data_St.req_timedelay_sec_u8 * 1000) <= (timer_ticks_1ms_u32 - timer_delay_for_security_u32 )){ 
            serv27_SA_handler = SA_NO_ACCESS_E;
            ISO14229_SA_L1_Gained_b = FALSE;
            req_delay_not_expire_b = FALSE;
        //}
    }    
    return;
}
/* *****************************************************************************
 End of File
 */

