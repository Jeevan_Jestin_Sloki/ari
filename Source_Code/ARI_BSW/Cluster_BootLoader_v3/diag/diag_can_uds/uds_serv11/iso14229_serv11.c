/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */

/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
#include"iso14229_serv11.h"
//#include "system_S32K144.h"
#include "main.h"
/* ************************************************************************** */
bool reset_b = false;
/**
*  FUNCTION NAME : iso14229_serv11
*  FILENAME      : iso14229_serv11.c
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_11 requests
*  @return       : Type of response.
*/

UDS_Serv_resptype_En_t iso14229_serv11(UDS_Serv_St_t* UDS_Serv_pSt)
{
	UDS_Serv_resptype_En_t serv_11_resp_En;
    uint8_t Reset_type_u8 = 0;
    Reset_type_u8 = UDS_Serv_pSt->RxBuff_pu8[1];
    if(SERVICE_10_MAX_LEN != UDS_Serv_pSt->RxLen_u16)
    {
    	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
    	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ECURESET;
    	UDS_Serv_pSt->TxBuff_pu8[2] = INVALID_MESSAGE_LENGTH;
    	UDS_Serv_pSt->TxLen_u16 = 3;
        serv_11_resp_En = UDS_SERV_RESP_NEG_E;

    }
    else
    {
        switch(Reset_type_u8)
        {
            case HARD_RESET_SUB_ID:
            {
            	//UDS_Serv_pSt->TxLen_u16 = 1;
                serv_11_resp_En = UDS_SERV_RESP_POS_E;
                break;
            }
            case SOFT_REST_SUB_ID:
            {
            	//UDS_Serv_pSt->TxLen_u16 = 1;
                serv_11_resp_En = UDS_SERV_RESP_POS_E;
                reset_b = true;  
                break;
            }
            default:
            {
            	UDS_Serv_pSt->TxBuff_pu8[0] = NEGATIVE_RESP;
            	UDS_Serv_pSt->TxBuff_pu8[1] = SID_ECURESET;
            	UDS_Serv_pSt->TxBuff_pu8[2] = SUB_FUNC_NOT_SUPPORTED;
            	UDS_Serv_pSt->TxLen_u16 = 3;
                serv_11_resp_En = UDS_SERV_RESP_NEG_E;
                break;
            }
        }
    }
    return serv_11_resp_En;
    
}


/* *****************************************************************************
 End of File
 */
