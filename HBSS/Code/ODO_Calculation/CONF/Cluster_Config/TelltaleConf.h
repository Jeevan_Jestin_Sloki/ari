/***********************************************************************************************************************
* File Name    : TelltaleConf.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 28/11/2021
***********************************************************************************************************************/

#ifndef TELLTALE_CONF_H
#define TELLTALE_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
/*
   Left-turn indicator telltale Configuration 
*/
extern  const SignalsValue_St_t		LeftInd_SignalsValue_aSt[LEFT_IND_SIG_LEN];
extern  const SignalConfig_St_t		LeftIndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			LeftInd_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			LeftInd_Off_SegConf_aSt[ONE_SEG_E];


/*
   Right-turn indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			RightInd_SignalsValue_aSt[RIGHT_IND_SIG_LEN];
extern  const SignalConfig_St_t			RightIndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			RightInd_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			RightInd_Off_SegConf_aSt[ONE_SEG_E];

/*
   High-Beam indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			HighBeamInd_SignalsValue_aSt[HIGH_BEAM_SIG_LEN];
extern  const SignalConfig_St_t			HighBeamIndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			HighBeamInd_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			HighBeamInd_Off_SegConf_aSt[ONE_SEG_E];


/*
   BLE indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			BLE_Ind_SignalsValue_aSt[BLE_SIG_LEN];
extern  const SignalConfig_St_t			BLE_IndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			BLE_Ind_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			BLE_Ind_Off_SegConf_aSt[ONE_SEG_E];

/*
   Warning indicator  telltale Configuration 
*/
extern  const SignalsValue_St_t			Warning_SignalsValue_aSt[WARN_IND_SIG_LEN];
extern  const SignalConfig_St_t			Warning_IndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			Warning_Ind_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			Warning_Ind_Off_SegConf_aSt[ONE_SEG_E];


/*
   Service Reminder  telltale Configuration 
*/
extern  const SignalsValue_St_t			ServRem_SignalsValue_aSt[SERV_REM_SIG_LEN];
extern  const SignalConfig_St_t			ServRem_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			ServRem_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			ServRem_Off_SegConf_aSt[ONE_SEG_E];

/*
   Neutral Mode indicator  telltale Configuration 
*/
extern  const SignalsValue_St_t			NeutralMode_SignalsValue_aSt[NEUTRAL_MODE_SIG_LEN];
extern  const SignalConfig_St_t			NeutralMode_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			NeutralMode_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			NeutralMode_Off_SegConf_aSt[ONE_SEG_E];

/*
   Economy Mode indicator  telltale Configuration 
*/
extern  const SignalsValue_St_t			EcoMode_SignalsValue_aSt[ECO_MODE_SIG_LEN];
extern  const SignalConfig_St_t			EcoMode_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			EcoMode_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			EcoMode_Off_SegConf_aSt[ONE_SEG_E];

/*
   Sports Mode indicator  telltale Configuration 
*/
extern  const SignalsValue_St_t			SportsMode_SignalsValue_aSt[SPORTS_MODE_SIG_LEN];
extern  const SignalConfig_St_t			SportsMode_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			SportsMode_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SportsMode_Off_SegConf_aSt[ONE_SEG_E];

/*
   Reverse Mode indicator  telltale Configuration 
*/
extern  const SignalsValue_St_t			ReverseMode_SignalsValue_aSt[REVERSE_MODE_SIG_LEN];
extern  const SignalConfig_St_t			ReverseMode_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			ReverseMode_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			ReverseMode_Off_SegConf_aSt[ONE_SEG_E];

/*
   Side-Stand indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			SideStand_SignalsValue_aSt[SIDE_STAND_SIG_LEN];
extern  const SignalConfig_St_t			SideStand_IndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			SideStand_Ind_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SideStand_Ind_Off_SegConf_aSt[ONE_SEG_E];

/*
   Motor-Fault indicator telltale Configuration 
*/
extern  const SignalsValue_St_t			MotorFlt_SignalsValue_aSt[MOTOR_FLT_SIG_LEN];
extern  const SignalConfig_St_t			MotorFlt_IndSigConf_ast[TWO_E];
extern  const SegConfig_St_t			MotorFlt_Ind_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			MotorFlt_Ind_Off_SegConf_aSt[ONE_SEG_E];

/*
   Kill-Switch telltale Configuration 
*/
extern  const SignalsValue_St_t			KillSwitch_SignalsValue_aSt[KILL_SWITCH_SIG_LEN];
extern  const SignalConfig_St_t			KillSwitch_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			KillSwitch_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			KillSwitch_Off_SegConf_aSt[ONE_SEG_E];

/*
   Engine Fault telltale Configuration 
*/
extern  const SignalsValue_St_t			Engine_fault_SignalsValue_aSt[ENGINE_FLT_SIG_LEN];
extern  const SignalConfig_St_t			Engine_fault_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			Engine_fault_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			Engine_fault_On_SegConf_aSt[ONE_SEG_E];


/*
   Network telltale Configuration 
*/

extern  const SignalsValue_St_t			Network_SignalsValue_aSt[ENGINE_FLT_SIG_LEN];
extern  const SignalConfig_St_t			Network_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			Network_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			Network_On_SegConf_aSt[ONE_SEG_E];

/*
   Common constant segments Configuration 
*/
extern  const SignalsValue_St_t			Common_SignalsValue_aSt[COMMON_SEG_SIG_LEN];
extern  const SignalConfig_St_t			Common_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			Common_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			Common_Off_SegConf_aSt[ONE_SEG_E];

#endif /* TELLTALE_CONF_H */


