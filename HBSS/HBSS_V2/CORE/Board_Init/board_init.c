/***********************************************************************************************************************
* File Name    : board_init.c
* Version      : 01
* Description  : This file contains the functions to initialize the Hardware and Software components required by the 
                 HMI Cluster Software.
* Created By   : Dileepa B S
* Creation Date: 15/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "board_init.h"
#include "r_cg_cgc.h"
#include "r_cg_port.h"
#include "r_cg_timer.h"
#include "can_driver.h"
#include "r_cg_lcd.h"
#include "r_cg_wdt.h"
#include "r_cg_intc.h"
#include "RAM_Vector.h"
#include "com_tasksched.h"
#include "PFlash_User.h"
#include "can_tranceiver.h"
#include "r_cg_serial.h"
#include "LightSensor.h"
#include "BackLightCtrl.h"
#include "board_conf.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/ 
//uint8_t temp[8]= {0};
/***********************************************************************************************************************
* Function Name: Hardware_Init
* Description  : This function initializes the Hardware peripherals.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Hardware_Init(void)
{
	DI();
	R_CGC_Get_ResetSource();  /*Get the reset flag status of the MCU*/
        R_CGC_Create();           /*Initialize the clock generator module*/
        R_PORT_Create();          /*Initialize the GPIO module*/
	R_SAU1_Create();	  /*Initialize the serial I2C module*/
        R_TAU0_Create();          /*Initialize the TAU0 timer module*/
	R_TAU2_Create();	  /*Initialize the TAU2 timer module*/
	R_WDT_Create();           /*Initialize the Watch Dog Timer Module - 0*/
	R_LCD_Create();           /*Initilaize the LCD Driver module*/
	R_INTC_Create();	  /*Initialize the INTC Driver module*/
	/* ----------------------------------------------*/
	/*
		CAN Driver Initialization
	*/
	
	PFlash_Init();
	Release_STBY_Mode();
	//CAN_Baud_Sel_En = (CAN_Baud_Sel_En_t)RestoreBaudrateFromFlash();
	CAN_Init();
	
	/* ----------------------------------------------*/
	
        IAWCTL = 0x00U;
        GUARD = 0x00U;	
	
	//Tx_MsgBuf_Processing(1,8, temp);
	return;
}


/***********************************************************************************************************************
* Function Name: Software_Init
* Description  : This function initailizes the Software components and starts the hardware peripherals.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Software_Init(void)
{
	EI();	/*Enable All Interrupts*/
	
	/*Assign the address of the ISR functions to the function-pointers*/
	RAM_INTWDTI_ISR 	= &r_wdt_interrupt;  
	
	#if(HMI_CAN_ONLY == TRUE)
	RAM_INTTM00_ISR 	= &r_tau0_channel0_interrupt;
	RAM_INTTM01_ISR 	= &r_tau0_channel1_interrupt;
	#endif
	RAM_INTTM02_ISR 	= &r_tau0_channel2_interrupt;
	RAM_INTTM20_ISR		= &r_tau2_channel0_interrupt;	
	RAM_INTTM24_ISR		= &r_tau2_channel4_interrupt; 
	RAM_INTC0REC_ISR 	= &Msgbuf_Receive;				
	RAM_INTC0ERR_ISR 	= &Error_Processing;			
	RAM_INTC0WUP_ISR 	= &ISR_CAN_Wakeup;	
	RAM_INTIIC11_ISR	= &r_iic11_interrupt;			
	RAM_INTP5_ISR		= &r_intc5_interrupt; 			
	
	R_TAU0_Channel2_Start();	/*Start the 5ms Interval-Timer*/
	
	R_INTC5_Start();	/*Start the extern interrupt - 5*/ 

	Diag_TS_Init();	/*Initialize the CIL-CAN Layer*/
	
	if(BackLightCtrl_En == BACKLIGHT_CTRL_ALS_E)
	{
		Reset_ALS();	/*Reset the ALS sensor before initialization*/
		ALS_Init(); 	/*Initialize the ALS sensor command and control registers over I2C*/
	}
	
	#if(STORE_RESTORE_SW_VERSION == TRUE)
	StoreSWVersion();	/*Store Software Version Number into the Data-Flash*/
	#endif
	
	return;
}

/********************************************************EOF***********************************************************/
