/***********************************************************************************************************************
* File Name    : SOC_Conf.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 05/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "SOC_Conf.h"

/***********************************************************************************************************************
Global variables
***********************************************************************************************************************/

/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/
/*
			Charging status Telltale
*/ 

const SignalsValue_St_t 		CH_status_SignalsValue_aSt[CH_STATUS_SIG_LEN]=
{
	{ZERO_E,	TWO_E,		CH_status_SigConf_ast},
};

const SignalConfig_St_t		CH_status_SigConf_ast[TWO_E]=
{
	{ON_E,		ONE_SEG_E,	CH_status_on_SegConf_ast  },
	{OFF_E,		ONE_SEG_E,	CH_status__off_Segconf_ast},
};

const SegConfig_St_t			CH_status_on_SegConf_ast[ONE_SEG_E]=
{
	{&SEG42,	0x0E,		0x01},
};
const SegConfig_St_t			CH_status__off_Segconf_ast[ONE_SEG_E]=
{
	{&SEG42,	0x0E,		0x00},
};



/*
 			Battery SOC bar
*/
const SignalsValue_St_t		SOC_SignalsValue_aSt[BATT_SOC_BAR_SIG_LEN] = 
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	{ ZERO_E,	THREE_E,	SOC_B1_SigConf_ast },
	{ ONE_E,	TWO_E,		SOC_B2_SigConf_ast },
	{ TWO_E,	TWO_E,		SOC_B3_SigConf_ast },
	{ THREE_E,	TWO_E,		SOC_B4_SigConf_ast },
	{ FOUR_E,	TWO_E,		SOC_B5_SigConf_ast },
};

 const SignalConfig_St_t		SOC_B1_SigConf_ast[THREE_E] = 
{
/*PositionSignalValue  SegmentsRequired  SegmentsConfiguration*/
	{ ON_E,		ONE_SEG_E,	SOC_B1_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B1_Off_SegConf_aSt},
	{ OFF_E,	ONE_SEG_E,	SOC_B1_Off_SegConf_aSt}
};

 const SignalConfig_St_t		SOC_B2_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B2_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B2_Off_SegConf_aSt}
};


 const SignalConfig_St_t		SOC_B3_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B3_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B3_Off_SegConf_aSt}
};


 const SignalConfig_St_t		SOC_B4_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B4_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B4_Off_SegConf_aSt}
};


 const SignalConfig_St_t		SOC_B5_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	SOC_B5_On_SegConf_aSt },
	{ OFF_BAR,	ONE_SEG_E,	SOC_B5_Off_SegConf_aSt}
};



 const SegConfig_St_t	SOC_B1_On_SegConf_aSt[ONE_SEG_E] = 
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	{&SEG34,		0x0EU,		0x01U}
};

 const SegConfig_St_t	SOC_B2_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG36,		0x0EU,		0x01U}
};

 const SegConfig_St_t	SOC_B3_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG36,		0x0DU,		0x02U}
};

 const SegConfig_St_t	SOC_B4_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG36,		0x0BU,		0x04U}
};

 const SegConfig_St_t	SOC_B5_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG36,		0x07U,		0x08U}
};


 const SegConfig_St_t	SOC_B1_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG34,		0x0EU,		0x00U}
};

 const SegConfig_St_t	SOC_B2_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG36,		0x0EU,		0x00U}
};

 const SegConfig_St_t	SOC_B3_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG36,		0x0DU,		0x00U}
};

 const SegConfig_St_t	SOC_B4_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG36,		0x0BU,		0x00U}
};

 const SegConfig_St_t	SOC_B5_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG36,		0x07U,		0x00U}
};






/*
		Battery Percentage Configration
*/


 const SignalsValue_St_t		SOC_percen_SignalsValue_aSt[BATT_SOC_DIGIT_SIG_LEN] =
{
	{ZERO_E, 	ELEVEN_E,	SOC_Digit1_SigConf_ast},
	{ONE_E,		ELEVEN_E,	SOC_Digit2_SigConf_ast},
	{TWO_E,		TWO_E,		SOC_Digit3_SigConf_ast},
};

 const SignalConfig_St_t	 		SOC_Digit1_SigConf_ast[ELEVEN_E]=
{
	{ ZERO_E,	TWO_SEG_E,	SOC_Digit1_0_SegConf_aSt	},
	{ ONE_E,	TWO_SEG_E,	SOC_Digit1_1_SegConf_aSt	},
	{ TWO_E,	TWO_SEG_E,	SOC_Digit1_2_SegConf_aSt	},
	{ THREE_E,	TWO_SEG_E,	SOC_Digit1_3_SegConf_aSt	},
	{ FOUR_E,	TWO_SEG_E,	SOC_Digit1_4_SegConf_aSt	},
	{ FIVE_E,	TWO_SEG_E,	SOC_Digit1_5_SegConf_aSt	},
	{ SIX_E,	TWO_SEG_E,	SOC_Digit1_6_SegConf_aSt	},
	{ SEVEN_E,	TWO_SEG_E,	SOC_Digit1_7_SegConf_aSt	},
	{ EIGHT_E,	TWO_SEG_E,	SOC_Digit1_8_SegConf_aSt	},
	{ NINE_E,	TWO_SEG_E,	SOC_Digit1_9_SegConf_aSt	},
	{ OFF_DIGIT,	TWO_SEG_E,	SOC_Digit1_off_SegConf_aSt},
};

 const SignalConfig_St_t	 		SOC_Digit2_SigConf_ast[ELEVEN_E]=
{
	{ ZERO_E,	TWO_SEG_E,	SOC_Digit2_0_SegConf_aSt	},
	{ ONE_E,	TWO_SEG_E,	SOC_Digit2_1_SegConf_aSt	},
	{ TWO_E,	TWO_SEG_E,	SOC_Digit2_2_SegConf_aSt	},
	{ THREE_E,	TWO_SEG_E,	SOC_Digit2_3_SegConf_aSt	},
	{ FOUR_E,	TWO_SEG_E,	SOC_Digit2_4_SegConf_aSt	},
	{ FIVE_E,	TWO_SEG_E,	SOC_Digit2_5_SegConf_aSt	},
	{ SIX_E,	TWO_SEG_E,	SOC_Digit2_6_SegConf_aSt	},
	{ SEVEN_E,	TWO_SEG_E,	SOC_Digit2_7_SegConf_aSt	},
	{ EIGHT_E,	TWO_SEG_E,	SOC_Digit2_8_SegConf_aSt	},
	{ NINE_E,	TWO_SEG_E,	SOC_Digit2_9_SegConf_aSt	},
	{ OFF_DIGIT,	TWO_SEG_E,	SOC_Digit2_off_SegConf_aSt},
};

/*
		Battery percaentage DIGIT 1

*/

 const SegConfig_St_t 			SOC_Digit1_0_SegConf_aSt[TWO_E]=
{
	{&SEG40,		0x01U,		0x0AU},
	{&SEG39,		0x00U,		0x0FU},
};

 const SegConfig_St_t 			SOC_Digit1_1_SegConf_aSt[TWO_E]=
{
	{&SEG40,		0x01U,		0x0AU},
	{&SEG39,		0x00U,		0x00U},
};

 const SegConfig_St_t 			SOC_Digit1_2_SegConf_aSt[TWO_E]=
{
	{&SEG40,		0x01U,		0x06U},
	{&SEG39,		0x00U,		0x0DU},
};

 const SegConfig_St_t 			SOC_Digit1_3_SegConf_aSt[TWO_E]=
{
	{&SEG40,		0x01U,		0x0EU},
	{&SEG39,		0x00U,		0x09U},
};

 const SegConfig_St_t 			SOC_Digit1_4_SegConf_aSt[TWO_E]=
{
	{&SEG40,		0x01U,		0x0EU},
	{&SEG39,		0x00U,		0x02U},
};

 const SegConfig_St_t 			SOC_Digit1_5_SegConf_aSt[TWO_E]=
{
	{&SEG40,		0x01U,		0x0CU},
	{&SEG39,		0x00U,		0x0BU},
};

 const SegConfig_St_t 			SOC_Digit1_6_SegConf_aSt[TWO_E]=
{
	{&SEG40,		0x01U,		0x0CU},
	{&SEG39,		0x00U,		0x0FU},
};

 const SegConfig_St_t 			SOC_Digit1_7_SegConf_aSt[TWO_E]=
{
	{&SEG40,		0x01U,		0x0AU},
	{&SEG39,		0x00U,		0x01U},
};

 const SegConfig_St_t 			SOC_Digit1_8_SegConf_aSt[TWO_E]=
{
	{&SEG40,		0x01U,		0x0EU},
	{&SEG39,		0x00U,		0x0FU},
};


 const SegConfig_St_t 			SOC_Digit1_9_SegConf_aSt[TWO_E]=
{
	{&SEG40,		0x01U,		0x0EU},
	{&SEG39,		0x00U,		0x0BU},
};
 const SegConfig_St_t 			SOC_Digit1_off_SegConf_aSt[TWO_E]=
{
	{&SEG40,		0x01U,		0x00U},
	{&SEG39,		0x00U,		0x00U},
};

/*
		Battery percaentage DIGIT 2

*/



 const SegConfig_St_t 			SOC_Digit2_0_SegConf_aSt[TWO_E]=
{
	{&SEG38,		0x01U,		0x0AU},
	{&SEG37,		0x00U,		0x0FU},
};

 const SegConfig_St_t 			SOC_Digit2_1_SegConf_aSt[TWO_E]=
{
	{&SEG38,		0x01U,		0x0AU},
	{&SEG37,		0x00U,		0x00U},
};

 const SegConfig_St_t 			SOC_Digit2_2_SegConf_aSt[TWO_E]=
{
	{&SEG38,		0x01U,		0x06U},
	{&SEG37,		0x00U,		0x0DU},
};

 const SegConfig_St_t 			SOC_Digit2_3_SegConf_aSt[TWO_E]=
{
	{&SEG38,		0x01U,		0x0EU},
	{&SEG37,		0x00U,		0x09U},
};

 const SegConfig_St_t 			SOC_Digit2_4_SegConf_aSt[TWO_E]=
{
	{&SEG38,		0x01U,		0x0EU},
	{&SEG37,		0x00U,		0x02U},
};

 const SegConfig_St_t 			SOC_Digit2_5_SegConf_aSt[TWO_E]=
{
	{&SEG38,		0x01U,		0x0CU},
	{&SEG37,		0x00U,		0x0BU},
};

 const SegConfig_St_t 			SOC_Digit2_6_SegConf_aSt[TWO_E]=
{
	{&SEG38,		0x01U,		0x0CU},
	{&SEG37,		0x00U,		0x0FU},
};

 const SegConfig_St_t 			SOC_Digit2_7_SegConf_aSt[TWO_E]=
{
	{&SEG38,		0x01U,		0x0AU},
	{&SEG37,		0x00U,		0x01U},
};

 const SegConfig_St_t 			SOC_Digit2_8_SegConf_aSt[TWO_E]=
{
	{&SEG38,		0x01U,		0x0EU},
	{&SEG37,		0x00U,		0x0FU},
};


 const SegConfig_St_t 			SOC_Digit2_9_SegConf_aSt[TWO_E]=
{
	{&SEG38,		0x01U,		0x0EU},
	{&SEG37,		0x00U,		0x0BU},
};
 const SegConfig_St_t 			SOC_Digit2_off_SegConf_aSt[TWO_E]=
{
	{&SEG38,		0x01U,		0x00U},
	{&SEG37,		0x00U,		0x00U},
};

/*
		Battery Percantage Digit 3
*/

 const SignalConfig_St_t	 		SOC_Digit3_SigConf_ast[TWO_E]=
{
	{ON_E,		ONE_SEG_E,	SOC_Digit3_on_SegConf_ast},
	{OFF_DIGIT,	ONE_SEG_E,	SOC_Digit3_off_SegConf_ast},
	
};

 const SegConfig_St_t			SOC_Digit3_on_SegConf_ast[ONE_E]=
{
	&SEG38,		0x0EU,		0x01		
};

 const SegConfig_St_t			SOC_Digit3_off_SegConf_ast[ONE_E]=
{
	&SEG38,		0x0EU,		0x00		
};



/********************************************************EOF***********************************************************/