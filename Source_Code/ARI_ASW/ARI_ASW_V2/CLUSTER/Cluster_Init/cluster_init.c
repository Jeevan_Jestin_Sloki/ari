/***********************************************************************************************************************
* File Name    : cluster_init.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 02/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "cluster_init.h"
#include "DataBank.h"
#include "r_cg_lcd.h"
#include "BackLightCtrl.h"
#include "can_driver.h"
#include "r_cg_timer.h"
#include "TelltaleDisp.h"
#include "task_scheduler.h"
#include "can_tranceiver.h"
#include "Cluster_Reset.h" 
#include "Communicator.h"
#include "board_conf.h"
#include "timer_user.h"
#include "ODOmeter_Calc.h"
#include "Speed_Calc.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool 		Cluster_Init_Success_b 	= false;
bool 		Cluster_Init_Flag_b 	= false;

/***********************************************************************************************************************
* Function Name: Cluster_Init
* Description  : This function Init/Deinit the cluster as per the Ignition_state
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Cluster_Init(void)
{
	if(CLUSTER_INIT == ClusterSignals_St.IgnitionSig_u8)
	{
		Cluster_Display_Init();
	}
	else if(CLUSTER_DE_INIT == ClusterSignals_St.IgnitionSig_u8)
	{
		Cluster_Display_Deinit();
	}
	else
	{
		;
	}
        return;
}


/***********************************************************************************************************************
* Function Name: Cluster_Display_Init
* Description  : This function Initialzes the cluster display on ignition-ON.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Cluster_Display_Init(void)
{
	if(false == Cluster_Init_Flag_b)
	{
		#if(STORE_RESTORE_ODO == TRUE)
		SET_PRESENT_ODOMETER(RestoreODOmtereFromFlash());	/*Restore Present ODOmeter from Data Flash*/
		Set_ODOmeter(GET_PRESENT_ODOMETER());
		Set_ODOmeter_in_mm(GET_PRESENT_ODO_MM());
		#endif
		#if(STORE_RESTORE_WHKM == TRUE)
		SET_PRESENT_WHKM(RestoreWhKmFromFlash());	/*Restore Last Session WH/KM from Data Flash*/
		#endif
		#if(STORE_RESTORE_SOC == TRUE)
		SET_PRESENT_SOC(RestoreSOCFromFlash());	/*Restore Present SOC from Data Flash*/
		#endif
		
		DownloadDataFromFlash();
		
		#if(HMI_CAN_ONLY == TRUE)
		R_TAU0_Channel0_Start();	//Start PWM-1 timer	
		#endif
		R_TAU2_Channel0_Start();	//Start PWM-2 timer	
		
		#if((STBY_MODE_ENABLE == TRUE) && (HMI_CAN_WITH_IO == TRUE))
			Release_STBY_Mode();	/*Release CAN Tranceiver device from Stand-By-Mode [Power-Save Mode]*/
		#endif
		
		#if((ENABLE_CAN_SLEEP_MODE == TRUE) && (HMI_CAN_WITH_IO == TRUE))
			CAN_SleepMode_Release();	/*Releases the CAN Module from the Sleep mode*/ 
		#endif
		
		BackLightCtrl_En = BACKLIGHT_CTRL_STATIC_E; /*On-Ignition Turn-On Back-Light with Static Mode*/
		Turn_ON_BackLight();	/*Turn-On BackLight*/		
		
		LCD_PowerSaveRelease();	/*Release the LCD Driver from the power save mode*/
		
		R_LCD_VoltageOn();	/*Voltage select for Common Pins*/
		
		R_LCD_Start();	/*Voltage select for Segment Pins*/

		Get_Distance_for_1_Pulse();
		
		SetPulseCount();
		
		R_TAU0_Channel1_Start(); /* external event counter start*/

		

		Cluster_State_En 	= DISPLAY_ANIMATION_E;	/*Set the cluster display state to Animation*/
		Cluster_Init_Flag_b 	= true;
		Cluster_Init_Success_b 	= true;
	}
	return;
}


/***********************************************************************************************************************
* Function Name: Cluster_Display_Deinit
* Description  : This function De-Initialzes the cluster display on ignition-OFF.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Cluster_Display_Deinit(void)
{
	if(true == Cluster_Init_Flag_b)
	{
		
		Cluster_State_En = DISPLAY_HOLD_E;	/*Set the Cluster Display state to Hold*/
	
		#if(STORE_RESTORE_ODO == TRUE)
		StoreODOmeterToFlash(GET_PRESENT_ODOMETER(), GET_PRESENT_ODO_MM());	/*Store Present ODOmeter to Data Flash*/
		#endif
		#if(STORE_RESTORE_WHKM == TRUE)
		StoreWhKmToFlash(GET_PRESENT_WHKM());	/*Store Last Session WH/KM to Data Flash*/
		#endif
		#if(STORE_RESTORE_SOC == TRUE)
		StoreSOCToFlash(GET_PRESENT_SOC());	/*Store Present SOC to Data Flash*/
		#endif
		
		UploadDataToFlash();
		
		R_TAU0_Channel1_Stop();	/* Stop the speed pulse counter*/

		Reset_Cluster();	/*Reset the States and Data*/
		
		Turn_OFF_BackLight();	/*Turn-ON BackLight 1 and 2*/
		
		
		Cluster_Init_Flag_b 	= false;
		Cluster_Init_Success_b 	= false;
		
		PowerSaveMode();	/*Power Save Mode*/
	}
	return;
}


/***********************************************************************************************************************
* Function Name: PowerSaveMode
* Description  : This function enables the CAN module Sleep mode and puts the CPU in the power save mode.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void PowerSaveMode(void)
{
	R_LCD_Stop();                 /*Voltage deselect for Segment Pins*/
	
	R_LCD_VoltageOff();           /*Voltage deselect for Common Pins*/

	#if(HMI_CAN_ONLY == TRUE)
	R_TAU0_Channel0_Stop();	//Stop PWM-1 timer	
	#endif
	R_TAU2_Channel0_Stop();	//Stop PWM-2 timer
	
	#if(ENABLE_CAN_SLEEP_MODE == TRUE)
		CAN_SleepMode_Setting();      /*CAN Driver Sleep mode setting*/
	#endif
	
	LCD_PowerSave();              /*LCD Driver Power save mode setting*/
	
	#if(STBY_MODE_ENABLE == TRUE)
		Set_STBY_Mode();	      /*Set CAN Tranceiver device to Stand-By-Mode [Power-Save Mode]*/
	#endif
	
	/*
		NOTE : STOP() instruction should be call;
			1) After setting all the peripherals [Of your choice] to power-save mode or
			   Sleep-Mode.
			2) After perfoming the required de-initializations.
	*/
	
	#if(STOP_MCU == TRUE)
		STOP();	
	#endif
	
	/*STOP instruction execution sets the STOP mode (Ultra-Low Power Consumption mode). 
	In the STOP mode, the High-Speed OCO is stop, stopping the whole system, there by considerably reducing the CPU 
	operating current.
	Note : STOP mode is cleared by an interrupt request*/				
	
	return;
}




/********************************************************EOF***********************************************************/