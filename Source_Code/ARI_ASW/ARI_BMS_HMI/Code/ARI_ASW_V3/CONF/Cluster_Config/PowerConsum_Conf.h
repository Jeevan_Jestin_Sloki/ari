/***********************************************************************************************************************
* File Name    : PowerConsum_Conf.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 03/12/2021
***********************************************************************************************************************/

#ifndef PWR_CONSUM_CONF_H
#define PWR_CONSUM_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
/*
		Power indicator configration
*/

extern  const SignalsValue_St_t		Power_ind_SiganlValue_ast[POWER_IND_SIG_LEN];
extern  const SignalConfig_St_t		Power_ind_SigConf_ast[TWO_E];
extern  const SegConfig_St_t		Power_ind_on_SegConf_ast[ONE_SEG_E];
extern  const SegConfig_St_t		Power_ind_off_SegConf_ast[ONE_SEG_E];

/*
		Power Bar_indicator configration
*/


extern  const SignalsValue_St_t			PwrConsum_SignalsValue_aSt[POWER_CONSUMP_SIG_LEN];

extern  const SignalConfig_St_t			PwrConsum_P6_SigConf_ast[THREE_E];
extern  const SignalConfig_St_t			PwrConsum_P7_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t			PwrConsum_P8_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t			PwrConsum_P9_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t			PwrConsum_P10_SigConf_ast[TWO_E];

extern  const SegConfig_St_t			PwrConsum_P6_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			PwrConsum_P7_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			PwrConsum_P8_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			PwrConsum_P9_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			PwrConsum_P10_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			PwrConsum_P6_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			PwrConsum_P7_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			PwrConsum_P8_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			PwrConsum_P9_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			PwrConsum_P10_Off_SegConf_aSt[ONE_SEG_E];

#endif /* PWR_CONSUM_CONF_H */


