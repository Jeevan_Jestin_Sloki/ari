/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _ISO14229_SERV27_H    /* Guard against multiple inclusion */
#define _ISO14229_SERV27_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
#include "uds_conf.h"
/* ************************************************************************** */

/* Section: Constants                                                   */
/* ************************************************************************** */
#define SA_SEED_REQ_LEN                 0x02
#define SA_KEY_REQ_LEN                  0x06

/* ************************************************************************** */

/* Section: Enumaration                                                 */
/* ************************************************************************** */
typedef enum {
    SEED_REQ_E = 1,
    KEY_SEND_E
}SA_SUB_FUN_En_t;

typedef enum
{
        SA_NO_ACCESS_E=0,               
                SA_RECEIVE_KEY_E,
                SA_ACCESS_OK_E                
}SA_STATE_MACHINE_En_t;

typedef union
{
    uint32_t random_number;
    uint8_t byte_buff[4];
}SPLIT_RAND_Un_t;

/* ************************************************************************** */
extern BOOL ISO14229_SA_L1_Gained_b;
extern uint8_t Total_number_of_attempts;
/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


/**
*  FUNCTION NAME : iso14229_serv27
*  FILENAME      : iso14229_serv27.h
*  @param        : UDS_Serv_St_t* UDS_Serv_pSt - pointer to service distributer table.
*  @brief        : This function will process the service_27 requests
*  @return       : Type of response.                  
*/

UDS_Serv_resptype_En_t iso14229_serv27(UDS_Serv_St_t*  UDS_Serv_pSt);

/**
*  FUNCTION NAME : iso14229_serv27_timeout
*  FILENAME      : iso14229_serv27.h
*  @param        : none.
*  @brief        : function resets the relevant variables/parameters when session timesout.
*  @return       : none.
*/
void iso14229_serv27_timeout(void);

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
