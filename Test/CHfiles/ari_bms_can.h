﻿#include <stdint.h>




#define BMS_MSG_1_ST_T_BATTERYVOLTAGE_U16_MASK0  16U
#define BMS_MSG_1_ST_T_BATTERYVOLTAGE_U16_MASK1  8U
#define BMS_MSG_1_ST_T_BATTERYCURRENT_U16_MASK0  8U
#define BMS_MSG_1_ST_T_BATTERYTOTALCAPACITY_MASK0  8U


#define SIGNLE_READ_Mask0     0U    
#define SIGNLE_READ_Mask1     0x01U 
#define SIGNLE_READ_Mask2     0x03U 
#define SIGNLE_READ_Mask3     0x07U 
#define SIGNLE_READ_Mask4     0x0FU 
#define SIGNLE_READ_Mask5     0x1FU 
#define SIGNLE_READ_Mask6     0x3FU 
#define SIGNLE_READ_Mask7     0x7FU 
#define SIGNLE_READ_Mask8     0xFFU 


#define SIGNLE_WRITE_Mask0    0x80U 
#define SIGNLE_WRITE_Mask1    0xC0U 
#define SIGNLE_WRITE_Mask2    0xE0U 
#define SIGNLE_WRITE_Mask3    0xF0U 
#define SIGNLE_WRITE_Mask4    0xF8U 
#define SIGNLE_WRITE_Mask5    0xFCU 
#define SIGNLE_WRITE_Mask6    0xFEU 
#define SIGNLE_WRITE_Mask7    0xFFU 
/* def @BMS_MSG_3 CAN Message                                   (515) */
#define BMS_MSG_3_ID                                            (515U)
#define BMS_MSG_3_IDE                                           (0U)
#define BMS_MSG_3_DLC                                           (8U)


#define BMS_MSG_3_BATTERYPACKSTATE_U8FACTOR                                   (1)
#define BMS_MSG_3_CANID_BATTERYPACKSTATE_U8_STARTBIT                           (1)
#define BMS_MSG_3_CANID_BATTERYPACKSTATE_U8_OFFSET                             (0)
#define BMS_MSG_3_CANID_BATTERYPACKSTATE_U8_MIN                                (0)
#define BMS_MSG_3_CANID_BATTERYPACKSTATE_U8_MAX                                (3)


typedef struct
{
  uint8_t BatteryPackState_u8;
}
BMS_MSG_3_t;


/* def @BMS_MSG_1_ST_T CAN Message                                   (513) */
#define BMS_MSG_1_ST_T_ID                                            (513U)
#define BMS_MSG_1_ST_T_IDE                                           (0U)
#define BMS_MSG_1_ST_T_DLC                                           (8U)


#define BMS_MSG_1_ST_T_BATTERYVOLTAGE_U16FACTOR                                   (0.001)
#define BMS_MSG_1_ST_T_CANID_BATTERYVOLTAGE_U16_STARTBIT                           (7)
#define BMS_MSG_1_ST_T_CANID_BATTERYVOLTAGE_U16_OFFSET                             (0)
#define BMS_MSG_1_ST_T_CANID_BATTERYVOLTAGE_U16_MIN                                (0)
#define BMS_MSG_1_ST_T_CANID_BATTERYVOLTAGE_U16_MAX                                (16777.215)
#define BMS_MSG_1_ST_T_BATTERYCURRENT_U16FACTOR                                   (1)
#define BMS_MSG_1_ST_T_CANID_BATTERYCURRENT_U16_STARTBIT                           (31)
#define BMS_MSG_1_ST_T_CANID_BATTERYCURRENT_U16_OFFSET                             (0)
#define BMS_MSG_1_ST_T_CANID_BATTERYCURRENT_U16_MIN                                (0)
#define BMS_MSG_1_ST_T_CANID_BATTERYCURRENT_U16_MAX                                (65535)
#define BMS_MSG_1_ST_T_BATTERYSOC_U16FACTOR                                   (1)
#define BMS_MSG_1_ST_T_CANID_BATTERYSOC_U16_STARTBIT                           (47)
#define BMS_MSG_1_ST_T_CANID_BATTERYSOC_U16_OFFSET                             (0)
#define BMS_MSG_1_ST_T_CANID_BATTERYSOC_U16_MIN                                (0)
#define BMS_MSG_1_ST_T_CANID_BATTERYSOC_U16_MAX                                (255)
#define BMS_MSG_1_ST_T_BATTERYTOTALCAPACITYFACTOR                                   (1)
#define BMS_MSG_1_ST_T_CANID_BATTERYTOTALCAPACITY_STARTBIT                           (55)
#define BMS_MSG_1_ST_T_CANID_BATTERYTOTALCAPACITY_OFFSET                             (0)
#define BMS_MSG_1_ST_T_CANID_BATTERYTOTALCAPACITY_MIN                                (0)
#define BMS_MSG_1_ST_T_CANID_BATTERYTOTALCAPACITY_MAX                                (65535)


typedef struct
{
  uint32_t BatteryVoltage_u16;
  uint16_t BatteryCurrent_u16;
  uint8_t BatterySOC_u16;
  uint16_t BatteryTotalCapacity;
}
BMS_MSG_1_St_t_t;


 extern uint32_t Deserialize_BMS_MSG_3(BMS_MSG_3_t* message, const uint8_t* data);
 extern uint32_t Serialize_BMS_MSG_3(BMS_MSG_3_t* message, uint8_t* data);
 extern uint32_t Deserialize_BMS_MSG_1_St_t(BMS_MSG_1_St_t_t* message, const uint8_t* data);
 extern uint32_t Serialize_BMS_MSG_1_St_t(BMS_MSG_1_St_t_t* message, uint8_t* data);
