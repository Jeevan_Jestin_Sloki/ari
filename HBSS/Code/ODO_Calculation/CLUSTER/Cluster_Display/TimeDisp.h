/***********************************************************************************************************************
* File Name    : TimeDisp.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 12/12/2021
***********************************************************************************************************************/

#ifndef TIME_DISP_H
#define TIME_DISP_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" /*TODO: Dileepa*/
#include "Cluster_Conf.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define TURN_OFF_MIN		60
#define TURN_OFF_HOUR		0x0F

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	MINUTES_DISPLAY_E,
	MINUTES_DISP_HOLD_E,
}MinutesDispState_En_t;


typedef enum
{
	HOURS_DISPLAY_E,
	HOURS_DISP_HOLD_E,
}HoursDispState_En_t;

/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern bool 	     Initial_Minutes_Disp_b;
extern bool 	     Initial_Hours_Disp_b;


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Display_Time(uint16_t, uint16_t, ClusterSignals_En_t, ClusterSignals_En_t,ClusterSignals_En_t );
extern void BlinkLowerMinuteTime(uint8_t BlnkLBMinuteTime_u8, uint8_t SolidHourTime_u8,uint8_t SolidHBMinuteTime_u8);
extern void BlinkHigherMinuteTime(uint8_t BlnkHBMinuteTime_u8, uint8_t SolidHourTime_u8, uint8_t SolidLBMinuteTime_u8);
extern void BlinkHourTime(uint8_t BlnkHourTime_u8, uint8_t SolidMinuteTime);
uint16_t ValidateMinutesTime(uint16_t);
uint16_t ValidateHoursTime(uint16_t);
extern void Blink_Time_Colon(ClusterSignals_En_t);

#endif /* TIME_DISP_H */


