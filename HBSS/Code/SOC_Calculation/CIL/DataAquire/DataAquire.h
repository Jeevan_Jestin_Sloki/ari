/***********************************************************************************************************************
* File Name    : DataAquire.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 01/09/2021
***********************************************************************************************************************/

#ifndef DATA_AQUIRE_H
#define DATA_AQUIRE_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"
#include "Time.h"
/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define TELLTALE_SOLID_ON					0x03U
#define TELLTALE_BLINK						0x01U
//#define TELLTALE_OFF						0x00U

#define BUZZER_OUTPUT						true

#define IGNITION_ON							0x01U
#define IGNITION_OFF						0x00U

#define GPIO_ACTIVEHIGH						0x00U
#define GPIO_ACTIVELOW						0x01U

#define DEFAULT_VALUE                        0x00
#define THRESHOLD_VALUE_3000MS             (3000U)
#define DEBOUNCE_50MS                       (50U)
#define RESET_VALUE                         (0x00)

#define _250_MS 250
#define _300_MS	300
#define _500_MS 500
#define _3000_MS 3000
#define _150_MS 150
#define _5000_MS 5000
#define _1000_MS 1000
#define _50_MS	50

#define PORT_OUTPUT_HIGH  0x01
#define PORT_OUTPUT_LOW   0x00

#define POWER_CONSUMP_FACTOR 0x05
#define LOW_SOC_RANGE		15
/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	USER_INPUT_SIG_START_E,
	INPUT_1_E = USER_INPUT_SIG_START_E,
	INPUT_2_E,
	INPUT_3_E,
	INPUT_4_E,
	INPUT_5_E,
	INPUT_6_E,
	INPUT_7_E,
	INPUT_8_E,
	INPUT_9_E,
#if (FALSE == BUZZER_OUTPUT)
	INPUT_10_E,
#endif
	INPUT_11_E,
	INPUT_12_E,
	TOTAL_USER_INPUT_SIG_E,
	USER_INPUT_SIG_END_E = TOTAL_USER_INPUT_SIG_E,
}UserInputSig_En_t;


typedef enum
{
    USER_BUTTON_RELEASED_E,
    USER_BUTTON_PRESSED_E,
}UserButtonState_En_t;

#pragma pack
typedef struct
{
	UserInputSig_En_t		UserInputSig_En;
	uint8_t 				UserInputVal_u8;
	uint8_t 				SigTurnOnVal_u8;
	uint8_t 				SigTurnOffVal_u8;
	uint8_t 				SignalState_u8;
	bool					UserButtonEnable_b;
}UserInputSigConf_St_t;

typedef struct 
{
    UserInputSig_En_t           UserButtonSig_En;
    UserButtonState_En_t        UserButtonState_En;
    uint32_t                    UserInputCaptureTime_u32;
    uint32_t                    UserInpActiveStateTime_u32;
    uint16_t                    LongPressedThresholdValue_u16;
    uint16_t                    UserInputDebounce_u16;
    bool                        UserInputLongPressed_b;

}UserButtonConf_St_t;

#pragma unpack
typedef enum
{
	IGNITION_NONE_E,
	IGNITION_CAN_E,
	IGNITION_GPIO_E,
}IgnitionInput_En_t;

typedef enum
{
	IGNITION_EVENT_E,
	TEMP_WARNING_EVENT_E,
    LOW_BATT_PRIORITY_EVENT_E,
    REVERSE_GEAR_EVENT_E,
    INDICATOR_EVENT_E,
    HAZARD_WARNING_EVENT_E,
    TOTAL_BUZZER_EVENT_E,
}BuzzerEvent_En_t;

typedef struct
{
	BuzzerEvent_En_t BuzzerEvent_En;
	uint32_t BuzzerEventFrequency_ms_u32;
	uint16_t BuzzerDutyCycle_u16;
	bool BuzzerEventTriggered_b;
	bool BuzzerEventStart_b;
}BuzzerEventData_St_t;

/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern UserInputSigConf_St_t	UserInputSigConf_St[TOTAL_USER_INPUT_SIG_E];
extern UserButtonConf_St_t		UserButtonConf_St[TOTAL_USER_INPUT_SIG_E];

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void ReadClusterInput(void);
extern void UpdateSigToDataBank(void);
extern void ResetTheUserInput(void);

void update_ODOtrip(uint8_t);
extern void UpdateBuzzerEvent(void);
extern void BuzzerScheduler(void);
extern void BuzzerAnimation(void);
#endif /* DATA_AQUIRE_H
*/