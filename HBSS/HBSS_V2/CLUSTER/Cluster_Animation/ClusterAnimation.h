/***********************************************************************************************************************
* File Name    : ClusterAnimation.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 10/01/2022
***********************************************************************************************************************/

#ifndef ANIMATION_H
#define ANIMATION_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"
#include "Cluster_Conf.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define INCREASE_IN_SPEED			5
#define DECREASE_IN_SPEED			10

#define MAXIMUM_SPEED				100
#define MINIMUM_SPEED				0

#define INCREASE_IN_NAVIG_DIST			100
#define INCREASE_IN_SOC_PER			10
#define INCREASE_IN_GEAR_PER			1

#define MAX_NAVIG_DIR_SIGNAL			5

#define ODO_DIGIT_FOR_ANIMATION			888888U
#define RANGEKM_FOR_ANIMATE			888U

#define HOURS_TM_FOR_ANIMATION			18U
#define MIN_TM_FOR_ANIMATION			88U
/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef struct
{
	uint32_t BarsCountValue_u32;
	uint16_t BarsCount_u16;
	int16_t  VehicleSpeed_s16;
	uint16_t SpeedDelayCounter_u16;
	uint16_t SOCpercentage_u16;
	uint8_t  GearDigit_u8;
}AnimationData_St_t;

typedef enum
{
	SPEED_NO_CHANGE_E,
	SPEED_ANIMATE_START_E,
	INCREASE_SPEED_E,
	INTERMEDIATE_DELAY_E,
	DECREASE_SPEED_E,
	SPEED_ANIMATE_END_E,
}SpeedAnimation_En_t;


typedef enum
{
	SIG_NO_CHANGE_E,
	TURN_ON_SIG_E,
	SET_SIG_ACTUAL_E,
}SignalState_En_t;

typedef enum
{
	D_MODE_E,
	N_MODE_E,
	R_MODE_E,
	P_MODE_E,
	DRV_NONE_E,
}Animate_DRV_Mode_En_t;

/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern bool	AnimationDone_b;

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void AnimateOnStart(void);
extern void ResetAnimationData(void);
void AnimateIncreaseVehicleSpeed(void);
void AnimateDecreaseVehicleSpeed(void);
void SetTopRowTelltales(SignalState_En_t);
void SetBottomRowTelltales(SignalState_En_t );
void SetDrivingModeRings(SignalState_En_t );
void SetBarsActualValue(void);

void Turn_On_Common_Signals(void);


#endif /* ANIMATION_H */


