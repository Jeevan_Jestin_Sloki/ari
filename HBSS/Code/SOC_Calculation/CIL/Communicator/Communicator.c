/***********************************************************************************************************************
* File Name    : Communicator.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Communicator.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
uint32_t TaskSchedulerCounter_u32 = 0;
uint32_t ODO_in_meter_u32 = 0;
uint16_t VehicleSpeedToDisplay_u16 = 0;
uint16_t PresentSessionWHkm_u16 = 0;
uint16_t PresentSessionRangeKm_u16 = 0;
uint8_t ODOtextState_u8 = SOLID_ON;
uint8_t TripATextState_u8  = OFF;
uint8_t TripBTextState_u8  = OFF;
uint8_t TripTextState_u8   = OFF; 
uint8_t TimeColonState_u8  = BLINK_TT;
uint8_t MotorFaultState_u8 = OFF;
uint8_t BatteryfaultState_u8 = OFF;
uint8_t TopTextState_u8    = OFF;
uint8_t ChargerPlugInState_u8 = OFF;
bool 	TimeSetting_b	   = false;
uint8_t Batterystate_u8 = OFF;
uint8_t Battery_SOC_u8 = 0;
float Accurate_SOC_f    = 0;
/********************************************************EOF***********************************************************/