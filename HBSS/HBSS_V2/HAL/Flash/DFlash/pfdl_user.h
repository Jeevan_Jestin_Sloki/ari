/***********************************************************************************************************************
* File Name    : pfdl_user.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

#ifndef PFDL_USER_H
#define PFDL_USER_H

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
/* Data flash library */
#include "pfdl.h"                /* Library header file */
#include "pfdl_types.h"          /* Library header file */
#include "App_typedefs.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
/* Basic data */
#define TOTAL_FDL_BLOCKS			8		/* Total Bocks in Data Flash */
#define EACH_FDL_BLOCK_SIZE   			0x400	  	/* Standard block size */
#define FDL_WRITE_BUFFER_SIZE	   		12      		/* Write data size */
#define FDL_READ_BUFFER_SIZE	   		12     		/* Write data size */


/* PFDL initial settings */
#define FDL_FRQUENCY		      		32      	/* Sets the frequency (32 MHz) */
#define FDL_VOLTAGE		      		0x00    	/* Sets the voltage mode (full-speed mode) */


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/


/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/

extern pfdl_u08        dubWriteBuffer[ FDL_WRITE_BUFFER_SIZE ];
extern pfdl_u08        dubReadBuffer[ FDL_READ_BUFFER_SIZE ];

pfdl_status_t 	r_pfdl_samFdlStart ( void );  /* pfdl initialization processing */
void          	r_pfdl_samFdlEnd   ( void );  /* pfdl end processing            */
extern void 		FDL_Erase(pfdl_u16 Block_Number, pfdl_u16 Total_Blocks);
extern void     FDL_Read(pfdl_u16 Block_Number, pfdl_u16 Position_at_Block, pfdl_u16 Num_Of_Bytes);
extern void 	FDL_Write(pfdl_u16 Block_Number, pfdl_u16 Position_at_Block, pfdl_u16 Num_Of_Bytes);

#endif /* PFDL_USER_H */


