/***********************************************************************************************************************
* File Name    : DataAquire.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 07/07/2022
***********************************************************************************************************************/

#ifndef DATA_AQUIRE_H
#define DATA_AQUIRE_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define IGNITION_DEBOUNCE_COUNT					50 /*50*5ms = 250ms*/
#define SWITCH_DEBOUNCE_MS						50 /* 50 millisec debounce value */

#define IGNITION_ON				0x01U
#define IGNITION_OFF				0x00U

#define RESET_VALUE                         (0x00)

#define _250_MS 250
#define _300_MS	300
#define _500_MS 500
#define _3000_MS 3000
#define _150_MS 150
#define _5000_MS 5000
#define _1000_MS 1000
#define _50_MS	50

#define PORT_OUTPUT_HIGH  0x01
#define PORT_OUTPUT_LOW   0x00
/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	USER_INPUT_SIG_START_E,
	INPUT_1_E = USER_INPUT_SIG_START_E, /* Side stand */
	INPUT_2_E,							/* Open */
	INPUT_3_E,							/* Open */
	INPUT_4_E,							/* Open */
	INPUT_5_E,							/* Ignition Key */
	INPUT_6_E,							/* Right Indicator */
	INPUT_7_E,							/* Left Indicator */
	INPUT_8_E,							/* Kill Switch */
	INPUT_9_E,							/* Open */
	INPUT_10_E,							/* High Beam */
	INPUT_11_E,							/* Low Beam */
	INPUT_12_E,							/* Speed pulse counter */
	TOTAL_USER_INPUT_SIG_E,
	USER_INPUT_SIG_END_E = TOTAL_USER_INPUT_SIG_E,
}UserInputSig_En_t;

typedef enum
{
	USER_SIG_OFF_IDLE_E,
	USER_SIG_ON_IDLE_E,
	USER_SIG_OFF_E,
	USER_SIG_ON_E,
}UserInputState_En_t;

typedef struct
{
	UserInputSig_En_t	UserInputSig_En;
	UserInputState_En_t UserInputState_En;
	uint8_t 			UserInputVal_u8;
	uint8_t 			SigTurnOnVal_u8;
	uint8_t 			SigTurnOffVal_u8;
	uint8_t 			SignalState_u8;
	uint16_t 			DebounceValue;
	uint32_t 			UserInpCaptureTime_u32;
	bool 				UserInputSigEnable_b;
}UserInputSigConf_St_t;


typedef struct
{
	uint16_t DebounceCounter_u32;
	bool	 IgnitionFlag_b;
	bool	 IgnitionHighEntry_b;
	bool	 IgnitionLowEntry_b;
}IgnitionDebounce_St_t;

typedef enum
{
    IGNITION_NONE_E,
    IGNITION_GPIO_E,
    IGNITION_CAN_E,
}IgnitionSource_En_t;

typedef enum
{
	IGNITION_EVENT_E,
	TEMP_WARNING_EVENT_E,
    BATTERY_FAULT_EVENT_E,
    REVERSE_GEAR_EVENT_E,
    INDICATOR_EVENT_E,
    HAZARD_WARNING_EVENT_E,
    TOTAL_BUZZER_EVENT_E,
}BuzzerEvent_En_t;

typedef struct
{
	BuzzerEvent_En_t BuzzerEvent_En;
	uint32_t BuzzerEventFrequency_ms_u32;
	uint16_t BuzzerDutyCycle_u16;
	bool BuzzerEventTriggered_b;
	bool BuzzerEventStart_b;
}BuzzerEventData_St_t;
/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void ReadClusterInput(void);
extern void UpdateSigToDataBank(void);
extern void ResetTheUserInput(void);
//void Meridien(void);
void update_ODOtrip(uint8_t);
void BuzzerScheduler(void);

#endif /* DATA_AQUIRE_H
*/