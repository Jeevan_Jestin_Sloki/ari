/***********************************************************************************************************************
* File Name    : Mileage_Conf.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 27/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Mileage_Conf.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/
 const SignalsValue_St_t	  Mileage_SignalsValue_aSt[MILEAGE_SIG_LEN] =
{
	/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	{     ZERO_E, 		ELEVEN_E,                  MileageSig1Conf_ast },
	{     ONE_E,  		ELEVEN_E,                  MileageSig2Conf_ast }
};

 const SignalConfig_St_t	     MileageSig1Conf_ast[ELEVEN_E] = 
{
/*PositionSignalValue  SegmentsRequired  SegmentsConfiguration*/
	{ ZERO_E,    	TWO_SEG_E,  	Mileage_S1_0_SegConf_aSt   },
	{ ONE_E,     	TWO_SEG_E,  	Mileage_S1_1_SegConf_aSt   },
	{ TWO_E,     	TWO_SEG_E,  	Mileage_S1_2_SegConf_aSt   },
	{ THREE_E,   	TWO_SEG_E,  	Mileage_S1_3_SegConf_aSt   },
	{ FOUR_E,    	TWO_SEG_E,  	Mileage_S1_4_SegConf_aSt   },
	{ FIVE_E,    	TWO_SEG_E,  	Mileage_S1_5_SegConf_aSt   },
	{ SIX_E,     	TWO_SEG_E,  	Mileage_S1_6_SegConf_aSt   },
	{ SEVEN_E,   	TWO_SEG_E,  	Mileage_S1_7_SegConf_aSt   },
	{ EIGHT_E,   	TWO_SEG_E,  	Mileage_S1_8_SegConf_aSt   },
	{ NINE_E,    	TWO_SEG_E,  	Mileage_S1_9_SegConf_aSt   },
	{ OFF_DIGIT, 	TWO_SEG_E,  	Mileage_S1_Off_SegConf_aSt }
};

 const SignalConfig_St_t	     MileageSig2Conf_ast[ELEVEN_E] = 
{
	{ ZERO_E,    TWO_SEG_E,  Mileage_S2_0_SegConf_aSt   },
	{ ONE_E,     TWO_SEG_E,  Mileage_S2_1_SegConf_aSt   },
	{ TWO_E,     TWO_SEG_E,  Mileage_S2_2_SegConf_aSt   },
	{ THREE_E,   TWO_SEG_E,  Mileage_S2_3_SegConf_aSt   },
	{ FOUR_E,    TWO_SEG_E,  Mileage_S2_4_SegConf_aSt   },
	{ FIVE_E,    TWO_SEG_E,  Mileage_S2_5_SegConf_aSt   },
	{ SIX_E,     TWO_SEG_E,  Mileage_S2_6_SegConf_aSt   },
	{ SEVEN_E,   TWO_SEG_E,  Mileage_S2_7_SegConf_aSt   },
	{ EIGHT_E,   TWO_SEG_E,  Mileage_S2_8_SegConf_aSt   },
	{ NINE_E,    TWO_SEG_E,  Mileage_S2_9_SegConf_aSt   },
	{ OFF_DIGIT, TWO_SEG_E,  Mileage_S2_Off_SegConf_aSt }
};

 const SegConfig_St_t	Mileage_S1_0_SegConf_aSt[TWO_SEG_E] = 
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	{ &SEG3,	        0x01U,	         0x0EU },
	{ &SEG2,                0x00U,           0x0BU }

};

 const SegConfig_St_t	Mileage_S1_1_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG3,        0x01U,       0x06U },
	{ &SEG2,        0x00U,       0x00U }
};

 const SegConfig_St_t	Mileage_S1_2_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG3,        0x01U,       0x0AU },
	{ &SEG2,        0x00U,       0x0DU }
};

 const SegConfig_St_t	Mileage_S1_3_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG3,        0x01U,       0x0EU },
	{ &SEG2,        0x00U,       0x05U }
};

 const SegConfig_St_t	Mileage_S1_4_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG3,        0x01U,       0x06U },
	{ &SEG2,        0x00U,       0x06U }
};

 const SegConfig_St_t	Mileage_S1_5_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG3,        0x01U,       0x0CU },
	{ &SEG2,        0x00U,       0x07U }
};

 const SegConfig_St_t	Mileage_S1_6_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG3,        0x01U,       0x0CU },
	{ &SEG2,        0x00U,       0x0FU }
};


 const SegConfig_St_t	Mileage_S1_7_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG3,        0x01U,       0x06U },
	{ &SEG2,        0x00U,       0x01U }
};


 const SegConfig_St_t	Mileage_S1_8_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG3,        0x01U,       0x0EU },
	{ &SEG2,        0x00U,       0x0FU }
};


 const SegConfig_St_t	Mileage_S1_9_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG3,        0x01U,       0x0EU },
	{ &SEG2,        0x00U,       0x07U }
};

 const SegConfig_St_t	Mileage_S1_Off_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG3,        0x01U,       0x0EU },
	{ &SEG2,        0x00U,       0x0BU }
};

 const SegConfig_St_t	Mileage_S2_0_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG1,       0x01U,        0x0EU },
	{ &SEG0,       0x00U,        0x0BU }
};

 const SegConfig_St_t	Mileage_S2_1_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG1,       0x01U,        0x06U },
	{ &SEG0,       0x00U,        0x00U }
};

 const SegConfig_St_t	Mileage_S2_2_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG1,       0x01U,        0x0AU },
	{ &SEG0,       0x00U,        0x0DU }
};

 const SegConfig_St_t	Mileage_S2_3_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG1,       0x01U,        0x0EU },
	{ &SEG0,       0x00U,        0x05U }
};

 const SegConfig_St_t	Mileage_S2_4_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG1,       0x01U,        0x06U },
	{ &SEG0,       0x00U,        0x06U }
};

 const SegConfig_St_t	Mileage_S2_5_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG1,       0x01U,         0x0CU },
	{ &SEG0,       0x00U,         0x07U }
};

 const SegConfig_St_t	Mileage_S2_6_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG1,       0x01U,         0x0CU },
	{ &SEG0,       0x00U,         0x0FU }
};


 const SegConfig_St_t	Mileage_S2_7_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG1,       0x01U,         0x06U },
	{ &SEG0,       0x00U,         0x01U }
};


 const SegConfig_St_t	Mileage_S2_8_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG1,       0x01U,         0x0EU },
	{ &SEG0,       0x00U,         0x0FU }
};


 const SegConfig_St_t	Mileage_S2_9_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG1,       0x01U,         0x0EU },
	{ &SEG0,       0x00U,         0x07U }
};

 const SegConfig_St_t	Mileage_S2_Off_SegConf_aSt[TWO_SEG_E] = 
{
	{ &SEG1,       0x01U,         0x00U },
	{ &SEG0,       0x00U,         0x00U }
};


/********************************************************EOF***********************************************************/