/***********************************************************************************************************************
* File Name    : TeltailsConf.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 30/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "TelltaleConf.h"

/***********************************************************************************************************************
Global variables
***********************************************************************************************************************/

/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/
/*
			Reverse mode  Configration 
*/

 const SignalsValue_St_t			Reverse_Mode_SignalsValue_aSt[ECO_MODE_SIG_LEN] = 
{
	{ ZERO_E,	TWO_E,		Reverse_Mode_SigConf_ast},
};

 const SignalConfig_St_t			Reverse_Mode_SigConf_ast[TWO_E] =
{
	{ OFF_E,	ONE_SEG_E,	Reverse_Mode_Off_SegConf_aSt },
	{ ON_E,		ONE_SEG_E,	Reverse_Mode_On_SegConf_aSt  },
};

 const SegConfig_St_t				Reverse_Mode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG13,		0x0EU,		0x00U},
};

 const SegConfig_St_t				Reverse_Mode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG13,		0x0EU,		0x01U},
};

/*
			Neutral mode  Configration 
*/
 const SignalsValue_St_t			Neutral_Mode_SignalsValue_aSt[ECO_MODE_SIG_LEN] = 
{
	{ ZERO_E,	TWO_E,		Neutral_Mode_SigConf_ast},
};

 const SignalConfig_St_t			Neutral_Mode_SigConf_ast[TWO_E] =
{
	{ OFF_E,	ONE_SEG_E,	Neutral_Mode_Off_SegConf_aSt },
	{ ON_E,		ONE_SEG_E,	Neutral_Mode_On_SegConf_aSt  },
};

 const SegConfig_St_t				Neutral_Mode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG9,		0x0EU,		0x00U},
};

 const SegConfig_St_t				Neutral_Mode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG9,		0x0EU,		0x01U},
};


/*
			ECO mode  Configration 
*/
 const SignalsValue_St_t			Eco_Mode_SignalsValue_aSt[ECO_MODE_SIG_LEN] = 
{
	{ ZERO_E,	TWO_E,		Eco_Mode_SigConf_ast},
};

 const SignalConfig_St_t			Eco_Mode_SigConf_ast[TWO_E] =
{
	{ OFF_E,	ONE_SEG_E,	Eco_Mode_Off_SegConf_aSt },
	{ ON_E,		ONE_SEG_E,	Eco_Mode_On_SegConf_aSt  },
};

 const SegConfig_St_t				Eco_Mode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG7,		0x0EU,		0x00U},
};

 const SegConfig_St_t				Eco_Mode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG7,		0x0EU,		0x01U},
};


/*
			Sports mode  Configration 
*/
 const SignalsValue_St_t			Sports_Mode_SignalsValue_aSt[SPORTS_MODE_SIG_LEN] = 
{
	{ ZERO_E,	TWO_E,		Sports_Mode_SigConf_ast},
};

 const SignalConfig_St_t			Sports_Mode_SigConf_ast[TWO_E] =
{
	{ OFF_E,	ONE_SEG_E,	Sports_Mode_Off_SegConf_aSt },
	{ ON_E,		ONE_SEG_E,	Sports_Mode_On_SegConf_aSt  },
};

 const SegConfig_St_t				Sports_Mode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG30,	0x0EU,		0x00U},
};

 const SegConfig_St_t				Sports_Mode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG30,	0x0EU,		0x01U},
};


/*
		Battery Indicator Configration
*/

 const SignalsValue_St_t			Batt_Ind_SignalsValue_aSt[BATTERY_IND_SIG_LEN] = 
{
	{ ZERO_E,	TWO_E,		Batt_Ind_SigConf_ast},
};

 const SignalConfig_St_t			Batt_Ind_SigConf_ast[TWO_E] =
{
	{ OFF_E,	ONE_SEG_E,	Batt_Ind_Off_SegConf_aSt },
	{ ON_E,		ONE_SEG_E,	Batt_Ind_On_SegConf_aSt  },
};

 const SegConfig_St_t				Batt_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG45,	0x0EU,		0x00U},
};

 const SegConfig_St_t				Batt_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG45,	0x0EU,		0x01U},
};


/*
		WH/KM Text Configration
*/
 const SignalsValue_St_t			WHKM_Text_SignalsValue_aSt[WHKM_TEXT_SIG_LEN] = 
{
	
	{ ZERO_E,	TWO_E,		WHKM_Text_Ind_SigConf_ast},
};

 const SignalConfig_St_t			WHKM_Text_Ind_SigConf_ast[TWO_E] =
{
	{ OFF_E,	ONE_SEG_E,	WHKM_Text_Off_SegConf_aSt },
	{ ON_E,		ONE_SEG_E,	WHKM_Text_On_SegConf_aSt  },
};

 const SegConfig_St_t			WHKM_Text_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG1,		0x0EU,		0x00U},
};

 const SegConfig_St_t			WHKM_Text_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG1,		0x0EU,		0x01U},
};

/*
		Power Indicator configration
*/

 const SignalsValue_St_t			Power_Ind_SignalsValue_aSt[POWER_IND_SIG_LEN] = 
{
	
	{ ZERO_E,	TWO_E,		Power_Ind_SigConf_ast},
};

 const SignalConfig_St_t			Power_Ind_SigConf_ast[TWO_E] =
{
	{ OFF_E,	ONE_SEG_E,	PWR_Ind_Off_SegConf_aSt },
	{ ON_E,		ONE_SEG_E,	PWR_Ind_On_SegConf_aSt  },
};

 const SegConfig_St_t			PWR_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG3,		0x0EU,		0x00U},
};

 const SegConfig_St_t			PWR_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG3,		0x0EU,		0x01U},
};

/*
		Logo Telltale configration
*/
 const SignalsValue_St_t			Logo_SignalsValue_aSt[LOGO_SIG_LEN] = 
{
	
	{ ZERO_E,	TWO_E,		LogoSigConf_ast},
};

 const SignalConfig_St_t			LogoSigConf_ast[TWO_E] =
{
	{ OFF_E,	ONE_SEG_E,	Logo_Off_SegConf_aSt },
	{ ON_E,		ONE_SEG_E,	Logo_On_SegConf_aSt  },
};

 const SegConfig_St_t			Logo_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG42,	0x0EU,		0x00U},
};

 const SegConfig_St_t			Logo_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG42,	0x0EU,		0x01U},
};

/*
   Left turn indicator telltale Configuration 
*/
 const SignalsValue_St_t			LeftInd_SignalsValue_aSt[LEFT_IND_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		LeftIndSigConf_ast,
	},
};

 const SignalConfig_St_t			LeftIndSigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		LeftInd_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		LeftInd_On_SegConf_aSt
	},
};

 const SegConfig_St_t			LeftInd_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG32,
		0x0EU,
		0x00U,
	},
};

 const SegConfig_St_t			LeftInd_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG32,
		0x0EU,
		0x01U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Right turn indicator telltale Configuration 
*/
 const SignalsValue_St_t			RightInd_SignalsValue_aSt[RIGHT_IND_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		RightIndSigConf_ast,
	},
};

 const SignalConfig_St_t			RightIndSigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		RightInd_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		RightInd_On_SegConf_aSt
	},
};

 const SegConfig_St_t			RightInd_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG15,
		0x07U,
		0x00U,
	},
};

 const SegConfig_St_t			RightInd_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG15,
		0x07U,
		0x08U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Low-Beam indicator telltale Configuration 
*/
 const SignalsValue_St_t			LowBeamInd_SignalsValue_aSt[LOW_BEAM_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		LowBeamIndSigConf_ast,
	},
};

 const SignalConfig_St_t			LowBeamIndSigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		LowBeamInd_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		LowBeamInd_On_SegConf_aSt
	},
};

 const SegConfig_St_t			LowBeamInd_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG39,
		0x0EU,
		0x00U,
	},
};

 const SegConfig_St_t			LowBeamInd_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG39,
		0x0EU,
		0x01U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   High-Beam indicator telltale Configuration 
*/
 const SignalsValue_St_t			HighBeamInd_SignalsValue_aSt[HIGH_BEAM_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		HighBeamIndSigConf_ast,
	},
};

 const SignalConfig_St_t			HighBeamIndSigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		HighBeamInd_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		HighBeamInd_On_SegConf_aSt
	},
};

 const SegConfig_St_t			HighBeamInd_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG40,
		0x0DU,
		0x00U,
	},
};

 const SegConfig_St_t			HighBeamInd_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG40,
		0x0DU,
		0x02U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   BLE indicator telltale Configuration 
*/
 const SignalsValue_St_t			BLE_Ind_SignalsValue_aSt[BLE_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		BLE_IndSigConf_ast,
	},
};

 const SignalConfig_St_t			BLE_IndSigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		BLE_Ind_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		BLE_Ind_On_SegConf_aSt
	},
};

 const SegConfig_St_t			BLE_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG40,
		0x0EU,
		0x00U,
	},
};

 const SegConfig_St_t			BLE_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG40,
		0x0EU,
		0x01U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Regen Braking  telltale Configuration 
*/
 const SignalsValue_St_t			RegenBrake_SignalsValue_aSt[REGEN_BRAKING_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		RegenBrake_IndSigConf_ast,
	},
};

 const SignalConfig_St_t			RegenBrake_IndSigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		RegenBrake_Ind_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		RegenBrake_Ind_On_SegConf_aSt
	},
};

 const SegConfig_St_t			RegenBrake_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG4,
		0x0EU,
		0x00U,
	},
};

 const SegConfig_St_t			RegenBrake_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG4,
		0x0EU,
		0x01U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Warning indicator telltale Configuration 
*/
 const SignalsValue_St_t			Warning_SignalsValue_aSt[WARN_IND_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		Warning_IndSigConf_ast,
	},
};

 const SignalConfig_St_t			Warning_IndSigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		Warning_Ind_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		Warning_Ind_On_SegConf_aSt
	},
};

 const SegConfig_St_t			Warning_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG5,
		0x0EU,
		0x00U,
	},
};

 const SegConfig_St_t			Warning_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG5,
		0x0EU,
		0x01U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Service Reminder telltale Configuration 
*/
 const SignalsValue_St_t			ServRem_SignalsValue_aSt[SERV_REM_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		ServRem_SigConf_ast,
	},
};

 const SignalConfig_St_t			ServRem_SigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		ServRem_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		ServRem_On_SegConf_aSt
	},
};

 const SegConfig_St_t			ServRem_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG15,
		0x0BU,
		0x00U,
	},
};

 const SegConfig_St_t			ServRem_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG15,
		0x0BU,
		0x04U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Neutral mode indicator telltale Configuration 
*/
 const SignalsValue_St_t			NeutralMode_SignalsValue_aSt[NEUTRAL_MODE_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		NeutralMode_SigConf_ast,
	},
};

 const SignalConfig_St_t			NeutralMode_SigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		NeutralMode_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		NeutralMode_On_SegConf_aSt
	},
};

 const SegConfig_St_t			NeutralMode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG25,
		0x0EU,
		0x00U,
	},
};

 const SegConfig_St_t			NeutralMode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG25,
		0x0EU,
		0x01U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Economy mode indicator telltale Configuration 
*/
 const SignalsValue_St_t			EcoMode_SignalsValue_aSt[ECO_MODE_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		EcoMode_SigConf_ast,
	},
};

 const SignalConfig_St_t			EcoMode_SigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		EcoMode_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		EcoMode_On_SegConf_aSt
	},
};

 const SegConfig_St_t			EcoMode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG26,
		0x0EU,
		0x00U,
	},
};

 const SegConfig_St_t			EcoMode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG26,
		0x0EU,
		0x01U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Sports mode indicator telltale Configuration 
*/
 const SignalsValue_St_t			SportsMode_SignalsValue_aSt[SPORTS_MODE_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		SportsMode_SigConf_ast,
	},
};

 const SignalConfig_St_t			SportsMode_SigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		SportsMode_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		SportsMode_On_SegConf_aSt
	},
};

 const SegConfig_St_t			SportsMode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG28,
		0x0EU,
		0x00U,
	},
};

 const SegConfig_St_t			SportsMode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG28,
		0x0EU,
		0x01U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Reverse mode indicator telltale Configuration 
*/
 const SignalsValue_St_t			ReverseMode_SignalsValue_aSt[REVERSE_MODE_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		ReverseMode_SigConf_ast,
	},
};

 const SignalConfig_St_t			ReverseMode_SigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		ReverseMode_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		ReverseMode_On_SegConf_aSt
	},
};

 const SegConfig_St_t			ReverseMode_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG15,
		0x0EU,
		0x00U,
	},
};

 const SegConfig_St_t			ReverseMode_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG15,
		0x0EU,
		0x01U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Side-Stand indicator telltale Configuration 
*/
 const SignalsValue_St_t			SideStand_SignalsValue_aSt[SIDE_STAND_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		SideStand_IndSigConf_ast,
	},
};

 const SignalConfig_St_t			SideStand_IndSigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		SideStand_Ind_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		SideStand_Ind_On_SegConf_aSt
	},
};

 const SegConfig_St_t			SideStand_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG25,
		0x07U,
		0x00U,
	},
};

 const SegConfig_St_t			SideStand_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG25,
		0x07U,
		0x08U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Safe-Mode indicator telltale Configuration 
*/
 const SignalsValue_St_t			SafeMode_SignalsValue_aSt[SAFE_MODE_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		SafeMode_IndSigConf_ast,
	},
};

 const SignalConfig_St_t			SafeMode_IndSigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		SafeMode_Ind_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		SafeMode_Ind_On_SegConf_aSt
	},
};

 const SegConfig_St_t			SafeMode_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG20,
		0x07U,
		0x00U,
	},
};

 const SegConfig_St_t			SafeMode_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG20,
		0x07U,
		0x08U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Battery-Fault indicator telltale Configuration 
*/
 const SignalsValue_St_t			BattFlt_SignalsValue_aSt[BATT_FLT_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		BattFlt_IndSigConf_ast,
	},
};

 const SignalConfig_St_t			BattFlt_IndSigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		BattFlt_Ind_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		BattFlt_Ind_On_SegConf_aSt
	},
};

 const SegConfig_St_t			BattFlt_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG19,
		0x07U,
		0x00U,
	},
};

 const SegConfig_St_t			BattFlt_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG19,
		0x07U,
		0x08U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Motor-Fault indicator telltale Configuration 
*/
 const SignalsValue_St_t			MotorFlt_SignalsValue_aSt[MOTOR_FLT_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		MotorFlt_IndSigConf_ast,
	},
};

 const SignalConfig_St_t			MotorFlt_IndSigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		MotorFlt_Ind_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		MotorFlt_Ind_On_SegConf_aSt
	},
};

 const SegConfig_St_t			MotorFlt_Ind_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG19,
		0x0BU,
		0x00U,
	},
};

 const SegConfig_St_t			MotorFlt_Ind_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG19,
		0x0BU,
		0x04U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/*
   Kill-Switch telltale Configuration 
*/
 const SignalsValue_St_t			KillSwitch_SignalsValue_aSt[KILL_SWITCH_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		KillSwitch_SigConf_ast,
	},
};

 const SignalConfig_St_t			KillSwitch_SigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		KillSwitch_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		KillSwitch_On_SegConf_aSt
	},
};

 const SegConfig_St_t			KillSwitch_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG16,
		0x07U,
		0x00U,
	},
};

 const SegConfig_St_t			KillSwitch_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG16,
		0x07U,
		0x08U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/

/*
   Common Constant Segments Configuration 
*/
 const SignalsValue_St_t			Common_SignalsValue_aSt[COMMON_SEG_SIG_LEN] = 
{
	{
		ZERO_E,
		TWO_E,
		Common_SigConf_ast,
	},
};

 const SignalConfig_St_t			Common_SigConf_ast[TWO_E] =
{
	{
		OFF_E,
		ONE_SEG_E,
		Common_Off_SegConf_aSt
	},
	{
		ON_E,
		ONE_SEG_E,
		Common_On_SegConf_aSt
	},
};

 const SegConfig_St_t			Common_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG38,
		0x0EU,
		0x00U,
	},
};

 const SegConfig_St_t			Common_On_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG38,
		0x0EU,
		0x01U,
	},
};

/*---------------------------------------------------------------------------------------------------------------------*/


/********************************************************EOF***********************************************************/