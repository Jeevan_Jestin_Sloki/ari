/***********************************************************************************************************************
* File Name    : PowerConsum_Conf.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 03/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "PowerConsum_Conf.h"

/***********************************************************************************************************************
Global variables
***********************************************************************************************************************/

/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/

/*
		Power indicator configration
*/

 const SignalsValue_St_t		Power_ind_SiganlValue_ast[POWER_IND_SIG_LEN]=
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	ZERO_E, 	TWO_E, 		Power_ind_SigConf_ast
};


 const SignalConfig_St_t		Power_ind_SigConf_ast[TWO_E]=
{
/*PositionSignalValue  SegmentsRequired  SegmentsConfiguration*/
	{ON_E, 		ONE_SEG_E,	Power_ind_on_SegConf_ast },
	{OFF_E, 	ONE_SEG_E,	Power_ind_off_SegConf_ast},
};


 const SegConfig_St_t		Power_ind_on_SegConf_ast[ONE_SEG_E]=
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	&SEG40,		0x0EU,		0x01U
};
 const SegConfig_St_t		Power_ind_off_SegConf_ast[ONE_SEG_E]=
{
	&SEG40,		0x0EU,		0x00U
};



/*
		Power Bar_indicator configration
*/

 const SignalsValue_St_t	   PwrConsum_SignalsValue_aSt[POWER_CONSUMP_SIG_LEN] = 
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	{ ZERO_E, 	THREE_E, 	PwrConsum_P6_SigConf_ast  },
	{ ONE_E, 	TWO_E, 		PwrConsum_P7_SigConf_ast  },
	{ TWO_E,	TWO_E,		PwrConsum_P8_SigConf_ast  },
	{ THREE_E,	TWO_E,		PwrConsum_P9_SigConf_ast  },
	{ FOUR_E,	TWO_E,		PwrConsum_P10_SigConf_ast },
};


 const SignalConfig_St_t	       PwrConsum_P6_SigConf_ast[THREE_E] = 
{
/*PositionSignalValue  SegmentsRequired  SegmentsConfiguration*/
	{ ON_E,		ONE_SEG_E,	PwrConsum_P6_On_SegConf_aSt  },
	{ OFF_BAR,	ONE_SEG_E,	PwrConsum_P6_Off_SegConf_aSt },
	{ OFF_E,	ONE_SEG_E,	PwrConsum_P6_Off_SegConf_aSt }
};

 const SignalConfig_St_t		PwrConsum_P7_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	PwrConsum_P7_On_SegConf_aSt  },
	{ OFF_BAR,	ONE_SEG_E,	PwrConsum_P7_Off_SegConf_aSt }
};

 const SignalConfig_St_t		PwrConsum_P8_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	PwrConsum_P8_On_SegConf_aSt  },
	{ OFF_BAR,	ONE_SEG_E,	PwrConsum_P8_Off_SegConf_aSt }
};

 const SignalConfig_St_t		PwrConsum_P9_SigConf_ast[TWO_E] = 
{
	{ ON_E, 	ONE_SEG_E,	PwrConsum_P9_On_SegConf_aSt  },
	{ OFF_BAR, 	ONE_SEG_E,	PwrConsum_P9_Off_SegConf_aSt }
};

 const SignalConfig_St_t		PwrConsum_P10_SigConf_ast[TWO_E] = 
{
	{ ON_E,		ONE_SEG_E,	PwrConsum_P10_On_SegConf_aSt  },
	{ OFF_BAR,	ONE_SEG_E,	PwrConsum_P10_Off_SegConf_aSt }
};



 const SegConfig_St_t	PwrConsum_P6_On_SegConf_aSt[ONE_SEG_E] = 
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	{ &SEG32,	0x0EU,		0x01U }
};

 const SegConfig_St_t	PwrConsum_P7_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG31,	0x0EU,		0x01U}
};

 const SegConfig_St_t	PwrConsum_P8_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG30,	0x0DU,		0x02U}
};

 const SegConfig_St_t	PwrConsum_P9_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG30,	0x0BU,		0x04U}
};

 const SegConfig_St_t	PwrConsum_P10_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG30,	0x07U,		0x08U}
};

 const SegConfig_St_t	PwrConsum_P6_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG32,0x0EU,0x00U}
};

 const SegConfig_St_t	PwrConsum_P7_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG31,0x0EU,0x00U}
};


 const SegConfig_St_t	PwrConsum_P8_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG30,0x0DU,0x00U}
};

 const SegConfig_St_t	PwrConsum_P9_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG30,0x0BU,0x00U}
};

 const SegConfig_St_t	PwrConsum_P10_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG30,0x07U,0x00U}
};





/********************************************************EOF***********************************************************/