/***********************************************************************************************************************
* File Name    : Speed_Conf.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 28/11/2021
***********************************************************************************************************************/

#ifndef SPEED_CONF_H
#define SPEED_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
/*
		TOP text configration
*/
extern  const SignalsValue_St_t		TOP_text_SiganlValue_ast[TOP_TEXT_SIG_LEN];
extern  const SignalConfig_St_t		TOP_text_Sig_ast[TWO_E];
extern  const SegConfig_St_t		TOP_text_on_Sig_ast[ONE_SEG_E];
extern  const SegConfig_St_t		TOP_text_off_Sig_ast[ONE_SEG_E];




/*
		SPEED digit configration
*/
extern  const SignalsValue_St_t			Speed_SignalsValue_aSt[SPEED_SIG_LEN];


extern  const SignalConfig_St_t			SpeedSig1Conf_ast[TWELVE_E];
extern  const SignalConfig_St_t			SpeedSig2Conf_ast[THIRTEEN_E];
extern  const SignalConfig_St_t			SpeedSig3Conf_ast[TWO_E];

extern  const SegConfig_St_t			Speed_S1_0_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S1_1_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S1_2_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S1_3_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S1_4_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S1_5_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S1_6_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S1_7_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S1_8_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S1_9_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S1_Off_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S1_H_SegConf_aSt[FOUR_SEG_E];

extern  const SegConfig_St_t			Speed_S2_0_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_1_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_2_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_3_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_4_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_5_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_6_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_7_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_8_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_9_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_Off_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_G_SegConf_aSt[FOUR_SEG_E];
extern  const SegConfig_St_t			Speed_S2_C_SegConf_aSt[FOUR_SEG_E];

extern  const SegConfig_St_t			Speed_S3_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			Speed_S3_1_SegConf_aSt[ONE_SEG_E];


#endif /* SPEED_CONF_H */


