/***********************************************************************************************************************
* File Name    : RAM_Vector.h
* Version      : 01
* Description  : This file contains the Declaration of RAM allocated ISR Fptr.
* Created By   : Sandeep K Y
* Creation Date: 01/02/2021
***********************************************************************************************************************/

#ifndef RAM_VECTOR_H
#define RAM_VECTOR_H

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
extern void (*RAM_INTWDTI_ISR)(void);
extern void (*RAM_INTLVI_ISR)(void);
extern void (*RAM_INTP0_ISR)(void);
extern void (*RAM_INTP1_ISR)(void);
extern void (*RAM_INTP2_ISR)(void);
extern void (*RAM_INTP3_ISR)(void);
extern void (*RAM_INTP4_ISR)(void);
extern void (*RAM_INTP5_ISR)(void);
extern void (*RAM_INTCLM_ISR)(void);
extern void (*RAM_INTCSI00_ISR)(void);
extern void (*RAM_INTCSI01_ISR)(void);
extern void (*RAM_INTDMA0_ISR)(void);
extern void (*RAM_INTDMA1_ISR)(void);
extern void (*RAM_INTRTC_ISR)(void);
extern void (*RAM_INTIT_ISR)(void);
extern void (*RAM_INTLT0_ISR)(void);
extern void (*RAM_INTLR0_ISR)(void);
extern void (*RAM_INTLS0_ISR)(void);
extern void (*RAM_INTPLR0_ISR)(void);
extern void (*RAM_INTSG_ISR)(void);
extern void (*RAM_INTTM00_ISR)(void);
extern void (*RAM_INTTM01_ISR)(void);
extern void (*RAM_INTTM02_ISR)(void);
extern void (*RAM_INTTM03_ISR)(void);
extern void (*RAM_INTAD_ISR)(void);
extern void (*RAM_INTLT1_ISR)(void);
extern void (*RAM_INTLR1_ISR)(void);
extern void (*RAM_INTLS1_ISR)(void);
extern void (*RAM_INTPLR1_ISR)(void);
extern void (*RAM_INTIIC11_ISR)(void);
extern void (*RAM_INTTM04_ISR)(void);
extern void (*RAM_INTTM05_ISR)(void);
extern void (*RAM_INTTM06_ISR)(void);
extern void (*RAM_INTTM07_ISR)(void);
extern void (*RAM_INTC0ERR_ISR)(void);
extern void (*RAM_INTC0WUP_ISR)(void);
extern void (*RAM_INTC0REC_ISR)(void);
extern void (*RAM_INTC0TRX_ISR)(void);
extern void (*RAM_INTTM10_ISR)(void);
extern void (*RAM_INTTM11_ISR)(void);
extern void (*RAM_INTTM12_ISR)(void);
extern void (*RAM_INTTM13_ISR)(void);
extern void (*RAM_INTMD_ISR)(void);
extern void (*RAM_INTFL_ISR)(void);
extern void (*RAM_INTTM14_ISR)(void);
extern void (*RAM_INTTM15_ISR)(void);
extern void (*RAM_INTTM16_ISR)(void);
extern void (*RAM_INTTM17_ISR)(void);
extern void (*RAM_INTTM20_ISR)(void);
extern void (*RAM_INTTM21_ISR)(void);
extern void (*RAM_INTTM22_ISR)(void);
extern void (*RAM_INTTM23_ISR)(void);
extern void (*RAM_INTTM24_ISR)(void);
extern void (*RAM_INTTM26_ISR)(void);

#endif /*RAM_VECTOR_H*/