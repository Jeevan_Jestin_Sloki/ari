﻿

#include "hmi_config_can.h"


HMI_CONF_MSG_1_t	  HMI_CONF_MSG_1;


 uint32_t Deserialize_HMI_CONF_MSG_1(HMI_CONF_MSG_1_t* message, const uint8_t* data)
{
  message->Brightness_u8 = ((data[0] & (SIGNLE_READ_Mask8))) + HMI_CONF_MSG_1_CANID_BRIGHTNESS_U8_OFFSET;
   return HMI_CONF_MSG_1_ID; 
}

/*----------------------------------------------------------------------------*/

void Clear_HMIConf_CANSignals(void)
{
	HMI_CONF_MSG_1.Brightness_u8 = 0;
	return;
}

/*----------------------------------------------------------------------------*/