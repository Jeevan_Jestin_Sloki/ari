/***********************************************************************************************************************
* File Name    : PowerConsum_Disp.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 04/12/2021
***********************************************************************************************************************/

#ifndef POWER_CONSUM_DISP_H
#define POWER_CONSUM_DISP_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" /*TODO: Dileepa*/
#include "Cluster_Conf.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define TOTAL_PWR_CONSUM_BARS				10
#define SAFE_MODE_PWR_CONSUM_RANGE			3

#define PC_RED_BAR_1_POS				4
#define PC_RED_BAR_2_POS				5
//#define PC_RED_BAR_3_POS				10

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	PWR_CONSUM_DISPLAY_E,
	PWR_CONSUM_HOLD_E,
}PwrConsum_DispState_En_t;

typedef enum
{
	PC_GREEN_BAR_DISP_E,
	PC_RED_BAR_DISP_E,
}PC_BarDispState_En_t;

/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern bool 			InitialPowerConsum_Disp_b;
extern PC_BarDispState_En_t	PC_BarDispState_En;
extern uint16_t             	Valid_PowerConsum_u16; 

/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
extern void   Display_PowerConsum(uint16_t, ClusterSignals_En_t );
uint16_t      Validate_PowerConsum(uint16_t);
uint32_t      Get_PwrConsum_Bars(uint16_t);
extern void   DispUSM_PwrConsum(ClusterSignals_En_t, uint16_t, bool); /*To display the Power consumption @ 
										Red Zone*/


#endif /* POWER_CONSUM_DISP_H */


