/***********************************************************************************************************************
* File Name    : Flash_Params.c
* Version      : 01
* Description  : This file contains the definition of functions and data related to Flash parameters Store 
				 and Restore.
* Created By   : Dileepa B S 
* Creation Date: 05/01/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Flash_Params.h"
#include "pfdl_user.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
DFB0_Params_St_t	DFB0_Params_St = 
{
		0,	/*ODOmeter*/
		0,	/*WH/KM*/
		0,	/*SOC*/
};

uint8_t DFB0_Bytes_au8[DFB0_TOTAL_BYTES] = {0x00U};


/***********************************************************************************************************************
* Function Name: StoreODOmeterToFlash
* Description  : This function stores the Present ODOmeter value to the Data Flash.
* Arguments    : uint32_t PresentODO_u32
* Return Value : None
***********************************************************************************************************************/
void StoreODOmeterToFlash(uint32_t PresentODO_u32)
{
	uint16_t BytesCount_u16 = 0;
	
	DFB0_Params_St.F_ODOmeter_u32 = PresentODO_u32;
	
	FDL_Read(FLASH_BLOCK_0, FLASH_BLOCK0_START_POS, DFB0_TOTAL_BYTES);
	
	for(BytesCount_u16 = 0; BytesCount_u16 < DFB0_TOTAL_BYTES; BytesCount_u16++)
	{
		DFB0_Bytes_au8[BytesCount_u16] = dubReadBuffer[BytesCount_u16];
	}
	
	DFB0_Bytes_au8[0] = ((DFB0_Params_St.F_ODOmeter_u32 >> 24) & (0xFFU));
	DFB0_Bytes_au8[1] = ((DFB0_Params_St.F_ODOmeter_u32 >> 16) & (0xFFU));
	DFB0_Bytes_au8[2] = ((DFB0_Params_St.F_ODOmeter_u32 >> 8) & (0xFFU));
	DFB0_Bytes_au8[3] = ((DFB0_Params_St.F_ODOmeter_u32) & (0xFFU));

	for(BytesCount_u16 = 0; BytesCount_u16 < DFB0_TOTAL_BYTES; BytesCount_u16++)
	{
		dubWriteBuffer[BytesCount_u16] = DFB0_Bytes_au8[BytesCount_u16];
	}
	
	FDL_Erase(FLASH_BLOCK_0, 1U);
	
	FDL_Write(FLASH_BLOCK_0, FLASH_BLOCK0_START_POS, DFB0_TOTAL_BYTES);
	
	return;
}


/***********************************************************************************************************************
* Function Name: RestoreODOmtereFromFlash
* Description  : This function restores the present ODOmeter value from the Data Falsh.
* Arguments    : None
* Return Value : uint32_t PresentODO_u32
***********************************************************************************************************************/
uint32_t RestoreODOmtereFromFlash(void)
{
	uint32_t PresentODO_u32 = 0;
	uint16_t BytesCount_u16 = 0;
	
	FDL_Read(FLASH_BLOCK_0, FLASH_BLOCK0_START_POS, DFB0_TOTAL_BYTES);
	
	for(BytesCount_u16 = 0; BytesCount_u16 < DFB0_TOTAL_BYTES; BytesCount_u16++)
	{
		DFB0_Bytes_au8[BytesCount_u16] = dubReadBuffer[BytesCount_u16];
	}
	
	PresentODO_u32 = (((((uint32_t)DFB0_Bytes_au8[0]) & 0xFFU ) << 24) | ((((uint32_t)DFB0_Bytes_au8[1]) & 0xFFU ) << 16) | \
					((((uint32_t)DFB0_Bytes_au8[2]) & 0xFFU ) << 8) | (((uint32_t)DFB0_Bytes_au8[3]) & 0xFFU ));
	
	if(PresentODO_u32 > MAX_ODOMETER)
	{
		PresentODO_u32 = 0;
	}
	
	DFB0_Params_St.F_ODOmeter_u32 = PresentODO_u32;
	
	return PresentODO_u32;
}


/***********************************************************************************************************************
* Function Name: StoreWhKmToFlash
* Description  : This function stores the last session WH/KM value to the Data Flash.
* Arguments    : uint16_t LastSessionWhKm_u16
* Return Value : None
***********************************************************************************************************************/
void StoreWhKmToFlash(uint16_t LastSessionWhKm_u16)
{
	uint16_t BytesCount_u16 = 0;
	
	DFB0_Params_St.F_Wh_Km_u16 = LastSessionWhKm_u16;
	
	FDL_Read(FLASH_BLOCK_0, FLASH_BLOCK0_START_POS, DFB0_TOTAL_BYTES);
	
	for(BytesCount_u16 = 0; BytesCount_u16 < DFB0_TOTAL_BYTES; BytesCount_u16++)
	{
		DFB0_Bytes_au8[BytesCount_u16] = dubReadBuffer[BytesCount_u16];
	}
	
	DFB0_Bytes_au8[4] = ((DFB0_Params_St.F_Wh_Km_u16 >> 8) & (0xFFU));
	DFB0_Bytes_au8[5] = ((DFB0_Params_St.F_Wh_Km_u16) & (0xFFU));

	FDL_Erase(FLASH_BLOCK_0, 1U);

	for(BytesCount_u16 = 0; BytesCount_u16 < DFB0_TOTAL_BYTES; BytesCount_u16++)
	{
		dubWriteBuffer[BytesCount_u16] = DFB0_Bytes_au8[BytesCount_u16];
	}
	
	FDL_Write(FLASH_BLOCK_0, FLASH_BLOCK0_START_POS, DFB0_TOTAL_BYTES);
	
	return;
}



/***********************************************************************************************************************
* Function Name: RestoreWhKmFromFlash
* Description  : This function restores the last session WH/KM value from the Data Flash.
* Arguments    : None
* Return Value : uint16_t LastSessionWhKm_u16
***********************************************************************************************************************/
uint16_t RestoreWhKmFromFlash(void)
{
	uint16_t LastSessionWhKm_u16 	= 0;
	uint16_t BytesCount_u16 		= 0;
	
	FDL_Read(FLASH_BLOCK_0, FLASH_BLOCK0_START_POS, DFB0_TOTAL_BYTES);
	
	for(BytesCount_u16 = 0; BytesCount_u16 < DFB0_TOTAL_BYTES; BytesCount_u16++)
	{
		DFB0_Bytes_au8[BytesCount_u16] = dubReadBuffer[BytesCount_u16];
	}
	
	LastSessionWhKm_u16 = (((((uint16_t)DFB0_Bytes_au8[4]) & 0xFFU ) << 8) | (((uint16_t)DFB0_Bytes_au8[5]) & 0xFFU ));
	
	if(LastSessionWhKm_u16 > MAX_WH_KM)
	{
		LastSessionWhKm_u16 = 0;
	}
	
	DFB0_Params_St.F_Wh_Km_u16 = LastSessionWhKm_u16;
	
	return LastSessionWhKm_u16; 
}



/***********************************************************************************************************************
* Function Name: StoreSOCToFlash
* Description  : This function stores the Present SOC value to the Data-Flash.
* Arguments    : uint8_t PresentSOC_u8
* Return Value : None
***********************************************************************************************************************/
void StoreSOCToFlash(uint8_t PresentSOC_u8)
{
	uint16_t BytesCount_u16 = 0;
	
	DFB0_Params_St.F_SOC_u8 = PresentSOC_u8;
	
	FDL_Read(FLASH_BLOCK_0, FLASH_BLOCK0_START_POS, DFB0_TOTAL_BYTES);
	
	for(BytesCount_u16 = 0; BytesCount_u16 < DFB0_TOTAL_BYTES; BytesCount_u16++)
	{
		DFB0_Bytes_au8[BytesCount_u16] = dubReadBuffer[BytesCount_u16];
	}
	
	DFB0_Bytes_au8[6] = ((DFB0_Params_St.F_SOC_u8) & (0xFFU));

	FDL_Erase(FLASH_BLOCK_0, 1U);
	
	for(BytesCount_u16 = 0; BytesCount_u16 < DFB0_TOTAL_BYTES; BytesCount_u16++)
	{
		dubWriteBuffer[BytesCount_u16] = DFB0_Bytes_au8[BytesCount_u16];
	}
	
	FDL_Write(FLASH_BLOCK_0, FLASH_BLOCK0_START_POS, DFB0_TOTAL_BYTES);
	
	return;
}



/***********************************************************************************************************************
* Function Name: RestoreSOCFromFlash
* Description  : This function restores the present SOC value from the Data-Flash.
* Arguments    : None
* Return Value : uint8_t PresentSOC_u8
***********************************************************************************************************************/
uint8_t RestoreSOCFromFlash(void)
{
	uint16_t 	BytesCount_u16 	= 0;
	uint8_t 	PresentSOC_u8 	= 0;
	
	FDL_Read(FLASH_BLOCK_0, FLASH_BLOCK0_START_POS, DFB0_TOTAL_BYTES);
	
	for(BytesCount_u16 = 0; BytesCount_u16 < DFB0_TOTAL_BYTES; BytesCount_u16++)
	{
		DFB0_Bytes_au8[BytesCount_u16] = dubReadBuffer[BytesCount_u16];
	}
	
	PresentSOC_u8 = ((DFB0_Bytes_au8[6]) & (0xFFU));
	
	if(PresentSOC_u8 > MAX_SOC)
	{
		PresentSOC_u8 = 0;
	}
	
	return PresentSOC_u8;
}


/********************************************************EOF***********************************************************/