/***********************************************************************************************************************
* File Name    : TelltaleDisp.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 30/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "TelltaleDisp.h"
#include "SegDispWrite.h"
#include "delay_flags.h"
#include "PowerConsum_Disp.h"
#include "SOC_Disp.h"
#include "DataBank.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/	


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
	Telltales_Conf_St_t	Telltales_Conf_St[TOTAL_TELLTALE] = 
	{
	     /*Telltale Signal Enum    SignalValue  SignalPrevValue  SignalBlinkState */
		{ LEFT_INDICATOR_E,	  0x00U,	0x00U,		FALSE },
		{ RIGHT_INDICATOR_E,	  0x00U,	0x00U,		FALSE },
		{ LOW_BEAM_IND_E,	  0x00U,	0x00U,		FALSE },
		{ HIGH_BEAM_IND_E,	  0x00U,	0x00U,		FALSE },
		{ BLUETOOTH_E,		  0x00U,	0x00U,		FALSE },
		{ REGEN_BRAKE_E,	  0x00U,	0x00U,		FALSE },
		{ WARNING_INDICATOR_E,	  0x00U,	0x00U,		FALSE },
		{ SERVICE_IND_E,	  0x00U,	0x00U,		FALSE },
		{ NEUTRAL_MODE_IND_E,	  0x00U,	0x00U,		FALSE },
		{ ECO_MODE_IND_E,	  0x00U,	0x00U,		FALSE },
		{ SPORTS_MODE_IND_E,	  0x00U,	0x00U,		FALSE },
		{ REVERSE_MODE_IND_E,	  0x00U,	0x00U,		FALSE },
		{ SIDE_STAND_IND_E,	  0x00U,	0x00U,		FALSE },
		{ SAFE_MODE_IND_E,	  0x00U,	0x00U,		FALSE },
		{ KILL_SWITCH_IND_E,	  0x00U,	0x00U,		FALSE },
		{ BATT_FLT_IND_E,	  0x00U,	0x00U,		FALSE },
		{ MOTOR_FLT_IND_E,	  0x00U,	0x00U,		FALSE }
	};

/*
	Present SOC and PowerConsumption value is updated to the variables 
*/
uint32_t SOC_BarsVal_u32 	= 0;
uint16_t PwrConsum_Bars_u16 	= 0;

bool InitialDispTellTales_b 	= false;


/***********************************************************************************************************************
* Function Name: Indicate_Telltales
* Description  : This function Turns-ON, Turs-OFF and Blinks the Telltales.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
	void Indicate_Telltales(void)
	{
		/*Update the Present Value of Telltale Signals*/
		Telltales_Conf_St[LEFT_IND_INDEX].Telltale_Value_u8 	= ClusterSignals_St.LeftIndicatorSig_u8;
		Telltales_Conf_St[RIGHT_IND_INDEX].Telltale_Value_u8 	= ClusterSignals_St.RightIndicatorSig_u8;
		Telltales_Conf_St[LOW_BEAM_INDEX].Telltale_Value_u8 	= ClusterSignals_St.LowBeamSig_u8;
		Telltales_Conf_St[HIGH_BEAM_INDEX].Telltale_Value_u8 	= ClusterSignals_St.HighBeamSig_u8;
		Telltales_Conf_St[BLE_INDEX].Telltale_Value_u8 		= ClusterSignals_St.BLE_Icon_Sig_u8;
		Telltales_Conf_St[REGEN_BRAKE_INDEX].Telltale_Value_u8 	= ClusterSignals_St.RegenSig_u8;
		Telltales_Conf_St[WARNING_INDEX].Telltale_Value_u8 	= ClusterSignals_St.WarningSig_u8;
		Telltales_Conf_St[SERV_REM_INDEX].Telltale_Value_u8 	= ClusterSignals_St.ServiceReminderSig_u8;
		Telltales_Conf_St[NEUTRAL_MODE_INDEX].Telltale_Value_u8 = ClusterSignals_St.NeutralModeSig_u8;
		Telltales_Conf_St[ECO_MODE_INDEX].Telltale_Value_u8 	= ClusterSignals_St.EcoModeSig_u8;
		Telltales_Conf_St[SPORTS_MODE_INDEX].Telltale_Value_u8 	= ClusterSignals_St.SportsModeSig_u8;
		Telltales_Conf_St[REVERSE_MODE_INDEX].Telltale_Value_u8 = ClusterSignals_St.ReverseModeSig_u8;
		Telltales_Conf_St[SIDE_STAND_INDEX].Telltale_Value_u8 	= ClusterSignals_St.SideStandSig_u8;
		Telltales_Conf_St[SAFE_MODE_INDEX].Telltale_Value_u8 	= ClusterSignals_St.SafeModeSig_u8;
		Telltales_Conf_St[KILL_SWITCH_INDEX].Telltale_Value_u8 	= ClusterSignals_St.KillSwitchSig_u8;
		Telltales_Conf_St[BATT_FLT_INDEX].Telltale_Value_u8 	= ClusterSignals_St.BatteryTextSig_u8;
		Telltales_Conf_St[MOTOR_FLT_INDEX].Telltale_Value_u8 	= ClusterSignals_St.MotorTextSig_u8;
		
		Turn_ON_Telltales();	/*Turn-OFF the Telltales*/
		Turn_OFF_Telltales();  	/*Turn-ON the Telltales*/
		Blink_Telltales();      /*Blink the Telltales*/
		return;
	}


/***********************************************************************************************************************
* Function Name: Turn_ON_Telltales
* Description  : This function Turns-ON the Telltales.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
	void Turn_ON_Telltales(void)
	{
		uint16_t Telltales_Count_u16 = 0; /*To count Total telltale signals*/
		
		for(Telltales_Count_u16 = 0; Telltales_Count_u16 < TOTAL_TELLTALE; Telltales_Count_u16++)
		{
			if((Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8 != \
				Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8) || (InitialDispTellTales_b == false))
			{
				if(TURN_ON == Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8)
				{
					Telltales_Conf_St[Telltales_Count_u16].TelltaleBlink_b = FALSE; /*Clear signal blink state*/
					
					/*Write_SEG(SignalEnum, ON);*/
					Write_SEG(((ClusterSignals_En_t)Telltales_Conf_St[Telltales_Count_u16].Telltales_En),(uint32_t)ON);
					
					/*Signal Previous value is assigned with signal present value*/
					Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8 = \
						Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8;
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
		}
		return;
	}

/***********************************************************************************************************************
* Function Name: Turn_OFF_Telltales
* Description  : This function Turns-OFF the Telltales.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
	void Turn_OFF_Telltales(void)
	{
		uint16_t Telltales_Count_u16 = 0; /*To count Total telltale signals*/
		
		for(Telltales_Count_u16 = 0; Telltales_Count_u16 < TOTAL_TELLTALE; Telltales_Count_u16++)
		{
			if((Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8 != \
				Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8) || (InitialDispTellTales_b == false))
			{
				if(TURN_OFF == Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8)
				{
					Telltales_Conf_St[Telltales_Count_u16].TelltaleBlink_b = FALSE; /*Clear signal blink state*/
					
					/*Write_SEG(SignalEnum, OFF);*/
					Write_SEG(((ClusterSignals_En_t)Telltales_Conf_St[Telltales_Count_u16].Telltales_En),(uint32_t)OFF);
					
					/*Signal Previous value is assigned with signal present value*/
					Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8 = \
						Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8;
				}
				else
				{
					;
				}
			}
			else
			{
				;
			}
		}
		
		return;
	}


/***********************************************************************************************************************
* Function Name: Blink_Telltales
* Description  : This function Blinks the Telltales.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
	void Blink_Telltales(void)
	{
		uint16_t Telltales_Count_u16 = 0; /*To count Total telltale signals*/
		
		static bool TelltaleState_b  = OFF;
		
		/*Condition is true for every 500ms*/
		if(Delay_Flag_500ms_b)
		{
			for(Telltales_Count_u16 = 0; Telltales_Count_u16 < TOTAL_TELLTALE; Telltales_Count_u16++)
			{
				/*Condition is true, if Signal presen value is not equal to the signal previous value, or 
			          if Signal blink state is true*/
				if((Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8 != \
					Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8)|| \
						(TRUE == Telltales_Conf_St[Telltales_Count_u16].TelltaleBlink_b))
				{
					/*Condition is true, if Signal value is equal to blink value or if 
					  Signal blink state is true*/
					if((BLINK == Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8) || \
						(TRUE == Telltales_Conf_St[Telltales_Count_u16].TelltaleBlink_b))
					{
						
						Telltales_Conf_St[Telltales_Count_u16].TelltaleBlink_b = TRUE;  /*Set signal blink state*/
						
						if(!TelltaleState_b)
						{
							/*Write_SEG(SignalEnum, ON);*/
							Write_SEG(((ClusterSignals_En_t)Telltales_Conf_St[Telltales_Count_u16].Telltales_En),(uint32_t)ON);
						}
						else
						{
							/*Write_SEG(SignalEnum, OFF);*/
							Write_SEG(((ClusterSignals_En_t)Telltales_Conf_St[Telltales_Count_u16].Telltales_En),(uint32_t)OFF);
						}
						
						/*Signal Previous value is assigned with signal present value*/
						Telltales_Conf_St[Telltales_Count_u16].Telltale_PrevValue_u8 = \
							Telltales_Conf_St[Telltales_Count_u16].Telltale_Value_u8;
					}
					else
					{
						;
					}
				}
			}
			
			/*
				To Blink the Red Bar of Powerconsumption  
			*/
			if(PC_BarDispState_En == PC_RED_BAR_DISP_E)
			{
				PwrConsum_Bars_u16 = Valid_PowerConsum_u16;
							/*Present updated valid  Power Consumption value is taken from the 
								PowerConsump display module*/
				DispUSM_PwrConsum(POWER_CONSUMP_E,PwrConsum_Bars_u16,TelltaleState_b);
							/*Display the RED-ZONE power consumption*/
			}
			else
			{
				;
			}
			
			/*
				To Blink the Red Bar of %SOC  
			*/
			if(SOC_BarDispState_En == SOC_RED_BAR_DISP_E)
			{
				SOC_BarsVal_u32 = SOC_BAR_Value_u32;
							/*Present updated Equivalent bars value for the corresponding SOC is 
								taken from the SOC display Module*/
				DispUSM_DischargeSOC(BATT_SOC_E, SOC_BarsVal_u32, TelltaleState_b);
							/*Display the RED-ZONE Battery SOC*/
			}
			else
			{
				;
			}
			
			/*To Blink, Switching of telltale state to ON and OFF*/
			if(TelltaleState_b)
			{
				TelltaleState_b = OFF;
			}
			else
			{
				TelltaleState_b = ON;
			}
			
			Delay_Flag_500ms_b = false;
		}
		else
		{
			;
		}
		return;
	}


/***********************************************************************************************************************
* Function Name: ClrTelltalesStates
* Description  : This function Clears the Telltales Previos value and telltales blink state.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
	void ClrTelltalesStates(void)
	{
		uint16_t TelltalesCount_u16 = 0;
		
		for(TelltalesCount_u16 = 0; TelltalesCount_u16 < TOTAL_TELLTALE; TelltalesCount_u16++)
		{
			Telltales_Conf_St[TelltalesCount_u16].Telltale_PrevValue_u8 	= RESET;
			Telltales_Conf_St[TelltalesCount_u16].TelltaleBlink_b 		= FALSE;
		}
		InitialDispTellTales_b 	= false;
		return;
	}

/********************************************************EOF***********************************************************/