/***********************************************************************************************************************
* File Name    : ODO_Calc.h
* Version      : 01
* Description  : The file calculate the ODO 
* Created By   : Jeevan Jestin N
* Creation Date: 26/05/2022
***********************************************************************************************************************/

#ifndef ODO_CALC_H
#define ODO_CALC_H
/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"
#include"Flash_Param.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
#define ODO_FLASH_BLOCK         0
#define ODO_TOTAL_FLSHA_BLOCK   1
#define ODO_START_POS           0

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


#define HOUR_TO_MILLISEC					3600000U
#define KM_TO_MM                            1000000
#define ODO_BYTES_LEN						4U


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
typedef struct
{
    uint32_t Main_ODOmeter_u32;
    uint32_t TripA_ODOmeter_u32;
    uint32_t TripB_ODOmeter_u32;
}ODOmeter_St_t;

extern uint32_t 		PreviousTime_ms_u32;
extern uint8_t			Service_reminder_u8;

extern ODOmeter_St_t	ODOmeter_St;

/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Calculate_ODOmeter(uint8_t);
extern uint32_t Get_ODOmeter(void);
extern void StoreODOtoFlash(void);
extern void RestoreODOfromFlash(void);
extern void ClearTripA_ODO(void);
extern void ClearTripB_ODO(void);
extern void Read_OdoMeter(Flash_Param_St_t* Flash_Param_pSt);
extern void Write_OdoMeter(Flash_Param_St_t* Flash_Param_pSt);
#endif