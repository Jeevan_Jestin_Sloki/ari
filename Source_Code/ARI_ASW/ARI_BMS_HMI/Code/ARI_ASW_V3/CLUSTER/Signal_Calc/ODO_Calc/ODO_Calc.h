/*******************************************************************************
|-------------------------------------------------------------------------------
|   FILE DESCRIPTION
|-------------------------------------------------------------------------------
|    File           :`ODO_Calc.h
|    Project        : OHM Automotive
|    Description    : The file contains the ODO calulation .
|-------------------------------------------------------------------------------
|
|-------------------------------------------------------------------------------
|               A U T H O R   I D E N T I T Y
|-------------------------------------------------------------------------------
|   Date              Name                        Company
| ----------     ---------------     -----------------------------------
| 16/12/2022       Jeevan Jestin N             Sloki Software Technologies LLP
|-------------------------------------------------------------------------------
|******************************************************************************/
#ifndef ODO_CALC_H
#define ODO_CALC_H

/*******************************************************************************
 *  HEADER FILE INCLUDES
 ******************************************************************************/
#include"App_typedefs.h"
/*******************************************************************************
 *  MACRO DEFINITION
 ******************************************************************************/
#define ODO_FLASH_BLOCK         4
#define ODO_TOTAL_FLSHA_BLOCK   1
#define ODO_START_POS           0

#define SPEED_PULSE_OVERFLOW_VALUE                  0xFFFFFFFF
#define PIE	3.143

#define GEAR_RATIO									4
#define PULSE_COUNT_PER_ROTATION	                (GEAR_RATIO * 5)
#define WHEEL_DIAMETER_MM			                467 /*Actual : 466.7 mm*/	
#define PIE							                3.143
#define WHEEL_CIRCUMFERENCE_MM		                (WHEEL_DIAMETER_MM*PIE)
/*******************************************************************************
 *  GLOBAL VARIABLES DEFNITION 
 ******************************************************************************/

/*******************************************************************************
 *  STRUCTURE AND ENUM DEFNITION 
 ******************************************************************************/
typedef struct
{
    uint32_t Main_ODOmeter_u32;
    uint32_t TripA_ODOmeter_u32;
    uint32_t TripB_ODOmeter_u32;
}ODOmeter_St_t;
/*******************************************************************************
 *  STATIC FUNCTION PROTOTYPES
 ******************************************************************************/
extern ODOmeter_St_t	ODOmeter_St;
extern uint32_t TripA_Reset_Value_u32;
extern uint32_t TripB_Reset_Value_u32;
/*******************************************************************************
 *  FUNCTION DEFINITIONS
 ******************************************************************************/
void ResetTripA(uint32_t Main_ODO_u32);
void ResetTripB(uint32_t Main_ODO_u32);
extern uint32_t Get_ODOmeter(void);
extern uint32_t Get_ODOmeter_in_mm(void);
uint32_t Get_TripA(void);
uint32_t Get_TripB(void);
uint32_t Update_ODOmeter(uint32_t *);
void Calculate_ODOmeter(void);
extern void Get_Distance_for_1_Pulse(void);
void ResetTheODOmeterData(void);
void StoreODOtoFlash(void);
void RestoreODOfromFlash(void);
#endif
/*---------------------- End of File -----------------------------------------*/