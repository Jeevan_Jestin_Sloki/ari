/***********************************************************************************************************************
* File Name    : Cluster_Conf.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 02/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"
#include "TelltaleConf.h"
#include "Speed_Conf.h"
#include "ODO_Conf.h"
#include "RangeKm_Conf.h"
#include "Time_Conf.h"
#include "SOC_Conf.h"
#include "PowerConsum_Conf.h"
#include "Gear_Conf.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Structure declaration
***********************************************************************************************************************/
const ClusterSigConf_St_t		ClusterSigConf_aSt[TOTAL_SIGNALS_E] = 
{
	/* Cluster Signal   	Signal Length            Signal Value Configuration  */
	{ COMMON_SIG_E,    	COMMON_SEG_SIG_LEN,      Common_SignalsValue_aSt      },
	{ SPEED_E,          	SPEED_SIG_LEN,           Speed_SignalsValue_aSt       },
	{ RANGE_KM_E,       	RANGE_SIG_LEN,           Range_SignalsValue_aSt       },
	{ LEFT_IND_E,      	LEFT_IND_SIG_LEN,        LeftInd_SignalsValue_aSt     },
	{ RIGHT_IND_E,      	RIGHT_IND_SIG_LEN,       RightInd_SignalsValue_aSt    },
	{ HIGH_BEAM_E,      	HIGH_BEAM_SIG_LEN,       HighBeamInd_SignalsValue_aSt },
	{ BLE_E,           	BLE_SIG_LEN,             BLE_Ind_SignalsValue_aSt     },
	{ WARNING_IND_E,   	WARN_IND_SIG_LEN,        Warning_SignalsValue_aSt     },
	{ SERV_REM_E,      	SERV_REM_SIG_LEN,        ServRem_SignalsValue_aSt     },
		
	
	{ SIDE_STAND_E,    	SIDE_STAND_SIG_LEN,      SideStand_SignalsValue_aSt   },
	{ KILL_SWITCH_E,    	KILL_SWITCH_SIG_LEN,     KillSwitch_SignalsValue_aSt  },
	{ BATT_SOC_BAR_E,      	BATT_SOC_BAR_SIG_LEN,    SOC_SignalsValue_aSt         },
	{ BATT_SOC_DIGIT_E,	BATT_SOC_DIGIT_SIG_LEN,  SOC_percen_SignalsValue_aSt  },
	{ POWER_CONSUMP_E, 	POWER_CONSUMP_SIG_LEN,   PwrConsum_SignalsValue_aSt   },
	{ ODO_E,           	ODO_SIG_LEN,             ODO_SignalsValue_aSt         },
	{ MINUTES_TIME_HB_E,   	MINUTES_TIME_SIG_LEN,    MinutesTimeHB_SignalsValue_aSt },
	{ MINUTES_TIME_LB_E,   	MINUTES_TIME_SIG_LEN,    MinutesTimeLB_SignalsValue_aSt },
	{ HOURS_TIME_E,     	HOURS_TIME_SIG_LEN,      HoursTime_SignalsValue_aSt   },
	{ TEXT_AM_E,  		TEXT_AM_SIG_LEN,  	 Text_AM_SignalsValue_aSt     },
	{ TEXT_PM_E,  		TEXT_PM_SIG_LEN,  	 Text_PM_SignalsValue_aSt     },

	{ TIME_COLON_E,     	TIME_COLON_SIG_LEN,      Colon_SignalsValue_aSt       },
	{ MOTOR_FAULT_E,    	MOTOR_FLT_SIG_LEN,       MotorFlt_SignalsValue_aSt    },
	{ BATTERY_FAULT_E,   	BATTERY_FLT_SIG_LEN,      Battery_fault_SignalsValue_aSt},
	{ PARKING_BREAK_E,	    	PARKING_BREAK_LEN,	 ParkingBreak_SignalsValue_aSt     },
	{ ODO_TEXT_E,		ODO_TEXT_SIG_LEN,	 ODO_TEXT_SignalsValue_aSt    },
	{ TRIP_TEXT_E,		TRIP_TEXT_SIG_LEN,   	 TRIP_Text_SignalsValue_aSt   },
	{ TEXT_A_E,		TEXT_A_SIG_LEN,		 Text_A_SignalsValue_aSt      },
	{ TEXT_B_E,		TEXT_B_SIG_LEN,		 Text_B_SignalsValue_aSt      },
	{ POWER_W_IND_E,	POWER_IND_SIG_LEN,	 Power_ind_SiganlValue_ast    },
	{ TOP_TEXT_E,		TOP_TEXT_SIG_LEN,	 TOP_text_SiganlValue_ast     },

	{CHARGING_STATUS_E,	CH_STATUS_SIG_LEN,	 CH_status_SignalsValue_aSt   },
	{ PERCENTAGE_TEXT_E, PERCEN_TEXT_SIG_LEN,     PercenText_SignalsValue_aSt  },
    { PARKING_TEXT_E,    PARKING_TEXT_SIG_LEN,    ParkingText_SignalsValue_aSt  },
	{ PARKING_MODE_E,    PARKING_MODE_SIG_LEN,    ParkingMode_SignalsValue_aSt  },
    { REVERSE_TEXT_E,  	REVERSE_TEXT_SIG_LEN,    ReverseText_SignalsValue_aSt },
	{ REVERSE_MODE_E,  	REVERSE_MODE_SIG_LEN,    ReverseMode_SignalsValue_aSt },
    { NEUTRAL_TEXT_E,  	NEUTRAL_TEXT_SIG_LEN,    NeutralText_SignalsValue_aSt },
    { NEUTRAL_MODE_E,  	NEUTRAL_MODE_SIG_LEN,    NeutralMode_SignalsValue_aSt },
    { DRIVING_TEXT_E,  	DRIVING_TEXT_SIG_LEN,    DrivingText_SignalsValue_aSt },
    { DRIVING_MODE_E,  	DRIVING_MODE_SIG_LEN,    DrivingMode_SignalsValue_aSt },

    { ECO_TEXT_E,       	ECO_TEXT_SIG_LEN,        EcoText_SignalsValue_aSt },
    { ECO_MODE_E,       	ECO_MODE_SIG_LEN,        EcoMode_SignalsValue_aSt },
	{ SPORTS_TEXT_E,   	SPORTS_TEXT_SIG_LEN,     SportsText_SignalsValue_aSt  },
    { SPORTS_MODE_E,   	SPORTS_MODE_SIG_LEN,     SportsMode_SignalsValue_aSt  },
	{ GEAR_TEXT_E,   	GEAR_TEXT_SIG_LEN,     GearText_SignalsValue_aSt },
	{ GEAR_DIGIT_E,   	GEAR_DIGIT_SIG_LEN,     GearDigit_SignalsValue_aSt },
	{ GEAR_DIGIT_TEXT_E,GEAR_DIGIT_SIG_LEN,    GearDigitText_SignalsValue_aSt}
};


/********************************************************EOF***********************************************************/