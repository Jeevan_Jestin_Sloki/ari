/***********************************************************************************************************************
* File Name    : SignalCalc_main.c
* Version      : 01
* Description  : The file compute signal calculation 
* Created By   : Jeevan Jestin N
* Creation Date: 26/05/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "SignalCalc_main.h"
#include "mcu_can.h"
#include "Speed_Calc.h"
#include "Communicator.h"
#include "ODO_Calc.h"
#include "bms_can.h"
#include"Range_WhKmCalc.h"
#include "DataBank.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
OdoDistDispSelect_En_t OdoDistDispSelect_En = DISP_MAIN_0D0_E;
uint8_t PreviousBattpakstate_b = false;
/***********************************************************************************************************************
* Function Name: CalculateSignal
* Description  : This function Calculates the Required Cluster Signals.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void CalculateSignal(void)
{
    if(MOTOR_TEMP_THRESHOLD_CELSIUS < MCU_MSG_2_St_t.MotorTemperature_u16 )
    {
        SET_MOTOR_FAULT_STATE(BLINK_TT);
    }
    else
    {
        SET_MOTOR_FAULT_STATE(OFF);
    }

    if(BATTERYFAULT_THRESHOLD_CELCIUS < BatteryGeneralInfo_0xBB_St.BatteryTemperture_u16)
    {
        SET_BATT_FAULT_STATE(BLINK_TT);
    }
    else
    {
        SET_BATT_FAULT_STATE(OFF);
    }

    CalculateVehicleSpeed(MCU_MSG_2_St_t.MotorRPM_u16);
    SET_VEHICLE_SPEED(Get_VehicleSpeed()); 

    Calculate_ODOmeter(GET_VEHICLE_SPEED());
    ODOtoDisplay();
    UpdateBatteryPackState(BatteryGeneralInfo_0xBB_St.BattPackState_u8);

    if((true == BattGeneralInfoRec_b) && (true == CapacityInfoRec_b))
    {
        EstimateWhKm(BatteryGeneralInfo_0xBB_St.BattVoltage_u16,BatteryGeneralInfo_0xBB_St.SOC,CapacityInfo_0xA3_St.AvailableCapacity);
        UpdateRangeKm(BatteryGeneralInfo_0xBB_St.BattVoltage_u16,BatteryGeneralInfo_0xBB_St.SOC,CapacityInfo_0xA3_St.AvailableCapacity);
    }


    if( MAX_SPEED_KMH <= GET_VEHICLE_SPEED())
    {
        /* Blink TOP icon to indicate the vehicle is crossing the speed limit*/
        SET_TOP_TEXT_STATE(BLINK_TT);
    }
    else
    {
        SET_TOP_TEXT_STATE(OFF);
    }
    return;
}

/***********************************************************************************************************************
* Function Name: CalculateSignal
* Description  : This function Calculates the Required Cluster Signals.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ODOtoDisplay(void)
{
    switch(OdoDistDispSelect_En)
    {
        case DISP_MAIN_0D0_E:
        {
            SET_ODO_METER(ODOmeter_St.Main_ODOmeter_u32);
            SET_ODO_TEXT_STATE(SOLID_ON); 
            SET_TRIP_A_TEXT_STATE(OFF);
            SET_TRIP_B_TEXT_STATE(OFF);
            SET_TRIP_TEXT_STATE(OFF);

            break;
        }
        case DISP_TRIP_A_ODO_E:
        {
            SET_ODO_METER(ODOmeter_St.TripA_ODOmeter_u32);
            SET_ODO_TEXT_STATE(OFF); 
            SET_TRIP_A_TEXT_STATE(SOLID_ON);
            SET_TRIP_B_TEXT_STATE(OFF);
            SET_TRIP_TEXT_STATE(SOLID_ON);
            break;
        }
        case DISP_TRIP_B_ODO_E:
        {
            SET_ODO_METER(ODOmeter_St.TripB_ODOmeter_u32);
            SET_ODO_TEXT_STATE(OFF); 
            SET_TRIP_A_TEXT_STATE(OFF);
            SET_TRIP_B_TEXT_STATE(SOLID_ON);
            SET_TRIP_TEXT_STATE(SOLID_ON);
            break;
        }
        default :
        {
            break;
        }
    }
    return;
}

/***********************************************************************************************************************
* Function Name: UpdateBatteryPackState
* Description  : This function updates the battery pack state.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void UpdateBatteryPackState(uint8_t BattPackstate_u8)
{
    if(BATTERY_CHARGING == BattPackstate_u8)
    {
        ClusterSignals_St.ChargingIndicator_u8 = BATTERY_CHARGING;
        PreviousBattpakstate_b = BATTERY_CHARGING;
    }
    else
    {
        if(BATTERY_CHARGING == PreviousBattpakstate_b)
        {
            PreviousBattpakstate_b = BATTERY_DISCHARGING;
            WhEstimateOnstart_b = false;
        }
        ClusterSignals_St.ChargingIndicator_u8 = BATTERY_DISCHARGING;
    }
    return;
}


