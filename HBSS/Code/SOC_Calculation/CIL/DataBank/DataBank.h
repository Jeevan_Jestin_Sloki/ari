/***********************************************************************************************************************
* File Name    : DataBank.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 01/09/2021
***********************************************************************************************************************/

#ifndef DATA_BANK_H
#define DATA_BANK_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef struct
{
	uint32_t 	ODOmeter_u32;
	uint16_t 	PowerConsumption_u16;
	uint16_t 	VehicleSpeed_u16;
	uint16_t 	NavigationDistance_u16;
	uint16_t 	RangeKm_u16;
	uint8_t 	ODOText_u8;
	uint8_t 	TRIPText_u8;
	uint8_t 	AText_u8;
	uint8_t 	BText_u8;
	uint8_t 	LeftIndicator_u8;
	uint8_t 	EngineFault_u8;
	uint8_t 	Warning_u8;
	uint8_t 	KillSwitch_u8;
	uint8_t 	MotorFault_u8;
	uint8_t 	ServiceReminder_u8;
	uint8_t 	RightIndicator_u8;
	uint8_t 	NavigationDirection_u8;
	uint8_t 	BatterySOC_u8;
	uint8_t 	PowerIcon_u8;
	uint8_t 	TOPText_u8;
	uint8_t 	ChargingIndicator_u8;
	uint8_t 	NeutralMode_u8;
	uint8_t 	EconomyMode_u8;
	uint8_t 	SportsMode_u8;
	uint8_t 	ReverseMode_u8;
	uint8_t 	SideStand_u8;
	uint8_t 	NetworkConnection_u8;
	uint8_t 	BLEIcon_u8;
	uint8_t 	HighBeam_u8;
	uint8_t 	HoursTime_u8;
	uint8_t 	MinutesTime_u8;
	uint8_t 	AMText_u8;
	uint8_t 	PMText_u8;
	uint8_t 	TimeColon_u8;
	uint8_t 	Navigation_Status_u8;
	uint8_t		Commonsig_u8;
}ClusterSignals_St_t;


typedef struct
{
	uint8_t IgnitionStatus_u8;
	uint8_t ChargingStatus_u8;
}ClusterInput_St_t;


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern ClusterSignals_St_t	ClusterSignals_St;
extern ClusterInput_St_t	ClusterInput_St;


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Clear_Data_Bank(void);

#endif /* DATA_BANK_H */


