//#include "ram_vector.h"

#include "MY_ram_vector.h"

//#pragma section @@DATA RAM_VECT AT 0FFD00H
//0FFD58H		//0FFA94H	// 4500 bytes of RAM available

//#pragma section DILEEP
#pragma section bss .ramvect
	void (*RAM_INTWDTI_ISR)(void);
	void (*RAM_INTLVI_ISR)(void);
	void (*RAM_INTP0_ISR)(void);
	void (*RAM_INTP1_ISR)(void);
	void (*RAM_INTP2_ISR)(void);
	void (*RAM_INTP3_ISR)(void);
	void (*RAM_INTP4_ISR)(void);
	void (*RAM_INTP5_ISR)(void);
	void (*RAM_INTCLM_ISR)(void);
	void (*RAM_INTCSI00_ISR)(void);
	void (*RAM_INTCSI01_ISR)(void);
	void (*RAM_INTDMA0_ISR)(void);
	void (*RAM_INTDMA1_ISR)(void);
	void (*RAM_INTRTC_ISR)(void);
	void (*RAM_INTIT_ISR)(void);
	void (*RAM_INTLT0_ISR)(void);
	void (*RAM_INTLR0_ISR)(void);
	void (*RAM_INTLS0_ISR)(void);
	void (*RAM_INTPLR0_ISR)(void);
	void (*RAM_INTSG_ISR)(void);
	void (*RAM_INTTM00_ISR)(void);
	void (*RAM_INTTM01_ISR)(void);
	void (*RAM_INTTM02_ISR)(void);
	void (*RAM_INTTM03_ISR)(void);
	void (*RAM_INTAD_ISR)(void);
	void (*RAM_INTLT1_ISR)(void);
	void (*RAM_INTLR1_ISR)(void);
	void (*RAM_INTLS1_ISR)(void);
	void (*RAM_INTPLR1_ISR)(void);
	void (*RAM_INTIIC11_ISR)(void);
	void (*RAM_INTTM04_ISR)(void);
	void (*RAM_INTTM05_ISR)(void);
	void (*RAM_INTTM06_ISR)(void);
	void (*RAM_INTTM07_ISR)(void);
	void (*RAM_INTC0ERR_ISR)(void);
	void (*RAM_INTC0WUP_ISR)(void);
	void (*RAM_INTC0REC_ISR)(void);
	void (*RAM_INTC0TRX_ISR)(void);
	void (*RAM_INTTM10_ISR)(void);
	void (*RAM_INTTM11_ISR)(void);
	void (*RAM_INTTM12_ISR)(void);
	void (*RAM_INTTM13_ISR)(void);
	void (*RAM_INTMD_ISR)(void);
	void (*RAM_INTFL_ISR)(void);
	void (*RAM_INTTM14_ISR)(void);
	void (*RAM_INTTM15_ISR)(void);
	void (*RAM_INTTM16_ISR)(void);
	void (*RAM_INTTM17_ISR)(void);
	void (*RAM_INTTM20_ISR)(void);
	void (*RAM_INTTM21_ISR)(void);
	void (*RAM_INTTM22_ISR)(void);
	void (*RAM_INTTM23_ISR)(void);
	void (*RAM_INTTM24_ISR)(void);
	void (*RAM_INTTM26_ISR)(void);
#pragma section