/***********************************************************************************************************************
* File Name    : Cluster_Conf.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 26/12/2020
***********************************************************************************************************************/

#ifndef CLUSTER_CONF_H
#define CLUSTER_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	ONE_SEG_E = 1,
	TWO_SEG_E,
	THREE_SEG_E,
	FOUR_SEG_E,
	FIVE_SEG_E,
}SegRequired_En_t;

/*
   HMI Cluster Signals
*/
typedef enum
{
	CLUSTER_SIG_START_E = 0,
	COMMON_SIG_E = CLUSTER_SIG_START_E,
	SPEED_E,
	MILEAGE_E,
	RANGE_KM_E,
	LEFT_IND_E,
	RIGHT_IND_E,
	LOW_BEAM_E,
	HIGH_BEAM_E,
	BLE_E,
	REGEN_BRAKING_E,
	WARNING_IND_E,
	SERV_REM_E,
	NEUTRAL_MODE_E,
	ECO_MODE_E,
	SPORTS_MODE_E,
	REVERSE_MODE_E,
	SIDE_STAND_E,
	SAFE_MODE_E,
	KILL_SWITCH_E,
	BATT_SOC_E,
	POWER_CONSUMP_E,
	ODO_E,
	MINUTES_TIME_E,
	HOURS_TIME_E,
	BATT_FAULT_E,
	MOTOR_FAULT_E,
	TIME_COLON_E,
	LOGO_E,
	POWER_IND_E,
	WHKM_TEXT_E,
	BATTTERY_IND_E,
	SPORTS_TEXT_E, 
	ECO_TEXT_E,	 
	NEUTRAL_TEXT_E,
	REVERSE_TEXT_E,
	TOTAL_SIGNALS_E,
}ClusterSignals_En_t;

typedef enum
{
	ZERO_E 		= 0,
	OFF_E 		= ZERO_E,
	ONE_E,
	ON_E 		= ONE_E,
	TWO_E,
	THREE_E,
	FOUR_E,
	FIVE_E,
	SIX_E,
	SEVEN_E,
	EIGHT_E,
	NINE_E,
	TEN_E,
	ELEVEN_E,
	TWELVE_E,
	THIRTEEN_E,
	OFF_DIGIT 	= 0x0F,
	OFF_BAR 	= OFF_DIGIT,
	DISPLAY_G 	= 'G',
	DISPLAY_C	= 'C',
	DISPLAY_H	= 'H',
}SignalsValue_En_t;

typedef struct
{
	volatile __near unsigned char  	*Seg_pu8;              /* Address of the Segment Register */
	uint8_t				 			Mask_u8;              /* Mask Value */
	uint8_t        	 		 		Value_u8;             /* Value need to be write into the Segment Register */
}SegConfig_St_t;

typedef struct
{
	SignalsValue_En_t  		SignalsValue_En;        /* Position Signal Value */
	uint8_t                 Seg_Count_u8;           /* Total Segments to be write to display the respective position signal. */
	const SegConfig_St_t	*SegConfig_pSt;         /* Segmets Configuration */
}SignalConfig_St_t;


typedef struct
{     
	SignalsValue_En_t			SignalPos_En;           /* Cluster Signal Position */
	SignalsValue_En_t			SignalLen_En;           /* Length of each position signal */
	const SignalConfig_St_t  	*SigConfig_pSt;         /* Each postion signal configuration */
}SignalsValue_St_t;

typedef struct
{
	ClusterSignals_En_t			ClusterSignals_En;       /* Cluster Signal Name */
	uint8_t						SigLength_u8;            /* Cluster Signal Length */
	const SignalsValue_St_t 	*SignalsValue_pSt;       /* Cluster Signal Value Configuration */
}ClusterSigConf_St_t;


/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
extern const ClusterSigConf_St_t		ClusterSigConf_aSt[TOTAL_SIGNALS_E];


#endif /* CLUSTER_CONF_H */


