/***********************************************************************************************************************
* File Name    : BackLightCtrl.c
* Version      : 01
* Description  : This file implements the control of LCD Back-Lights.
* Created By   : Dileepa B S
* Creation Date: 08/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "BackLightCtrl.h"
#include "timer_user.h"
#include "LightSensor.h"
#include "LuxDuty_Data.h"
#include "board_conf.h"
#include "hmi_config_can.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/



/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
DutyCycle_Conf_St_t	DutyCycle_Conf_St = { 0, 0, false, false };

#if(BACKLIGHT_CTRL_STATIC_MODE == TRUE)
	BackLightCtrl_En_t		BackLightCtrl_En = BACKLIGHT_CTRL_STATIC_E;
#elif(BACKLIGHT_CTRL_ALS_MODE == TRUE)
	BackLightCtrl_En_t		BackLightCtrl_En = BACKLIGHT_CTRL_ALS_E; 
#elif(BACKLIGHT_CTRL_CAN_MODE == TRUE)
	BackLightCtrl_En_t		BackLightCtrl_En = BACKLIGHT_CTRL_CAN_E; 
#endif

uint16_t DutyCycleOut_u16 = 0;


/***********************************************************************************************************************
* Function Name: Turn_ON_BackLight
* Description  : This function turns-on the LCD Back-Lights on Ignition -ON.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Turn_ON_BackLight(void)
{
	uint16_t PWMDutyCycle_u16 = 0;
	
	if(BackLightCtrl_En == BACKLIGHT_CTRL_ALS_E)
	{
		/*BackLight Brightness adjustment using ALS Data*/
		uint32_t LightIntensity_u32 = 0;
		
		LightIntensity_u32 = GetALSData(); 	/*Get present Lux Value*/
		
		PWMDutyCycle_u16   = GetDutyCycleValue(LightIntensity_u32); 
						/*Get Duty cycle value corresponding to the Lux*/
		
	}
	else if((BackLightCtrl_En == BACKLIGHT_CTRL_STATIC_E) || (BackLightCtrl_En == BACKLIGHT_CTRL_CAN_E))
	{
		PWMDutyCycle_u16 = PWM_DUTY_ON_IGNITION_ON;
	}
	else
	{
		;
	}
	
	
	SetPWMDutyCycle(PWMDutyCycle_u16); 	/*Turn-On BackLights with required Duty Cycle value*/
	
	DutyCycleOut_u16 = PWMDutyCycle_u16;
	
	//HMI_CONF_MSG_1.Brightness_u8 = (uint8_t)PWMDutyCycle_u16;
	
	return;
}


/***********************************************************************************************************************
* Function Name: Turn_OFF_BackLight
* Description  : This function turns-off the LCD Back-Lights on Ignition-Off.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Turn_OFF_BackLight(void)
{
	SetPWMDutyCycle(PWM_DUTY_ON_IGNITION_OFF); 	/* Turns-Off the BackLights by writing Duty Cycle value 0 */
	return;
}


/***********************************************************************************************************************
* Function Name: SetPWMDutyCycle
* Description  : This function controls the brightness of the backlight based on the Duty Cycle Received.
* Arguments    : uint16_t ReceivedDutyCycle_u16
* Return Value : None
***********************************************************************************************************************/
void SetPWMDutyCycle(uint16_t ReceivedDutyCycle_u16)
{
	DutyCycle_Conf_St.DutyCycle_u16 = ReceivedDutyCycle_u16;
	
	if(DutyCycle_Conf_St.DutyCycle_u16 != DutyCycle_Conf_St.PrevDutyCycle_u16)
	{
		if(DutyCycle_Conf_St.DutyCycle_u16 > MAX_DUTY_CYCLE)
		{
			DutyCycle_Conf_St.DutyCycle_u16 = MAX_DUTY_CYCLE;
		}
		
		#if(HMI_CAN_ONLY == TRUE)
		Set_PWM1_Duty(DutyCycle_Conf_St.DutyCycle_u16);	
		#endif
		Set_PWM2_Duty(DutyCycle_Conf_St.DutyCycle_u16);
		DutyCycle_Conf_St.PrevDutyCycle_u16 = DutyCycle_Conf_St.DutyCycle_u16;
	}
	else
	{
		;
	}
	
	return;
}


/***********************************************************************************************************************
* Function Name: GetDutyCycleValue
* Description  : This function calculates the duty cycle value corresponding to the Light Intensity Lux.
* Arguments    : uint32_t LightLux_u32
* Return Value : uint16_t DutyCycle_u16
***********************************************************************************************************************/
uint16_t GetDutyCycleValue(uint32_t LightLux_u32)
{
	uint16_t DutyCycle_u16 = 0;
	
	DutyCycle_u16 = MU1DInterpol_u16_u16X_u16Y_tblptr(IntensityLux_DutyCycle_Table,
						INTENSITY_DUTY_COL_SIZE, (uint16_t)LightLux_u32);
	
	if(DutyCycle_u16 == 0)
	{
		DutyCycle_u16 = PWM_DUTY_ON_IGNITION_ON;
	}
	
	return DutyCycle_u16;
}

#if(BACKLIGHT_CTRL_ALS_MODE == TRUE)
/***********************************************************************************************************************
* Function Name: AdjustLightIntensity
* Description  : 
* Arguments    : 
* Return Value : 
***********************************************************************************************************************/
void AdjustLightIntensity(void)
{
	uint32_t PresentLightIntensity_u32 	= 0;
	uint16_t FiterDutyCycleOut_u16		= 0;
	uint16_t CalDutyCycle_u16 		= 0;
	
	PresentLightIntensity_u32 = GetALSData(); 
				/*Get the Light Intensity [LUX] data from the ALS Sensor*/
				
	
	CalDutyCycle_u16 = GetDutyCycleValue(PresentLightIntensity_u32); 
	
	if(BackLightCtrl_En == BACKLIGHT_CTRL_ALS_E)
	{
		FiterDutyCycleOut_u16 = DutyCycle_LPF(CalDutyCycle_u16, &DutyCycleOut_u16, SPEED_LPF_POS_GAIN,
						SPEED_LPF_NEG_GAIN);
	}
	else
	{
		FiterDutyCycleOut_u16 = CalDutyCycle_u16;
	}
	
	
	SetPWMDutyCycle(FiterDutyCycleOut_u16);
	
	return;
}
#endif
/***********************************************************************************************************************
* Function Name: ControlBrightness0verCAN
* Description  : 
* Arguments    : 
* Return Value : 
***********************************************************************************************************************/
void ControlBrightness0verCAN(uint8_t Brightness_u8)
{
	SetPWMDutyCycle(Brightness_u8);
}
/***********************************************************************************************************************
* Function Name: DutyCycle_LPF
* Description  : This function implements the Lo-Pass Filter to smooth the duty cycle socillations.
* Arguments    : uint16_t Input_u16, uint16_t *Output_pu16, int16_t PosConstant_s16, int16_t NegConstant_s16
* Return Value : uint16_t *Output_pu16
***********************************************************************************************************************/
uint16_t DutyCycle_LPF(uint16_t Input_u16, uint16_t *Output_pu16, int16_t PosConstant_s16, int16_t NegConstant_s16)
{
	uint16_t DiffVar_u16 	= 0;
	uint16_t OffsetVal_u16  = 0;
	
	// If the current measeured value is not equal to the input
	if(Input_u16 != *Output_pu16)
	{
		// Compute the filter output by using the formula for the first order low pass filter
        	// y[i] = y[i-1] + alpha(x[i] - y[i-1])
		if(Input_u16 > *Output_pu16)
		{
			DiffVar_u16 = (uint16_t)(Input_u16 - *Output_pu16);
			
			if(PosConstant_s16 > 0)
			{
				OffsetVal_u16 = (uint16_t)(((uint32_t)DiffVar_u16 * (uint32_t)PosConstant_s16) >> 15);
									// [x]=[x]*[15]/[15]
			}
			else
			{
				;
			}
		}
		else
		{
			DiffVar_u16 = (uint16_t)(*Output_pu16 - Input_u16);
			
			if(NegConstant_s16 > 0)
			{
				OffsetVal_u16 = (uint16_t)(((uint32_t)DiffVar_u16 * (uint32_t)NegConstant_s16) >> 15);
									// [x]=[x]*[15]/[15]
			}
			else
			{
				;
			}
		}
		
		// If the step value is 0 then set it to 1
		if(OffsetVal_u16 == 0)
		{
			OffsetVal_u16 = 1;
		}
		else
		{
			;
		}
		
		if(Input_u16 > *Output_pu16)
		{
			// Input curve is moving up, hence step up the result.
			*Output_pu16 += OffsetVal_u16;
		}
		else
		{
			// Input curve is moving down, hence step down the result.
			*Output_pu16 -= OffsetVal_u16;
		}
	}
	else
	{
		;
	}
	
	return (*Output_pu16);
}



/***********************************************************************************************************************
* Function Name: ClrBrightnessCtrlData
* Description  : This function clears all the status flags and data related to Auto Brightness adjustment.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ClrBrightnessCtrlData(void)
{
	DutyCycle_Conf_St.DutyCycle_u16 		= RESET;
	DutyCycle_Conf_St.PrevDutyCycle_u16 		= RESET;
	DutyCycle_Conf_St.B1_Duty_State_b 		= false;
	DutyCycle_Conf_St.B2_Duty_State_b 		= false;
	
	/*Reset the Speed filter output to 0*/
	DutyCycleOut_u16 = RESET;
	
	//BackLightCtrl_En = BACKLIGHT_CTRL_STATIC_E;
	
	return;
}

/********************************************************EOF***********************************************************/