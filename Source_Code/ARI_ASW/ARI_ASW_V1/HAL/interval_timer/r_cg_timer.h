/***********************************************************************************************************************
* File Name    : r_cg_timer.h
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for TAU module.
* Creation Date: 05/12/2020
***********************************************************************************************************************/

#ifndef TAU_H
#define TAU_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 
#include "board_conf.h"

/***********************************************************************************************************************
Macro definitions (Register bit)
***********************************************************************************************************************/



/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Global functions
***********************************************************************************************************************/
void R_TAU0_Create(void);
void R_TAU0_Channel2_Start(void);
void R_TAU0_Channel2_Stop(void);
#if(HMI_CAN_ONLY == TRUE)
	void R_TAU0_Channel0_Start(void);
	void R_TAU0_Channel0_Stop(void);
	
	void r_tau0_channel0_interrupt(void);
	void r_tau0_channel1_interrupt(void);
#endif
void r_tau0_channel2_interrupt(void);


void R_TAU2_Create(void);
void R_TAU2_Channel0_Start(void);
void R_TAU2_Channel0_Stop(void);

void r_tau2_channel0_interrupt(void);
void r_tau2_channel4_interrupt(void);


#endif /* TAU_H */
