/***********************************************************************************************************************
* File Name    : Communicator.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Communicator.h"
#include "DataAquire.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
uint32_t SpeedPulseCounter_u32 = 0;
uint32_t PresentODOmeter_u32 	  = 0;
uint32_t PresentODO_mm_u32	= 0;
uint16_t Brightness_u16		   = 0;
uint16_t PresentSessionWHkm_u16 = 0;
uint16_t PresentSessionRangeKm_u16 = 0;
uint16_t ActualSpeed_u16 = 0;
uint8_t  BatteryState_u8 = 0;
uint8_t  RegenState_u8 = 0;
uint8_t  SocPercentage_u8 = 0;

FlashBlock0Data_St_t FlashBlock0Data_St;


/***********************************************************************************************************************
* Function Name: UploadDataToFlash
* Description  : The function Upload the necessary data to the flash.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void UploadDataToFlash(void)
{
    uint8_t* WrtFlashBlock0Data_pu8 = NULL; 
    uint16_t WttBlock0TotalSize_u8   = 0;
    uint16_t WrtBlock0Indx_u16 = 0;

    FlashBlock0Data_St.OdoMeter_u32 = GET_PRESENT_ODOMETER();
    FlashBlock0Data_St.UnusedOdoMilliMeter_u32 = GET_PRESENT_ODO_MM();
    FlashBlock0Data_St.PreviousRangeKm_u16 = GET_PRESENT_RANGEKM();
    FlashBlock0Data_St.PreviousSessionWHkm_u16 = GET_PRESENT_WHKM();

    WrtFlashBlock0Data_pu8 = (uint8_t *)(&FlashBlock0Data_St);
    WttBlock0TotalSize_u8 = sizeof(FlashBlock0Data_St);

    if(NULL != WrtFlashBlock0Data_pu8)
    {
        for(WrtBlock0Indx_u16 = 0; WrtBlock0Indx_u16 < WttBlock0TotalSize_u8; WrtBlock0Indx_u16++)
        {
            dubWriteBuffer[WrtBlock0Indx_u16] = WrtFlashBlock0Data_pu8[WrtBlock0Indx_u16];
        }

        FDL_Erase(FLASH_BLOCK_0,ERASE_1_BLOCK);
        FDL_Write(FLASH_BLOCK_0,FLASH_BLOCK0_POS,WttBlock0TotalSize_u8);
    }

    return;
}

/***********************************************************************************************************************
* Function Name: DownloadDataFromFlah
* Description  : The function Upload the necessary data to the flash.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void DownloadDataFromFlash(void)
{
    uint8_t* ReadlashBlock0Data_pu8 = NULL; 
    uint16_t ReadBlock0TotalSize_u8   = 0;
    uint16_t ReadBlock0Indx_u16 = 0;

    ReadlashBlock0Data_pu8 = (uint8_t *)(&FlashBlock0Data_St);
    ReadBlock0TotalSize_u8 = sizeof(FlashBlock0Data_St);
     

    if(NULL != ReadlashBlock0Data_pu8)
    {
        FDL_Read(FLASH_BLOCK_0,FLASH_BLOCK0_POS,ReadBlock0TotalSize_u8);
        for(ReadBlock0Indx_u16 = 0;ReadBlock0Indx_u16 < ReadBlock0TotalSize_u8; ReadBlock0Indx_u16++)
        {
            ReadlashBlock0Data_pu8[ReadBlock0Indx_u16] = dubReadBuffer[ReadBlock0Indx_u16];
        }
    }

    if(FlashBlock0Data_St.OdoMeter_u32 == 0xFFFFFFFF)
    {
        FlashBlock0Data_St.OdoMeter_u32 = CLEAR;
    }
    if (FlashBlock0Data_St.UnusedOdoMilliMeter_u32 == 0xFFFFFFFF)
    {
        FlashBlock0Data_St.UnusedOdoMilliMeter_u32 = CLEAR;
    }
    if(FlashBlock0Data_St.PreviousRangeKm_u16 == 0xFFFF)
    {
        FlashBlock0Data_St.PreviousRangeKm_u16 = NOMINAL_RANGEKM;
    }
    if(FlashBlock0Data_St.PreviousSessionWHkm_u16 == 0xFFFF)
    {
        FlashBlock0Data_St.PreviousSessionWHkm_u16 = CLEAR;
    }
    if(FlashBlock0Data_St.BackLightBrightness_u8 == 0xFF)
    {
        FlashBlock0Data_St.BackLightBrightness_u8 = CLEAR;
    }
              
    SET_PRESENT_ODOMETER(FlashBlock0Data_St.OdoMeter_u32);					
    SET_PRESENT_ODO_MM(FlashBlock0Data_St.UnusedOdoMilliMeter_u32);				
    SET_PRESENT_WHKM(FlashBlock0Data_St.PreviousSessionWHkm_u16) ;                   
    SET_PRESENT_RANGEKM( FlashBlock0Data_St.PreviousRangeKm_u16);                               
    return;
}

/***********************************************************************************************************************
* Function Name: ResetCommunicatorData
* Description  : The function Upload the necessary data to the flash.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetCommunicatorData(void)
{
    SpeedPulseCounter_u32 = 0;
    PresentODOmeter_u32 	  = 0;
    PresentODO_mm_u32	= 0;
    Brightness_u16		   = 0;
    PresentSessionWHkm_u16 = 0;
    PresentSessionRangeKm_u16 = 0;
}
/*********************************************************End_Of_File**************************************************/
