/***********************************************************************************************************************
* File Name    : r_cg_lcd.c
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for LCD module.
* Creation Date: 14/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 
#include "r_cg_lcd.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


/***********************************************************************************************************************
* Function Name: R_LCD_Create
* Description  : This function initializes the LCD module.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_LCD_Create(void)
{   
    LCDMD = _10_LCD_VOLTAGE_MODE_INTERNAL;
    
    /* Set segment output */
    LCDPF0 = 0xFFU;
    LCDPF1 = 0xCFU;
    LCDPF3 = 0xFFU;
    LCDPF5 = 0xF0U;
    LCDPF7 = 0x3CU;
    LCDPF8 = 0xFFU;
    LCDPF9 |= 0xEFU; 
    
    LCDM = _00_LCD_DISPLAY_MODE0;
    
    LCDC0 = _60_LCD_SOURCE_CLOCK_FMAIN_1024 | _02_LCD_CLOCK_FLCD_64;
    
}

/***********************************************************************************************************************
* Function Name: R_LCD_Start
* Description  : This function enables the LCD display.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_LCD_Start(void)
{
    LCDON = 1U;
}

/***********************************************************************************************************************
* Function Name: R_LCD_Stop
* Description  : This function disables the LCD display.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_LCD_Stop(void)
{
    LCDON = 0U;
}

/***********************************************************************************************************************
* Function Name: R_LCD_VoltageOn
* Description  : This function enables voltage boost circuit or capacitor split circuit.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_LCD_VoltageOn(void)
{
    SCOC = 1U;
}

/***********************************************************************************************************************
* Function Name: R_LCD_VoltageOff
* Description  : This function disables voltage boost circuit or capacitor split circuit.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_LCD_VoltageOff(void)
{
    SCOC = 0U;
}

/* Start user code for adding. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: LCD_PowerSave
* Description  : This function enables the LCD driver Powersave mode.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void LCD_PowerSave(void)
{
	LCDMD = _00_LCD_VOLTAGE_MODE_EXTERNAL; /* No internal resistor connection, Power Save Mode. */
	return;
}


/***********************************************************************************************************************
* Function Name: LCD_PowerSaveRelease
* Description  : This function releases the LCD from the power save mode.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void LCD_PowerSaveRelease(void)
{
	LCDMD = _10_LCD_VOLTAGE_MODE_INTERNAL; /* internal resistance division method (no step-down transforming) */
	return;
}

/* End user code. Do not edit comment generated here */

/********************************************************EOF***********************************************************/