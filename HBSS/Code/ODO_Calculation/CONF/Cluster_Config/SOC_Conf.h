/***********************************************************************************************************************
* File Name    : SOC_Conf.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 29/12/2021
***********************************************************************************************************************/

#ifndef SOC_CONF_H
#define SOC_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
/*
			Charging sttatus 			
*/
extern	const SignalsValue_St_t 		CH_status_SignalsValue_aSt[CH_STATUS_SIG_LEN];
extern	const SignalConfig_St_t		CH_status_SigConf_ast[TWO_E];
extern  const SegConfig_St_t			CH_status_on_SegConf_ast[ONE_SEG_E];
extern	const SegConfig_St_t			CH_status__off_Segconf_ast[ONE_SEG_E];

/*
			Battery SOC Bar
*/
extern  const SignalsValue_St_t		SOC_SignalsValue_aSt[BATT_SOC_BAR_SIG_LEN];

extern  const SignalConfig_St_t		SOC_B1_SigConf_ast[THREE_E];
extern  const SignalConfig_St_t		SOC_B2_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t		SOC_B3_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t		SOC_B4_SigConf_ast[TWO_E];
extern  const SignalConfig_St_t		SOC_B5_SigConf_ast[TWO_E];

extern  const SegConfig_St_t			SOC_B1_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B2_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B3_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B4_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B5_On_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B1_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B2_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B3_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B4_Off_SegConf_aSt[ONE_SEG_E];
extern  const SegConfig_St_t			SOC_B5_Off_SegConf_aSt[ONE_SEG_E];



/*
		Battery_Percantage_configration
*/

extern  const SignalsValue_St_t		SOC_percen_SignalsValue_aSt[BATT_SOC_DIGIT_SIG_LEN];


extern  const SignalConfig_St_t	 	SOC_Digit1_SigConf_ast[ELEVEN_E];
extern  const SignalConfig_St_t	 	SOC_Digit2_SigConf_ast[ELEVEN_E];
extern  const SignalConfig_St_t	 	SOC_Digit3_SigConf_ast[TWO_E];

/*
		Battery Percantage Digit 1
*/

extern  const SegConfig_St_t 			SOC_Digit1_0_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit1_1_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit1_2_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit1_3_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit1_4_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit1_5_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit1_6_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit1_7_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit1_8_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit1_9_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit1_off_SegConf_aSt[TWO_E];


/*
		Battery Percantage Digit 2
*/

extern  const SegConfig_St_t 			SOC_Digit2_0_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit2_1_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit2_2_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit2_3_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit2_4_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit2_5_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit2_6_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit2_7_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit2_8_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit2_9_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			SOC_Digit2_off_SegConf_aSt[TWO_E];

/*
		Battery Percantage Digit 3
*/

extern  const SegConfig_St_t			SOC_Digit3_on_SegConf_ast[ONE_E];
extern  const SegConfig_St_t			SOC_Digit3_off_SegConf_ast[ONE_E];

#endif /* SOC_CONF_H */


