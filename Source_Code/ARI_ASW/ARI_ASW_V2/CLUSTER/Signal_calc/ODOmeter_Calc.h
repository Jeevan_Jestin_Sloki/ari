/***********************************************************************************************************************
* File Name    : ODO_Calc.h
* Version      : 01
* Description  : This file contains the macro definition related to main
* Created By   : Dileepa B S
* Creation Date: 06/10/2021
***********************************************************************************************************************/

#ifndef ODO_CALC_H
#define ODO_CALC_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	
#include "App_typedefs.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define GEAR_RATIO									4
#define PULSE_COUNT_PER_ROTATION	                (GEAR_RATIO * 5)
#define WHEEL_DIAMETER_MM			                467 /*Actual : 466.7 mm*/	
#define PIE							                3.143
#define WHEEL_CIRCUMFERENCE_MM		                (WHEEL_DIAMETER_MM*PIE)
        
#define ODO_M_BYTES_LEN				                4
#define ODO_MM_BYTES_LEN			                4
#define ODO_BYTES_LEN				                (ODO_M_BYTES_LEN + ODO_MM_BYTES_LEN)

#define SPEED_PULSE_OVERFLOW_VALUE                  0xFFFFFFFF
/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum 
{
    ODO_CAL_NONE_E,
    ODO_CAN_E,
    ODO_CAL_PULSECOUNT_E,
    ODO_CAL_SPEED_E,
}ODOmeterCal_En_t;

/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern ODOmeterCal_En_t ODOmeterCal_En;
extern uint32_t         Present_ODO_in_mm_u32;
extern uint32_t         Present_ODOmeter_u32;


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Calculate_ODOmeter(void);
uint16_t Find_Wheel_Rotations(uint16_t);
uint32_t Update_ODOmeter(uint32_t *);
extern uint32_t Get_ODOmeter(void);
extern void Set_ODOmeter(uint32_t);
extern uint32_t Get_ODOmeter_in_mm(void);
extern void Set_ODOmeter_in_mm(uint32_t);
extern void ResetTheODOmeterData(void);
extern void Get_Distance_for_1_Pulse(void);

#endif /* ODO_CALC_H */


