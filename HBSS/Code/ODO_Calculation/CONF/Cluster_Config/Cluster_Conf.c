/***********************************************************************************************************************
* File Name    : Cluster_Conf.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 02/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"
#include "TelltaleConf.h"
#include "Speed_Conf.h"
#include "ODO_Conf.h"
#include "RangeKm_Conf.h"
#include "Time_Conf.h"
#include "SOC_Conf.h"
#include "PowerConsum_Conf.h"
#include "Navigation_Conf.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Structure declaration
***********************************************************************************************************************/
const ClusterSigConf_St_t		ClusterSigConf_aSt[TOTAL_SIGNALS_E] = 
{
	/* Cluster Signal   	Signal Length            Signal Value Configuration  */
	{ COMMON_SIG_E,    	COMMON_SEG_SIG_LEN,      Common_SignalsValue_aSt      },
	{ SPEED_E,          	SPEED_SIG_LEN,           Speed_SignalsValue_aSt       },
	{ RANGE_KM_E,       	RANGE_SIG_LEN,           Range_SignalsValue_aSt       },
	{ LEFT_IND_E,      	LEFT_IND_SIG_LEN,        LeftInd_SignalsValue_aSt     },
	{ RIGHT_IND_E,      	RIGHT_IND_SIG_LEN,       RightInd_SignalsValue_aSt    },
	{ HIGH_BEAM_E,      	HIGH_BEAM_SIG_LEN,       HighBeamInd_SignalsValue_aSt },
	{ BLE_E,           	BLE_SIG_LEN,             BLE_Ind_SignalsValue_aSt     },
	{ WARNING_IND_E,   	WARN_IND_SIG_LEN,        Warning_SignalsValue_aSt     },
	{ SERV_REM_E,      	SERV_REM_SIG_LEN,        ServRem_SignalsValue_aSt     },
	{ NEUTRAL_MODE_E,  	NEUTRAL_MODE_SIG_LEN,    NeutralMode_SignalsValue_aSt },
	{ ECO_MODE_E,       	ECO_MODE_SIG_LEN,        EcoMode_SignalsValue_aSt     },
	{ SPORTS_MODE_E,   	SPORTS_MODE_SIG_LEN,     SportsMode_SignalsValue_aSt  },
	{ REVERSE_MODE_E,  	REVERSE_MODE_SIG_LEN,    ReverseMode_SignalsValue_aSt },
	{ SIDE_STAND_E,    	SIDE_STAND_SIG_LEN,      SideStand_SignalsValue_aSt   },
	{ KILL_SWITCH_E,    	KILL_SWITCH_SIG_LEN,     KillSwitch_SignalsValue_aSt  },
	{ BATT_SOC_BAR_E,      	BATT_SOC_BAR_SIG_LEN,    SOC_SignalsValue_aSt         },
	{ BATT_SOC_DIGIT_E,	BATT_SOC_DIGIT_SIG_LEN,  SOC_percen_SignalsValue_aSt  },
	{ POWER_CONSUMP_E, 	POWER_CONSUMP_SIG_LEN,   PwrConsum_SignalsValue_aSt   },
	{ ODO_E,           	ODO_SIG_LEN,             ODO_SignalsValue_aSt         },
	{ MINUTES_TIME_HB_E,   	MINUTES_TIME_SIG_LEN,    MinutesTimeHB_SignalsValue_aSt },
	{ MINUTES_TIME_LB_E,   	MINUTES_TIME_SIG_LEN,    MinutesTimeLB_SignalsValue_aSt },
	{ HOURS_TIME_E,     	HOURS_TIME_SIG_LEN,      HoursTime_SignalsValue_aSt   },
	{ TEXT_AM_E,  		TEXT_AM_SIG_LEN,  	 Text_AM_SignalsValue_aSt     },
	{ TEXT_PM_E,  		TEXT_PM_SIG_LEN,  	 Text_PM_SignalsValue_aSt     },
	{ TIME_COLON_E,     	TIME_COLON_SIG_LEN,      Colon_SignalsValue_aSt       },
	{ MOTOR_FAULT_E,    	MOTOR_FLT_SIG_LEN,       MotorFlt_SignalsValue_aSt    },
	{ STRAIGHT_E,		STRAIGHT_SIG_LEN,	 Navig_SignalStraight_aSt     },
	{ LEFT_TURN_E,		LEFT_TURN_SIG_LEN,	 Navig_SignalLeft_aSt         },
	{ RIGHT_TURN_E,		RIGHT_TURN_SIG_LEN,	 Navig_SignalRight_aSt        },
	{ LEFT_U_TURN_E,	LEFT_U_TURN_SIG_LEN,     Navig_SignalLeftUTurn_aSt    },
	{ RIGHT_U_TURN_E,	RIGHT_U_TURN_SIG_LEN,	 Navig_SignalRightUTURN_aSt   }, 
	{ NAVIG_DIST_E,		NAVIG_DIST_SIG_LEN,	 Navig_dist_SignalsValue_aSt  },
	{ ENGINE_FAULT_E,   	ENGINE_FLT_SIG_LEN,      Engine_fault_SignalsValue_aSt},
	{ NETWORK_E,	    	NETWORK_SIG_LEN,	 Network_SignalsValue_aSt     },
	{ ODO_TEXT_E,		ODO_TEXT_SIG_LEN,	 ODO_TEXT_SignalsValue_aSt    },
	{ TRIP_TEXT_E,		TRIP_TEXT_SIG_LEN,   	 TRIP_Text_SignalsValue_aSt   },
	{ TEXT_A_E,		TEXT_A_SIG_LEN,		 Text_A_SignalsValue_aSt      },
	{ TEXT_B_E,		TEXT_B_SIG_LEN,		 Text_B_SignalsValue_aSt      },
	{ POWER_W_IND_E,	POWER_IND_SIG_LEN,	 Power_ind_SiganlValue_ast    },
	{ TOP_TEXT_E,		TOP_TEXT_SIG_LEN,	 TOP_text_SiganlValue_ast     },
	{CHARGING_STATUS_E,	CH_STATUS_SIG_LEN,	 CH_status_SignalsValue_aSt   }
};


/********************************************************EOF***********************************************************/