﻿#include "App_typedefs.h"

#ifndef HBSS_BMS_CAN_H
#define HBSS_BMS_CAN_H



#define CAPACITYINFO_AVAILABLECAPACITY_MASK0  8U
#define CAPACITYINFO_FULLCHARGECAPACITY_MASK0  8U


#define BATTERYGENERALINFO_BATTVOLTAGE_MASK0  8U
#define BATTERYGENERALINFO_BATTERYCURRENT_MASK0  8U


#define SIGNLE_READ_Mask0     0U    
#define SIGNLE_READ_Mask1     0x01U 
#define SIGNLE_READ_Mask2     0x03U 
#define SIGNLE_READ_Mask3     0x07U 
#define SIGNLE_READ_Mask4     0x0FU 
#define SIGNLE_READ_Mask5     0x1FU 
#define SIGNLE_READ_Mask6     0x3FU 
#define SIGNLE_READ_Mask7     0x7FU 
#define SIGNLE_READ_Mask8     0xFFU 


#define SIGNLE_WRITE_Mask0    0x80U 
#define SIGNLE_WRITE_Mask1    0xC0U 
#define SIGNLE_WRITE_Mask2    0xE0U 
#define SIGNLE_WRITE_Mask3    0xF0U 
#define SIGNLE_WRITE_Mask4    0xF8U 
#define SIGNLE_WRITE_Mask5    0xFCU 
#define SIGNLE_WRITE_Mask6    0xFEU 
#define SIGNLE_WRITE_Mask7    0xFFU 
/* def @REQUESTFRAME CAN Message                                   (22) */
#define REQUESTFRAME_ID                                            (22U)
#define REQUESTFRAME_IDE                                           (0U)
#define REQUESTFRAME_DLC                                           (8U)


#define REQUESTFRAME_BATTERYIDFACTOR                                   (1)
#define REQUESTFRAME_CANID_BATTERYID_STARTBIT                           (7)
#define REQUESTFRAME_CANID_BATTERYID_OFFSET                             (0)
#define REQUESTFRAME_CANID_BATTERYID_MIN                                (0)
#define REQUESTFRAME_CANID_BATTERYID_MAX                                (255)
#define REQUESTFRAME_CMDFACTOR                                   (1)
#define REQUESTFRAME_CANID_CMD_STARTBIT                           (15)
#define REQUESTFRAME_CANID_CMD_OFFSET                             (0)
#define REQUESTFRAME_CANID_CMD_MIN                                (0)
#define REQUESTFRAME_CANID_CMD_MAX                                (255)


typedef struct
{
  uint8_t BatteryID;
  uint8_t CMD;
}
RequestFrame_0x16_St_t;


/* def @CAPACITYINFO CAN Message                                   (163) */
#define CAPACITYINFO_ID                                            (163U)
#define CAPACITYINFO_IDE                                           (0U)
#define CAPACITYINFO_DLC                                           (8U)


#define CAPACITYINFO_AVAILABLECAPACITYFACTOR                                   (0.1)
#define CAPACITYINFO_CANID_AVAILABLECAPACITY_STARTBIT                           (23)
#define CAPACITYINFO_CANID_AVAILABLECAPACITY_OFFSET                             (0)
#define CAPACITYINFO_CANID_AVAILABLECAPACITY_MIN                                (0)
#define CAPACITYINFO_CANID_AVAILABLECAPACITY_MAX                                (6553.5)
#define CAPACITYINFO_FULLCHARGECAPACITYFACTOR                                   (0.1)
#define CAPACITYINFO_CANID_FULLCHARGECAPACITY_STARTBIT                           (39)
#define CAPACITYINFO_CANID_FULLCHARGECAPACITY_OFFSET                             (0)
#define CAPACITYINFO_CANID_FULLCHARGECAPACITY_MIN                                (0)
#define CAPACITYINFO_CANID_FULLCHARGECAPACITY_MAX                                (6553.5)


typedef struct
{
  uint16_t AvailableCapacity;
  uint16_t FullChargeCapacity;
}
CapacityInfo_0xA3_St_t;


/* def @BATTERYGENERALINFO CAN Message                                   (187) */
#define BATTERYGENERALINFO_ID                                            (187U)
#define BATTERYGENERALINFO_IDE                                           (0U)
#define BATTERYGENERALINFO_DLC                                           (8U)


#define BATTERYGENERALINFO_BATTVOLTAGEFACTOR                                   (0.1)
#define BATTERYGENERALINFO_CANID_BATTVOLTAGE_STARTBIT                           (7)
#define BATTERYGENERALINFO_CANID_BATTVOLTAGE_OFFSET                             (0)
#define BATTERYGENERALINFO_CANID_BATTVOLTAGE_MIN                                (0)
#define BATTERYGENERALINFO_CANID_BATTVOLTAGE_MAX                                (6553.5)
#define BATTERYGENERALINFO_BATTERYCURRENTFACTOR                                   (0.1)
#define BATTERYGENERALINFO_CANID_BATTERYCURRENT_STARTBIT                           (23)
#define BATTERYGENERALINFO_CANID_BATTERYCURRENT_OFFSET                             (0)
#define BATTERYGENERALINFO_CANID_BATTERYCURRENT_MIN                                (0)
#define BATTERYGENERALINFO_CANID_BATTERYCURRENT_MAX                                (6553.5)
#define BATTERYGENERALINFO_SOCFACTOR                                   (1)
#define BATTERYGENERALINFO_CANID_SOC_STARTBIT                           (39)
#define BATTERYGENERALINFO_CANID_SOC_OFFSET                             (0)
#define BATTERYGENERALINFO_CANID_SOC_MIN                                (0)
#define BATTERYGENERALINFO_CANID_SOC_MAX                                (255)
#define BATTERYGENERALINFO_BATTERYTEMPERTURE_U16FACTOR                                   (1)
#define BATTERYGENERALINFO_CANID_BATTERYTEMPERTURE_U16_STARTBIT                           (47)
#define BATTERYGENERALINFO_CANID_BATTERYTEMPERTURE_U16_OFFSET                             (40)
#define BATTERYGENERALINFO_CANID_BATTERYTEMPERTURE_U16_MIN                                (-128)
#define BATTERYGENERALINFO_CANID_BATTERYTEMPERTURE_U16_MAX                                (127)
#define BATTERYGENERALINFO_BATTPACKSTATE_U8FACTOR                                   (1)
#define BATTERYGENERALINFO_CANID_BATTPACKSTATE_U8_STARTBIT                           (55)
#define BATTERYGENERALINFO_CANID_BATTPACKSTATE_U8_OFFSET                             (0)
#define BATTERYGENERALINFO_CANID_BATTPACKSTATE_U8_MIN                                (0)
#define BATTERYGENERALINFO_CANID_BATTPACKSTATE_U8_MAX                                (255)

typedef struct
{
  uint16_t BattVoltage_u16;
  uint16_t BatteryCurrent_u16;
  int16_t BatteryTemperture_u16;
  uint8_t SOC;
  uint8_t BattPackState_u8;
}
BatteryGeneralInfo_0xBB_St_t;

 extern BatteryGeneralInfo_0xBB_St_t BatteryGeneralInfo_0xBB_St;
 extern CapacityInfo_0xA3_St_t CapacityInfo_0xA3_St;
 extern RequestFrame_0x16_St_t RequestFrame_0x16_St;
 extern bool BmsframeRecived_b;
 extern bool CapacityInfoRec_b;
 extern bool BattGeneralInfoRec_b;
 extern uint32_t Deserialize_RequestFrame(RequestFrame_0x16_St_t* message, const uint8_t* data);
 extern uint32_t Serialize_RequestFrame(RequestFrame_0x16_St_t* message, uint8_t* data);
 extern uint32_t Deserialize_CapacityInfo(CapacityInfo_0xA3_St_t* message, const uint8_t* data);
 extern uint32_t Serialize_CapacityInfo(CapacityInfo_0xA3_St_t* message, uint8_t* data);
 extern uint32_t Deserialize_BatteryGeneralInfo(BatteryGeneralInfo_0xBB_St_t* message, const uint8_t* data);
 extern uint32_t Serialize_BatteryGeneralInfo(BatteryGeneralInfo_0xBB_St_t* message, uint8_t* data);
 extern void ResetBatteryGeneralInfo(void);
 extern void ResetCapacityInfo(void);
 extern void ResetBmsData(void);
 #endif