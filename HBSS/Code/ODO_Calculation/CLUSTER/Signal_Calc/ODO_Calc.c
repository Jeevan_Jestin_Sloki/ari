/***********************************************************************************************************************
* File Name    : ODO_Calc.c
* Version      : 01
* Description  : This function calculate the ODO 
* Created By   : Jeevan Jestin N
* Creation Date: 26/05/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include"ODO_Calc.h"
#include "pfdl_user.h"
#include"Range_WhKmCalc.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
uint32_t 		PreviousTime_ms_u32 	= 0;
uint16_t		ODO_in_mm_u16		    = 0;
uint16_t 		Time_Difference_ms_u16 	= 0;

ODOmeter_St_t	ODOmeter_St;
bool DataFlashFlag_b = false;
/***********************************************************************************************************************
* Function Name: Calculate_ODOmeter
* Description  : The function calculate the ODO meter value based on the speed and time
* Arguments    : uint8_t Speed_u8
* Return Value : None
***********************************************************************************************************************/
void Calculate_ODOmeter(uint8_t Speed_u8)
{
	uint16_t Dist_covered_mm_u16	= 0;
    	uint16_t DistanceCovered_Meter_u16 = 0;
	
	Time_Difference_ms_u16 = (uint16_t)(GET_TIME_MS() - PreviousTime_ms_u32);/* Time difference between present time
									and previous time(time when ODO is upadted )*/
	PreviousTime_ms_u32 = GET_TIME_MS();
	
	Dist_covered_mm_u16 = ((float)Speed_u8 * ((float)(Time_Difference_ms_u16) / HOUR_TO_MILLISEC) * KM_TO_MM);/*Distance covered in milli meter present time and previous time*/
	
	ODO_in_mm_u16 = ODO_in_mm_u16 + Dist_covered_mm_u16;/* adding the distance covered between present time and previous time to the leftout mm (milli meter)distance in last update*/

	DistanceCovered_Meter_u16 = ODO_in_mm_u16/1000;

	ODOmeter_St.Main_ODOmeter_u32 = ODOmeter_St.Main_ODOmeter_u32 + DistanceCovered_Meter_u16;/* adding to the present odo meter reading if ODO_in_mm_u16 is greater the 1 meter*/

	ODOmeter_St.TripA_ODOmeter_u32 = ODOmeter_St.TripA_ODOmeter_u32 + DistanceCovered_Meter_u16;

	ODOmeter_St.TripB_ODOmeter_u32 = ODOmeter_St.TripB_ODOmeter_u32 + DistanceCovered_Meter_u16;

	ODO_in_mm_u16 = (ODO_in_mm_u16 % 1000);
	
//	if((ODOmeter_St.Main_ODOmeter_u32 != 0 )&&( ODOmeter_St.Main_ODOmeter_u32 % 100) == 0)
//	{
//		if(false == DataFlashFlag_b)
//		{
//			DataFlashFlag_b = true;
//			StoreODOtoFlash();
//			StoreRangekmParam();
//		}	
//	}
//	else if(DataFlashFlag_b == true)
//	{
//		DataFlashFlag_b = false;
//	}
	
	return;
}

	
        
/***********************************************************************************************************************
* Function Name: Get_ODOmeter
* Description  : 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
uint32_t Get_ODOmeter(void)
{
	return ODOmeter_St.Main_ODOmeter_u32;
}

/***********************************************************************************************************************
* Function Name: ClearTripA_ODO
* Description  : The Function clear the Trip A ODO meter
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ClearTripA_ODO(void)
{
    ODOmeter_St.TripA_ODOmeter_u32 = CLEAR;
    return;
}

/***********************************************************************************************************************
* Function Name: ClearTripA_ODO
* Description  : The Function clear the Trip A ODO meter
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ClearTripB_ODO(void)
{
    ODOmeter_St.TripB_ODOmeter_u32 = CLEAR;
    return;
}

/***********************************************************************************************************************
* Function Name: StoreODOtoFlash
* Description  : The function Stores the Main ODO, Trip_A ODO, Trip_B ODO.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void StoreODOtoFlash(void)
{
	// uint8_t ODOmeterSize_u8 = 0;
	// uint8_t ByteCount_u8 = 0;
	// uint8_t *ODOmeterData_pu8 = 0;

	// ODOmeterSize_u8 = sizeof(ODOmeter_St);
	// ODOmeterData_pu8 = (uint8_t *)&ODOmeter_St;

	// FDL_Erase(ODO_FLASH_BLOCK,ODO_TOTAL_FLSHA_BLOCK);

	// for(ByteCount_u8 = 0; ByteCount_u8<ODOmeterSize_u8;ByteCount_u8++)
	// {
	// 	dubWriteBuffer[ByteCount_u8] = ODOmeterData_pu8[ByteCount_u8];
	// }

	// FDL_Write(ODO_FLASH_BLOCK, ODO_START_POS, ODOmeterSize_u8);

    return;
}

/***********************************************************************************************************************
* Function Name: RestoreODOfromFlash
* Description  : The function Restores the Main ODO, Trip_A ODO, Trip_B ODO.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void RestoreODOfromFlash(void)
{
	uint8_t ODOmeterSize_u8 = 0;
	uint8_t ByteCount_u8 = 0;
	uint8_t *ODOmeterData_pu8 = 0;

	ODOmeterSize_u8 = sizeof(ODOmeter_St);
	ODOmeterData_pu8 = (uint8_t *)&ODOmeter_St;

	FDL_Read(ODO_FLASH_BLOCK, ODO_START_POS, ODOmeterSize_u8);

	for(ByteCount_u8 = 0; ByteCount_u8<ODOmeterSize_u8;ByteCount_u8++)
	{
		ODOmeterData_pu8[ByteCount_u8] = dubReadBuffer[ByteCount_u8];
	}
	
	if(ODOmeter_St.Main_ODOmeter_u32 == 0xFFFFFFFF)
	{
		ODOmeter_St.Main_ODOmeter_u32 = 0;
	}
	if(ODOmeter_St.TripA_ODOmeter_u32 == 0xFFFFFFFF)
	{
		ODOmeter_St.TripA_ODOmeter_u32 = 0;
	}
	if(ODOmeter_St.TripB_ODOmeter_u32 == 0xFFFFFFFF)
	{
		ODOmeter_St.TripB_ODOmeter_u32 = 0;
	}
}

/***********************************************************************************************************************
* Function Name: Read_OdoMeter
* Description  : The function Restores the Main ODO, Trip_A ODO, Trip_B ODO.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Read_OdoMeter(Flash_Param_St_t* Flash_Param_pSt)
{
	Flash_Param_pSt->ODO_Meter_u32 = 	ODOmeter_St.Main_ODOmeter_u32;
	Flash_Param_pSt->TripA_Meter_u32 = 	ODOmeter_St.TripA_ODOmeter_u32;
	Flash_Param_pSt->TripB_Meter_u32 = 	ODOmeter_St.TripB_ODOmeter_u32;
	return;
}

/***********************************************************************************************************************
* Function Name: Read_OdoMeter
* Description  : The function Restores the Main ODO, Trip_A ODO, Trip_B ODO.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Write_OdoMeter(Flash_Param_St_t* Flash_Param_pSt)
{

	if(0xFFFFFFFF == Flash_Param_pSt->ODO_Meter_u32)
	{
		ODOmeter_St.Main_ODOmeter_u32 = 0;
	}
	else
	{
		ODOmeter_St.Main_ODOmeter_u32 = Flash_Param_pSt->ODO_Meter_u32 ;
	}

	if(0xFFFFFFFF == Flash_Param_pSt->TripA_Meter_u32)
	{
		ODOmeter_St.TripA_ODOmeter_u32 = 0;
	}
	else
	{
		ODOmeter_St.TripA_ODOmeter_u32 = Flash_Param_pSt->TripA_Meter_u32;
	}

	if(0xFFFFFFFF == Flash_Param_pSt->TripB_Meter_u32)
	{
		ODOmeter_St.TripB_ODOmeter_u32 = 0;
	}
	else
	{
		ODOmeter_St.TripB_ODOmeter_u32 = Flash_Param_pSt->TripB_Meter_u32;
	}

	return;
}