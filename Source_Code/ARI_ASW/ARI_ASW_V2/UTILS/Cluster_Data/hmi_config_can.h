﻿
#include "App_typedefs.h" 


#define SET_WATT_CONSUM_PER_BAR                   0x01U
#define SET_BRIGHTNESS_DUTY                       0x02U
#define SET_SPEED_CAL_DATA                        0x03U
#define REAL_TIME_SET                             0x04U

#define SUB_FUNC_NONE                             0x00U
#define SUB_FUNC_WHEEL_DIA                        0x01U
#define SUB_FUNC_GEAR_RATIO                       0x02U
#define SUB_FUNC_PULSE_COUNT                      0x03U


#define FDL_BLOCK_3                               3
#define FDL3_TOTAL_BYTES                          7


typedef enum
{
  PWR_CONSUM_PER_BAR_E,
  WHEEL_DIAMETER_E,
  PULSE_COUNT_E,
  BRIGHTNESS_DUTY_E,
  GEAR_RATIO_E,
}FDL_BLK3_Sig_En_t;


extern uint16_t PerBarPwrConsum_u16;
extern uint16_t WheelDia_in_mm_u16;
extern uint8_t  PulseCountper1Rotation_u8;
extern uint8_t  BrightnessDuty_u8;
extern uint8_t  GearRatio_u8;

#define SET_WATTS_PER_BAR(x)                       (PerBarPwrConsum_u16 = x)
#define GET_WATTS_PER_BAR(x)                       (PerBarPwrConsum_u16)
#define SET_WHEEL_DIA(x)                           (WheelDia_in_mm_u16 = x)
#define GET_WHEEL_DIA()                            (WheelDia_in_mm_u16)
#define SET_PULSE_PER_ROTATION(x)                  (PulseCountper1Rotation_u8 = x)
#define GET_PULSE_PER_ROTATION()                   (PulseCountper1Rotation_u8)
#define SET_BRIGHTNESS_PER(x)                      (BrightnessDuty_u8 = x)
#define GET_BRIGHTNESS_PER()                       (BrightnessDuty_u8)
#define SET_GEAR_RATIO(x)                          (GearRatio_u8 = x)
#define GET_GEAR_RATIO()                           (GearRatio_u8)

extern void SetHMIConfigParameter(uint8_t *);
extern void Clear_HMIConf_CANSignals(void);
extern void StoreUserConfHMIData(FDL_BLK3_Sig_En_t);
extern void ReStoreUserConfHMIData(void);