/***********************************************************************************************************************
* File Name    : r_cg_timer_user.c
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for TAU module.
* Creation Date: 06/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/ 
#include "r_cg_timer.h"
#include "timer_user.h"
#include "ClusterAnimation.h"
#include "task_scheduler.h"
#include "App_typedefs.h"
#include "com_tasksched.h"
#include "cluster_main.h"
#include "UserButtuonIn.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/*
	NOTE : Interrupt Vector Table Moved to the RAM 
*/
//#if(HMI_CAN_ONLY == TRUE)
//#pragma interrupt r_tau0_channel0_interrupt(vect=INTTM00)
//#pragma interrupt r_tau0_channel1_interrupt(vect=INTTM01)	
//#endif
//#pragma interrupt r_tau0_channel2_interrupt(vect=INTTM02)
//#pragma interrupt r_tau2_channel0_interrupt(vect=INTTM20)
//#pragma interrupt r_tau2_channel4_interrupt(vect=INTTM24)

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
volatile uint32_t	Time_Tick_u32 		= 0;
volatile uint32_t 	TimeTick1ms_u32 	= 0;
volatile uint16_t   _1ms_Counter_u16 	= 0;
bool 				_5ms_TS_Flag_b 		= false;
bool				_1Second_Flag_b 	= false;
bool 				OneSecDelay_b 		= true; 


/***********************************************************************************************************************
* Function Name: r_tau0_channel2_interrupt 
* Description  : This function is INTTM02 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_tau0_channel2_interrupt(void)
{
	Time_Tick_u32++;	/* Time Tick will be incremented by 1 for every 1ms */

	INC_TIME_MS();
	
	if((Time_Tick_u32 % _5MS_DIVISOR_5) == 0)
	{
		_5ms_TS_Flag_b = true;
	}
	
	if(0 == (Time_Tick_u32 % _1SEC_DIVISOR_1000)) 
	{
		_1Second_Flag_b = true;	/*This 1-second flag is used for updation of time*/
	}
	else
	{
		;
	}
	
	/*TODO  CLUSTER ANIMATION FOR 3.2 SECOND*/
	if(Cluster_State_En == DISPLAY_ANIMATION_E)
	{
		_1ms_Counter_u16++;
		if(0 == (_1ms_Counter_u16 % _3200MS_ANIMATION_DIVISOR))
		{
			_1ms_Counter_u16 = RESET; /*Reset this counter after 1 second*/
			
			AnimationDone_b = true;
			
			/*change the cluster display state to DISPLAY_DATA*/
			Cluster_State_En = DISPLAY_DATA_E;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
	
	if(BatteryState_En == CHARGING_MODE_E)
	{
		TimeTick1ms_u32++;
		
		if(0 == (TimeTick1ms_u32 % _1SEC_DIVISOR_1000))
		{
			OneSecDelay_b = true;
		}
		else
		{
			;
		}
	}
	else
	{
		;
	}
	
	#if(READ_UIB_INPUT == TRUE) 
		INC_TIME_MS_2();
	#endif
	
	return;
}


#if(HMI_CAN_ONLY == TRUE)
/***********************************************************************************************************************
* Function Name: r_tau0_channel0_interrupt
* Description  : This function is INTTM00 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_tau0_channel0_interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    /* End user code. Do not edit comment generated here */
}

/***********************************************************************************************************************
* Function Name: r_tau0_channel1_interrupt
* Description  : This function is INTTM01 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_tau0_channel1_interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    /* End user code. Do not edit comment generated here */
}
#endif


/***********************************************************************************************************************
* Function Name: r_tau2_channel0_interrupt
* Description  : This function is INTTM20 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_tau2_channel0_interrupt(void)
{
    return;
}


/***********************************************************************************************************************
* Function Name: r_tau2_channel4_interrupt
* Description  : This function is INTTM24 interrupt service routine.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void r_tau2_channel4_interrupt(void)
{
    return;
}


#if(HMI_CAN_ONLY == TRUE)
/***********************************************************************************************************************
* Function Name: Set_PWM1_Duty
* Description  : This function controls the PWM duty cycle of the BackLight-1 PWM.
* Arguments    : uint16_t DutyCycle_u16
* Return Value : None
***********************************************************************************************************************/
void Set_PWM1_Duty(uint16_t DutyCycle_u16)
{
	uint16_t EquRegValue_u16 = 0;
	
	if(DutyCycle_u16 > MAX_DUTY_CYCLE)
	{
		DutyCycle_u16 = MAX_DUTY_CYCLE;
	}
	
	EquRegValue_u16 = (DutyCycle_u16 * REG_VALUE_PER_1PER_DUTY);
	
	TDR01 = EquRegValue_u16;
	
	return;
}
#endif


/***********************************************************************************************************************
* Function Name: Set_PWM2_Duty
* Description  : This function controls the PWM duty cycle of the BackLight-2 PWM.
* Arguments    : uint16_t DutyCycle_u16
* Return Value : None
***********************************************************************************************************************/
void Set_PWM2_Duty(uint16_t DutyCycle_u16)
{
	uint16_t EquRegValue_u16 = 0;
	
	if(DutyCycle_u16 > MAX_DUTY_CYCLE)
	{
		DutyCycle_u16 = MAX_DUTY_CYCLE;
	}
	
	EquRegValue_u16 = (DutyCycle_u16 * REG_VALUE_PER_1PER_DUTY);
	
	TDR24 = EquRegValue_u16;
	
	return;
}

/********************************************************EOF***********************************************************/
