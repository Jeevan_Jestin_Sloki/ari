/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
* No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED
* OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY
* LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR
* ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability 
* of this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2012, 2018 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/

/***********************************************************************************************************************
* File Name    : r_cg_timer.c
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for TAU module.
* Creation Date: 22/11/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "r_cg_macrodriver.h"
#include "r_cg_timer.h"
/* Start user code for include. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
#include "r_cg_userdefine.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/
/* Start user code for pragma. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
/* Start user code for global. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */

/***********************************************************************************************************************
* Function Name: R_TAU0_Create
* Description  : This function initializes the TAU0 module.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU0_Create(void)
{
    TAU0EN = 1U;        /* supply input clock */
    TPS0 = _0000_TAU_CKM0_FCLK_0 | _0020_TAU_CKM1_FCLK_2 | _0000_TAU_CKM2_FCLK_0 | _0000_TAU_CKM3_FCLK_0;
    /* Stop all channels */
    TT0 = _0001_TAU_CH0_STOP_TRG_ON | _0002_TAU_CH1_STOP_TRG_ON | _0004_TAU_CH2_STOP_TRG_ON |
          _0008_TAU_CH3_STOP_TRG_ON | _0010_TAU_CH4_STOP_TRG_ON | _0020_TAU_CH5_STOP_TRG_ON |
          _0040_TAU_CH6_STOP_TRG_ON | _0080_TAU_CH7_STOP_TRG_ON;
    /* Mask channel 2 interrupt */
    TMMK02 = 1U;        /* disable INTTM02 interrupt */
    TMIF02 = 0U;        /* clear INTTM02 interrupt flag */
    /* Set INTTM02 level 1 priority */
    TMPR102 = 0U;
    TMPR002 = 1U;
    /* Channel 2 used as interval timer */
    TMR02 = _4000_TAU_CLOCK_SELECT_CKM1 | _0000_TAU_CLOCK_MODE_CKS | _0000_TAU_MODE_INTERVAL_TIMER |
            _0000_TAU_START_INT_UNUSED;
    TDR02 = _9C3F_TAU_TDR02_VALUE;
    TOM0 &= ~_0004_TAU_CH2_OUTPUT_COMBIN;
    TOL0 &= ~_0004_TAU_CH2_OUTPUT_LEVEL_L;
    TOE0 &= ~_0004_TAU_CH2_OUTPUT_ENABLE;
    /* Set noise filter sampling clock divisor and channels selected*/
    TNFSMP0 = _00_TAU0_NOISE_DIVISOR;
    TNFCS0 = _00_TAU0_NOISE_CHANNEL_SELECT;
}

/***********************************************************************************************************************
* Function Name: R_TAU0_Channel2_Start
* Description  : This function starts TAU0 channel 2 counter.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU0_Channel2_Start(void)
{
    TMIF02 = 0U;        /* clear INTTM02 interrupt flag */
    TMMK02 = 0U;        /* enable INTTM02 interrupt */
    TS0 |= _0004_TAU_CH2_START_TRG_ON;
}

/***********************************************************************************************************************
* Function Name: R_TAU0_Channel2_Stop
* Description  : This function stops TAU0 channel 2 counter.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU0_Channel2_Stop(void)
{
    TT0 |= _0004_TAU_CH2_STOP_TRG_ON;
    TMMK02 = 1U;        /* disable INTTM02 interrupt */
    TMIF02 = 0U;        /* clear INTTM02 interrupt flag */
}

/***********************************************************************************************************************
* Function Name: R_TAU2_Create
* Description  : This function initializes the TAU2 module.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU2_Create(void)
{
    TAU2EN = 1U;        /* supply input clock */
    TPS2 = _0000_TAU_CKM0_FCLK_0 | _0000_TAU_CKM1_FCLK_0 | _0000_TAU_CKM2_FCLK_0 | _0000_TAU_CKM3_FCLK_0;
    /* Stop all channels */
    TT2 = _0001_TAU_CH0_STOP_TRG_ON | _0002_TAU_CH1_STOP_TRG_ON | _0004_TAU_CH2_STOP_TRG_ON |
          _0008_TAU_CH3_STOP_TRG_ON | _0010_TAU_CH4_STOP_TRG_ON | _0020_TAU_CH5_STOP_TRG_ON |
          _0040_TAU_CH6_STOP_TRG_ON | _0080_TAU_CH7_STOP_TRG_ON;
    /* Mask channel 0 interrupt */
    TMMK20 = 1U;        /* disable INTTM20 interrupt */
    TMIF20 = 0U;        /* clear INTTM20 interrupt flag */
    /* Mask channel 4 interrupt */
    TMMK24 = 1U;        /* disable INTTM24 interrupt */
    TMIF24 = 0U;        /* clear INTTM24 interrupt flag */
    /* Set INTTM20 level 2 priority */
    TMPR120 = 1U;
    TMPR020 = 0U;
    /* Set INTTM24 level 2 priority */
    TMPR124 = 1U;
    TMPR024 = 0U;
    /* Channel 0 is used as master channel for PWM output function */
    TMR20 = _0000_TAU_CLOCK_SELECT_CKM0 | _0000_TAU_CLOCK_MODE_CKS | _0800_TAU_COMBINATION_MASTER |
            _0000_TAU_TRIGGER_SOFTWARE | _0001_TAU_MODE_PWM_MASTER;
    TDR20 = _7CFF_TAU_TDR20_VALUE;
    TOM2 &= ~_0001_TAU_CH0_OUTPUT_COMBIN;
    TOL2 &= ~_0001_TAU_CH0_OUTPUT_LEVEL_L;
    TOE2 &= ~_0001_TAU_CH0_OUTPUT_ENABLE;
    /* Channel 4 is used as slave channel for PWM output function */
    TMR24 = _0000_TAU_CLOCK_SELECT_CKM0 | _0000_TAU_CLOCK_MODE_CKS | _0000_TAU_COMBINATION_SLAVE |
            _0400_TAU_TRIGGER_MASTER_INT | _0009_TAU_MODE_PWM_SLAVE;
    TDR24 = _0000_TAU_TDR24_VALUE;
    TOM2 |= _0010_TAU_CH4_OUTPUT_COMBIN;
    TOL2 &= ~_0010_TAU_CH4_OUTPUT_LEVEL_L;
    TO2 &= ~_0010_TAU_CH4_OUTPUT_VALUE_1;
    TOE2 |= _0010_TAU_CH4_OUTPUT_ENABLE;
    /* Set noise filter sampling clock divisor and channels selected*/
    TNFSMP2 = _00_TAU2_NOISE_DIVISOR;
    TNFCS2 = _00_TAU2_NOISE_CHANNEL_SELECT;
    /* Set TO24 pin */
    TOS21 &= 0xFCU;  
    P6 &= 0xBFU;
    PM6 &= 0xBFU;
}

/***********************************************************************************************************************
* Function Name: R_TAU2_Channel0_Start
* Description  : This function starts TAU2 channel 0 counter.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU2_Channel0_Start(void)
{
    TMIF20 = 0U;        /* clear INTTM20 interrupt flag */
    TMMK20 = 0U;        /* enable INTTM20 interrupt */
    TMIF24 = 0U;        /* clear INTTM24 interrupt flag */
    TMMK24 = 0U;        /* enable INTTM24 interrupt */
    TOE2 |= _0010_TAU_CH4_OUTPUT_ENABLE;
    TS2 |= _0001_TAU_CH0_START_TRG_ON | _0010_TAU_CH4_START_TRG_ON;
}

/***********************************************************************************************************************
* Function Name: R_TAU2_Channel0_Stop
* Description  : This function stops TAU2 channel 0 counter.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void R_TAU2_Channel0_Stop(void)
{
    TT2 |= _0001_TAU_CH0_STOP_TRG_ON | _0010_TAU_CH4_STOP_TRG_ON;
    TOE2 &= ~_0010_TAU_CH4_OUTPUT_ENABLE;
    TMMK20 = 1U;        /* disable INTTM20 interrupt */
    TMIF20 = 0U;        /* clear INTTM20 interrupt flag */
    TMMK24 = 1U;        /* disable INTTM24 interrupt */
    TMIF24 = 0U;        /* clear INTTM24 interrupt flag */
}

/* Start user code for adding. Do not edit comment generated here */
/* End user code. Do not edit comment generated here */
