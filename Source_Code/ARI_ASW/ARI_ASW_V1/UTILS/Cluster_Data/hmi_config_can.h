﻿
#include "App_typedefs.h" 


#define SIGNLE_READ_Mask0     									                0U    
#define SIGNLE_READ_Mask1     									                0x01U 
#define SIGNLE_READ_Mask2     									                0x03U 
#define SIGNLE_READ_Mask3     									                0x07U 
#define SIGNLE_READ_Mask4     									                0x0FU 
#define SIGNLE_READ_Mask5     									                0x1FU 
#define SIGNLE_READ_Mask6     									                0x3FU 
#define SIGNLE_READ_Mask7     									                0x7FU 
#define SIGNLE_READ_Mask8     									                0xFFU 
                
                
#define SIGNLE_WRITE_Mask0    									                0x80U 
#define SIGNLE_WRITE_Mask1    									                0xC0U 
#define SIGNLE_WRITE_Mask2    									                0xE0U 
#define SIGNLE_WRITE_Mask3    									                0xF0U 
#define SIGNLE_WRITE_Mask4    									                0xF8U 
#define SIGNLE_WRITE_Mask5    									                0xFCU 
#define SIGNLE_WRITE_Mask6    									                0xFEU 
#define SIGNLE_WRITE_Mask7    									                0xFFU 
/* def @HMI_CONF_MSG_1 CAN Message                              (2026) */
#define HMI_CONF_MSG_1_ID                                       (2026U)
#define HMI_CONF_MSG_1_IDE                                      (0U)
#define HMI_CONF_MSG_1_DLC                                      (8U)


#define HMI_CONF_MSG_1_BRIGHTNESS_U8FACTOR                      (1)
#define HMI_CONF_MSG_1_CANID_BRIGHTNESS_U8_STARTBIT             (7)
#define HMI_CONF_MSG_1_CANID_BRIGHTNESS_U8_OFFSET               (0)
#define HMI_CONF_MSG_1_CANID_BRIGHTNESS_U8_MIN                  (0)
#define HMI_CONF_MSG_1_CANID_BRIGHTNESS_U8_MAX                  (100)


typedef struct
{
  uint8_t Brightness_u8;
}HMI_CONF_MSG_1_t;


extern HMI_CONF_MSG_1_t	HMI_CONF_MSG_1;


extern uint32_t Deserialize_HMI_CONF_MSG_1(HMI_CONF_MSG_1_t* message, const uint8_t* data);
extern void Clear_HMIConf_CANSignals(void);