/***********************************************************************************************************************
* File Name    : DataAquire.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 01/9/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "DataAquire.h"
#include "DataBank.h"
#include "ari_bms_can.h"
#include "Time.h"
#include "ODO_Disp.h"
#include"signal_calc.h"
#include "Communicator.h"
#include "timer_user.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/

#define AM	0x01
#define PM  0x02

#define SOLID_ON	0x03
#define BLINK		0x01
#define TURN_OFF	0x00
/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
BuzzerEventData_St_t BuzzerEventData_St[TOTAL_BUZZER_EVENT_E] =
{
	{IGNITION_EVENT_E,		_5000_MS,  _250_MS, false, false},
	{TEMP_WARNING_EVENT_E, 		_3000_MS,  _500_MS, false, false},
	{BATTERY_FAULT_EVENT_E,	_5000_MS,  _500_MS, false, false},
	{REVERSE_GEAR_EVENT_E, 		_300_MS,   _150_MS, false, false},
	{INDICATOR_EVENT_E, 		_50_MS ,   _500_MS, false, false},
	{HAZARD_WARNING_EVENT_E, 	_500_MS,  _250_MS, false, false}
};


UserInputSigConf_St_t	UserInputSigConf_St[TOTAL_USER_INPUT_SIG_E] = 
{
/* 	UserInp_signal  UserInp_Sigstate Inputvalue TurnOn_value  TurnOff_value SignalValue       Debouncecount_in_ms	   CaptureTime  UserInp Enable*/
	{INPUT_1_E, 	USER_SIG_OFF_E, 	OFF, 	SIG_SOLID_ON, 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	true},
	{INPUT_2_E, 	USER_SIG_OFF_E, 	OFF, 	SIG_SOLID_ON, 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	true},
	{INPUT_3_E, 	USER_SIG_OFF_E, 	OFF, 	SIG_SOLID_ON, 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	true},
	{INPUT_4_E, 	USER_SIG_OFF_E, 	OFF, 	SIG_SOLID_ON, 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	true},
	{INPUT_5_E, 	USER_SIG_OFF_E, 	OFF, 	IGNITION_ON , 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	true},
	{INPUT_6_E, 	USER_SIG_OFF_E, 	OFF, 	SIG_SOLID_ON, 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	true},
	{INPUT_7_E, 	USER_SIG_OFF_E, 	OFF, 	SIG_SOLID_ON, 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	false},
	{INPUT_8_E, 	USER_SIG_OFF_E, 	OFF, 	SIG_SOLID_ON, 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	false},
	{INPUT_9_E, 	USER_SIG_OFF_E, 	OFF, 	SIG_SOLID_ON, 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	false},
	{INPUT_10_E, 	USER_SIG_OFF_E, 	OFF, 	SIG_SOLID_ON, 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	false},
	{INPUT_11_E, 	USER_SIG_OFF_E, 	OFF, 	SIG_SOLID_ON, 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	false},
	{INPUT_12_E, 	USER_SIG_OFF_E, 	OFF, 	SIG_SOLID_ON, 	SIG_OFF, 	OFF,		SWITCH_DEBOUNCE_MS,    RESET, 	false},
};
IgnitionSource_En_t IgnitionSource_En = IGNITION_NONE_E;

/***********************************************************************************************************************
* Function Name: ReadClusterInput
* Description  : This function reads the user inputs.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ReadClusterInput(void)
{

	uint16_t SigCount_u16 = 0;

	UserInputSigConf_St[INPUT_1_E].UserInputVal_u8 		= P2_bit.no6;	//High Beam
	UserInputSigConf_St[INPUT_2_E].UserInputVal_u8 		= P2_bit.no1;	//Left Indicator
	UserInputSigConf_St[INPUT_3_E].UserInputVal_u8 		= P12_bit.no3;	//Right indicator
	UserInputSigConf_St[INPUT_4_E].UserInputVal_u8 		= P2_bit.no0;	//Hand brake
	UserInputSigConf_St[INPUT_5_E].UserInputVal_u8 		= P13_bit.no7;	// Ignition
	UserInputSigConf_St[INPUT_6_E].UserInputVal_u8 		= P2_bit.no2;	
	UserInputSigConf_St[INPUT_7_E].UserInputVal_u8 		= P2_bit.no2;
	UserInputSigConf_St[INPUT_8_E].UserInputVal_u8 		= P2_bit.no4;
	UserInputSigConf_St[INPUT_9_E].UserInputVal_u8 		= P2_bit.no5;
	UserInputSigConf_St[INPUT_10_E].UserInputVal_u8 	= P2_bit.no7;
	UserInputSigConf_St[INPUT_11_E].UserInputVal_u8 	= P12_bit.no4;
	UserInputSigConf_St[INPUT_12_E].UserInputVal_u8 	= P9_bit.no4;

	/*-------------------------------------------------------------------*/
	for(SigCount_u16 = USER_INPUT_SIG_START_E; SigCount_u16 < TOTAL_USER_INPUT_SIG_E; SigCount_u16++)
	{
		if( true == UserInputSigConf_St[SigCount_u16].UserInputVal_u8 )
		{
			if (USER_SIG_OFF_E == UserInputSigConf_St[SigCount_u16].UserInputState_En)
			{
				/* Change the userinput state USER_SIG_ON_IDLE_E, capture time and wait for  SWITCH_DEBOUNCE_MS sec*/
				UserInputSigConf_St[SigCount_u16].UserInputState_En = USER_SIG_ON_IDLE_E;
				UserInputSigConf_St[SigCount_u16].UserInpCaptureTime_u32 = GET_TIME_MS() + UserInputSigConf_St[SigCount_u16].DebounceValue;
			}
			
			if ( (USER_SIG_ON_IDLE_E == UserInputSigConf_St[SigCount_u16].UserInputState_En) && 
					  (UserInputSigConf_St[SigCount_u16].UserInpCaptureTime_u32 <=  GET_TIME_MS()) )
			{
				/* If signal is turned ON more than SWITCH_DEBOUNCE_MS sec, turn on the respective signal. */
				UserInputSigConf_St[SigCount_u16].SignalState_u8 = UserInputSigConf_St[SigCount_u16].SigTurnOnVal_u8;
				UserInputSigConf_St[SigCount_u16].UserInputState_En = USER_SIG_ON_E;
				UserInputSigConf_St[SigCount_u16].UserInpCaptureTime_u32 = RESET;
			}
			else if(USER_SIG_OFF_IDLE_E == UserInputSigConf_St[SigCount_u16].UserInputState_En)
			{
				UserInputSigConf_St[SigCount_u16].UserInputState_En = USER_SIG_ON_E;
			}
			else
			{
				;
			}
		}
		else if(UserInputSigConf_St[SigCount_u16].UserInputVal_u8 == false)
		{
			if(USER_SIG_ON_E == UserInputSigConf_St[SigCount_u16].UserInputState_En)
			{
				UserInputSigConf_St[SigCount_u16].UserInputState_En = USER_SIG_OFF_IDLE_E;
				UserInputSigConf_St[SigCount_u16].UserInpCaptureTime_u32 = GET_TIME_MS() + UserInputSigConf_St[SigCount_u16].DebounceValue;
			}
			
			if ( (USER_SIG_OFF_IDLE_E == UserInputSigConf_St[SigCount_u16].UserInputState_En) && 
					  (UserInputSigConf_St[SigCount_u16].UserInpCaptureTime_u32 <=  GET_TIME_MS()))
			{
				/* If signal is turned OFF more than SWITCH_DEBOUNCE_MS sec, turn OFF the respective signal. */
				UserInputSigConf_St[SigCount_u16].SignalState_u8 = UserInputSigConf_St[SigCount_u16].SigTurnOffVal_u8;
				UserInputSigConf_St[SigCount_u16].UserInputState_En = USER_SIG_OFF_E;
				UserInputSigConf_St[SigCount_u16].UserInpCaptureTime_u32 = RESET;
			}
			else if(USER_SIG_ON_IDLE_E == UserInputSigConf_St[SigCount_u16].UserInputState_En)
			{
				UserInputSigConf_St[SigCount_u16].UserInputState_En = USER_SIG_OFF_E;
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}
	}


	if(IGNITION_CAN_E == IgnitionSource_En)
	{
		ClusterSignals_St.IgnitionStatus_u8 = 0;//HMI_MSG_1_St.Ignition_u8;
	}
	else if (IGNITION_GPIO_E == IgnitionSource_En)
	{
		ClusterSignals_St.IgnitionStatus_u8 =  UserInputSigConf_St[INPUT_5_E].SignalState_u8;
	}
	else if (IGNITION_NONE_E == IgnitionSource_En)
	{
		ClusterSignals_St.IgnitionStatus_u8 = 1;
	}
	else
	{
		;
	}
	
	return;
}


/***********************************************************************************************************************
* Function Name: UpdateSigToDataBank
* Description  : This function updates the user input signals and signals received over CAN to the Data Bank.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void UpdateSigToDataBank(void)
{

	// if( true == UserInputSigConf_St[INPUT_1_E].UserInputSigEnable_b )
	// {
	// 	/* The signal is mapped with GPIO */
	// 	ClusterSignals_St.HighBeam_u8 	= UserInputSigConf_St[INPUT_1_E].SignalState_u8;
	// }
	// else
	// {
	// 	/* The signal is mapped with CAN */
	// 	ClusterSignals_St.HighBeam_u8  = HMI_MSG_1_St.Side_Stand_u8;
	// }

	// if( true == UserInputSigConf_St[INPUT_4_E].UserInputSigEnable_b )
	// {
	// 	/* The signal is mapped with GPIO */
	// 	ClusterSignals_St.ParkingBreak_u8 	= UserInputSigConf_St[INPUT_4_E].SignalState_u8;
	// }
	// else
	// {
	// 	/* The signal is mapped with CAN */
	// 	ClusterSignals_St.ParkingBreak_u8  = HMI_MSG_1_St.Side_Stand_u8;
	// }

	// if( true == UserInputSigConf_St[INPUT_3_E].UserInputSigEnable_b )
	// {
	// 	ClusterSignals_St.RightIndicator_u8 	= UserInputSigConf_St[INPUT_3_E].SignalState_u8;
	// }
	// else
	// {
	// 	ClusterSignals_St.RightIndicator_u8 	= HMI_MSG_1_St.Right_Indicator_u8;
	// }

	// if( true == UserInputSigConf_St[INPUT_2_E].UserInputSigEnable_b )
	// {
	// 	ClusterSignals_St.LeftIndicator_u8 	=  UserInputSigConf_St[INPUT_2_E].SignalState_u8;
	// }
	// else
	// {
	// 	ClusterSignals_St.LeftIndicator_u8 	=  HMI_MSG_1_St.Left_Indicator_u8;
	// }

	// if( true == UserInputSigConf_St[INPUT_8_E].UserInputSigEnable_b )
	// {
	// 	ClusterSignals_St.KillSwitch_u8 		=   UserInputSigConf_St[INPUT_8_E].SignalState_u8;
	// }
	// else
	// {
	// 	ClusterSignals_St.KillSwitch_u8 		=   HMI_MSG_1_St.Kill_Switch_u8;
	// }

	// if( true == UserInputSigConf_St[INPUT_10_E].UserInputSigEnable_b )
	// {
	// 	ClusterSignals_St.SideStand_u8 		=   UserInputSigConf_St[INPUT_10_E].SignalState_u8;
	// }
	// else
	// {
	// 	ClusterSignals_St.SideStand_u8 			= HMI_MSG_1_St.High_Beam_u8;
	// }

	ClusterSignals_St.ChargingStatus_u8		= GET_BATTPACK_STATUS();;//HMI_MSG_1_St.Battery_Status_u8;
	
	ClusterSignals_St.ODOmeter_u32 			= GET_PRESENT_ODOMETER();
	
	ClusterSignals_St.PowerConsumption_u16 	= 0;//HMI_MSG_3_St.Power_Consumption_u16;
	
	ClusterSignals_St.VehicleSpeed_u16 		= GET_VEHICLE_SPEED();//HMI_MSG_2_St.Vehicle_Speed_u8;
	
	ClusterSignals_St.RangeKm_u16 			= GET_RANGEKM();//HMI_MSG_3_St.Range_Km_u16;
	
	ClusterSignals_St.BatteryFault_u8 		= GET_BATT_FAULT_STATE();//HMI_MSG_1_St.Battery_Fault_u8;
	
	ClusterSignals_St.Warning_u8 			= 0;//HMI_MSG_1_St.Warning_Symbol_u8;
	
	ClusterSignals_St.MotorFault_u8 		= 0;//HMI_MSG_1_St.Motor_Fault_u8;
	
	ClusterSignals_St.ServiceReminder_u8 	= 0;//HMI_MSG_1_St.Service_Indicator_u8;
	
	
	
	ClusterSignals_St.BatterySOC_u8 		= BMS_INFO_MSG_1_St.SOC/10;
	
	ClusterSignals_St.PowerIcon_u8 			= 0;
	
	ClusterSignals_St.TOPText_u8 			= 0; /* Todo jeevan : Turn ON based on the maximum speed limit*/
	
	ClusterSignals_St.ChargingIndicator_u8 	= GET_CHARGER_PLUG_IN_STATE();//HMI_MSG_1_St.Battery_Pack_State_u8;

	ClusterSignals_St.NeutralMode_u8 		= 0;//HMI_MSG_1_St.Neutral_Mode_u8;

	ClusterSignals_St.EconomyMode_u8 		= 0;//HMI_MSG_1_St.Eco_Mode_u8;

	ClusterSignals_St.SportsMode_u8 		= 0;//HMI_MSG_1_St.Sports_Mode_u8;

	ClusterSignals_St.ReverseMode_u8 		= 0;//HMI_MSG_1_St.Reverse_Mode_u8;
	
	ClusterSignals_St.BLEIcon_u8 			= 0;//HMI_MSG_3_St.BLE_Icon_u8;
	
	ClusterSignals_St.HoursTime_u8 			= BMS_INFO_MSG_4_St.System_Hour;//HMI_MSG_1_St.Hours_Time_u8;
	
	ClusterSignals_St.MinutesTime_u8 		= BMS_INFO_MSG_4_St.System_Minute;//HMI_MSG_1_St.Minutes_Time_u8;
	
	ClusterSignals_St.TimeColon_u8 			= 1;
	
	ClusterSignals_St.ParkingMode_u8        = 0;
	
	ClusterSignals_St.Commonsig_u8			= 3;

	
	/* Driving mode text start*/
	ClusterSignals_St.ParkingText_u8        = 0;

	ClusterSignals_St.ReverseText_u8        = 3;

	ClusterSignals_St.NeutralText_u8        = 3;

	ClusterSignals_St.DrivingText_u8        = 3;

	ClusterSignals_St.EcoText_u8            = 3;

    	ClusterSignals_St.SportsText_u8         = 3;
	/* Driving mode text end*/


    	ClusterSignals_St.GearText_u8           = 0;

	ClusterSignals_St.GearDigit_u8          = 0;

	ClusterSignals_St.PercenText_u8         = 3;

	ClusterSignals_St.RegenBraking_u8		= 0;//HMI_MSG_3_St.Regen_Braking_u8;
	
//	if((SOLID_ON == HMI_MSG_1_St.Eco_Mode_u8)||(SOLID_ON == HMI_MSG_1_St.Sports_Mode_u8)||
//		(BLINK == HMI_MSG_1_St.Eco_Mode_u8)||(BLINK == HMI_MSG_1_St.Sports_Mode_u8))
//	{
//		ClusterSignals_St.DrivingMode_u8        = 3;
//	}
//	else
//	{
//		ClusterSignals_St.DrivingMode_u8        = 0;
//	}

	//update_ODOtrip(HMI_MSG_2_St.Trip_Meter);
	//Meridien();

	if((0x02 == BMS_INFO_MSG_3_St.Alarm_Level)||(0x03 == BMS_INFO_MSG_3_St.Alarm_Level))
	{
		BuzzerEventData_St[BATTERY_FAULT_EVENT_E].BuzzerEventStart_b = true;
	}
	else
	{
		BuzzerEventData_St[BATTERY_FAULT_EVENT_E].BuzzerEventStart_b = false;
	}
	return;
}

/***********************************************************************************************************************
* Function Name: update_ODOtrip
* Description  : The function updates trip meter status
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void update_ODOtrip(uint8_t Tripmeter_state)
{
	switch(Tripmeter_state)
	{
		case 0:
		{
			ClusterSignals_St.AText_u8 = 0;
			ClusterSignals_St.BText_u8 = 0;
			ClusterSignals_St.TRIPText_u8 = 0;
			ClusterSignals_St.ODOText_u8 = 3;
			break;
		}
		case 1:
		{
			ClusterSignals_St.ODOText_u8  = 0;
			ClusterSignals_St.TRIPText_u8 = 3;
			ClusterSignals_St.AText_u8 = 3;
			ClusterSignals_St.BText_u8 = 0;
			break;
		}
		case 2:
		{
			ClusterSignals_St.ODOText_u8  = 0;
			ClusterSignals_St.TRIPText_u8 = 3;
			ClusterSignals_St.AText_u8 = 0;
			ClusterSignals_St.BText_u8 = 3;
			break;
		}
		default:
		{
			break;
		}
	}
	return;
}

/***********************************************************************************************************************
* Function Name: update_ODOtrip
* Description  : The function updates trip meter status
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
//void Meridien(void)
//{
//	if(AM == HMI_MSG_1_St.Meridien_u8)
//	{
//		ClusterSignals_St.AMText_u8 = 3;
//		ClusterSignals_St.PMText_u8 = 0;
//	}
//	else if(PM ==  HMI_MSG_1_St.Meridien_u8)
//	{
//		ClusterSignals_St.AMText_u8 = 0;
//		ClusterSignals_St.PMText_u8 = 3;
//	}
//	else
//	{

//	}
//	return;
//}
/***********************************************************************************************************************
* Function Name: ResetTheUserInput
* Description  : This function resets the user input signals state.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ResetTheUserInput(void)
{
	P2_bit.no7 =  PORT_OUTPUT_LOW;
	return;
}

/***********************************************************************************************************************
* Function Name: BuzzerScheduler
* Description  : This function implements main function.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void BuzzerScheduler(void)
{
	BuzzerEvent_En_t BuzzerEventCount_En;
	static uint32_t BuzzerFrequency_u32 = 0;
	static uint16_t DutyCycleBuzzer_u16 = 0;
	static uint8_t BuzzerTriggered_u8 = 0;
	static bool BeepSoundInit_b = false;
	static int16_t DutyCycleCount_s16 = 0;

	for(BuzzerEventCount_En = IGNITION_EVENT_E; BuzzerEventCount_En < TOTAL_BUZZER_EVENT_E; BuzzerEventCount_En++)
	{
		if(true == BuzzerEventData_St[BuzzerEventCount_En].BuzzerEventTriggered_b)
		{
			BuzzerFrequency_u32 = BuzzerEventData_St[BuzzerEventCount_En].BuzzerEventFrequency_ms_u32;
			DutyCycleBuzzer_u16 = BuzzerEventData_St[BuzzerEventCount_En].BuzzerDutyCycle_u16;
			BuzzerEventData_St[BuzzerEventCount_En].BuzzerEventStart_b = true;
			BuzzerTriggered_u8 = true;
			break;
		}
		else
		{
			BuzzerTriggered_u8 = false;
		}
	}

	if(true == BuzzerTriggered_u8)
	{
		if(0 == (TaskSchedulerCounter_u32 % BuzzerFrequency_u32 ) && ( false == BeepSoundInit_b))
		{
			P2_bit.no7 = PORT_OUTPUT_HIGH;
			BeepSoundInit_b = true;
			DutyCycleCount_s16 = DutyCycleBuzzer_u16;
		}
		if(true == BeepSoundInit_b)
		{
			if(0 >= DutyCycleCount_s16 )
			{
				P2_bit.no7 =  PORT_OUTPUT_LOW;
				BeepSoundInit_b = false;
			}
			else
			{
				DutyCycleCount_s16-=5;
			}
		}
	}
	else
	{
		if(PORT_OUTPUT_HIGH == P2_bit.no7)
		{
			P2_bit.no7 =  PORT_OUTPUT_LOW;
			DutyCycleCount_s16 = RESET_VALUE;
			BeepSoundInit_b = false;
			BuzzerFrequency_u32 = RESET_VALUE;
			DutyCycleCount_s16 = RESET_VALUE;
		}
	}


}
/********************************************************EOF***********************************************************/