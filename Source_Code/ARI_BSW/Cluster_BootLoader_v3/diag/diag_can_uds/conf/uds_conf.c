/******************************************************************************
 *    FILENAME    : uds_conf.c
 *    DESCRIPTION : This file contains configuration data for UDS services.
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki     18/01/2017		   Initial version
 ******************************************************************************/
// Sloki
/*
 **************************************************************************************************
 *    Includes
 **************************************************************************************************
 */

#include "uds_conf.h"
#include "iso14229_serv10.h"
#include "iso14229_serv11.h"
#include "iso14229_serv27.h"
#include "iso14229_serv31.h"
#include "iso14229_serv34.h"
#include "iso14229_serv36.h"
#include "iso14229_serv37.h"
#include "diag_appl_test.h"
 


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */
/**
Constant service distributor table for OBD services. Duplicate the table for ISO 14229 or other diagnostic protocol
*/

const ServiceDistUDS_St_t UDSServDist_apt[ServDistNumEntries_u32] = 
{
    
    /*  SID     Initialization function   service implementation   */
	//{ SID_TESTERPRESENT,        NULL,                     &iso14229_serv3E,       (UINT8 )(UDS_SESS_EXTENDED|UDS_SESS_DEFAULT|UDS_SESS_PROG)},
	  { SID_SESSIONCONTROL,       NULL,                     &iso14229_serv10,       (UINT8 )(UDS_SESS_EXTENDED|UDS_SESS_DEFAULT|UDS_SESS_PROG)},
	  { SID_ECURESET,             NULL,                     &iso14229_serv11,       (UINT8 )(UDS_SESS_EXTENDED|UDS_SESS_DEFAULT|UDS_SESS_PROG)},
	  { SID_SA,                   NULL,                     &iso14229_serv27,       (UINT8 )(UDS_SESS_EXTENDED|UDS_SESS_PROG)},
	  { SID_ROUTINE_CONTROL,      NULL,                     &iso14229_serv31,       (UINT8 )(UDS_SESS_EXTENDED|UDS_SESS_PROG)},// Configuration structure for service 31
	  { SID_REQUEST_DOWNLOAD,     NULL,                     &iso14229_serv34,       (UINT8 )(UDS_SESS_EXTENDED|UDS_SESS_PROG)}, // Configuration structure for service 34
	  { SID_TRANSFER_DATA,        NULL,                     &iso14229_serv36,       (UINT8 )(UDS_SESS_EXTENDED|UDS_SESS_PROG)}, // Configuration structure for service 36
	  { SID_TRANSFER_DATA_EXIST,  NULL,                     &iso14229_serv37,       (UINT8 )(UDS_SESS_EXTENDED|UDS_SESS_PROG)}, // Configuration structure for service 37

};


    /* ************************************************************************** */
    // Section: Structure definition                                                  */
    /* ************************************************************************** */

    MemBlockConf_St_t   MemBlockConf_aSt[TOTAL_ASW_BLOCKS] =                                //structure of arrays  for memory blocks
    {
       {ASW1, 	0x00009000,         0x00007000,             ERASABLE_E}, // Block - 0
       {ASW2, 	0x00010000,         0x00010000,             ERASABLE_E}, // Block - 1
       //{ASW3, 	0x00020000,         0x00007000,             ERASABLE_E}, // Block - 2
    };

    RIDConf_St_t RIDConf_aSt[TOTAL_RID] =
    {

     {RID_ERASE         ,RID_ERASE_MIN_LENGTH       ,EraseMemory        ,SERVICE_IDLE_E                    ,SERVICE31_ERASE_E          ,   0x04U },
     {RID_CHECKSUM      ,RID_CHECKSUM_MIN_LENGTH    ,CalculateCheckSum  ,SERVICE37_E                    ,SERVICE_IDLE_E       ,   0x04U }
    };


/******************************************************************************
 End of File
 ******************************************************************************/
