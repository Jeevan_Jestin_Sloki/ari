/***********************************************************************************************************************
* File Name    : Navigation_Conf.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 03/12/2021
***********************************************************************************************************************/

#ifndef NAVIGATION_CONF_H
#define NAVIGATION_CONF_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Cluster_Conf.h"
#include "GenConfig.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
*/
extern  const SignalsValue_St_t		Navig_SignalStraight_aSt[STRAIGHT_SIG_LEN];
extern	const SignalsValue_St_t		Navig_SignalLeft_aSt[STRAIGHT_SIG_LEN];
extern	const SignalsValue_St_t		Navig_SignalRight_aSt[STRAIGHT_SIG_LEN];
extern	const SignalsValue_St_t		Navig_SignalLeftUTurn_aSt[STRAIGHT_SIG_LEN];
extern	const SignalsValue_St_t		Navig_SignalRightUTURN_aSt[STRAIGHT_SIG_LEN];
//extern  const SignalsValue_St_t		Navigation_SignalsValue_aSt[NAVIG_DIR_SIG_LEN];
extern  const SignalConfig_St_t 		Navigation_LT_sigconf_ast[TWO_E];
extern  const SignalConfig_St_t 		Navigation_LUT_sigconf_ast[TWO_E];
extern  const SignalConfig_St_t 		Navigation_ST_sigconf_ast[THREE_E];
extern  const SignalConfig_St_t 		Navigation_RT_sigconf_ast[TWO_E];
extern  const SignalConfig_St_t 		Navigation_RUT_sigconf_ast[TWO_E];
extern  const SegConfig_St_t			Navigation_LT_OFF_segconf_ast[THREE_E];
extern  const SegConfig_St_t			Navigation_LT_ON_segconf_ast[THREE_E];
extern  const SegConfig_St_t			Navigation_LUT_OFF_segconf_ast[THREE_E];
extern  const SegConfig_St_t			Navigation_LUT_ON_segconf_ast[THREE_E];
extern  const SegConfig_St_t			Navigation_ST_OFF_segconf_ast[THREE_E];
extern  const SegConfig_St_t			Navigation_ST_ON_segconf_ast[THREE_E];
extern  const SegConfig_St_t			Navigation_ST_5_segconf_ast[THREE_E];
extern  const SegConfig_St_t			Navigation_RT_OFF_segconf_ast[THREE_E];
extern  const SegConfig_St_t			Navigation_RT_ON_segconf_ast[THREE_E];
extern  const SegConfig_St_t			Navigation_RUT_OFF_segconf_ast[THREE_E];
extern  const SegConfig_St_t			Navigation_RUT_ON_segconf_ast[THREE_E];


/*

		Naviagtion distanace configration
*/

extern  const SignalsValue_St_t			Navig_dist_SignalsValue_aSt[NAVIG_DIST_SIG_LEN];



extern  const SignalConfig_St_t 		Navig_dist_Sign1conf_aSt[TWO_E];
extern  const SignalConfig_St_t 		Navig_dist_Sign2conf_aSt[TWO_E];
extern  const SignalConfig_St_t 		Navig_dist_Sign3conf_aSt[ELEVEN_E];



extern  const SegConfig_St_t 			Navig_dist_seg1ON_segconf_ast[ONE_SEG_E];
extern  const SegConfig_St_t 			Navig_dist_seg1OFF_segconf_ast[ONE_SEG_E];


extern  const SegConfig_St_t 			Navig_dist_seg2ON_segconf_ast[ONE_SEG_E];
extern  const SegConfig_St_t 			Navig_dist_seg2OFF_segconf_ast[ONE_SEG_E];


extern  const SegConfig_St_t 			Navig_dist_Segn3_0_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			Navig_dist_Segn3_1_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			Navig_dist_Segn3_2_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			Navig_dist_Segn3_3_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			Navig_dist_Segn3_4_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			Navig_dist_Segn3_5_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			Navig_dist_Segn3_6_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			Navig_dist_Segn3_7_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			Navig_dist_Segn3_8_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			Navig_dist_Segn3_9_SegConf_aSt[TWO_E];
extern  const SegConfig_St_t 			Navig_dist_Segn3_OFF_SegConf_aSt[TWO_E];


#endif