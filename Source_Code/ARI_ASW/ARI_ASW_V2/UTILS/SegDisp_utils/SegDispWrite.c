/***********************************************************************************************************************
* File Name    : SegDispWrite.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 25/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "SegDispWrite.h"
#include "digits_utils.h"
#include "GenConfig.h"
#include "SOC_Disp.h"

/***********************************************************************************************************************
macro directive
***********************************************************************************************************************/
#define     REG_WRITE(i,j,k,l)   	(*(ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Seg_pu8) =  \
					(*(ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Seg_pu8) &  \
					ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Mask_u8) | 	  \
					ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SegConfig_pSt[l].Value_u8)	

/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


/***********************************************************************************************************************
* Function Name: Write_SEG
* Description  : This function Writes Data into the Segment registers to display the signals on the cluster.
* Arguments    : ClusterSignals_En_t ClusterSignals_En, uint32_t Signal_Value_u32
* Return Value : None
***********************************************************************************************************************/
void Write_SEG(ClusterSignals_En_t ClusterSignals_En, uint32_t Signal_Value_u32)
{
	uint16_t	i = 0; 
	uint16_t	j = 0;
	uint16_t	k = 0;
	uint16_t	l = 0;
	uint8_t 	SignalVal_au8[MAX_LENGTH_SIGNAL] = {0x0F,0x0F,0x0F,0x0F,0x0F,0x0F,0x0F,0x0F,0x0F,0x0F};
	bool		SeperateDigitsb = true;
	
	if((CH_SOC_Disp_En == DISP_CH_TEXT_E)&&(ClusterSignals_En == SPEED_E))
	{
		SignalVal_au8[0] = 'H'; 	/*To Display 'H'*/
		SignalVal_au8[1] = 'C'; 	/*To Display 'C'*/
		SeperateDigitsb = false;
	}
	else if((ClusterSignals_En == HOURS_TIME_E) && (Signal_Value_u32 == (MAX_HOURS_TIME + 1)))
	{
		SeperateDigitsb = false;
	}
	else if((ClusterSignals_En == MINUTES_TIME_E) && (Signal_Value_u32 == (MAX_MINUTES_TIME + 1)))
	{
		SeperateDigitsb = false;
	}
	else
	{
		;
	}
	
	if(true == SeperateDigitsb)
	{
		if(9 < Signal_Value_u32)
		{
			GetDigitsFromNum(Signal_Value_u32,SignalVal_au8);	
		}
		else
		{
			SignalVal_au8[0] = (uint8_t)Signal_Value_u32;
			
			if((ClusterSignals_En == HOURS_TIME_E) || (ClusterSignals_En == MINUTES_TIME_E))
			{
				SignalVal_au8[1] = 0U;
			}
		}
	}
	
	for(i=CLUSTER_SIG_START_E; i < TOTAL_SIGNALS_E; i++)
	{
		if(ClusterSigConf_aSt[i].ClusterSignals_En == ClusterSignals_En)
		{
			for(j=0; j < ClusterSigConf_aSt[i].SigLength_u8; j++)
			{
				for(k=0; k < ClusterSigConf_aSt[i].SignalsValue_pSt[j].SignalLen_En; k++)
				{
					if(SignalVal_au8[j] == ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].SignalsValue_En)
					{
						for(l=0; l < ClusterSigConf_aSt[i].SignalsValue_pSt[j].SigConfig_pSt[k].Seg_Count_u8; l++)
						{
							REG_WRITE(i,j,k,l);
						}
					}	
				}			
			}
			
		break;
		}
	}

	return;	
}	


/***********************************************************************************************************************
* Function Name: SetOrClearSegmentReg
* Description  : This function Set/clears all segment registers (SEG0 - SEG47) except unused segments.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void SetOrClearSegmentReg(uint8_t SegRegValue_u8)
{
	uint16_t   SegCount_u16 = 0;
	volatile __near uint8_t  	*Segment_pu8; 
	Segment_pu8 = &SEG0;           /* Assign address of the SEG0 Register to the pointer */
	
	for(SegCount_u16 = 0; SegCount_u16 < TOTAL_SEG_REGISTERS; SegCount_u16++)
	{
		if((SegCount_u16 != _23_UNUSED_SEGMENT)|| (SegCount_u16 != _24_UNUSED_SEGMENT) || \
					(SegCount_u16 != _44_UNUSED_SEGMENT))
		{
			*Segment_pu8 = SegRegValue_u8;  /*Write Clear value to the Segment Register*/
		}
		else
		{
			;
		}
		Segment_pu8++;         /*Point the Next Segment Register*/
	}
	
	return;
}


/********************************************************EOF***********************************************************/