﻿

#include "ari_bms_can.h"


 uint32_t Deserialize_BMS_MSG_3(BMS_MSG_3_t* message, const uint8_t* data)
{
  message->BatteryPackState_u8 = ((data[0] & (SIGNLE_READ_Mask2))) + BMS_MSG_3_CANID_BATTERYPACKSTATE_U8_OFFSET;
   return BMS_MSG_3_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_BMS_MSG_3(BMS_MSG_3_t* message, uint8_t* data)
{
  message->BatteryPackState_u8 = (message->BatteryPackState_u8  - BMS_MSG_3_CANID_BATTERYPACKSTATE_U8_OFFSET);
  data[0] = (message->BatteryPackState_u8 & (SIGNLE_READ_Mask2)) ;
   return BMS_MSG_3_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Deserialize_BMS_MSG_1_St_t(BMS_MSG_1_St_t_t* message, const uint8_t* data)
{
  message->BatteryVoltage_u16 = (((data[0] & (SIGNLE_READ_Mask8)) << BMS_MSG_1_ST_T_BATTERYVOLTAGE_U16_MASK0) | ((data[1] & (SIGNLE_READ_Mask8)) << BMS_MSG_1_ST_T_BATTERYVOLTAGE_U16_MASK1) | (data[2] & (SIGNLE_READ_Mask8))) + BMS_MSG_1_ST_T_CANID_BATTERYVOLTAGE_U16_OFFSET;
  message->BatteryCurrent_u16 = (((data[3] & (SIGNLE_READ_Mask8)) << BMS_MSG_1_ST_T_BATTERYCURRENT_U16_MASK0) | (data[4] & (SIGNLE_READ_Mask8))) + BMS_MSG_1_ST_T_CANID_BATTERYCURRENT_U16_OFFSET;
  message->BatterySOC_u16 = ((data[5] & (SIGNLE_READ_Mask8))) + BMS_MSG_1_ST_T_CANID_BATTERYSOC_U16_OFFSET;
  message->BatteryTotalCapacity = (((data[6] & (SIGNLE_READ_Mask8)) << BMS_MSG_1_ST_T_BATTERYTOTALCAPACITY_MASK0) | (data[7] & (SIGNLE_READ_Mask8))) + BMS_MSG_1_ST_T_CANID_BATTERYTOTALCAPACITY_OFFSET;
   return BMS_MSG_1_ST_T_ID; 
}


/*----------------------------------------------------------------------------*/


 uint32_t Serialize_BMS_MSG_1_St_t(BMS_MSG_1_St_t_t* message, uint8_t* data)
{
  message->BatteryVoltage_u16 = (message->BatteryVoltage_u16  - BMS_MSG_1_ST_T_CANID_BATTERYVOLTAGE_U16_OFFSET);
  message->BatteryCurrent_u16 = (message->BatteryCurrent_u16  - BMS_MSG_1_ST_T_CANID_BATTERYCURRENT_U16_OFFSET);
  message->BatterySOC_u16 = (message->BatterySOC_u16  - BMS_MSG_1_ST_T_CANID_BATTERYSOC_U16_OFFSET);
  message->BatteryTotalCapacity = (message->BatteryTotalCapacity  - BMS_MSG_1_ST_T_CANID_BATTERYTOTALCAPACITY_OFFSET);
  data[0] = ((message->BatteryVoltage_u16 >> BMS_MSG_1_ST_T_BATTERYVOLTAGE_U16_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[1] = ((message->BatteryVoltage_u16 >> BMS_MSG_1_ST_T_BATTERYVOLTAGE_U16_MASK1) & (SIGNLE_READ_Mask8)) ;
  data[2] = (message->BatteryVoltage_u16 & (SIGNLE_READ_Mask8)) ;
  data[3] = ((message->BatteryCurrent_u16 >> BMS_MSG_1_ST_T_BATTERYCURRENT_U16_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[4] = (message->BatteryCurrent_u16 & (SIGNLE_READ_Mask8)) ;
  data[5] = (message->BatterySOC_u16 & (SIGNLE_READ_Mask8)) ;
  data[6] = ((message->BatteryTotalCapacity >> BMS_MSG_1_ST_T_BATTERYTOTALCAPACITY_MASK0) & (SIGNLE_READ_Mask8)) ;
  data[7] = (message->BatteryTotalCapacity & (SIGNLE_READ_Mask8)) ;
   return BMS_MSG_1_ST_T_ID; 
}
