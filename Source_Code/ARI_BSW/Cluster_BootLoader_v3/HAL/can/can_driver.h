/***********************************************************************************************************************
* File Name    : can_driver.h
* Version      : 01
* Description  : This file implements the device driver for the CAN module.
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

#ifndef CAN_DRIVER_H
#define CAN_DRIVER_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/



/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define	CAN_MSG_BUFFER_ADDRESS		((unsigned long)(0x0F0600))	    /* Base address of the CAN-0 Message buffers. */


/*
	CAN register address data
*/
#define	ADD_MBUF_00			(MSGBUF_BASE_ADDR + (unsigned short)(0x0000))
#define	ADD_MBUF_01			(MSGBUF_BASE_ADDR + (unsigned short)(0x0010))
#define	ADD_MBUF_02			(MSGBUF_BASE_ADDR + (unsigned short)(0x0020))
#define	ADD_MBUF_03			(MSGBUF_BASE_ADDR + (unsigned short)(0x0030))
#define	ADD_MBUF_04			(MSGBUF_BASE_ADDR + (unsigned short)(0x0040))
#define	ADD_MBUF_05			(MSGBUF_BASE_ADDR + (unsigned short)(0x0050))
#define	ADD_MBUF_06			(MSGBUF_BASE_ADDR + (unsigned short)(0x0060))
#define	ADD_MBUF_07			(MSGBUF_BASE_ADDR + (unsigned short)(0x0070))
#define	ADD_MBUF_08			(MSGBUF_BASE_ADDR + (unsigned short)(0x0080))
#define	ADD_MBUF_09			(MSGBUF_BASE_ADDR + (unsigned short)(0x0090))
#define	ADD_MBUF_10			(MSGBUF_BASE_ADDR + (unsigned short)(0x00a0))
#define	ADD_MBUF_11			(MSGBUF_BASE_ADDR + (unsigned short)(0x00b0))
#define	ADD_MBUF_12			(MSGBUF_BASE_ADDR + (unsigned short)(0x00c0))
#define	ADD_MBUF_13			(MSGBUF_BASE_ADDR + (unsigned short)(0x00d0))
#define	ADD_MBUF_14			(MSGBUF_BASE_ADDR + (unsigned short)(0x00e0))
#define	ADD_MBUF_15			(MSGBUF_BASE_ADDR + (unsigned short)(0x00f0))



/*
	CAN TX-MSG ID's
*/
#define TX_ID_7F1	                        0x07F1

/*
	CAN RX-MSG ID's
*/
#define RX_ID7F0	                        0x07F0
/*
	DLC
*/
#define MSG_LENGTH                           0x08


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
void CAN_Init(void);
void CAN_MsgBuf_Init(void);
void CAN_StopMode_Setting(void);
void CAN_StopMode_Release(void);
void CAN_SleepMode_Setting(void);
void CAN_SleepMode_Release(void);
void Rx_MsgBuf_Init(uint8_t ,unsigned int);
void Tx_MsgBuf_Init(uint8_t ,unsigned int ,uint8_t);
void Tx_MsgBuf_Processing(uint8_t ,uint8_t  ,uint8_t*);
void ISR_Msgbuf_Receive(void);
void CAN_Error_Process(void);
extern void SetCAN_STBY_Pin(void);

#endif /* CAN_DRIVER_H */


