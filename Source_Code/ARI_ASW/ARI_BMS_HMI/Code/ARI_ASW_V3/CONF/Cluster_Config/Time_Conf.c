/***********************************************************************************************************************
* File Name    : Time_Conf.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 28/11/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Time_Conf.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/
/*
			Minutes Time Configuration
*/
//  const SignalsValue_St_t		MinutesTime_SignalsValue_aSt[MINUTES_TIME_SIG_LEN] =
// {
// /* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
// 	{ ZERO_E, 	SEVENTEEN_E,	MinutesTime_Sig1Conf_ast },
// 	{ ONE_E, 	SEVENTEEN_E,	MinutesTime_Sig2Conf_ast }
// };

 const SignalsValue_St_t		MinutesTimeLB_SignalsValue_aSt[MINUTE_HBLB_SIG_LEN] =
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	{ ZERO_E, 	SIXTEEN_E,	MinutesTime_Sig1Conf_ast },
};

 const SignalsValue_St_t		MinutesTimeHB_SignalsValue_aSt[MINUTE_HBLB_SIG_LEN] =
{
/* Signal Position   PositionSignalLength    PositionSignalValueConfiguration */
	{ ZERO_E, 	SIXTEEN_E,		MinutesTime_Sig2Conf_ast }
};

 const SignalConfig_St_t		MinutesTime_Sig1Conf_ast[SIXTEEN_E] = 
{
/*PositionSignalValue  SegmentsRequired  SegmentsConfiguration*/
	{ ZERO_E,	TWO_SEG_E,	MinutesTime_S1_0_SegConf_aSt	},
	{ ONE_E,	TWO_SEG_E,	MinutesTime_S1_1_SegConf_aSt	},
	{ TWO_E,	TWO_SEG_E,	MinutesTime_S1_2_SegConf_aSt	},
	{ THREE_E,	TWO_SEG_E,	MinutesTime_S1_3_SegConf_aSt	},
	{ FOUR_E,	TWO_SEG_E,	MinutesTime_S1_4_SegConf_aSt	},
	{ FIVE_E,	TWO_SEG_E,	MinutesTime_S1_5_SegConf_aSt	},
	{ SIX_E,	TWO_SEG_E,	MinutesTime_S1_6_SegConf_aSt	},
	{ SEVEN_E,	TWO_SEG_E,	MinutesTime_S1_7_SegConf_aSt	},
	{ EIGHT_E,	TWO_SEG_E,	MinutesTime_S1_8_SegConf_aSt	},
	{ NINE_E,	TWO_SEG_E,	MinutesTime_S1_9_SegConf_aSt	},
	{ DISPLAY_A,	TWO_SEG_E,	MinutesTime_S1_A_SegConf_aSt	},
	{ DISPLAY_B,	TWO_SEG_E,	MinutesTime_S1_B_SegConf_aSt	},
	{ DISPLAY_C,	TWO_SEG_E,	MinutesTime_S1_C_SegConf_aSt	},
	{ DISPLAY_D,	TWO_SEG_E,	MinutesTime_S1_D_SegConf_aSt	},
	{ DISPLAY_E,	TWO_SEG_E,	MinutesTime_S1_E_SegConf_aSt	},
	{ DISPLAY_F,	TWO_SEG_E,	MinutesTime_S1_F_SegConf_aSt	},
};

 const SignalConfig_St_t		MinutesTime_Sig2Conf_ast[SIXTEEN_E] = 
{
	{ ZERO_E,	TWO_SEG_E,	MinutesTime_S2_0_SegConf_aSt},
	{ ONE_E,	TWO_SEG_E,	MinutesTime_S2_1_SegConf_aSt},
	{ TWO_E,	TWO_SEG_E,	MinutesTime_S2_2_SegConf_aSt},
	{ THREE_E,	TWO_SEG_E,	MinutesTime_S2_3_SegConf_aSt},
	{ FOUR_E,	TWO_SEG_E,	MinutesTime_S2_4_SegConf_aSt},
	{ FIVE_E,	TWO_SEG_E,	MinutesTime_S2_5_SegConf_aSt},
    { SIX_E,	TWO_SEG_E,	MinutesTime_S2_6_SegConf_aSt},
	{ SEVEN_E,	TWO_SEG_E,	MinutesTime_S2_7_SegConf_aSt},
	{ EIGHT_E,	TWO_SEG_E,	MinutesTime_S2_8_SegConf_aSt},
	{ NINE_E,	TWO_SEG_E,	MinutesTime_S2_9_SegConf_aSt},
	{ DISPLAY_A,	TWO_SEG_E,	MinutesTime_S2_A_SegConf_aSt},
	{ DISPLAY_B,	TWO_SEG_E,	MinutesTime_S2_B_SegConf_aSt},
	{ DISPLAY_C,	TWO_SEG_E,	MinutesTime_S2_C_SegConf_aSt},
	{ DISPLAY_D,	TWO_SEG_E,	MinutesTime_S2_D_SegConf_aSt},
	{ DISPLAY_E,	TWO_SEG_E,	MinutesTime_S2_E_SegConf_aSt},
	{ DISPLAY_F,	TWO_SEG_E,	MinutesTime_S2_F_SegConf_aSt},
};


 const SegConfig_St_t			MinutesTime_S1_0_SegConf_aSt[TWO_SEG_E] = 
{
/* SegmentRegisterAddress   SegmentMaskValue  Segment Value */
	{&SEG6,		0x01U,		0x0AU},
	{&SEG5,		0x00U,		0x0FU},

};

 const SegConfig_St_t			MinutesTime_S1_1_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x0AU},
	{&SEG5,		0x00U,		0x00U},
};

 const SegConfig_St_t			MinutesTime_S1_2_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x06U,},
	{&SEG5,		0x00U,		0x0DU},
};

 const SegConfig_St_t			MinutesTime_S1_3_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x0EU,},
	{&SEG5,		0x00U,		0x09U},
};

 const SegConfig_St_t			MinutesTime_S1_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x0EU},
	{&SEG5,		0x00U,		0x02U},
};

 const SegConfig_St_t			MinutesTime_S1_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x0C},
	{&SEG5,		0x00U,		0x0BU},
};

 const SegConfig_St_t			MinutesTime_S1_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x0CU},
	{&SEG5,		0x00U,		0x0FU},
};


 const SegConfig_St_t			MinutesTime_S1_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x0AU},
	{&SEG5,		0x00U,		0x01U},
};


 const SegConfig_St_t			MinutesTime_S1_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x0EU},
	{&SEG5,		0x00U,		0x0FU},
};


 const SegConfig_St_t			MinutesTime_S1_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x0EU},
	{&SEG5,		0x00U,		0x0BU},
};

 const SegConfig_St_t			MinutesTime_S1_Off_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x00U},
	{&SEG5,		0x00U,		0x00U},
};

const SegConfig_St_t			MinutesTime_S1_A_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x0EU},
	{&SEG5,		0x00U,		0x07U},
};

const SegConfig_St_t			MinutesTime_S1_B_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x0CU},
	{&SEG5,		0x00U,		0x0EU},
};
const SegConfig_St_t			MinutesTime_S1_C_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x00U},
	{&SEG5,		0x00U,		0x0FU},
};
const SegConfig_St_t			MinutesTime_S1_D_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x0EU},
	{&SEG5,		0x00U,		0x0CU},
};
const SegConfig_St_t			MinutesTime_S1_E_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x04U},
	{&SEG5,		0x00U,		0x0FU},
};
const SegConfig_St_t			MinutesTime_S1_F_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG6,		0x01U,		0x04U},
	{&SEG5,		0x00U,		0x07U},
};

 const SegConfig_St_t			MinutesTime_S2_0_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0AU},
	{&SEG3,		0x00U,		0x0FU},

};

 const SegConfig_St_t			MinutesTime_S2_1_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0AU},
	{&SEG3,		0x00U,		0x00U},
};

 const SegConfig_St_t			MinutesTime_S2_2_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x06U},
	{&SEG3,		0x00U,		0x0DU},
};

 const SegConfig_St_t			MinutesTime_S2_3_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0EU},
	{&SEG3,		0x00U,		0x09U},
};

 const SegConfig_St_t			MinutesTime_S2_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0EU},
	{&SEG3,		0x00U,		0x02U},
};

 const SegConfig_St_t			MinutesTime_S2_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0CU},
	{&SEG3,		0x00U,		0x0BU},
};

 const SegConfig_St_t			MinutesTime_S2_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0CU},
	{&SEG3,		0x00U,		0x0FU},
};

 const SegConfig_St_t			MinutesTime_S2_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0AU},
	{&SEG3,		0x00U,		0x01U},
};

 const SegConfig_St_t			MinutesTime_S2_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0EU},
	{&SEG3,		0x00U,		0x0FU},
};

 const SegConfig_St_t			MinutesTime_S2_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0EU},
	{&SEG3,		0x00U,		0x0BU},
};


 const SegConfig_St_t			MinutesTime_S2_Off_SegConf_aSt[TWO_SEG_E] = 
{	
	{&SEG4,		0x01U,		0x0AU},
	{&SEG3,		0x00U,		0x0FU},
};

const SegConfig_St_t			MinutesTime_S2_A_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0EU},
	{&SEG3,		0x00U,		0x07U},
};

const SegConfig_St_t			MinutesTime_S2_B_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0CU},
	{&SEG3,		0x00U,		0x0EU},
};
const SegConfig_St_t			MinutesTime_S2_C_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x00U},
	{&SEG3,		0x00U,		0x0FU},
};
const SegConfig_St_t			MinutesTime_S2_D_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x0EU},
	{&SEG3,		0x00U,		0x0CU},
};
const SegConfig_St_t			MinutesTime_S2_E_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x04U},
	{&SEG3,		0x00U,		0x0FU},
};
const SegConfig_St_t			MinutesTime_S2_F_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG4,		0x01U,		0x04U},
	{&SEG3,		0x00U,		0x07U},
};



/*
	Hours Time Configuration
*/
 const SignalsValue_St_t		HoursTime_SignalsValue_aSt[HOURS_TIME_SIG_LEN] =
{
	{ ZERO_E,	THIRTEEN_E,	HoursTime_Sig1Conf_ast },
	{ ONE_E, 	TWO_E,		HoursTime_Sig2Conf_ast }
};


 const SignalConfig_St_t		HoursTime_Sig1Conf_ast[THIRTEEN_E] = 
{
	{ ZERO_E,	TWO_SEG_E,	HoursTime_S1_0_SegConf_aSt	},
	{ ONE_E,	TWO_SEG_E,	HoursTime_S1_1_SegConf_aSt	},
	{ TWO_E,	TWO_SEG_E,	HoursTime_S1_2_SegConf_aSt	},
	{ THREE_E,	TWO_SEG_E,	HoursTime_S1_3_SegConf_aSt	},
	{ FOUR_E,	TWO_SEG_E,	HoursTime_S1_4_SegConf_aSt	},
	{ FIVE_E,	TWO_SEG_E,	HoursTime_S1_5_SegConf_aSt	},
	{ SIX_E,	TWO_SEG_E,	HoursTime_S1_6_SegConf_aSt	},
	{ SEVEN_E,	TWO_SEG_E,	HoursTime_S1_7_SegConf_aSt	},
	{ EIGHT_E,	TWO_SEG_E,	HoursTime_S1_8_SegConf_aSt	},
	{ NINE_E,	TWO_SEG_E,	HoursTime_S1_9_SegConf_aSt	},
	{ OFF_DIGIT,	TWO_SEG_E,	HoursTime_S1_Off_SegConf_aSt	},
	{ DISPLAY_T,	TWO_SEG_E,	HoursTime_S1_T_SegConf_aSt	},
	{ DISPLAY_E,	TWO_SEG_E,HoursTime_S1_E_SegConf_aSt}

};

 const SignalConfig_St_t		HoursTime_Sig2Conf_ast[TWO_E] = 
{
	{ ON_E,		ONE_E,		HoursTime_S2_ON_SegConf_aSt	},
	{ OFF_DIGIT,	ONE_E,		HoursTime_S2_OFF_SegConf_aSt	},
	
};


 const SegConfig_St_t			HoursTime_S1_0_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x0AU},
	{&SEG1,		0x00U,		0x0FU},

};

 const SegConfig_St_t			HoursTime_S1_1_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x0AU},
	{&SEG1,		0x00U,		0x00U},
};

 const SegConfig_St_t			HoursTime_S1_2_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x06U},
	{&SEG1,		0x00U,		0x0DU},
};

 const SegConfig_St_t			HoursTime_S1_3_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x0EU},
	{&SEG1,		0x00U,		0x09U},
};

 const SegConfig_St_t			HoursTime_S1_4_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x0EU},
	{&SEG1,		0x00U,		0x02U},
};

 const SegConfig_St_t			HoursTime_S1_5_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x0CU},
	{&SEG1,		0x00U,		0x0BU},
};

 const SegConfig_St_t			HoursTime_S1_6_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x0CU},
	{&SEG1,		0x00U,		0x0FU},
};


 const SegConfig_St_t			HoursTime_S1_7_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x0AU},
	{&SEG1,		0x00U,		0x01U},
};


 const SegConfig_St_t			HoursTime_S1_8_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x0EU},
	{&SEG1,		0x00U,		0x0FU},
};


 const SegConfig_St_t			HoursTime_S1_9_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x0EU},
	{&SEG1,		0x00U,		0x0BU},
};

 const SegConfig_St_t			HoursTime_S1_Off_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x00U},
	{&SEG1,		0x00U,		0x00U},
};

 const SegConfig_St_t			HoursTime_S1_T_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x04U},
	{&SEG1,		0x00U,		0x0EU},
};

 const SegConfig_St_t			HoursTime_S1_E_SegConf_aSt[TWO_SEG_E] = 
{
	{&SEG2,		0x01U,		0x04U},
	{&SEG1,		0x00U,		0x0FU},
};


 const SegConfig_St_t			HoursTime_S2_ON_SegConf_aSt[ONE_E] = 
{
	{&SEG4,		0x0EU,		0x01U},
	

};

 const SegConfig_St_t			HoursTime_S2_OFF_SegConf_aSt[ONE_E] = 
{
	{&SEG4,		0x0EU,		0x00U},
	
};
/*
   Time COLON teltail Configuration 
*/
 const SignalsValue_St_t			Colon_SignalsValue_aSt[TIME_COLON_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		ColonSigConf_ast,},
};

 const SignalConfig_St_t			ColonSigConf_ast[TWO_E] =
{
	{OFF_E,		ONE_SEG_E,	Colon_Off_SegConf_aSt},
	{ON_E,		ONE_SEG_E,	Colon_On_SegConf_aSt},
};

 const SegConfig_St_t			Colon_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG2,		0x0EU,		0x00U},
};

 const SegConfig_St_t			Colon_On_SegConf_aSt[ONE_SEG_E] = 
{
	{&SEG2,		0x0EU,		0x01U},
};

/*
   	Ante Meridiem teltail Congfigration
*/


 const SignalsValue_St_t			Text_AM_SignalsValue_aSt[TEXT_AM_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		AM_SigConf_ast},
};

 const SignalConfig_St_t			AM_SigConf_ast[TWO_E]=
{
	{OFF_E, 	ONE_SEG_E,	AM_off_Segconf_ast},
	{ON_E,	 	ONE_SEG_E,	AM_on_Segconf_ast}, 
	
};

 const SegConfig_St_t 			AM_off_Segconf_ast[ONE_SEG_E]=
{
	&SEG7, 		0x07U,		0x00U
};

 const SegConfig_St_t 			AM_on_Segconf_ast[ONE_SEG_E]=
{
	&SEG7, 		0x07U,		0x08U
};

/*
   	Post Meridiem teltail Congfigration
*/

const SignalsValue_St_t			Text_PM_SignalsValue_aSt[TEXT_PM_SIG_LEN] = 
{
	{ZERO_E,	TWO_E,		PM_SigConf_ast},
};

const SignalConfig_St_t			PM_SigConf_ast[TWO_E]=
{
	{OFF_E, 	ONE_SEG_E,	PM_off_Segconf_ast},
	{ON_E,	 	ONE_SEG_E,	PM_on_Segconf_ast},
	
};

const SegConfig_St_t 				PM_off_Segconf_ast[ONE_SEG_E]=
{
	&SEG7, 		0x0BU,		0x00U
};
 const SegConfig_St_t 			PM_on_Segconf_ast[ONE_SEG_E]=
{
	&SEG7, 		0x0BU,		0x04U
};



/*---------------------------------------------------------------------------------------------------------------------*/

/********************************************************EOF***********************************************************/