/***********************************************************************************************************************
* File Name    : ClusterAnimation.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "ClusterAnimation.h"
#include "MileageDisp.h"
#include "ODO_Disp.h"
#include "PowerConsum_Disp.h"
#include "RangeKmDisp.h"
#include "SOC_Disp.h"
#include "SpeedDisp.h"
#include "TelltaleDisp.h"
#include "TimeDisp.h"
#include "timer_user.h"
#include "Cluster_Conf.h"
#include "SegDispWrite.h"
#include "digits_utils.h"
#include "DataBank.h"
#include "CommonSigDisp.h"
#include "BackLightCtrl.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/

AnimationData_St_t	AnimationData_St = 
{
	0,
	0,
	0,
	0,
};

SpeedAnimation_En_t		SpeedAnimation_En 		= SPEED_NO_CHANGE_E;
SignalState_En_t		SignalState_En			= SIG_NO_CHANGE_E;
CentralTellTale_En_t	CentralTellTale_En 		= CENTRAL_TELLTALE_NONE_E;

volatile uint32_t 	Animation5msCounter_u32 	= 0;
uint16_t			Anim_PresentDutyCycle_u16	= (uint16_t)PWM_DUTY_ON_IGNITION_ON_1;
uint16_t			Anim_PrevDutyCycle_u16		= (uint16_t)PWM_DUTY_ON_IGNITION_ON_1;
bool 				AnimationDone_b 			= false;
bool 				TurnOnLogo_b 				= true;
bool				StopBrightnessCtrl_b		= false;

/***********************************************************************************************************************
* Function Name: AnimateOnStart
* Description  : This function schedules the Anmation tasks at different rates.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void AnimateOnStart(void)
{
	/*Schedule 5ms tasks here*/
	Animation5msCounter_u32++; /*Increments by 1 for each 5 ms*/
	
	/*Schedule 10ms Animation Tasks here*/
//	 if((Animation5msCounter_u32 % 2) == 0)
//	 {
//	 	if(false == StopBrightnessCtrl_b)
//	 	{
//	 		ControlBrightness(++Anim_PresentDutyCycle_u16);
//	 	}
//	 }

	if(TurnOnLogo_b)
	{
		/*todo:dileepabs*/ //Update the Animation as required for the LOGO Controlled LCD-DISPLAY.
		TurnOnLogo_b = false;
		Control_LOGO(OFF);	/*todo:dileepabs*/ //BatteryIcon & Logo segments interchanged
	}

	if(((Animation5msCounter_u32 % 80) == 0)&&(SpeedAnimation_En == SPEED_NO_CHANGE_E))
	{
		/*To Provide 1000ms delay after Logo-On*/
		SpeedAnimation_En = SPEED_ANIMATE_START_E;
		StopBrightnessCtrl_b = true;
		
		ControlCommonSig(ON);
		Control_WHKM_Text(ON);
		Control_BatteryIcon(ON);
		Control_PowerIcon(ON);
		Control_DrvMode_Texts(DRV_SPORTS_MODE_E, ON);
		Control_DrvMode_Texts(DRV_ECO_MODE_E, ON);
		Control_DrvMode_Texts(DRV_NEUTRAL_MODE_E, ON);
		Control_DrvMode_Texts(DRV_REVERSE_MODE_E, ON);

		#if(BACKLIGHT_CTRL_STATIC_MODE == TRUE)
		BackLightCtrl_En = BACKLIGHT_CTRL_STATIC_E;
		#elif(BACKLIGHT_CTRL_ALS_MODE == TRUE)
			BackLightCtrl_En = BACKLIGHT_CTRL_ALS_E;
		#elif(BACKLIGHT_CTRL_CAN_MODE == TRUE)
			BackLightCtrl_En = BACKLIGHT_CTRL_CAN_E;
		#endif
		StaticControlMode_En = STATIC_MODE_CTRL_2_E;
		Turn_ON_BackLight(); //Set Backlight Brightness to actual after the Logo Fade-In.

	}
	if((Animation5msCounter_u32 % 9) == 0)
	{
		/*Schedule 45ms Animation Tasks here*/
		if((SpeedAnimation_En == SPEED_ANIMATE_START_E)||(SpeedAnimation_En == INCREASE_SPEED_E))
		{
			/*Start Animating the vehicle speed*/
			AnimateIncreaseVehicleSpeed();
		}
		
	}
	if((Animation5msCounter_u32 % 8) == 0)
	{
		/*Schedule 40ms Animation Tasks here*/
		if(SpeedAnimation_En == DECREASE_SPEED_E)
		{
			/*Start Animating the vehicle speed*/
			AnimateDecreaseVehicleSpeed();
		}
		
	}
	if(SpeedAnimation_En == INTERMEDIATE_DELAY_E)
	{
		AnimationData_St.SpeedDelayCounter_u16++; 
			/*Increments by 1 for each 5ms (Starts after Speed reaches 100) */
		if(0 == (AnimationData_St.SpeedDelayCounter_u16 % 80)) /*Provide Intermediate delay of 400ms*/
		{
			SpeedAnimation_En = DECREASE_SPEED_E;
			AnimationData_St.SpeedDelayCounter_u16 = 0;
			
			SetTopRowTelltales(SET_SIG_ACTUAL_E);
			Set_ODO_and_Time(SET_SIG_ACTUAL_E);
			Set_CentreTelltales(SET_SIG_ACTUAL_E);
			SetBarsActualValue();
		}
	}
	return;
}


void AnimateIncreaseVehicleSpeed(void)
{
	if(SpeedAnimation_En == SPEED_ANIMATE_START_E)
	{
		SpeedAnimation_En = INCREASE_SPEED_E;
	}
	
	Write_SEG(SPEED_E, (uint32_t)AnimationData_St.VehicleSpeed_s16);
	
	Write_SEG(BATT_SOC_E, AnimationData_St.BarsCountValue_u32);
	Write_SEG(POWER_CONSUMP_E, AnimationData_St.BarsCountValue_u32);
	
	if(SpeedAnimation_En == INCREASE_SPEED_E)
	{
		AnimationData_St.VehicleSpeed_s16 += INCREASE_IN_SPEED;
		
		if(0 == (AnimationData_St.VehicleSpeed_s16 % 10))
		{
			AnimationData_St.BarsCount_u16 = (AnimationData_St.VehicleSpeed_s16 / 10); 
			AnimationData_St.BarsCountValue_u32 += Find_Power_Of_10((AnimationData_St.BarsCount_u16 - 1));
		}
		
		if(0 == (AnimationData_St.VehicleSpeed_s16 % 35))
		{
			Set_CentreTelltales(TURN_ON_SIG_E);
		}
		
		if(AnimationData_St.VehicleSpeed_s16 == (MAXIMUM_SPEED + INCREASE_IN_SPEED))
		{
			AnimationData_St.VehicleSpeed_s16 = MAXIMUM_SPEED;
			
			SetTopRowTelltales(TURN_ON_SIG_E);
			Set_ODO_and_Time(TURN_ON_SIG_E);
			
			SpeedAnimation_En = INTERMEDIATE_DELAY_E;
		}
	}
	else
	{
		;
	}
	return;
}


void AnimateDecreaseVehicleSpeed(void)
{
	Write_SEG(SPEED_E, (uint32_t)AnimationData_St.VehicleSpeed_s16);
	
	AnimationData_St.VehicleSpeed_s16 -= DECREASE_IN_SPEED;
			
	if(AnimationData_St.VehicleSpeed_s16 == (MINIMUM_SPEED - DECREASE_IN_SPEED))
	{
		AnimationData_St.VehicleSpeed_s16 = MINIMUM_SPEED;
		SetBottomRowTelltales(TURN_ON_SIG_E);
		Set_Range_and_Mileage(TURN_ON_SIG_E);
		SetDrivingModeRings(TURN_ON_SIG_E);
		SpeedAnimation_En = SPEED_ANIMATE_END_E;
	}
	else
	{
		;
	}
	return;
}


void SetTopRowTelltales(SignalState_En_t SignalStateEn)
{
	switch(SignalStateEn)
	{
		case TURN_ON_SIG_E:
		{
			Write_SEG(SIDE_STAND_E,    (uint32_t)ON);
			Write_SEG(SAFE_MODE_E,     (uint32_t)ON);
			Write_SEG(BATT_FAULT_E,    (uint32_t)ON);
			Write_SEG(MOTOR_FAULT_E,   (uint32_t)ON);
			Write_SEG(KILL_SWITCH_E,   (uint32_t)ON);
			break;	
		}
		case SET_SIG_ACTUAL_E:
		{
			if(ClusterSignals_St.SideStandSig_u8 == 0x00U)
			{
				Write_SEG(SIDE_STAND_E,    (uint32_t)OFF);
			}
			if(ClusterSignals_St.SafeModeSig_u8 == 0x00U)
			{
				Write_SEG(SAFE_MODE_E,     (uint32_t)OFF);
			}
			if(ClusterSignals_St.BatteryTextSig_u8 == 0x00U)
			{
				Write_SEG(BATT_FAULT_E,    (uint32_t)OFF);
			}
			if(ClusterSignals_St.MotorTextSig_u8 == 0x00U)
			{
				Write_SEG(MOTOR_FAULT_E,   (uint32_t)OFF);
			}
			if(ClusterSignals_St.KillSwitchSig_u8 == 0x00U)
			{
				Write_SEG(KILL_SWITCH_E,   (uint32_t)OFF);
			}
			break;	
		}
		default:
		{
			;	
		}
	}
	return;
}

void Set_ODO_and_Time(SignalState_En_t SignalStateEn)
{
	uint32_t ODOmeter_u32 	= 0;
	uint8_t HoursTime_u8 	= 0;
	uint8_t MinutesTime_u8 	= 0;
	
	switch(SignalStateEn)
	{
		case TURN_ON_SIG_E:
		{
			ODOmeter_u32 = 888888;
			HoursTime_u8 = 88;
			MinutesTime_u8 = 88;
			Write_SEG(ODO_E, ODOmeter_u32);
			Write_SEG(MINUTES_TIME_E, (uint32_t)MinutesTime_u8);
			Write_SEG(HOURS_TIME_E,   (uint32_t)HoursTime_u8);
			Write_SEG(TIME_COLON_E, (uint32_t)ON);
			
			break;	
		}
		case SET_SIG_ACTUAL_E:
		{
			ODOmeter_u32 = ClusterSignals_St.ODOmeterSig_u32;
			HoursTime_u8 = (uint8_t)ClusterSignals_St.HoursTimeSig_u16;
			MinutesTime_u8 = (uint8_t)ClusterSignals_St.MinutesTimeSig_u16;
			Write_SEG(ODO_E, (ODOmeter_u32 / 100));
			Write_SEG(MINUTES_TIME_E, (uint32_t)MinutesTime_u8);
			Write_SEG(HOURS_TIME_E,   (uint32_t)HoursTime_u8);
			Write_SEG(TIME_COLON_E, (uint32_t)ON);
			break;	
		}
		default:
		{
			;	
		}
	}
	return;
}

void SetBottomRowTelltales(SignalState_En_t SignalStateEn)
{
	switch(SignalStateEn)
	{
		case TURN_ON_SIG_E:
		{
			Write_SEG(BLE_E,           (uint32_t)ON);
			Write_SEG(REGEN_BRAKING_E, (uint32_t)ON);
			break;	
		}
		case SET_SIG_ACTUAL_E:
		{
			/*todo:animation*/
			/*Signals Actual state/Value will be set from DisplayClusterData task*/
			break;	
		}
		default:
		{
			;	
		}
	}
	return;
}


void SetDrivingModeRings(SignalState_En_t SignalStateEn)
{
	switch(SignalStateEn)
	{
		case TURN_ON_SIG_E:
		{
			Write_SEG(REVERSE_MODE_E, (uint32_t)ON);
			Write_SEG(NEUTRAL_MODE_E, (uint32_t)ON);
			Write_SEG(ECO_MODE_E,     (uint32_t)ON);
			Write_SEG(SPORTS_MODE_E, (uint32_t)ON);
			break;	
		}
		case SET_SIG_ACTUAL_E:
		{
			/*todo:animation*/
			/*Signals Actual state/Value will be set from DisplayClusterData task*/
			break;	
		}
		default:
		{
			;	
		}
	}
	return;
}

void Set_Range_and_Mileage(SignalState_En_t SignalStateEn)
{
	uint16_t RangeKm_u16 = 0;
	uint8_t Mileage_u8 = 0;
	
	switch(SignalStateEn)
	{
		case TURN_ON_SIG_E:
		{
			RangeKm_u16 = 888;
			Mileage_u8 = 88;
			Write_SEG(RANGE_KM_E, (uint32_t)RangeKm_u16);
			Write_SEG(MILEAGE_E, (uint32_t)Mileage_u8);
			break;	
		}
		case SET_SIG_ACTUAL_E:
		{
			/*todo:animation*/
			/*Signals Actual state/Value will be set from DisplayClusterData task*/
			break;	
		}
		default:
		{
			;	
		}
	}
	return;
}

void Set_CentreTelltales(SignalState_En_t SignalStateEn)
{
	if(CentralTellTale_En == CENTRAL_TELLTALE_NONE_E)
	{
		CentralTellTale_En = CENTER_TELLTALE_1_E;
	}
	switch(SignalStateEn)
	{
		case TURN_ON_SIG_E:
		{
			switch(CentralTellTale_En)
			{
				case CENTER_TELLTALE_1_E:
				{
					Write_SEG(HIGH_BEAM_E,     (uint32_t)ON);
					Write_SEG(WARNING_IND_E,   (uint32_t)ON);
					CentralTellTale_En = CENTER_TELLTALE_2_E;
					break;
				}
				case CENTER_TELLTALE_2_E:
				{
					Write_SEG(LOW_BEAM_E,      (uint32_t)ON);
					Write_SEG(SERV_REM_E,      (uint32_t)ON);
					CentralTellTale_En = CENTER_TELLTALE_3_E;
					break;
				}
				case CENTER_TELLTALE_3_E:
				{
					Write_SEG(LEFT_IND_E,      (uint32_t)ON);
					Write_SEG(RIGHT_IND_E,     (uint32_t)ON);
					CentralTellTale_En = CENTRAL_TELLTALE_NONE_E;
					break;
				}
				default:
				{
					;
				}
			}
			break;	
		}
		case SET_SIG_ACTUAL_E:
		{
			if(ClusterSignals_St.LeftIndicatorSig_u8 == 0x00U)
			{
				Write_SEG(LEFT_IND_E,      (uint32_t)OFF);
			}
			if(ClusterSignals_St.RightIndicatorSig_u8 == 0x00U)
			{
				Write_SEG(RIGHT_IND_E,     (uint32_t)OFF);
			}
			if(ClusterSignals_St.LowBeamSig_u8 == 0x00U)
			{
				Write_SEG(LOW_BEAM_E,      (uint32_t)OFF);
			}
			if(ClusterSignals_St.HighBeamSig_u8 == 0x00U)
			{
				Write_SEG(HIGH_BEAM_E,     (uint32_t)OFF);
			}
			if(ClusterSignals_St.WarningSig_u8  == 0x00U)
			{
				Write_SEG(WARNING_IND_E,   (uint32_t)OFF);
			}
			if(ClusterSignals_St.ServiceReminderSig_u8 == 0x00U)
			{
				Write_SEG(SERV_REM_E,      (uint32_t)OFF);
			}
			break;	
		}
		default:
		{
			;	
		}
	}
	return;	
}

void SetBarsActualValue(void)
{
	uint32_t BarCountValue_u32	= 0;
	uint16_t PowrConsump_u16 	= 0;
	uint16_t  BattSOC_u8 		= 0;
	
	PowrConsump_u16 = ClusterSignals_St.PowerConsumBarsSig_u16;
	BattSOC_u8 	= (uint16_t)ClusterSignals_St.SOCSig_u8;
	BarCountValue_u32 = Get_Equi_Bars_Value(PowrConsump_u16);
	Write_SEG(POWER_CONSUMP_E, BarCountValue_u32);
				/*Display Actual Power Consumption*/
	BarCountValue_u32 = Get_SOC_Bars(BattSOC_u8, 0x0U);
	Write_SEG(BATT_SOC_E, BarCountValue_u32);
	return;
}


void ControlBrightness(uint16_t DutyCycle_u16)
{
	if(DutyCycle_u16 > 100)
	{
		DutyCycle_u16 = 100;
	}
	if(DutyCycle_u16 != Anim_PrevDutyCycle_u16)
	{
		SetPWMDutyCycle(DutyCycle_u16);
		Anim_PrevDutyCycle_u16 = DutyCycle_u16;
	}
	else
	{
		;
	}
	return;
}


void ResetAnimationData(void)
{
	/*Reset Flags Related to the cluster data display*/
	InitialMileage_Disp_b 		= false;
	InitialODO_Disp_b 			= false;
	InitialPowerConsum_Disp_b 	= false;
	InitialRangeKm_Disp_b 		= false;
	InitialSOC_Disp_b 			= false;
	InitialSpeed_Disp_b 		= false;
	Initial_Minutes_Disp_b 		= false;
	Initial_Hours_Disp_b 		= false;

	ClrTelltalesStates();

	/*Reset the Animation Related Data*/
	_1ms_Counter_u16 						= RESET; /*Exported Variable from the Timer*/
	Animation5msCounter_u32 				= RESET; /*Reset the Task Scheduling Counter*/
	AnimationDone_b 						= false;
	TurnOnLogo_b							= true;
	AnimationData_St.VehicleSpeed_s16 		= RESET;
	SpeedAnimation_En 						= SPEED_NO_CHANGE_E;
	AnimationData_St.SpeedDelayCounter_u16 	= 0;
	AnimationData_St.BarsCountValue_u32 	= 0;
	AnimationData_St.BarsCount_u16 			= 0;
	SignalState_En							= SIG_NO_CHANGE_E;
	CentralTellTale_En 						= CENTRAL_TELLTALE_NONE_E;

	Anim_PresentDutyCycle_u16				= (uint16_t)PWM_DUTY_ON_IGNITION_ON_1;
	Anim_PrevDutyCycle_u16					= (uint16_t)PWM_DUTY_ON_IGNITION_ON_1;
	StopBrightnessCtrl_b					= false;

	return;
}

/********************************************************EOF***********************************************************/