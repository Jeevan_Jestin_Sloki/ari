/***********************************************************************************************************************
* File Name    : ODO_Disp.c
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 06/12/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "ODO_Disp.h"
#include "SegDispWrite.h"
#include "GenConfig.h"
#include "DataBank.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool 	     		InitialODO_Disp_b = false;

/***********************************************************************************************************************
* Function Name: Display_ODO
* Description  : This function validate and displays the ODO from 0000.00 to 9999.99
* Arguments    : uint32_t ODO_u32, ClusterSignals_En_t ODO_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void ODOTrip_Disp(uint32_t ODO_u32, ClusterSignals_En_t ODO_ENUM_E)
{
	Disp_ODODigit(ODO_u32,ODO_ENUM_E);
	return;
}

/***********************************************************************************************************************
* Function Name: Disp_ODODigit
* Description  : This function validate and displays the ODO from 0000.00 to 9999.99
* Arguments    : uint32_t ODO_u32, ClusterSignals_En_t ODO_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void Disp_ODODigit(uint32_t ODO_u32, ClusterSignals_En_t ODO_ENUM_E )
{
	uint32_t	     ODO_Km_u32 = 0;
	static uint32_t      PrevODO_u32 = 0;
	
	ODO_DispState_En_t  ODO_DispState_En = ODO_DISP_HOLD_E;
	
	if(PrevODO_u32 == ODO_u32)
	{
		ODO_DispState_En = ODO_DISP_HOLD_E;
		if(false == InitialODO_Disp_b)
		{
			ODO_DispState_En = ODO_DISPLAY_E;
			InitialODO_Disp_b = true;
		}
	}
	else
	{
		ODO_DispState_En = ODO_DISPLAY_E;
		PrevODO_u32 = ODO_u32;
	}
	switch(ODO_DispState_En)
	{
		case ODO_DISPLAY_E:
		{
			
			ODO_Km_u32 = Get_ODO_in_Km(ODO_u32);
			Write_SEG(ODO_ENUM_E,ODO_Km_u32,DECIMAL); 
			break;
		}
		case ODO_DISP_HOLD_E:
		{
			break;
		}
		default:
		{
			;	
		}
	}
}
/***********************************************************************************************************************
* Function Name: Get_ODO_in_Km
* Description  : This function validates the ODO and converts ODO value from meters to Km.
* Arguments    : uint32_t   ODO_u32
* Return Value : uint32_t   ODO_To_Disp_u32
***********************************************************************************************************************/
uint32_t Get_ODO_in_Km(uint32_t ODO_u32)
{
	uint32_t	ODO_To_Disp_u32 = 0;
	if(ODO_u32 > MAX_ODO_RANGE_IN_METER)
	{
		ODO_u32 = MAX_ODO_RANGE_IN_METER;
	}
	else
	{
		;	
	}
	
	ODO_To_Disp_u32 = (ODO_u32/100);
	
	return ODO_To_Disp_u32;
}


/********************************************************EOF***********************************************************/