/***********************************************************************************************************************
* File Name    : intc_user.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 07/09/2021
***********************************************************************************************************************/

#ifndef INTC_USER_H
#define INTC_USER_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/



/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern uint8_t IgnitionPinValue_u8;


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/

#endif /* INTC_USER_H */


