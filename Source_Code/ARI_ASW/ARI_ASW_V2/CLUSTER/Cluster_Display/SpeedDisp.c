/***********************************************************************************************************************
* File Name    : SpeedDisp.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 28/01/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "SpeedDisp.h"
#include "SegDispWrite.h"
#include "GenConfig.h"


/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
bool 	     			InitialSpeed_Disp_b 	= false;
bool SetSpeedToZero_b = false;

/***********************************************************************************************************************
* Function Name: Display_Speed
* Description  : This function validate and displays the vehicle speed from 0 -199 KM/HR
* Arguments    : uint16_t Speed_u16, ClusterSignals_En_t SPPED_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void Display_Speed(uint16_t Speed_u16, ClusterSignals_En_t SPPED_ENUM_E) 
{
	uint16_t	     ValidSpeed_u16 = 0;
	static uint16_t      PrevSpeed_u16 = 0;
	
	SpeedDispState_En_t  SpeedDispState_En = SPEED_DISP_HOLD_E;
	
	
		if(PrevSpeed_u16 == Speed_u16)
		{
			SpeedDispState_En = SPEED_DISP_HOLD_E;
			if(false == InitialSpeed_Disp_b)
			{
				SpeedDispState_En = SPEED_DISPLAY_E;
				InitialSpeed_Disp_b = true;
			}
		}
		else
		{
			SpeedDispState_En = SPEED_DISPLAY_E;
			PrevSpeed_u16 = Speed_u16;
		}
		switch(SpeedDispState_En)
		{
			case SPEED_DISPLAY_E:
			{
				ValidSpeed_u16 = ValidateSpeed(Speed_u16);
				Write_SEG(SPPED_ENUM_E, (uint32_t)ValidSpeed_u16); 
				break;
			}
			case SPEED_DISP_HOLD_E:
			{
				break;
			}
			default:
			{
				;	
			}
		}
	return;
}


/***********************************************************************************************************************
* Function Name: ValidateSpeed
* Description  : This function validates the speed value received.
* Arguments    : uint16_t  SpeedCheck_u16
* Return Value : uint16_t  SpeedCheck_u16
***********************************************************************************************************************/
uint16_t ValidateSpeed(uint16_t SpeedCheck_u16)
{
	if(SpeedCheck_u16 > MAX_SPEED_RANGE)
	{
		SpeedCheck_u16 = MAX_SPEED_RANGE;
	}
	else
	{
		;
	}
	return SpeedCheck_u16;
}


/***********************************************************************************************************************
* Function Name: SetSpeedToZero
* Description  : This function sets the speed to 0 during discharging mode and immediate after the animation.
* Arguments    :uint16_t SpeedZero_u16, ClusterSignals_En_t SPPED_ENUM_E
* Return Value : None
***********************************************************************************************************************/
void SetSpeedToZero(uint16_t SpeedZero_u16, ClusterSignals_En_t SPPED_ENUM_E)
{
	Write_SEG(SPPED_ENUM_E, (uint32_t)SpeedZero_u16); 
			/*Set Speed to Zero*/
	return;
}

/********************************************************EOF***********************************************************/