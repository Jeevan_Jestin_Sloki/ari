/***********************************************************************************************************************
* File Name    : version.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 05/01/2022
***********************************************************************************************************************/

#ifndef VERSION_H
#define VERSION_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	
#include "App_typedefs.h" 


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
/* The number to be incremented whenever a new release happens internally/to customer */
/*
Format
	Version numbering schemes: A.B.C.D
	A - incremented whenever a new module/file is added.
	B - incremented whenever a new API/function is added.
	C - incremented whenever a bug fixing with file/module/function/API
	D - incremented for any minor/cosmetics/comments/...etc changes.
*/

#define ASW_SW_VERSION							 				"1.0.0.0" 
		
#define ASW_SW_VERSION_LEN										15
		
#define DFB1_TOTAL_BYTES										(ASW_SW_VERSION_LEN)
#define FLASH_BLOCK_1											1
#define FLASH_BLOCK1_START_POS									0
		
		
#define STORE_RESTORE_SW_VERSION								TRUE //todo:configurable	/* TRUE OR FALSE*/


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void StoreSWVersion(void);
extern void RestoreSWVersion(void);


#endif /* VERSION_H */


