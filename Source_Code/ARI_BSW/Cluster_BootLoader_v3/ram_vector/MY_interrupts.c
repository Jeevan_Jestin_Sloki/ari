////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include "r_cg_macrodriver.h"
#include "MY_RAM_vector.h"
#include "can_driver.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma interrupt Watchdog_Interrupt(vect=INTWDTI)
#pragma interrupt INTLVI_Interrupt(vect=INTLVI)
#pragma interrupt INTP0_Interrupt(vect=INTP0)
#pragma interrupt INTP1_Interrupt(vect=INTP1)
#pragma interrupt INTP2_Interrupt(vect=INTP2)
#pragma interrupt INTP3_Interrupt(vect=INTP3)
#pragma interrupt INTP4_Interrupt(vect=INTP4)
#pragma interrupt INTP5_Interrupt(vect=INTP5)
#pragma interrupt INTCLM_Interrupt(vect=INTCLM)
#pragma interrupt INTCSI00_Interrupt(vect=INTCSI00)
#pragma interrupt INTCSI01_Interrupt(vect=INTCSI01)
#pragma interrupt INTDMA0_Interrupt(vect=INTDMA0)
#pragma interrupt INTDMA1_Interrupt(vect=INTDMA1)
#pragma interrupt INTRTC_Interrupt(vect=INTRTC)
#pragma interrupt INTIT_Interrupt(vect=INTIT)
#pragma interrupt INTLT0_Interrupt(vect=INTLT0)
#pragma interrupt INTLR0_Interrupt(vect=INTLR0)
#pragma interrupt INTLS0_Interrupt(vect=INTLS0)
#pragma interrupt INTSG_Interrupt(vect=INTSG)
#pragma  interrupt Timer_Interrupt(vect=INTTM00)
#pragma  interrupt INTTM01_Interrupt(vect=INTTM01)
#pragma  interrupt INTTM02_Interrupt(vect=INTTM02)
#pragma  interrupt INTTM03_Interrupt(vect=INTTM03)
#pragma  interrupt INTAD_Interrupt(vect=INTAD)
#pragma  interrupt INTLT1_Interrupt(vect=INTLT1)
#pragma  interrupt INTLR1_Interrupt(vect=INTLR1)
#pragma  interrupt INTLS1_Interrupt(vect=INTLS1)
#pragma  interrupt INTPLR1_Interrupt(vect=INTPLR1)
#pragma  interrupt INTIIC11_Interrupt(vect=INTIIC11)
#pragma  interrupt INTTM04_Interrupt(vect=INTTM04)
#pragma  interrupt INTTM05_Interrupt(vect=INTTM05)
#pragma  interrupt INTTM06_Interrupt(vect=INTTM06)
#pragma  interrupt INTTM07_Interrupt(vect=INTTM07)
#pragma  interrupt INTC0ERR_Interrupt(vect=INTC0ERR)
#pragma  interrupt INTC0WUP_Interrupt(vect=INTC0WUP)
#pragma  interrupt INTC0REC_Interrupt(vect=INTC0REC)
#pragma  interrupt INTC0TRX_Interrupt(vect=INTC0TRX)
#pragma  interrupt INTTM10_Interrupt(vect=INTTM10)
#pragma  interrupt INTTM11_Interrupt(vect=INTTM11)
#pragma  interrupt INTTM12_Interrupt(vect=INTTM12)
#pragma  interrupt INTTM13_Interrupt(vect=INTTM13)
#pragma  interrupt INTMD_Interrupt(vect=INTMD)
#pragma  interrupt INTFL_Interrupt(vect=INTFL)
#pragma  interrupt INTTM14_Interrupt(vect=INTTM14)
#pragma  interrupt INTTM15_Interrupt(vect=INTTM15)
#pragma  interrupt INTTM16_Interrupt(vect=INTTM16)
#pragma  interrupt INTTM17_Interrupt(vect=INTTM17)
#pragma  interrupt INTTM20_Interrupt(vect=INTTM20)
#pragma  interrupt INTTM21_Interrupt(vect=INTTM21)
#pragma  interrupt INTTM22_Interrupt(vect=INTTM22)
#pragma  interrupt INTTM23_Interrupt(vect=INTTM23)
#pragma  interrupt INTTM24_Interrupt(vect=INTTM24)
#pragma  interrupt INTTM26_Interrupt(vect=INTTM26)

//------------------------------------------------

static void __near Watchdog_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTWDTI_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTLVI_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTLVI_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTP0_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTP0_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTP1_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTP1_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTP2_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTP2_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTP3_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTP3_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTP4_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTP4_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTP5_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTP5_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTCLM_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTCLM_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTCSI00_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTCSI00_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTCSI01_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTCSI01_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTDMA0_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTDMA0_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTDMA1_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTDMA1_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTRTC_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTRTC_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTIT_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTIT_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTLT0_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTLT0_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTLR0_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTLR0_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTLS0_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTLS0_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTSG_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTSG_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near Timer_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*(*RAM_INTTM00_ISR))();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM01_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM01_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM02_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM02_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM03_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM03_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTAD_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTAD_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTLT1_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTLT1_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTLR1_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTLR1_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTLS1_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTLS1_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTPLR1_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTPLR1_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTIIC11_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTIIC11_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM04_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM04_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM05_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM05_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM06_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM06_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM07_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM07_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTC0ERR_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTC0ERR_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTC0WUP_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTC0WUP_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTC0REC_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    //ISR)_Msgbuf_Receive();
    (*RAM_INTC0REC_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTC0TRX_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTC0TRX_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM10_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM10_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM11_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM11_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM12_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM12_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM13_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM13_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTMD_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTMD_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTFL_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTFL_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM14_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM14_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM15_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM15_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM16_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM16_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM17_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM17_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM20_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM20_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM21_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM21_ISR)();
    /* End user code. Do not edit comment generated here */
}

static void __near INTTM22_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM22_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM23_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM23_ISR)();
    /* End user code. Do not edit comment generated here */
}

static void __near INTTM24_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM24_ISR)();
    /* End user code. Do not edit comment generated here */
}
static void __near INTTM26_Interrupt(void)
{
    /* Start user code. Do not edit comment generated here */
    (*RAM_INTTM26_ISR)();
    /* End user code. Do not edit comment generated here */
}


//----------------------------------------------
