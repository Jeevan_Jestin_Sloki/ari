/***********************************************************************************************************************
* File Name    : Deserialize.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 01/03/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Deserialize.h"
#include "Time.h"
#include "hmi_config_can.h"
#include "ari_bms_can.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/


/***********************************************************************************************************************
* Function Name: CANSched_HMI_RxMsgCallback
* Description  : This function works as a call back function to the upper layer segemented reception and Calls the
                 deserialize functions to update the HMI Signals.
* Arguments    : uint16_t CIL_SigName_En,CAN_MessageFrame1_St_t* Can_Applidata_St
* Return Value : None
***********************************************************************************************************************/
void CANSched_HMI_RxMsgCallback(uint16_t CIL_SigName_En,CAN_MessageFrame_St_t* Can_Applidata_St)
{
	switch(CIL_SigName_En)
	{
		case CIL_BMS_MSG_1_E:
		{
			Deserialize_BMS_MSG_1(&BMS_MSG_1_St,&Can_Applidata_St->DataBytes_au8[0]);
			break;
		}
		case CIL_BMS_MSG_2_E:
		{
			Deserialize_BMS_MSG_2(&BMS_MSG_2_St,&Can_Applidata_St->DataBytes_au8[0]);
			break;
		}
		case CIL_HMI_CONF_MSG_E:
		{
			//Deserialize_HMI_CONF_MSG_1(&HMI_CONF_MSG_1, Can_Applidata_St->DataBytes_au8);
			SetHMIConfigParameter(&Can_Applidata_St->DataBytes_au8[0]);
			break;
		}
		default:
		{
			;
		}
	}
  	return;
}


/********************************************************EOF***********************************************************/