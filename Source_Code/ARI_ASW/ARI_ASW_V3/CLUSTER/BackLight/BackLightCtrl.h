/***********************************************************************************************************************
* File Name    : BackLightCtrl.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 08/01/2021
***********************************************************************************************************************/

#ifndef BACKLIGHT_CTRL_H
#define BACKLIGHT_CTRL_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h"


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define PWM_DUTY_ON_IGNITION_ON					100
#define PWM_DUTY_ON_IGNITION_OFF				0 

#define SPEED_LPF_POS_GAIN				3000 /*todo:configurable*/
#define SPEED_LPF_NEG_GAIN				SPEED_LPF_POS_GAIN

#define BACKLIGHT_CTRL_STATIC_MODE			FALSE  /* TRUE FALSE */
#define BACKLIGHT_CTRL_ALS_MODE				TRUE
#define BACKLIGHT_CTRL_CAN_MODE				FALSE

/*****************************************`******************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef struct
{
	uint16_t	DutyCycle_u16;			/* Present Duty Cycle */
	uint16_t	PrevDutyCycle_u16;		/* Previous Duty Cycle Value (Presently in Registers)*/
	bool		B1_Duty_State_b;		/* BackLight-1 Duty Status Flag*/
	bool 		B2_Duty_State_b;		/* BackLight-2 Duty Status Flag*/
}DutyCycle_Conf_St_t;

typedef enum
{
	BACKLIGHT_CTRL_STATIC_E,
	BACKLIGHT_CTRL_ALS_E,
	BACKLIGHT_CTRL_CAN_E, 
}BackLightCtrl_En_t;


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern BackLightCtrl_En_t		BackLightCtrl_En;


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void Turn_ON_BackLight(void);
extern void Turn_OFF_BackLight(void);
void SetPWMDutyCycle(uint16_t);
uint16_t GetDutyCycleValue(uint32_t);
extern void ClrBrightnessCtrlData(void);
uint16_t DutyCycle_LPF(uint16_t, uint16_t *, int16_t, int16_t);

extern void AdjustLightIntensity(void);
extern void ControlBrightness0verCAN(uint8_t Brightness_u8);

#endif /* BACKLIGHT_CTRL_H */


