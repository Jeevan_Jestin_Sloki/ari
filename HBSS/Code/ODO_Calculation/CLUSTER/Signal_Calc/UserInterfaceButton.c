/***********************************************************************************************************************
* File Name    : Speed_Calc.c
* Version      : 01
* Description  : The file implement the calculation of speed data
* Created By   : Jeevan Jestin N
* Creation Date: 26/05/2022
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "DataAquire.h"
#include "UserInterfaceButton.h"
#include "SignalCalc_main.h"
#include "DataBank.h"
#include "ODO_Calc.h"
#include "TimeDisp.h"
/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
uint32_t MenuButtonIdleTime_u32 = RESET;
uint32_t SetButtonIdleTime_u32 = RESET;
uint8_t UIB_LowerMin_time_u8 = RESET;
uint8_t UIB_HighMin_time_u8 = RESET;
uint8_t UIB_Min_time_u8 = RESET;
uint8_t UIB_Hour_time_u8 = RESET;

UserButtonTimeSetState_En_t UserButtonTimeSetState_En = UIB_TIME_SET_NONE_E;
MerdienSel_En_t MerdienSel_En = MERDIEN_NONE_E;

/***********************************************************************************************************************
* Function Name: CalculateClusterSignals
* Description  : This function Calculates the Required Cluster Signals.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void UpdateUserButton(void)
{
    ModeButton();
    //SetButton();
    //ValidateHourMinTime();
    //UserButtonTimeout();
}

/***********************************************************************************************************************
* Function Name: ModeButton
* Description  : The function Service te mode button.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ModeButton(void)
{
    if( ( UserInputSigConf_St[INPUT_2_E].SigTurnOnVal_u8 == UserInputSigConf_St[INPUT_2_E].SignalState_u8 ) && 
        ( DEBOUNCE_50MS <= UserButtonConf_St[INPUT_2_E].UserInpActiveStateTime_u32 ))
    {
        /* The user pressed(short) the button, 50ms Debounce is used to avoid the Noise*/
        if(UserButtonTimeSetState_En == UIB_TIME_SET_NONE_E)
        {
            /* Time setting should not be active during Mode select */
            switch(OdoDistDispSelect_En)
            {
                case DISP_MAIN_0D0_E:
                {
                    OdoDistDispSelect_En = DISP_TRIP_A_ODO_E;
                    break;
                }
                case DISP_TRIP_A_ODO_E:
                {
                    OdoDistDispSelect_En = DISP_TRIP_B_ODO_E;
                    break;
                }
                case DISP_TRIP_B_ODO_E:
                {
                    OdoDistDispSelect_En = DISP_MAIN_0D0_E;
                    break;
                }
                default:
                {

                }
            }
        }
        else
        {
            /* If time setting is active , exit from the time setting mode */
            TimeSettingUpdate();
        }
        MenuButtonIdleTime_u32 = GET_TIME_MS();
    }

    if( (THRESHOLD_VALUE_3000MS <= UserButtonConf_St[INPUT_2_E].UserInpActiveStateTime_u32) &&
				( false == UserButtonConf_St[INPUT_2_E].UserInputLongPressed_b ) )
    {
        /* if user long pressed more than 3sec(3000ms) clear the respective Trip meter */
        switch (OdoDistDispSelect_En)
        {
            case DISP_MAIN_0D0_E:
            {
                break;
            }
            case DISP_TRIP_A_ODO_E:
            {
                ClearTripA_ODO();
                break;
            }
            case DISP_TRIP_B_ODO_E:
            {
                ClearTripB_ODO();
                break;
            }
        }
        UserButtonConf_St[INPUT_2_E].UserInputLongPressed_b = true;
    }
    return;
}


/***********************************************************************************************************************
* Function Name: SetButton
* Description  : The function service the SET button.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void SetButton(void)
{
    if( ( UserInputSigConf_St[INPUT_1_E].SigTurnOnVal_u8 == UserInputSigConf_St[INPUT_1_E].SignalState_u8 ) &&
        ( DEBOUNCE_50MS <= UserButtonConf_St[INPUT_1_E].UserInpActiveStateTime_u32 ))
    {
        /* The user pressed(short) the button, 50ms Debounce is used to avoid the Noise*/
        switch (UserButtonTimeSetState_En)
        {
            case UIB_TIME_SET_NONE_E:
            {
                /* extract the minute higher and lower byte */
                UIB_LowerMin_time_u8 = MinutesTime_u8 % TEN_E;
                UIB_HighMin_time_u8 = ( MinutesTime_u8/TEN_E ); 
                UIB_Min_time_u8 = MinutesTime_u8;
                UIB_Hour_time_u8 = HoursTime_u8;
                UserButtonTimeSetState_En = UIB_TIME_HH_E;
                SET_TIME_COLON_STATE(TELLTALE_SOLID_ON); /* Time colon is made solid*/

                /* Capture the Active Merdien */
		        if(ClusterSignals_St.AMText_u8 == TELLTALE_SOLID_ON)
                {
                    MerdienSel_En = AM_SEL_E;
                }
                else
                {
                    MerdienSel_En = PM_SEL_E;
                }
		        TimeSetting_b = true;
                break;
            }
            case UIB_TIME_HH_E:
            {
                UIB_Hour_time_u8++;
                break;
            }
            case UIB_TIME_HM_E:
            {
                UIB_HighMin_time_u8 ++; /* Increment only Higher nibble of minute time*/
		        UIB_Min_time_u8 = UIB_HighMin_time_u8 + UIB_LowerMin_time_u8;
                break;
            }
            case UIB_TIME_LM_E:
            {
                UIB_LowerMin_time_u8++; /* Increment lower nibble of minute time*/
		        UIB_Min_time_u8 = UIB_HighMin_time_u8 + UIB_LowerMin_time_u8;
                break;
            }
            case UIB_TIME_AM_E:
            {
                ClusterSignals_St.PMText_u8 = TELLTALE_BLINK; /* Swap am->pm */
                ClusterSignals_St.AMText_u8 = OFF;
                UserButtonTimeSetState_En = UIB_TIME_PM_E;
                break;
            }
            case UIB_TIME_PM_E:
            {
                ClusterSignals_St.AMText_u8 = TELLTALE_BLINK; /* swap pm->am */
                ClusterSignals_St.PMText_u8 = OFF;
                UserButtonTimeSetState_En = UIB_TIME_AM_E;
                break;
            }
            default:
            {
                ;
            }
        }
        SetButtonIdleTime_u32 = GET_TIME_MS();
    }

    if( (THRESHOLD_VALUE_3000MS <= UserButtonConf_St[INPUT_1_E].UserInpActiveStateTime_u32) &&
				( false == UserButtonConf_St[INPUT_1_E].UserInputLongPressed_b ) )
    {
        /* If user long press more than 3sec Set the respective timing parameter*/
        switch (UserButtonTimeSetState_En)
        {
            case UIB_TIME_SET_NONE_E:
            {
                break;
            }
            case UIB_TIME_HH_E:
            {
                HoursTime_u8 = UIB_Hour_time_u8; /* Set hour time to Real time data*/
                UserButtonTimeSetState_En = UIB_TIME_HM_E;
                break;
            }
            case UIB_TIME_HM_E:
            {
                MinutesTime_u8 = ( UIB_HighMin_time_u8 * TEN_E) + UIB_LowerMin_time_u8; /* set the higher nibble to real time*/
                UserButtonTimeSetState_En = UIB_TIME_LM_E;
                break;
            }
            case UIB_TIME_LM_E:
            {
                MinutesTime_u8 = UIB_LowerMin_time_u8 + ( MinutesTime_u8/TEN_E ) *TEN_E; /* Set teh lower nibble to real time*/
                ClusterSignals_St.AMText_u8 = TELLTALE_BLINK;
                UserButtonTimeSetState_En = UIB_TIME_AM_E;
                Initial_Minutes_Disp_b = false;
	            Initial_Hours_Disp_b = false;
                break;
            }
            case UIB_TIME_AM_E:
            {
                ClusterSignals_St.AMText_u8 = TELLTALE_SOLID_ON;
                MerdienSel_En = AM_SEL_E;
                TimeSettingUpdate();
                break;
            }
            case UIB_TIME_PM_E:
            {
                ClusterSignals_St.PMText_u8 = TELLTALE_SOLID_ON;
                MerdienSel_En = PM_SEL_E;
                TimeSettingUpdate();
                break;
            }
            default:
            {
                ;
            }
        }
        UserButtonConf_St[INPUT_1_E].UserInputLongPressed_b = true;
	    SetButtonIdleTime_u32 = GET_TIME_MS();
    }
    return;
}

/***********************************************************************************************************************
* Function Name: UserButtonTimeout
* Description  : The function serviced when timeout of user button occur.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void UserButtonTimeout(void)
{
    if( ( UserButtonTimeSetState_En != UIB_TIME_SET_NONE_E) && (TIMESETTING_TIMEOUT() < GET_TIME_MS() ) )
    {
        TimeSettingUpdate();
    }
    return;
}

/***********************************************************************************************************************
* Function Name: TimeSettingUpdate
* Description  : The function update real time after the user set the time and timeout.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void TimeSettingUpdate(void)
{
	SET_TIME_COLON_STATE(TELLTALE_BLINK);
	UserButtonTimeSetState_En = UIB_TIME_SET_NONE_E;
	TimeSetting_b = false;
	Initial_Minutes_Disp_b = false;
	Initial_Hours_Disp_b = false;
	Set_Real_Time(HoursTime_u8,MinutesTime_u8,(uint8_t)MerdienSel_En);

    return;
}

/***********************************************************************************************************************
* Function Name: ValidateHourMinTime
* Description  : The function serviced when timeout of user button occur.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ValidateHourMinTime(void)
{
    if(UIB_TIME_HH_E == UserButtonTimeSetState_En && UIB_Hour_time_u8 > MAX_HOURS_TIME)
    {
        UIB_Hour_time_u8 = HOUR_RESET_TIME;
    }
    else if (UIB_TIME_HM_E == UserButtonTimeSetState_En && UIB_HighMin_time_u8 > MAX_HIGHERMIN_TIME)
    {
        UIB_HighMin_time_u8 = MIN_RESET_TIME;
    }
    else if (UIB_TIME_LM_E == UserButtonTimeSetState_En  && UIB_LowerMin_time_u8 > MAX_LOWERMIN_TIME)
    {
        UIB_LowerMin_time_u8 = MIN_RESET_TIME;
    }
    else
    {
        ;
    }
    
    return;
}

/***********************************************************************************************************************
* Function Name: ClearUIBdata
* Description  : The function Clears the User interface button data.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ClearUIBdata(void)
{
    UIB_Min_time_u8 = CLEAR;
    UIB_Hour_time_u8 = CLEAR;
    UIB_LowerMin_time_u8 = CLEAR;
    UIB_HighMin_time_u8 = CLEAR;
    UserButtonTimeSetState_En = UIB_TIME_SET_NONE_E;
    MerdienSel_En = MERDIEN_NONE_E;
    SetButtonIdleTime_u32 = CLEAR;
    MenuButtonIdleTime_u32 = CLEAR;
    
    return;
}