/***********************************************************************************************************************
* File Name    : Speed_Conf.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 30/12/2020
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "Speed_Conf.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Structure Declaration
***********************************************************************************************************************/
 const SignalsValue_St_t		Speed_SignalsValue_aSt[SPEED_SIG_LEN] =
{
	{ ZERO_E,	TWELVE_E,  		SpeedSig1Conf_ast },
	{ ONE_E, 	THIRTEEN_E,		SpeedSig2Conf_ast },
	{ TWO_E, 	TWO_E,			SpeedSig3Conf_ast },
};

 const SignalConfig_St_t			SpeedSig1Conf_ast[TWELVE_E] = 
{
	{ ZERO_E,		FIVE_SEG_E,		Speed_S1_0_SegConf_aSt	},
	{ ONE_E,		FIVE_SEG_E,		Speed_S1_1_SegConf_aSt	},
	{ TWO_E,		FIVE_SEG_E,		Speed_S1_2_SegConf_aSt	},
	{ THREE_E,		FIVE_SEG_E,		Speed_S1_3_SegConf_aSt	},
	{ FOUR_E,		FIVE_SEG_E,		Speed_S1_4_SegConf_aSt	},
	{ FIVE_E,		FIVE_SEG_E,		Speed_S1_5_SegConf_aSt	},
	{ SIX_E,		FIVE_SEG_E,		Speed_S1_6_SegConf_aSt	},
	{ SEVEN_E,		FIVE_SEG_E,		Speed_S1_7_SegConf_aSt	},
	{ EIGHT_E,		FIVE_SEG_E,		Speed_S1_8_SegConf_aSt	},
	{ NINE_E,		FIVE_SEG_E,		Speed_S1_9_SegConf_aSt	},
	{ OFF_DIGIT,	FIVE_SEG_E,		Speed_S1_Off_SegConf_aSt},
	{ DISPLAY_H,	FIVE_SEG_E,		Speed_S1_H_SegConf_aSt	},
};

 const SignalConfig_St_t			SpeedSig2Conf_ast[THIRTEEN_E] = 
{
	{
		ZERO_E,
		FIVE_SEG_E,
		Speed_S2_0_SegConf_aSt,
	},
	{
		ONE_E,
		FIVE_SEG_E,
		Speed_S2_1_SegConf_aSt,
	},
	{
		TWO_E,
		FIVE_SEG_E,
		Speed_S2_2_SegConf_aSt,
	},
	{
		THREE_E,
		FIVE_SEG_E,
		Speed_S2_3_SegConf_aSt,
	},
	{
		FOUR_E,
		FIVE_SEG_E,
		Speed_S2_4_SegConf_aSt,
	},
	{
		FIVE_E,
		FIVE_SEG_E,
		Speed_S2_5_SegConf_aSt,
		
	},
	{
		SIX_E,
		FIVE_SEG_E,
		Speed_S2_6_SegConf_aSt,
	},
	{
		SEVEN_E,
		FIVE_SEG_E,
		Speed_S2_7_SegConf_aSt,
	},
	{
		EIGHT_E,
		FIVE_SEG_E,
		Speed_S2_8_SegConf_aSt,
	},
	{
		NINE_E,
		FIVE_SEG_E,
		Speed_S2_9_SegConf_aSt,
	},
	{
		OFF_DIGIT,
		FIVE_SEG_E,
		Speed_S2_Off_SegConf_aSt,
	},
	{
		DISPLAY_G,
		FIVE_SEG_E,
		Speed_S2_G_SegConf_aSt,
	},
	{
		DISPLAY_C,
		FIVE_SEG_E,
		Speed_S2_C_SegConf_aSt,
	},
};

 const SignalConfig_St_t			SpeedSig3Conf_ast[TWO_E] = 
{
	{
		OFF_DIGIT,
		ONE_SEG_E,
		Speed_S3_Off_SegConf_aSt,
	},
	{
		ONE_E,
		ONE_SEG_E,
		Speed_S3_1_SegConf_aSt,
	},
};

 const SegConfig_St_t			Speed_S1_0_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x02U,
	},
	{
		&SEG18,
		0x00U,
		0x0FU,
	},
	{
		&SEG17,
		0x00U,
		0x0DU,
	},
	{
		&SEG16,
		0x08U,
		0x07U,
	},
	{
		&SEG15,
		0x0DU,
		0x02U,
	},
};

 const SegConfig_St_t			Speed_S1_1_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x00U,
	},
	{
		&SEG18,
		0x00U,
		0x00U,
	},
	{
		&SEG17,
		0x00U,
		0x08U,
	},
	{
		&SEG16,
		0x08U,
		0x07U,
	},
	{
		&SEG15,
		0x0DU,
		0x02U,
	},
};

 const SegConfig_St_t			Speed_S1_2_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x02U,
	},
	{
		&SEG18,
		0x00U,
		0x0BU,
	},
	{
		&SEG17,
		0x00U,
		0x0FU,
	},
	{
		&SEG16,
		0x08U,
		0x06U,
	},
	{
		&SEG15,
		0x0DU,
		0x02U,
	},
};

 const SegConfig_St_t			Speed_S1_3_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x02U,
	},
	{
		&SEG18,
		0x00U,
		0x0AU,
	},
	{
		&SEG17, 
		0x00U,
		0x0FU,
	},
	{
		&SEG16,
		0x08U,
		0x07U,
	},
	{
		&SEG15,
		0x0DU,
		0x02U,
	},
};

 const SegConfig_St_t			Speed_S1_4_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x00U,
	},
	{
		&SEG18,
		0x00U,
		0x0EU,
	},
	{
		&SEG17,
		0x00U,
		0x0AU,
	},
	{
		&SEG16,
		0x08U,
		0x07U,
	},
	{
		&SEG15,
		0x0DU,
		0x02U,
	},
};

 const SegConfig_St_t			Speed_S1_5_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x02U,
	},
	{
		&SEG18,
		0x00U,
		0x0EU,
	},
	{
		&SEG17,
		0x00U,
		0x0FU,
	},
	{
		&SEG16,
		0x08U,
		0x03U,
	},
	{
		&SEG15,
		0x0DU,
		0x02U,
	},
};

 const SegConfig_St_t			Speed_S1_6_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x02U,
	},
	{
		&SEG18,
		0x00U,
		0x0FU,
	},
	{
		&SEG17,
		0x00U,
		0x0FU,
	},
	{
		&SEG16,
		0x08U,
		0x03U,
	},
	{
		&SEG15,
		0x0DU,
		0x02U,
	},
};


 const SegConfig_St_t			Speed_S1_7_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x00U,
	},
	{
		&SEG18,
		0x00U,
		0x08U,
	},
	{
		&SEG17,
		0x00U,
		0x0CU,
	},
	{
		&SEG16,
		0x08U,
		0x07U,
	},
	{
		&SEG15,
		0x0DU,
		0x02U,
	},
};


 const SegConfig_St_t			Speed_S1_8_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x02U,
	},
	{
		&SEG18,
		0x00U,
		0x0FU,
	},
	{
		&SEG17,
		0x00U,
		0x0FU,
	},
	{
		&SEG16,
		0x08U,
		0x07U,
	},
	{
		&SEG15,
		0x0DU,
		0x02U,
	},
};


 const SegConfig_St_t			Speed_S1_9_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x02U,
	},
	{
		&SEG18,
		0x00U,
		0x0EU,
	},
	{
		&SEG17,
		0x00U,
		0x0FU,
	},
	{
		&SEG16,
		0x08U,
		0x07U,
	},
	{
		&SEG15,
		0x0DU,
		0x02U,
	},
};

 const SegConfig_St_t			Speed_S1_Off_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x00U,
	},
	{
		&SEG18,
		0x00U,
		0x00U,
	},
	{
		&SEG17,
		0x00U,
		0x00U,
	},
	{
		&SEG16,
		0x08U,
		0x00U,
	},
	{
		&SEG15,
		0x0DU,
		0x00U,
	},
};

 const SegConfig_St_t			Speed_S1_H_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG19,
		0x0DU,
		0x02U,
	},
	{
		&SEG18,
		0x00U,
		0x0FU,
	},
	{
		&SEG17,
		0x00U,
		0x0AU,
	},
	{
		&SEG16,
		0x08U,
		0x07U,
	},
	{
		&SEG15,
		0x0DU,
		0x02U,
	},
};

 const SegConfig_St_t			Speed_S2_0_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x02U,
	},
	{
		&SEG22,
		0x00U,
		0x0FU,
	},
	{
		&SEG21,
		0x00U,
		0x0DU,
	},
	{
		&SEG20,
		0x08U,
		0x07U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};

 const SegConfig_St_t			Speed_S2_1_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x00U,
	},
	{
		&SEG22,
		0x00U,
		0x00U,
	},
	{
		&SEG21,
		0x00U,
		0x08U,
	},
	{
		&SEG20,
		0x08U,
		0x07U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};

 const SegConfig_St_t			Speed_S2_2_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x02U,
	},
	{
		&SEG22,
		0x00U,
		0x0BU,
	},
	{
		&SEG21,
		0x00U,
		0x0FU,
	},
	{
		&SEG20,
		0x08U,
		0x06U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};

 const SegConfig_St_t			Speed_S2_3_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x02U,
	},
	{
		&SEG22,
		0x00U,
		0x0AU,
	},
	{
		&SEG21,
		0x00U,
		0x0FU,
	},
	{
		&SEG20,
		0x08U,
		0x07U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};

 const SegConfig_St_t			Speed_S2_4_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x00U,
	},
	{
		&SEG22,
		0x00U,
		0x0EU,
	},
	{
		&SEG21,
		0x00U,
		0x0AU,
	},
	{
		&SEG20,
		0x08U,
		0x07U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};

 const SegConfig_St_t			Speed_S2_5_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x02U,
	},
	{
		&SEG22,
		0x00U,
		0x0EU,
	},
	{
		&SEG21,
		0x00U,
		0x0FU,
	},
	{
		&SEG20,
		0x08U,
		0x03U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};

 const SegConfig_St_t			Speed_S2_6_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x02U,
	},
	{
		&SEG22,
		0x00U,
		0x0FU,
	},
	{
		&SEG21,
		0x00U,
		0x0FU,
	},
	{
		&SEG20,
		0x08U,
		0x03U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};


 const SegConfig_St_t			Speed_S2_7_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x00U,
	},
	{
		&SEG22,
		0x00U,
		0x08U,
	},
	{
		&SEG21,
		0x00U,
		0x0CU,
	},
	{
		&SEG20,
		0x08U,
		0x07U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};


 const SegConfig_St_t			Speed_S2_8_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x02U,
	},
	{
		&SEG22,
		0x00U,
		0x0FU,
	},
	{
		&SEG21,
		0x00U,
		0x0FU,
	},
	{
		&SEG20,
		0x08U,
		0x07U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};


 const SegConfig_St_t			Speed_S2_9_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x02U,
	},
	{
		&SEG22,
		0x00U,
		0x0EU,
	},
	{
		&SEG21,
		0x00U,
		0x0FU,
	},
	{
		&SEG20,
		0x08U,
		0x07U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};

 const SegConfig_St_t			Speed_S2_Off_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x00U,
	},
	{
		&SEG22,
		0x00U,
		0x00U,
	},
	{
		&SEG21,
		0x00U,
		0x00U,
	},
	{
		&SEG20,
		0x08U,
		0x00U,
	},
	{
		&SEG19,
		0x0EU,
		0x00U,
	},
};


 const SegConfig_St_t			Speed_S2_G_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x02U,
	},
	{
		&SEG22,
		0x00U,
		0x0FU,
	},
	{
		&SEG21,
		0x00U,
		0x0DU,
	},
	{
		&SEG20,
		0x08U,
		0x03U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};

 const SegConfig_St_t			Speed_S2_C_SegConf_aSt[FIVE_SEG_E] = 
{
	{
		&SEG25,
		0x0DU,
		0x02U,
	},
	{
		&SEG22,
		0x00U,
		0x0FU,
	},
	{
		&SEG21,
		0x00U,
		0x0DU,
	},
	{
		&SEG20,
		0x08U,
		0x00U,
	},
	{
		&SEG19,
		0x0EU,
		0x01U,
	},
};


 const SegConfig_St_t			Speed_S3_Off_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG25,
		0x0BU,
		0x00U,
	},
};

 const SegConfig_St_t			Speed_S3_1_SegConf_aSt[ONE_SEG_E] = 
{
	{
		&SEG25,
		0x0BU,
		0x04U,
	},
};


/********************************************************EOF***********************************************************/