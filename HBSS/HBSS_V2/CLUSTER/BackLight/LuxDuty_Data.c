/***********************************************************************************************************************
* File Name    : LuxDuty_Data.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 05/05/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "LuxDuty_Data.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
const uint16_t IntensityLux_DutyCycle_Table[(2*INTENSITY_DUTY_COL_SIZE)] = 
{
    /* x = Light Intensity in Lux */
    1U, 3U, 7U, 10U, 15U, 18U, 20U, 50U, 80U, 100U, 150U, 200U, 500U, 800U,
    1000U, 2000U, 3000U, 4000U, 6000U, 8000U, 10000U, 12500U, 15000U, 17500U,
    20000U, 22500U, 25000U, 30000U, 35000U, 40000U, 45000U, 50000U, 55000U,
    60000U, 65535U,
    
    /* y = Duty Cycle in % */
    15U, 	20U, 	25U, 	30U, 	35U, 	40U, 	45U, 	50U, 	55U, 	60U, 
	65U, 	70U, 	75U, 	80U, 	85U, 	90U,	95U, 	100U, 	100U, 	100U, 
	100U, 	100U, 	100U, 	100U, 	100U, 	100U, 	100U, 	100U, 	100U,	100U, 
	100U, 	100U, 	99U, 	99U, 	100U
};


/*
*
* @brief This function implements 1D interpolation for the following data types
* Xaxis entry - unsigned int
* Yaxis entry - unsigned int
*
* @param UINT16* : Function look-up table
* @param UINT16  : number of breakpoints/columns
* @param UINT16  : X-axis entry into the look-up table
* @return UINT16 : Interpolated value
* @code tag     :@[CODE_MU_1D_INTPOL_u16_u16X_u16Y_tblptr]
                 @{SDD_MU_1D_INTPOL_u16_u16X_u16Y_tblptr}
*/
uint16_t MU1DInterpol_u16_u16X_u16Y_tblptr(const uint16_t* ptrTable_pu16, uint16_t num_cols_u16, uint16_t x_input_u16)
{
    uint32_t        Offset_u32         = 0;
    uint32_t        Numer_u32          = 0; // (x - x0)
    uint16_t  	    diff_y_u16         = 0; // (y0 - y1)
    bool            SignFlag_b         = 0; // ascending/descending order of y-values

    //check if value is greater than the first entry
    if (x_input_u16 <= ptrTable_pu16[0])
    {
        //return first y value
        return ((uint16_t)(ptrTable_pu16[num_cols_u16]));
    }

    //check if input value is lesser than the last entry
    if (x_input_u16 >= (ptrTable_pu16[num_cols_u16 - 1]))
    {
        //return corresponding y value
        return ((uint16_t)(ptrTable_pu16[2 * num_cols_u16 - 1]));
    }

    // increment pointer to find the nearest value of x
    while (x_input_u16 > (ptrTable_pu16[1]))
    {
        ptrTable_pu16++;
    } // end while

    //check for ascending/descending order of y-values
    if (ptrTable_pu16[num_cols_u16 + 1] > ptrTable_pu16[num_cols_u16])
    {
        // Set sign flag to 1 if y values are in ascending order
        SignFlag_b = 1;
        diff_y_u16 = (uint16_t)(ptrTable_pu16[num_cols_u16 + 1] - ptrTable_pu16[num_cols_u16]);
    }
    else
    {
        // Set sign flag to 1 if y values are in descending order
        SignFlag_b = 0;
        diff_y_u16 = (uint16_t)(ptrTable_pu16[num_cols_u16] - ptrTable_pu16[num_cols_u16 + 1]);
    }

    // compute the y-axis value for the given x
    // y = (y0 - ((x-x0)*(y1-y0)/(x1-x0)))

    Numer_u32 = ((uint32_t) diff_y_u16 * (x_input_u16 - ptrTable_pu16[0]));

    // ((y0 - y1) * (x - x0 ) / (x1 - x0)
    Offset_u32 = (uint16_t)(Numer_u32 / (ptrTable_pu16[1] - ptrTable_pu16[0]));

    // if values are in ascending order
    if (1 == SignFlag_b)
    {
        return ((uint16_t)(ptrTable_pu16[num_cols_u16] + Offset_u32));
    }
    else // if values are in descending order
    {
        return ((uint16_t)(ptrTable_pu16[num_cols_u16] - Offset_u32));
    }
}


/********************************************************EOF***********************************************************/