/******************************************************************************
 *    FILENAME    : cil_can_conf.c
 *    DESCRIPTION : File contains the common declarations related to CIL layers.
 ******************************************************************************
 * Revision history
 *  
 * Ver   Author       Date               Description
 * 1     Sushil      27/10/2018		     Initial version
 ******************************************************************************
*/ 

/* Section: Included Files                                       */
#include "cil_can_conf.h"
#include "diag_appl_test.h"
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

/* ************************************************************************** */
/*
 * @summary:- can structure message buffer  
 */
CanSchedMsg_St_t CanSB_St [CIL_DCAN_TOTAL_RX_E] = {0};


const CIL_CAN_Conf_St_t CIL_CAN_Conf_aSt[CIL_DCAN_END_E] =
{
#if(TRUE == DIAG_CONF_UDS_SUPPORTED && TRUE == DIAG_CONF_CANTP_SUPPORTED)
  { {0x7F0,  0x7FF, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}  , CIL_CANTP_REQ_TESTER_RX_E   , CanSched_RxCallBackInt    , &CanSB_St[CIL_CANTP_REQ_TESTER_RX_E].msg  }  ,//@
#endif
  { { 0X1E1,0x7FF, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}, CIL_HMI_PER_1_RX_E,    CanSched_RxCallBackInt, &CanSB_St[CIL_HMI_PER_1_RX_E].msg  },
  { { 0X1E4,0x7FF, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}, CIL_HMI_PER_2_RX_E,   	CanSched_RxCallBackInt, &CanSB_St[CIL_HMI_PER_2_RX_E].msg  },
  { { 0X1F5,0x7FF, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}, CIL_HMI_PER_3_RX_E,   	CanSched_RxCallBackInt, &CanSB_St[CIL_HMI_PER_3_RX_E].msg  },
  { { 0X202,0x7FF, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}, CIL_HMI_PER_4_RX_E,   	CanSched_RxCallBackInt, &CanSB_St[CIL_HMI_PER_4_RX_E].msg  },
  { { 0X205,0x7FF, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}, CIL_HMI_PER_5_RX_E,   	CanSched_RxCallBackInt, &CanSB_St[CIL_HMI_PER_5_RX_E].msg  },
  
  { { 0x7EA,0x7FF, CAN1_E, 0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_RX}, CIL_HMI_CONF_RX_E,   	CanSched_RxCallBackInt, &CanSB_St[CIL_HMI_CONF_RX_E].msg  },
#if(TRUE == DIAG_CONF_UDS_SUPPORTED && TRUE == DIAG_CONF_CANTP_SUPPORTED)
	{ {0x7F1,  0U, CAN1_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}  , CIL_CANTP_RESP_TESTER_TX_E  , NULL                  	, NULL  }  ,
#endif
  { { 0x600,  0U, CAN1_E,  0x10,    STD_CAN_E,    DATA_FRAME_E,   FALSE,    FALSE,    CAN_TX}, CIL_TEST3_TX_E, NULL, NULL},

};



/* *****************************************************************************
 End of File
 */

