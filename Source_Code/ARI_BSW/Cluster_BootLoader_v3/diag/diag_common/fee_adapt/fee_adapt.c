/******************************************************************************
 *    FILENAME    : fee_adapt.c
 ******************************************************************************
 * Revision history
 *  
 * Ver Author       Date               Description
 * 1   Sloki       10/01/2019		   Initial version
 ******************************************************************************
*/ 

#include "fee_adapt.h"
#include "fee_conf.h"
#include "diag_adapt.h"
#include "diag_appl_test.h"
#include "diag_sys_conf.h"
#if(TRUE == DIAG_EEPROM)
	#include "eeprom.h"//include the eeprom related files
#endif
#if(TRUE == DIAG_FLASH)
	#include "PFlash.h"//include the flash related files
#endif



bool Flash_Erase(uint32_t address,uint32_t size)
{
#if(TRUE == DIAG_FLASH)
	return PFlash_Erase(address,size);
#else
	return FALSE	
#endif
}
bool Flash_Write(uint32_t address,uint32_t size,uint8_t* data_buff)
{
#if(TRUE == DIAG_FLASH)
	return PFlash_Write(address, size, data_buff);
#else
	return FALSE
#endif
}
bool Flash_Pattern_Write(uint32_t address,uint32_t data)
{
#if(TRUE == DIAG_FLASH)
	return PFlash_Pattern_Write(address, data);
#else
	return FALSE	
#endif
}


/* End of File */
