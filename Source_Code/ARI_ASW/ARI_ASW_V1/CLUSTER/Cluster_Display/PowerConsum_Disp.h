/***********************************************************************************************************************
* File Name    : PowerConsum_Disp.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 16/01/2021
***********************************************************************************************************************/

#ifndef POWER_CONSUM_DISP_H
#define POWER_CONSUM_DISP_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 
#include "Cluster_Conf.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define TOTAL_PWR_CONSUM_BARS						10
#define SAFE_MODE_PWR_CONSUM_RANGE					7

#define PC_RED_BAR_1_POS							8
#define PC_RED_BAR_2_POS							9
#define PC_RED_BAR_3_POS							10


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	PWR_CONSUM_DISPLAY_E,
	PWR_CONSUM_HOLD_E,
}PwrConsum_DispState_En_t;

typedef enum
{
	PC_GREEN_BAR_DISP_E,
	PC_RED_BAR_DISP_E,
}PC_BarDispState_En_t;

/***********************************************************************************************************************
Export Variables 
***********************************************************************************************************************/
extern bool 			InitialPowerConsum_Disp_b;
extern PC_BarDispState_En_t	PC_BarDispState_En;
extern uint16_t             	Valid_PowerConsum_u16; 

/***********************************************************************************************************************
Export Variables and Functions
***********************************************************************************************************************/
extern void   Display_PowerConsum(uint16_t, ClusterSignals_En_t );
uint16_t      Validate_PowerConsum(uint16_t);
extern void   DispUSM_PwrConsum(ClusterSignals_En_t, uint16_t, bool); /*To display the Power consumption @ 
										Red Zone*/
extern void Control_PowerIcon(bool);

#endif /* POWER_CONSUM_DISP_H */


