/***********************************************************************************************************************
* File Name    : r_cg_lcd.h
* Version      : Applilet3 for RL78/D1A V2.04.02.01 [21 Mar 2018]
* Device(s)    : R5F10DMJ
* Tool-Chain   : CCRL
* Description  : This file implements device driver for LCD module.
* Creation Date: 14/12/2020
***********************************************************************************************************************/

#ifndef LCD_H
#define LCD_H

/***********************************************************************************************************************
Macro definitions (Register bit)
***********************************************************************************************************************/
/*
    LCD Mode Register (LCDMD)
*/
/* LCD drive voltage generator selection (MDSET1, MDSET0) */
#define _00_LCD_VOLTAGE_MODE_EXTERNAL   (0x00U)    /* no internal resistor connection */
#define _10_LCD_VOLTAGE_MODE_INTERNAL   (0x10U)    /* internal resistance division method (no step-down transforming) */
#define _30_LCD_VOLTAGE_MODE_CAPACITOR  (0x30U)    /* internal resistance division method (step-down transforming) */

/*
    LCD Display Mode Register (LCDM)
*/
/* LCD display enable/disable (LCDON, SCOC) */
#define _00_LCD_DISPLAY_GROUND          (0x00U)    /* output ground level to segment/common pin */
#define _40_LCD_DISPLAY_OFF             (0x40U)    /* display off (all segment outputs are deselected) */
#define _C0_LCD_DISPLAY_ON              (0xC0U)    /* display on */
/* LCD controller/driver display mode selection (LCDM2, LCDM0) */
#define _00_LCD_DISPLAY_MODE0           (0x00U)    /* 4 time slices, 1/3 bias mode */
#define _01_LCD_DISPLAY_MODE1           (0x01U)    /* 3 time slices, 1/3 bias mode */
#define _04_LCD_DISPLAY_STATIC          (0x04U)    /* static */

/*
    LCD Clock Control Register (LCDC0)
*/
/* LCD source clock (fLCD) selection (LCDC6 - LCDC4) */
#define _00_LCD_SOURCE_CLOCK_FSUB       (0x00U)    /* fSUB */
#define _10_LCD_SOURCE_CLOCK_FMAIN_32   (0x10U)    /* fMAIN/2^5 */
#define _20_LCD_SOURCE_CLOCK_FMAIN_64   (0x20U)    /* fMAIN/2^6 */
#define _30_LCD_SOURCE_CLOCK_FMAIN_128  (0x30U)    /* fMAIN/2^7 */
#define _40_LCD_SOURCE_CLOCK_FMAIN_256  (0x40U)    /* fMAIN/2^8 */
#define _50_LCD_SOURCE_CLOCK_FMAIN_512  (0x50U)    /* fMAIN/2^9 */
#define _60_LCD_SOURCE_CLOCK_FMAIN_1024 (0x60U)    /* fMAIN/2^10 */
#define _70_LCD_SOURCE_CLOCK_FIL        (0x70U)    /* fIL */
/* LCD clock (LCDCL) selection (LCDC2 - LCDC0) */
#define _00_LCD_CLOCK_FLCD_16           (0x00U)    /* fLCD/2^4 */
#define _01_LCD_CLOCK_FLCD_32           (0x01U)    /* fLCD/2^5 */
#define _02_LCD_CLOCK_FLCD_64           (0x02U)    /* fLCD/2^6 */
#define _03_LCD_CLOCK_FLCD_128          (0x03U)    /* fLCD/2^7 */
#define _04_LCD_CLOCK_FLCD_256          (0x04U)    /* fLCD/2^8 */
#define _05_LCD_CLOCK_FLCD_512          (0x05U)    /* fLCD/2^9 */


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Global functions
***********************************************************************************************************************/
void R_LCD_Create(void);
void R_LCD_Start(void);
void R_LCD_Stop(void);
void R_LCD_VoltageOn(void);
void R_LCD_VoltageOff(void);

/* Start user code for function. Do not edit comment generated here */
void LCD_PowerSave(void);
void LCD_PowerSaveRelease(void);
/* End user code. Do not edit comment generated here */

#endif /* LCD_H */
