/***********************************************************************************************************************
* File Name    : Flash_Params.h
* Version      : 01
* Description  : This file contains the declaration of Export functions and data related to Flash parameters Store 
				 and Restore.
* Created By   : Dileepa B S 
* Creation Date: 05/01/2022
***********************************************************************************************************************/

#ifndef FLASH_PARAMS_H
#define FLASH_PARAMS_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	
#include "App_typedefs.h" 


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define STORE_RESTORE_ODO						TRUE //todo:configurable	/* TRUE OR FALSE*/
#define STORE_RESTORE_WHKM						FALSE //todo:configurable	/* TRUE OR FALSE*/	
#define STORE_RESTORE_SOC						FALSE //todo:configurable	/* TRUE OR FALSE*/


#define ODOMETER_BYTES_LEN						4
#define ODO_IN_MM_BYTES_LEN						4
#define WHKM_BYTES_LEN							2
#define SOC_BYTES_LEN							1

#define DFB0_TOTAL_BYTES			(ODOMETER_BYTES_LEN + ODO_IN_MM_BYTES_LEN + WHKM_BYTES_LEN + SOC_BYTES_LEN)	

#define FLASH_BLOCK_0							0
#define FLASH_BLOCK0_START_POS						0

#define MAX_ODOMETER								99999900 /*Meters*/
#define MAX_ODO_IN_MM								1000 /*mm*/
#define MAX_WH_KM								99
#define MAX_SOC									100


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef struct 
{
	uint32_t F_ODOmeter_u32;
	uint32_t F_ODO_in_mm_u32;
	uint16_t F_Wh_Km_u16;
	uint8_t  F_SOC_u8;
}DFB0_Params_St_t;



/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/


/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
extern void StoreODOmeterToFlash(uint32_t, uint32_t);
extern uint32_t RestoreODOmtereFromFlash(void);
extern void StoreWhKmToFlash(uint16_t);
extern uint16_t RestoreWhKmFromFlash(void);
extern void StoreSOCToFlash(uint8_t);
extern uint8_t RestoreSOCFromFlash(void);


#endif /* FLASH_PARAMS_H */


