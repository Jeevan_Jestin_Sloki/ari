/***********************************************************************************************************************
* File Name    : LightSensor.c
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 28/04/2021
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "LightSensor.h"
#include"r_cg_serial.h"
#include "serial_user.h"
#include "BackLightCtrl.h"

/***********************************************************************************************************************
Pragma directive
***********************************************************************************************************************/


/***********************************************************************************************************************
Global variables and functions
***********************************************************************************************************************/
uint32_t		LightSensorData_u32 	= 0;
uint8_t 		ALS_ID			= 0x88;
ALS_Status_En_t		ALS_Status_En		= ALS_TEST_E;

/***********************************************************************************************************************
* Function Name: ALS_Init
* Description  : This function Initializes the Command, Control and Data registers of the ALS sensor.
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void ALS_Init(void)
{
	uint32_t  i = 0;			/*Loop Count variable*/
	
	Reset_ALS();
	
	if(ALS_Status_En == ALS_OK_E)
	{
		IIC_Tx_Buff_au8[0] = 0x00; 		/*Command Register*/
	    	IIC_Tx_Buff_au8[1] = 0x80; 		/*Function*/
	    	R_IIC11_Master_Send(ALS_ID, IIC_Tx_Buff_au8, sizeof(IIC_Tx_Buff_au8));
	    	for(i = 0; i < 6000; i++);		/*Softwrae Delay*/
	    	while(IIC_Tx_Flag_b == false);		/*Wait untill IIC Send completes*/
	    	IIC_Tx_Flag_b = false;

		IIC_Tx_Buff_au8[0] = 0x01;		/*Control Register*/
		IIC_Tx_Buff_au8[1] = 0x0C;		/*Function*/
		R_IIC11_Master_Send(ALS_ID, IIC_Tx_Buff_au8, sizeof(IIC_Tx_Buff_au8));
		for(i = 0; i < 6000; i++);		/*Softwrae Delay*/
		while(IIC_Tx_Flag_b == false);		/*Wait untill IIC Send completes*/
		IIC_Tx_Flag_b = false;

		IIC_Tx_Buff_au8[0] = 0x04;		/*Sensor Data LSB*/
		IIC_Tx_Buff_au8[1] = 0x05;		/*Sensor Data MSB*/
	}
	else
	{
		;
	}
	
	if(IIC_Err_Flag_b == true)
	{
		//ALS Sensor is not responding
		ALS_Status_En = ALS_NOT_OK_E;
		BackLightCtrl_En = BACKLIGHT_CTRL_STATIC_E; 
					//change the brightness control method to static
	}
	else
	{
		//ALS sensor is working fine
		ALS_Status_En = ALS_OK_E;
	}
	
	return;
}

/***********************************************************************************************************************
* Function Name: GetALSData
* Description  : This function reads the  Ambient Light Sensor Data 
* Arguments    : None
* Return Value : uint32_t LightSensorData_u32
***********************************************************************************************************************/
uint32_t GetALSData(void)
{
	uint32_t  i = 0;			/*Loop Count variable*/
	
	if(ALS_Status_En == ALS_OK_E)
	{
		/*Receive the LSB Sensor data by sending the command 0x04*/
		R_IIC11_Master_Send(ALS_ID, &IIC_Tx_Buff_au8[0], 1U);
		for(i = 0; i < 6000; i++);		/*Softwrae Delay*/
		while(IIC_Tx_Flag_b == false);		/*Wait untill IIC Master Send completes*/
		IIC_Tx_Flag_b = false;
		    
		R_IIC11_Master_Receive(ALS_ID, &IIC_Rx_Buff_au8[0], 1U);
		for(i = 0; i < 6000; i++);		/*Softwrae Delay*/
		while(IIC_Rx_Flag_b == false);		/*Wait untill IIC Master reception completes*/
		IIC_Rx_Flag_b = false;
		
		/*Receive the MSB Sensor data by sending the command 0x05*/
		R_IIC11_Master_Send(ALS_ID, &IIC_Tx_Buff_au8[1], 1U);
		for(i = 0; i < 6000; i++);		/*Softwrae Delay*/
		while(IIC_Tx_Flag_b == false);		/*Wait untill IIC Master Send completes*/
		IIC_Tx_Flag_b = false;
		   
		    
		R_IIC11_Master_Receive(ALS_ID, &IIC_Rx_Buff_au8[1], 1U);
		for(i = 0; i < 6000; i++);		/*Softwrae Delay*/
		while(IIC_Rx_Flag_b == false);		/*Wait untill IIC Master reception completes*/
		IIC_Rx_Flag_b = false;
		    
		LightSensorData_u32 = IIC_Rx_Buff_au8[1];
		    
		LightSensorData_u32 = ((LightSensorData_u32 << 8) + IIC_Rx_Buff_au8[0]); /*Add MSB + LSB Sensor Data*/
	}
	else
	{
		;
	}
	
	if(IIC_Err_Flag_b == true)
	{
		//ALS Sensor is not responding
		ALS_Status_En = ALS_NOT_OK_E;
		BackLightCtrl_En = BACKLIGHT_CTRL_STATIC_E; 
					//change the brightness control method to static
					
		LightSensorData_u32 = 65535;
				//Set LUX value to constant.
	}
	else
	{
		//ALS sensor is working fine
		ALS_Status_En = ALS_OK_E;
	}
	
	return LightSensorData_u32;
}


/***********************************************************************************************************************
* Function Name: Reset_ALS
* Description  : This function Disables the ALS ADC core to put ALS sensor in reset mode. 
* Arguments    : None
* Return Value : None
***********************************************************************************************************************/
void Reset_ALS(void)
{
	uint32_t  i = 0;			/*Loop Count variable*/
	
	IIC_Tx_Buff_au8[0] = 0x00; 		/*Command Register*/
    	IIC_Tx_Buff_au8[1] = 0x00; 		/*Function*/
    	R_IIC11_Master_Send(ALS_ID, IIC_Tx_Buff_au8, sizeof(IIC_Tx_Buff_au8));
    	for(i = 0; i < 6000; i++);		/*Softwrae Delay*/
    	while(IIC_Tx_Flag_b == false);		/*Wait untill IIC Send completes*/
    	IIC_Tx_Flag_b = false;
	
	if(IIC_Err_Flag_b == true)
	{
		//ALS Sensor is not responding
		ALS_Status_En = ALS_NOT_OK_E;
		BackLightCtrl_En = BACKLIGHT_CTRL_STATIC_E; 
					//change the brightness control method to static
	}
	else
	{
		//ALS sensor is working fine
		ALS_Status_En = ALS_OK_E;
	}
	return;
}


/********************************************************EOF***********************************************************/