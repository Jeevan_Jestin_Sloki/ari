/***********************************************************************************************************************
* File Name    : Communicator.h
* Version      : 01
* Description  : 
* Created By   : Dileepa B S
* Creation Date: 05/01/2022
***********************************************************************************************************************/

#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/	
#include "App_typedefs.h"


/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern uint32_t PresentODOmeter_u32;
extern uint16_t LastSessionWhKm_u16;
extern uint16_t Brightness_u16;
extern uint16_t Power_Consum_Watts_u16;
extern uint8_t  BatterySOC_u8;


/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define GET_PRESENT_ODOMETER()					(PresentODOmeter_u32)
#define GET_PRESENT_WHKM()						(LastSessionWhKm_u16)
#define GET_PRESENT_SOC()						(BatterySOC_u8)
#define GET_PRESENT_BRIGHTNESS()				(Brightness_u16)
#define GET_PWRCONSUM_WATTS()					(Power_Consum_Watts_u16)

#define SET_PRESENT_ODOMETER(x)					(PresentODOmeter_u32 = x)
#define SET_PRESENT_WHKM(x)						(LastSessionWhKm_u16 = x)
#define SET_PRESENT_SOC(x)						(BatterySOC_u8 = x)
#define SET_PRESENT_BRIGHTNESS(x)				(Brightness_u16 = x)
#define SET_PWRCONSUM_WATTS(x)					(Power_Consum_Watts_u16 = x)


/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/



/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/


#endif /* COMMUNICATOR_H */


