/***********************************************************************************************************************
* File Name    : ODO_Disp.h
* Version      : 01
* Description  : 
* Created By   : Jeevan Jestin N
* Creation Date: 04/12/2021
***********************************************************************************************************************/

#ifndef ODO_DISP_H
#define ODO_DISP_H


/***********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "App_typedefs.h" 
#include "Cluster_Conf.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define MAX_ODO_RANGE_IN_METER 99999900

/***********************************************************************************************************************
Structures and Enums
***********************************************************************************************************************/
typedef enum
{
	ODO_DISPLAY_E,
	ODO_DISP_HOLD_E,
}ODO_DispState_En_t;


typedef enum
{
	ODO_DISP_E,
	TRIP_DISP_E,
	ODOTRIP_DISP_NONE_E,
}OdoTrip_Sel_En_t;

typedef enum
{
	DISP_TRIP_NONE_E,
	DISP_TRIP_A_E,
	DISP_TRIP_B_E,
}TripAB_Sel_En_t;




/***********************************************************************************************************************
Export Variables
***********************************************************************************************************************/
extern bool 	    		 InitialODO_Disp_b;
extern OdoTrip_Sel_En_t		OdoTrip_Sel_En;
extern TripAB_Sel_En_t		TripAB_Sel_En;
/***********************************************************************************************************************
Export Functions
***********************************************************************************************************************/
//extern void Display_ODO(uint32_t, ClusterSignals_En_t);
extern void ODOTrip_Disp(uint32_t ODO_u32, ClusterSignals_En_t ODO_ENUM_E);
extern void ODO_Trip(uint32_t ODO_u32, ClusterSignals_En_t ODO_ENUM_E , uint8_t Tripmeter_u8);
void Disp_ODO(uint32_t ODO_u32, ClusterSignals_En_t ODO_ENUM_E);
void Disp_ODODigit(uint32_t ODO_u32, ClusterSignals_En_t ODO_ENUM_E );
void Disp_Trip(uint32_t ODO_u32, ClusterSignals_En_t ODO_ENUM_E);
uint32_t Get_ODO_in_Km(uint32_t);

#endif /* SPEED_DISP_H */


